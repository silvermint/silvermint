// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package transaction

import (
	"encoding/binary"
	"fmt"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
)

type TransactionHash [crypto.HASH_SIZE]byte

func (th TransactionHash) IsEmpty() bool {
	for i := range th {
		if th[i] != 0 {
			return false
		}
	}
	return true
}

type T interface {
	isTransaction()
}

type TransactionOutput interface {
	Amount() uint64
	Dest() crypto.WalletAddress
}

type Transaction interface {
	Type() uint16
	Length() uint16
	ConflictDomain() CD
	Data() []byte
	Source() crypto.WalletAddress
	Output(n int) (TransactionOutput, error)
	Hash() TransactionHash
	IsValid() bool
	IsSlashable() bool
	HasValidOutput() bool
	CheckNumber() uint16
	Inputs() []Utxo
	TxnId() ids.TransactionId
	SetId(ids.TransactionId)

	MarshalBinary() ([]byte, error)
	MarshalSignedBinary(crypto.Wallet) ([]byte, error)
	UnmarshalBinary([]byte) error
	UnmarshalSignedBinary(crypto.Wallet, []byte) error
}

const (
	PaymentTxn uint16 = 0
	MintTxn    uint16 = 1
	SweepTxn   uint16 = 2
	TestingTxn uint16 = 3
)

func (l TransactionHash) Greater(r TransactionHash) bool {
	for i := 0; i < crypto.HASH_SIZE; i++ {
		if l[i] < r[i] {
			return false
		}
		if l[i] > r[i] {
			return true
		}
	}
	return false
}

func (l TransactionHash) Lesser(r TransactionHash) bool {
	return r.Greater(l)
}

func (l TransactionHash) Equal(r TransactionHash) bool {
	for i := 0; i < crypto.HASH_SIZE; i++ {
		if l[i] != r[i] {
			return false
		}
	}
	return true
}

func (h TransactionHash) Short() []byte {
	return h[0:3]
}

func HashTransaction(t Transaction) TransactionHash {
	buf, err := t.MarshalBinary()
	if err != nil {
		log.Errorln("Error calculating transaction hash, returning empty buffer.")
		h := TransactionHash{}
		return h
	}

	return crypto.HashBytes(buf)
}

func Compare(l, r CD) int {
	if l.CheckNumber() < r.CheckNumber() {
		return -1
	} else if l.CheckNumber() > r.CheckNumber() {
		return 1
	} else {
		return crypto.Compare(l.Source(), r.Source())
	}
}

func UnmarshalTxn(data []byte) (Transaction, error) {
	// Determine the transaction type to unmarshal to
	if len(data) < 4 {
		return nil, fmt.Errorf("invalid transaction data")
	}
	txnType := binary.LittleEndian.Uint16(data[2:4])

	var txn Transaction
	switch txnType {
	case PaymentTxn:
		// Payment
		txn = new(Payment)
	case MintTxn:
		// Mint
		txn = new(Mint)
	case SweepTxn:
		// Sweep
		txn = new(Sweep)
	default:
		return nil, fmt.Errorf("unrecognized transaction type: %d",
			txnType)
	}

	err := txn.UnmarshalBinary(data)
	return txn, err
}
