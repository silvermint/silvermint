// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package transaction

import (
	//"encoding/binary"
	"encoding/binary"
	"fmt"
	"reflect"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
)

type SweepOutput struct {
	Amount_ uint64
	Dest_   crypto.WalletAddress
}

func (out SweepOutput) Amount() uint64             { return out.Amount_ }
func (out SweepOutput) Dest() crypto.WalletAddress { return out.Dest_ }

type SweepCD struct {
	fmt.Stringer
	Source_      crypto.WalletAddress
	CheckNumber_ uint16
	Numeric      uint64
}

func (s SweepCD) isConflictDomain() {}
func (s SweepCD) Source() crypto.WalletAddress {
	return s.Source_
}
func (s SweepCD) CheckNumber() uint16 {
	return s.CheckNumber_
}

func (s SweepCD) String() string {
	return fmt.Sprintf("SweepCD:[chk=%d]:%x", s.CheckNumber_, s.Source_)
}
func (sweep SweepCD) Uint64() uint64 {
	return sweep.Numeric
}

func (sweep SweepCD) Length() uint16 {
	var l uint16 = 2            // Length
	l += 2                      // Transaction type
	l += sweep.Source_.Length() // Source
	l += 2                      // CheckNumber
	return l
}

func (sweep SweepCD) MarshalBinary() ([]byte, error) {
	tlen := sweep.Length()
	data := make([]byte, tlen)
	var offset uint16 = 0

	// Length
	binary.LittleEndian.PutUint16(data[offset:offset+2], tlen)
	offset += 2

	// Transaction type
	binary.LittleEndian.PutUint16(data[offset:offset+2], SweepTxn)
	offset += 2

	// Source
	addr := sweep.Source_.Address()
	copy(data[offset:offset+uint16(len(addr))], addr)
	offset += uint16(len(addr))

	// CheckNumber
	binary.LittleEndian.PutUint16(data[offset:offset+2], sweep.CheckNumber_)
	offset += 2

	if offset != tlen {
		return nil, fmt.Errorf("ERROR: Offset didn't match computed PaymentCD length, %d != %d",
			offset, tlen)
	}

	return data, nil
}

type Sweep struct {
	// NB(leaf): The wire format here is as follows:
	//    Source:			32 bytes
	//    CheckNumber:		 2 bytes
	//    NumInputs:  		 2 bytes
	//    Inputs:           34 bytes/ea
	//    Output:           40 bytes
	//    Signature:        64 bytes
	Src             crypto.WalletAddress
	CheckNumber_    uint16
	Inputs_         []Utxo
	Output_         SweepOutput
	Hash_           *TransactionHash
	Bytes           []byte
	Sig             crypto.Signature
	ConflictDomain_ *SweepCD
	TxnId_          ids.TransactionId
}

func (sweep Sweep) isTransaction() {}

func (sweep Sweep) Type() uint16 {
	return SweepTxn
}

func (sweep *Sweep) ConflictDomain() CD {
	if sweep.ConflictDomain_ == nil {
		sweep.ConflictDomain_ = &SweepCD{
			Source_:      sweep.Src,
			CheckNumber_: sweep.CheckNumber_,
			Numeric:      sweep.Src.Uint64(),
		}
	}
	return *sweep.ConflictDomain_
}

func (sweep Sweep) Value() Sweep {
	return sweep
}

func (sweep Sweep) Output(n int) (TransactionOutput, error) {
	if n != 0 {
		return nil, fmt.Errorf("Invalid output index %d: Sweep has 1 outputs.", n)
	}
	output := sweep.Output_
	return &output, nil
}

func (pout SweepOutput) Length() uint16 {
	return uint16(reflect.TypeOf(pout.Amount_).Size()) + pout.Dest_.Length()
}

func (sweep Sweep) Length() uint16 {
	var length uint16 = 0
	// Length (2 bytes)
	length += 2
	// Txn Type (2 bytes)
	length += 2
	// Source (32 bytes)
	length += sweep.Src.Length()
	// CheckNumber (2 bytes)
	length += 2
	// NumInputs
	length += 2
	// Inputs
	length += uint16(UtxoLength() * uint64(len(sweep.Inputs_)))
	// Output
	length += 8
	length += sweep.Output_.Dest_.Length()
	// Signature (64 bytes)
	length += crypto.SIGNATURE_SIZE
	return length
}

func (sweep Sweep) Data() []byte {
	data := make([]byte, len(sweep.Bytes))
	copy(data, sweep.Bytes)
	return data
}

func (sweep Sweep) Source() crypto.WalletAddress {
	return sweep.Src
}

func (sweep Sweep) CheckNumber() uint16 {
	return sweep.CheckNumber_
}

// TODO(chuck): we probably want more robust checks to ensure that the sweep
// isn't zero-value. As-is this relies on there being no zero-valued UTXOs
// in the system to begin with.
func (sweep Sweep) IsValid() bool {
	return len(sweep.Inputs_) > 0
}

func (sweep Sweep) MarshalBinary() ([]byte, error) {
	var d []byte

	var sweep_sz uint16 = sweep.Length()
	d = make([]byte, sweep_sz)

	// Length
	var offset int = 0
	binary.LittleEndian.PutUint16(d[offset:offset+2], sweep_sz)
	offset += 2

	// Txn Type
	binary.LittleEndian.PutUint16(d[offset:offset+2], sweep.Type())
	offset += 2

	// Source PubKey
	for i := 0; i < int(sweep.Src.Length()); i++ {
		d[offset+i] = sweep.Src.Address()[i]
	}
	offset += int(sweep.Src.Length())

	// Check Number
	binary.LittleEndian.PutUint16(d[offset:offset+2], sweep.CheckNumber_)
	offset += 2

	// Num Inputs
	binary.LittleEndian.PutUint16(d[offset:offset+2], uint16(len(sweep.Inputs_)))
	offset += 2

	// Inputs
	ulen := int(UtxoLength())
	for _, input := range sweep.Inputs_ {
		udat, err := input.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(d[offset:offset+ulen], udat)
		offset += ulen
	}

	// Output
	binary.LittleEndian.PutUint64(d[offset:offset+8], sweep.Output_.Amount_)
	offset += 8
	dstkey := sweep.Output_.Dest().Address()
	copy(d[offset:offset+int(sweep.Output_.Dest_.Length())], dstkey[:])
	offset += int(sweep.Output_.Dest_.Length())

	// Signature
	copy(d[offset:offset+crypto.SIGNATURE_SIZE], sweep.Sig[:])
	return d, nil

}

func (sweep Sweep) MarshalSignedBinary(wallet crypto.Wallet) ([]byte, error) {
	buf, err := sweep.MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("Sign: Can't marshal payment for signing: %s", err.Error())
	}
	sig, err := wallet.Sign(buf[0 : len(buf)-crypto.SIGNATURE_SIZE])
	if err != nil {
		return nil, fmt.Errorf("Sign: Can't sign payment: %s", err.Error())
	}
	copy(buf[len(buf)-crypto.SIGNATURE_SIZE:], sig)
	return buf, nil
}

func (sweep *Sweep) UnmarshalBinary(data []byte) error {
	if len(data) == 0 {
		return fmt.Errorf("Sweep: can't unmarshal empty byte array.")
	}
	sweep.Bytes = data

	var offset int = 0
	sweepLength := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	sweepTyp := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	if sweepTyp != SweepTxn {
		return fmt.Errorf("Can't parse binary sweep: expected txn type %d, got %d.",
			SweepTxn, sweepTyp)
	}

	walletType := binary.LittleEndian.Uint16(data[offset : offset+2])
	srcLength := int(binary.LittleEndian.Uint16(data[offset+2 : offset+4]))
	sweep.Src = crypto.NewWalletAddress(data[offset : offset+srcLength])
	if sweep.Src == nil {
		return fmt.Errorf("walletType does not exist %d", walletType)
	}
	offset += srcLength

	sweep.CheckNumber_ = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Num Inputs
	numInputs := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Inputs
	sweep.Inputs_ = make([]Utxo, numInputs)
	ulen := int(UtxoLength())
	for i := uint16(0); i < numInputs; i++ {
		var err error
		sweep.Inputs_[i], err = UnmarshalUtxo(data[offset : offset+ulen])
		if err != nil {
			return err
		}
		offset += ulen
	}

	// Output
	out := SweepOutput{}
	out.Amount_ = binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8

	destWalletType := binary.LittleEndian.Uint16(data[offset : offset+2])
	destLength := int(binary.LittleEndian.Uint16(data[offset+2 : offset+4]))
	out.Dest_ = crypto.NewWalletAddress(data[offset : offset+destLength])
	if out.Dest_ == nil {
		return fmt.Errorf("walletType does not exist %d", destWalletType)
	}
	offset += destLength

	sweep.Output_ = out

	// Signature
	var sig crypto.Signature
	copy(sig[:], data[offset:offset+crypto.SIGNATURE_SIZE])
	offset += crypto.SIGNATURE_SIZE
	sweep.Sig = sig

	// NB(leaf): We can't do this check until we've decoded the outputs, because Length()
	// gives the wrong result.
	if sweep.Length() != sweepLength {
		return fmt.Errorf("Sweep: can't unmarshal: declared length %d != "+
			"actual length %d.", sweepLength, sweep.Length())
	}

	return nil
}

func (sweep *Sweep) UnmarshalSignedBinary(wallet crypto.Wallet, data []byte) error {
	err := sweep.UnmarshalBinary(data)
	if err != nil {
		return err
	}

	if wallet.Verify(data) {
		return nil
	}

	return fmt.Errorf("Binary decoded correctly, but signature is invalid.")
}

func (sweep *Sweep) Hash() TransactionHash {
	if sweep.Hash_ == nil {
		data, err := sweep.MarshalBinary()
		if err != nil {
			return TransactionHash{}
		}
		hash := TransactionHash(crypto.HashBytes(data[:len(data)-crypto.SIGNATURE_SIZE]))
		sweep.Hash_ = &hash
	}
	return *sweep.Hash_
}

func (sweep *Sweep) HasValidOutput() bool {
	if sweep.Output_.Amount_ <= 0 {
		return false
	}
	if sweep.Output_.Dest_ != sweep.Src {
		return false
	}
	return true
}

func (sweep *Sweep) Inputs() []Utxo {
	return sweep.Inputs_
}

func (sweep *Sweep) TxnId() ids.TransactionId {
	return sweep.TxnId_
}

func (sweep *Sweep) SetId(tid ids.TransactionId) {
	sweep.TxnId_ = tid
}

// A sweep is slashable if it was never valid and never will be (unlike the check for if it's
// valid, which is a determination that might change with time)
func (sweep *Sweep) IsSlashable() bool {
	sourceWallet, err := crypto.UnmarshalBinary(sweep.Source().Address())
	if err != nil {
		log.Warningf("Mint %s rejected: unable to create wallet from mint source: %s",
			sweep.TxnId().String(), err.Error())
		return true
	}

	if !sourceWallet.Verify(sweep.Data()) {
		log.Warningf("Mint %s rejected: bad signature", sweep.TxnId().String())
		return true
	}
	return false
}
