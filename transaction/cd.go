package transaction

import (
	"encoding/binary"
	"fmt"

	"gitlab.com/silvermint/silvermint/crypto"
)

type CD interface {
	isConflictDomain()
	Source() crypto.WalletAddress
	CheckNumber() uint16
	Uint64() uint64
	Length() uint16
	MarshalBinary() ([]byte, error)
}

// NB(chuck): Kinda dumb, but we can't have an UnmarshalBinary([]byte) error
// interface function for a CD. That would require changing the CD interface
// from being an interface to {MintCD, PaymentCD, SweepCD} to being an
// interface to {*MintCD, *PaymentCD, *SweepCD} (since UnmarshalBinary must
// have a pointer receiver). This changes all the Consensus stuff because we
// could no longer readily store/lookup CDs in maps or check them for equality,
// due to the fact that we'd be comparing pointers instead of structs.
func UnmarshalCD(data []byte) (CD, error) {
	var offset uint16 = 0

	// Length
	cdlen := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	if cdlen != uint16(len(data)) {
		return nil, fmt.Errorf("CD has length %d bytes, "+
			"but we have %d bytes of data", cdlen, len(data))
	}

	// Transaction type
	ttype := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	var cd CD
	switch ttype {
	case MintTxn:
		mcd := MintCD{}
		// Source Length
		slen := binary.LittleEndian.Uint16(data[offset+2 : offset+4])
		// Source
		swal, err := crypto.UnmarshalBinary(data[offset : offset+slen])
		if err != nil {
			return nil, err
		}
		mcd.Source_ = swal.WalletAddress()
		offset += slen

		// CheckNumber
		mcd.CheckNumber_ = binary.LittleEndian.Uint16(data[offset : offset+2])
		offset += 2

		cd = mcd
	case PaymentTxn:
		pcd := PaymentCD{}
		// Source Length
		slen := binary.LittleEndian.Uint16(data[offset+2 : offset+4])
		// Source
		swal, err := crypto.UnmarshalBinary(data[offset : offset+slen])
		if err != nil {
			return nil, err
		}
		pcd.Source_ = swal.WalletAddress()
		offset += slen

		// CheckNumber
		pcd.CheckNumber_ = binary.LittleEndian.Uint16(data[offset : offset+2])
		offset += 2

		cd = pcd
	case SweepTxn:
		scd := SweepCD{}
		// Source Length
		slen := binary.LittleEndian.Uint16(data[offset+2 : offset+4])
		// Source
		swal, err := crypto.UnmarshalBinary(data[offset : offset+slen])
		if err != nil {
			return nil, err
		}
		scd.Source_ = swal.WalletAddress()
		offset += slen

		// CheckNumber
		scd.CheckNumber_ = binary.LittleEndian.Uint16(data[offset : offset+2])
		offset += 2

		cd = scd
	default:
		return nil, fmt.Errorf("Unrecognized CD type: %v", ttype)
	}

	if offset != uint16(len(data)) {
		return nil, fmt.Errorf("Offset didn't match PaymentCD data len, %d != %d",
			offset, len(data))
	}

	return cd, nil
}
