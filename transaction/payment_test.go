// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package transaction

import (
	"bytes"
	"fmt"
	"testing"

	"gitlab.com/silvermint/silvermint/crypto"
)

func TestMarshal(t *testing.T) {

	src, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	pmt := Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 123,
		Outputs_: []PaymentOutput{{
			Amount_: 100,
			Dest_:   dst.WalletAddress(),
		}},
	}

	buf, err := pmt.MarshalBinary()
	if err != nil {
		t.Fatalf("Marshal Binary failed: %s", err.Error())
	} else if len(buf) == 0 {
		t.Fatalf("Marshal Binary failed: buf was empty.")
	} else {
		fmt.Printf("MarshalBinary: %v => %x\n", pmt, buf)
	}
}

func TestPaymentMarshalRoundTrip(t *testing.T) {

	src, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	pmt := Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 123,
		Outputs_: []PaymentOutput{{
			Amount_: 100,
			Dest_:   dst.WalletAddress(),
		}},
	}

	buf, err := pmt.MarshalBinary()
	if err != nil {
		t.Fatalf("Marshal Binary failed: %s", err.Error())
	} else if len(buf) == 0 {
		t.Fatalf("Marshal Binary failed: buf was empty.")
	} else {
		fmt.Printf("MarshalBinary: %v => %x\n", pmt, buf)
	}

	pmt2 := Payment{}
	err = pmt2.UnmarshalBinary(buf)
	if err != nil {
		t.Fatalf("Unmarshal Binary failed: %s", err.Error())
	}

	if bytes.Compare(pmt.Src.Address(), pmt2.Src.Address()) != 0 {
		t.Fatalf("Unmarshal Binary: source public keys not equal: %x != %x\n",
			pmt.Src, pmt2.Src)
	}

	if pmt2.CheckNumber_ != pmt.CheckNumber_ {
		t.Fatalf("Unmarshal Binary failed: check numbers not equal: %d != %d\n",
			pmt.CheckNumber_, pmt2.CheckNumber_)
	}

	if len(pmt2.Outputs_) != len(pmt.Outputs_) {
		t.Fatalf("Unmarshal Binary failed: %v outputs != %v outputs",
			pmt.Outputs_, pmt2.Outputs_)
	}
}

func TestPaymentMarshalRoundTrip2(t *testing.T) {

	src, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst1, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst2, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst3, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	pmt := Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 123,
		Outputs_: []PaymentOutput{
			{
				Amount_: 100,
				Dest_:   dst1.WalletAddress(),
			},
			{
				Amount_: 200,
				Dest_:   dst2.WalletAddress(),
			},
			{
				Amount_: 300,
				Dest_:   dst3.WalletAddress(),
			},
		},
	}

	buf, err := pmt.MarshalBinary()
	if err != nil {
		t.Fatalf("Marshal Binary failed: %s", err.Error())
	} else if len(buf) == 0 {
		t.Fatalf("Marshal Binary failed: buf was empty.")
	} else {
		fmt.Printf("MarshalBinary: %v => %x\n", pmt, buf)
	}

	pmt2 := Payment{}
	err = pmt2.UnmarshalBinary(buf)
	if err != nil {
		t.Fatalf("Unmarshal Binary failed: %s", err.Error())
	}

	if bytes.Compare(pmt.Src.Address(), pmt2.Src.Address()) != 0 {
		t.Fatalf("Unmarshal Binary: source public keys not equal: %x != %x\n",
			pmt.Src, pmt2.Src)
	}

	if pmt2.CheckNumber_ != pmt.CheckNumber_ {
		t.Fatalf("Unmarshal Binary failed: check numbers not equal: %d != %d\n",
			pmt.CheckNumber_, pmt2.CheckNumber_)
	}

	if len(pmt2.Outputs_) != len(pmt.Outputs_) {
		t.Fatalf("Unmarshal Binary failed: %d outputs != %d outputs",
			pmt.Outputs_, pmt2.Outputs_)
	}

	for i, out := range pmt.Outputs_ {
		if pmt.Outputs_[i].Amount() != out.Amount() {
			t.Fatalf("Unmarshal Binary failed: output amounts: %d != %d",
				pmt.Outputs_[i].Amount(), out.Amount())
		}

		if crypto.Compare(pmt.Outputs_[i].Dest(), out.Dest()) != 0 {
			t.Fatalf("Unmarshal Binary failed: output destinations: %d != %d",
				pmt.Outputs_[i].Dest(), out.Dest())
		}
	}
}

func TestPaymentMarshalSignRoundTrip(t *testing.T) {

	src, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst1, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst2, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst3, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	pmt := Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 123,
		Outputs_: []PaymentOutput{
			{
				Amount_: 100,
				Dest_:   dst1.WalletAddress(),
			},
			{
				Amount_: 200,
				Dest_:   dst2.WalletAddress(),
			},
			{
				Amount_: 300,
				Dest_:   dst3.WalletAddress(),
			},
		},
	}

	buf, err := pmt.MarshalSignedBinary(src)
	if err != nil {
		t.Fatalf("Marshal Signed Binary failed: %s", err.Error())
	} else if len(buf) == 0 {
		t.Fatalf("Marshal Signed Binary failed: buf was empty.")
	} else if !src.Verify(buf) {
		t.Fatalf("Marshal Signed Binary failed: invalid signature.")
	} else {
		fmt.Printf("MarshalSignedBinary: %v => %x\n", pmt, buf)
	}

	pmt2 := Payment{}
	err = pmt2.UnmarshalSignedBinary(src, buf)
	if err != nil {
		t.Fatalf("Unmarshal Binary failed: %s", err.Error())
	}

	if bytes.Compare(pmt.Src.Address(), pmt2.Src.Address()) != 0 {
		t.Fatalf("Unmarshal Binary: source public keys not equal: %x != %x\n",
			pmt.Src, pmt2.Src)
	}

	if pmt2.CheckNumber_ != pmt.CheckNumber_ {
		t.Fatalf("Unmarshal Binary failed: check numbers not equal: %d != %d\n",
			pmt.CheckNumber_, pmt2.CheckNumber_)
	}

	if len(pmt2.Outputs_) != len(pmt.Outputs_) {
		t.Fatalf("Unmarshal Binary failed: %d outputs != %d outputs",
			pmt.Outputs_, pmt2.Outputs_)
	}

	for i, out := range pmt.Outputs_ {
		if pmt2.Outputs_[i].Amount() != out.Amount() {
			t.Fatalf("Unmarshal Binary failed: output amounts: %d != %d",
				pmt.Outputs_[i].Amount(), out.Amount())
		}

		if crypto.Compare(pmt2.Outputs_[i].Dest(), out.Dest()) != 0 {
			t.Fatalf("Unmarshal Binary failed: output destinations: %d != %d",
				pmt.Outputs_[i].Dest(), out.Dest())
		}
	}

	for i, input := range pmt.Inputs_ {
		if pmt2.Inputs_[i].Amount != input.Amount {
			t.Fatalf("Unmarshal Binary failed: input %d: %v != %v",
				i, input, pmt2.Inputs_[i])
		}
		if pmt2.Inputs_[i].TxnId != input.TxnId {
			t.Fatalf("Unmarshal Binary failed: input %d: %v != %v",
				i, input, pmt2.Inputs_[i])
		}
	}
}

func TestMarshalWithTestWallet(t *testing.T) {
	src, _ := crypto.NewWallet("", crypto.TEST_WALLET)
	dst, _ := crypto.NewWallet("", crypto.TEST_WALLET)

	pmt := Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 123,
		Outputs_: []PaymentOutput{{
			Amount_: 100,
			Dest_:   dst.WalletAddress(),
		}},
	}

	buf, err := pmt.MarshalBinary()
	if err != nil {
		t.Fatalf("Marshal Binary failed: %s", err.Error())
	} else if len(buf) == 0 {
		t.Fatalf("Marshal Binary failed: buf was empty.")
	}

	pmt2 := Payment{}
	pmt2.UnmarshalBinary(buf)

	srcTestWallet := crypto.NewTestWallet(pmt2.Src.Address())
	destTestWallet := crypto.NewTestWallet(pmt2.Outputs_[0].Dest_.Address())
	if srcTestWallet.CreatorName_ != destTestWallet.CreatorName_ {
		t.Fatalf("Marshal Binary failed: TestWallet Name dont match: %s != %s", srcTestWallet.CreatorName_, destTestWallet.CreatorName_)
	}
}

func TestMarshalWithDiffWallet(t *testing.T) {
	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	src, _ := crypto.NewWallet("", crypto.TEST_WALLET)

	pmt := Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 123,
		Outputs_: []PaymentOutput{{
			Amount_: 100,
			Dest_:   dst.WalletAddress(),
		}},
	}

	buf, err := pmt.MarshalBinary()
	if err != nil {
		t.Fatalf("Marshal Binary failed: %s", err.Error())
	} else if len(buf) == 0 {
		t.Fatalf("Marshal Binary failed: buf was empty.")
	}

	pmt2 := Payment{}
	pmt2.UnmarshalBinary(buf)

	srcTestWallet := crypto.NewTestWallet(pmt2.Src.Address())
	destSWallet := crypto.NewSilvermintWallet(pmt2.Outputs_[0].Dest_.Address())
	if srcTestWallet.Type_ == destSWallet.Type_ {
		t.Fatalf("Marshal Binary failed: TestWallet Type should not match: %d != %d", srcTestWallet.Type_, destSWallet.Type_)
	}
}
