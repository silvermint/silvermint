// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package transaction

import (
	"fmt"
	"math"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func TestTransact(t *testing.T) {
	ssl := NewSSLedger(0)
	l := NewIncrLedger(ssl)

	wal1, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	wal2, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	mint := Mint{
		Src:          wal1.WalletAddress(),
		CheckNumber_: uint16(1),
		Output_: MintOutput{
			Amount_: 100,
			Dest_:   wal1.WalletAddress(),
		},
	}

	err := l.doMint(&mint)
	if err != nil {
		t.Fatalf("Mint failed: %s", err.Error())
	}

	wal1Balance := l.Balance(wal1.WalletAddress())
	if wal1Balance != uint64(100) {
		t.Fatalf("Mint transaction failed: balance was %d, expected 100 for key %v",
			wal1Balance, wal1.WalletAddress())
	}

	state, _ := l.wstates.Get(wal1.WalletAddress())
	myUtxos := state.utxos
	if len(myUtxos) != 1 {
		t.Fatalf("Wrong number of Utxos in wallet: expected 1, got %d", len(myUtxos))
	}

	pmt := Payment{
		Src:          wal1.WalletAddress(),
		CheckNumber_: uint16(2),
		Inputs_:      myUtxos,
		Outputs_: []PaymentOutput{
			{Amount_: 75, Dest_: wal2.WalletAddress()},
			{Amount_: 25, Dest_: wal1.WalletAddress()},
		},
	}

	err = l.doPayment(&pmt)
	if err != nil {
		t.Fatalf("Payment failed: %v", err.Error())
	}

	wal1Bal := l.Balance(wal1.WalletAddress())
	wal2Bal := l.Balance(wal2.WalletAddress())
	if wal2Bal != uint64(75) {
		t.Fatalf("Receiver wasn't properly credited balance, should be 75, was %d\n", wal2Bal)
	}
	if wal1Bal != uint64(25) {
		fmt.Printf("\n\nWrong balance for sender: %d != %d\n", wal1Bal, uint64(25))
		for _, seq := range l.wstates.sequences {
			for k, v := range seq.states {
				fmt.Printf("Utxos: %v\n", k)
				for _, u := range v.utxos {
					fmt.Printf("   > %v\n", u)
				}
			}
		}
		t.Fatalf("Sender wasn't properly deducted balance, should be 25, was %d\n", wal1Bal)
	}
}

func TestSign(t *testing.T) {
	ssl := NewSSLedger(0)
	l := NewIncrLedger(ssl)

	wal1, err := crypto.NewWallet("", crypto.BASIC_WALLET)
	if err != nil {
		t.Fatalf("Couldn't generate wallet.")
	}
	wal2, err := crypto.NewWallet("", crypto.BASIC_WALLET)
	if err != nil {
		t.Fatalf("Couldn't generate wallet.")
	}

	mint := Mint{
		Src:          wal1.WalletAddress(),
		CheckNumber_: uint16(0),
		Output_: MintOutput{
			Amount_: 100,
			Dest_:   wal1.WalletAddress(),
		},
	}

	l.doMint(&mint)

	pmt := Payment{
		Src:          wal1.WalletAddress(),
		CheckNumber_: uint16(1),
		Outputs_:     []PaymentOutput{{Amount_: 75, Dest_: wal2.WalletAddress()}},
	}
	buf, err := pmt.MarshalSignedBinary(wal1)
	if len(buf) == 0 {
		t.Fatalf("MarshalSignedBinary returned empty slice.")
	}

	pmt2 := new(Payment)
	err = pmt2.UnmarshalSignedBinary(wal1, buf)
	if err != nil {
		t.Fatalf("Couldn't decode signed binary.")
	}
}

func TestDoubleSpend(t *testing.T) {
	ssl := NewSSLedger(0)
	l := NewIncrLedger(ssl)

	wal1, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	wal2, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	mint := Mint{
		Src:          wal1.WalletAddress(),
		CheckNumber_: uint16(1),
		Output_: MintOutput{
			Amount_: 100,
			Dest_:   wal1.WalletAddress(),
		},
	}

	l.doMint(&mint)

	wal1Balance := l.Balance(wal1.WalletAddress())
	if wal1Balance != uint64(100) {
		t.Fatalf("Mint transaction failed: balance was %d, expected 100 for key %v",
			wal1Balance, wal1.WalletAddress())
	}

	state, _ := l.wstates.Get(wal1.WalletAddress())
	myUtxos := state.utxos
	if len(myUtxos) != 1 {
		t.Fatalf("Wrong number of Utxos in wallet: expected 1, got %d", len(myUtxos))
	}

	pmt := Payment{
		Src:          wal1.WalletAddress(),
		CheckNumber_: uint16(2),
		Inputs_:      myUtxos,
		Outputs_: []PaymentOutput{
			{Amount_: 75, Dest_: wal2.WalletAddress()},
			{Amount_: 25, Dest_: wal1.WalletAddress()},
		},
	}

	err := l.doPayment(&pmt)
	if err != nil {
		t.Fatalf("Payment failed: %v", err.Error())
	}

	wal1Bal := l.Balance(wal1.WalletAddress())
	wal2Bal := l.Balance(wal2.WalletAddress())
	if wal2Bal != uint64(75) {
		t.Fatalf("Receiver wasn't properly credited balance, should be 75, was %d\n", wal2Bal)
	}
	if wal1Bal != uint64(25) {
		fmt.Printf("\n\nWrong balance for sender: %d != %d\n", wal1Bal, uint64(25))
		for _, seq := range l.wstates.sequences {
			for k, v := range seq.states {
				fmt.Printf("Utxos: %v\n", k)
				for _, u := range v.utxos {
					fmt.Printf("   > %v\n", u)
				}
			}
		}
		t.Fatalf("Sender wasn't properly deducted balance, should be 25, was %d\n", wal1Bal)
	}

	dbl := Payment{
		Src:          wal1.WalletAddress(),
		CheckNumber_: uint16(2),
		Inputs_:      myUtxos,
		Outputs_: []PaymentOutput{
			{Amount_: 75, Dest_: wal2.WalletAddress()},
			{Amount_: 25, Dest_: wal1.WalletAddress()},
		},
	}

	err = l.doPayment(&dbl)
	if err == nil {
		t.Fatalf("Double spend of input Utxos not detected in doPayment.")
	}
}

func RandTransaction() Transaction {
	ttype := rand.Intn(3)
	src := RandWallet()

	var txn Transaction
	switch ttype {
	case 0:
		mint := Mint{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Output_: MintOutput{
				Amount_: uint64(rand.Intn(math.MaxInt)),
				Dest_:   src.WalletAddress(),
			},
		}
		txn = &mint
	case 1:
		th := TransactionHash{}
		copy(th[:], RandBytes(crypto.HASH_SIZE))
		pmt := Payment{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Inputs_: []Utxo{
				{
					TxnId:  ids.TransactionId{},
					Amount: rand.Uint64(),
				},
			},
			Outputs_: []PaymentOutput{
				{
					Amount_: uint64(rand.Intn(math.MaxInt)),
					Dest_:   src.WalletAddress(),
				},
			},
		}
		txn = &pmt
	case 2:
		th := TransactionHash{}
		copy(th[:], RandBytes(crypto.HASH_SIZE))
		sweep := Sweep{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Inputs_: []Utxo{
				{
					TxnId:  ids.TransactionId{},
					Amount: rand.Uint64(),
				},
			},
			Output_: SweepOutput{
				Amount_: uint64(rand.Intn(math.MaxInt)),
				Dest_:   src.WalletAddress(),
			},
		}
		txn = &sweep
	}
	return txn
}

func TestIncrLedger(t *testing.T) {
	parent := NewSSLedger(0)
	var ledger Ledger
	ledger = NewIncrLedger(parent)
	if ledger.Parent() == nil {
		t.Fatalf("Increment Ledger not implemented correctly, Parent should not be nil")
	}
}

func RandSSLedger(nWals int, nMaxUtxos int, nFinalized int, parent Ledger, child Ledger) *SSLedger {
	ledger := NewSSLedger(0)

	n := 0
	ledger.TxnLog_ = make([]ids.TransactionId, nFinalized)
	for i := 0; i < nWals; i++ {
		// Make account
		wal := RandWallet()
		addr := wal.WalletAddress()
		ledger.states[addr] = new(walletState)
		// Give it a random check number
		ledger.states[addr].checkNumber = uint16(rand.Intn(math.MaxUint16))
		// Give it a random number of UTXOs
		nUtxos := rand.Intn(nMaxUtxos)
		ledger.states[addr].utxos = make([]Utxo, nUtxos)
		for j := 0; j < nUtxos; j++ {
			// Randomly generate the UTXO transaction.
			txn := RandTransaction()
			// Assign it as having finalized with this block if we've
			// generated less than nFinalized transactions so far).
			if n < nFinalized {
				ledger.TxnLog_[n] = txn.TxnId()
				n++
			}
			// Randomly generate the UTXO
			utxo := Utxo{
				TxnId:  ids.TransactionId{},
				Amount: rand.Uint64(),
			}
			// Put UTXO in the wallet
			ledger.states[addr].utxos[j] = utxo
		}
	}

	// Slice off any empty elements if we generated less than nFinalized txns.
	ledger.TxnLog_ = ledger.TxnLog_[:n]

	// Assign the parent / child
	ledger.Child_ = child

	return ledger
}

func VerifySSLedger(ledger, loadedLedger *SSLedger, t *testing.T) {
	// Lmap
	for wal, acc := range ledger.states {
		loadedAcc, ok := loadedLedger.states[wal]
		if !ok {
			t.Fatalf("Wallet %x not found in loaded ledger", wal.Address())
		}
		if acc.checkNumber != loadedAcc.checkNumber {
			t.Fatalf("Loaded wallet %x checknum didn't match original %d != %d",
				wal.Address(), loadedAcc.checkNumber, acc.checkNumber)
		}
		for i, utxo := range acc.utxos {
			if utxo != loadedAcc.utxos[i] {
				t.Fatalf("Loaded wallet %x utxo didn't match original %v != %v",
					wal.Address(), loadedAcc.utxos[i], utxo)
			}
		}
	}
	// TxnLog
	for i, txn := range ledger.TxnLog_ {
		loadedTxn := loadedLedger.TxnLog_[i]
		if txn != loadedTxn {
			t.Fatalf("Loaded txn hash didn't match original %x != %x",
				loadedTxn, txn)
		}
	}
}

func TestLedgerRoundTrip(t *testing.T) {
	nWals := 1000
	nMaxUtxos := 5
	nFinalized := 100
	var parent *SSLedger
	var child *SSLedger
	ss1 := RandSSLedger(nWals, nMaxUtxos, nFinalized, parent, child)
	ss2 := RandSSLedger(nWals, nMaxUtxos, nFinalized, ss1, child)
	ss3 := RandSSLedger(nWals, nMaxUtxos, nFinalized, ss2, child)

	lbytes, err := ss3.MarshalBinary()
	if err != nil {
		t.Fatalf("Couldn't marshal ledger to bytes: %s", err.Error())
	}
	loadedSS := NewSSLedger(0)
	err = loadedSS.UnmarshalBinary(lbytes)
	if err != nil {
		t.Fatalf("Couldn't unmarshal ledger bytes: %s", err.Error())
	}
	VerifySSLedger(ss3, loadedSS, t)
}

func TestProcessManyTxns(t *testing.T) {
	nTxns := 100000
	txns := make([]Transaction, 0, nTxns)
	for i := 0; i < nTxns; i++ {
		mint := RandMint()
		mint.(*Mint).CheckNumber_ = 1
		txns = append(txns, mint)
	}
	ssl := NewSSLedger(0)
	incr := NewIncrLedger(ssl)

	nowMicro := time.Now().UnixMicro()
	incr.Wg.Add(1)
	go incr.QueueFinalized(txns...)
	incr.Wait()
	fmt.Printf("Took %f seconds to compute %d mints\n", float64(time.Now().UnixMicro()-nowMicro)*0.000001, nTxns)
	nActual := incr.wstates.Length()
	if nActual != nTxns {
		t.Fatalf("Had %d wallets in map, expected %d", nActual, nTxns)
	}
}
