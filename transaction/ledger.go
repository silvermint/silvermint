// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package transaction

import (
	"encoding/binary"
	"fmt"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"golang.org/x/exp/slices"
)

var (
	NumWalletsInLedger = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "num_wallets_in_ledger",
		Help: "Number of wallets in the Ledger.",
	})
	NumUtxosInWallets = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "num_utxos_in_wallets",
		Help: "Net number of UTXOs in all wallets.",
	})
	TxnsOutputsTotalFinalized = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_txns_outputs_finalized_total",
		Help: "Total output transactions finalized.",
	})
)

func init() {
	prometheus.MustRegister(NumWalletsInLedger)
	prometheus.MustRegister(NumUtxosInWallets)
	prometheus.MustRegister(TxnsOutputsTotalFinalized)
}

type Utxo struct {
	fmt.Stringer
	TxnId  ids.TransactionId
	Output uint16
	Amount uint64
}

func (u Utxo) String() string {
	return fmt.Sprintf("[vid=%d, ht=%d, txn=%d, output=%d, amt=%d]",
		u.TxnId.BlockId.ValId_, u.TxnId.BlockId.Height_, u.TxnId.Index, u.Output, u.Amount)
}

func UtxoLength() uint64 {
	return ids.TransactionIdLength() + 2 + 8
}

func (u Utxo) MarshalBinary() ([]byte, error) {
	data := make([]byte, UtxoLength())
	offset := 0
	tdat, err := u.TxnId.MarshalBinary()
	if err != nil {
		return nil, err
	}
	copy(data[offset:offset+len(tdat)], tdat)
	offset += len(tdat)

	binary.LittleEndian.PutUint16(data[offset:offset+2], u.Output)
	offset += 2

	binary.LittleEndian.PutUint64(data[offset:offset+8], u.Amount)
	offset += 8

	if offset != len(data) {
		return nil, fmt.Errorf("Utxo length didn't match computed: %d != %d",
			len(data), offset)
	}
	return data, nil
}

func UnmarshalUtxo(data []byte) (Utxo, error) {
	ulen := UtxoLength()
	utxo := Utxo{}
	if int(ulen) != len(data) {
		return utxo, fmt.Errorf("Utxo length didn't match computed: %d != %d",
			len(data), ulen)
	}
	offset := 0
	tlen := int(ids.TransactionIdLength())
	tid, err := ids.UnmarshalTransactionId(data[offset : offset+tlen])
	if err != nil {
		return utxo, err
	}
	utxo.TxnId = tid
	offset += tlen

	utxo.Output = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	utxo.Amount = binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8

	return utxo, nil
}

type walletState struct {
	checkNumber uint16
	utxos       []Utxo
	mu          sync.RWMutex
}

func newConcurrentWalletState() *walletState {
	return &walletState{
		checkNumber: 0,
		utxos:       []Utxo{},
	}
}

func (state *walletState) Has(utxo Utxo) bool {
	state.mu.RLock()
	defer state.mu.RUnlock()

	for _, u := range state.utxos {
		if u == utxo {
			return true
		}
	}
	return false
}

func (state *walletState) Add(utxo Utxo) {
	state.mu.Lock()
	defer state.mu.Unlock()

	state.utxos = append(state.utxos, utxo)
}

func (state *walletState) SetCheckNumber(checknum uint16) {
	state.mu.Lock()
	defer state.mu.Unlock()

	state.checkNumber = checknum
}

func (state *walletState) Delete(utxo Utxo) bool {
	state.mu.Lock()
	defer state.mu.Unlock()

	for i, u := range state.utxos {
		if u == utxo {
			state.utxos = slices.Delete(state.utxos, i, i+1)
			return true
		}
	}
	return false
}

func (state *walletState) Copy() *walletState {
	state.mu.RLock()
	defer state.mu.RUnlock()

	utxosCopy := make([]Utxo, len(state.utxos))
	copy(utxosCopy, state.utxos)
	return &walletState{
		checkNumber: state.checkNumber,
		utxos:       utxosCopy,
	}
}

func (cws *walletState) CheckNumber() uint16 {
	cws.mu.RLock()
	defer cws.mu.RUnlock()

	return cws.checkNumber
}

// Returns a copy to avoid concurrency issues
func (cws *walletState) Utxos() []Utxo {
	cws.mu.RLock()
	defer cws.mu.RUnlock()

	utxosCopy := make([]Utxo, len(cws.utxos))
	copy(utxosCopy, cws.utxos)

	return utxosCopy
}

type LedgerType uint8

const (
	SSLEDGER   LedgerType = 0
	INCRLEDGER LedgerType = 1
)

type Ledger interface {
	WalletState(crypto.WalletAddress) (*walletState, bool)
	Utxos(crypto.WalletAddress) []Utxo
	TxnLog() []ids.TransactionId
	Parent() Ledger
	Child() Ledger

	Balance(crypto.WalletAddress) uint64
	CheckNumber(crypto.WalletAddress) uint16
	ExpectedCheckNumber(crypto.WalletAddress) uint16
	IsValid(Transaction) error

	QueueFinalized(...Transaction)
	ProcessQueued() error
	Wait()
	SetParent(Ledger)
	SetChild(Ledger)

	Compact(Ledger)

	Length() uint64
	MarshalBinary() ([]byte, error)
	UnmarshalBinary([]byte) error
}

func UnmarshalLedger(data []byte) (Ledger, error) {
	if len(data) < 9 {
		return nil, fmt.Errorf("Unable to unmarshal ledger from %d bytes",
			len(data))
	}
	ltype := LedgerType(data[8])

	var ledger Ledger
	switch ltype {
	case SSLEDGER:
		ssl := NewSSLedger(0)
		err := ssl.UnmarshalBinary(data)
		if err != nil {
			return nil, err
		}
		ledger = ssl
	case INCRLEDGER:
		// Leave parent unassigned for now. Another routine will have to come
		// in and assign it.
		inc := NewIncrLedger(nil)
		err := inc.UnmarshalBinary(data)
		if err != nil {
			return nil, err
		}
		ledger = inc
	default:
		return nil, fmt.Errorf("Unrecognized ledger type: %d", ltype)
	}

	return ledger, nil
}
