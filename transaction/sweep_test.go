// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package transaction

import (
	"bytes"
	"fmt"
	"testing"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
)

func TestMarshalSweep(t *testing.T) {

	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	sweep := Sweep{
		Src:          dst.WalletAddress(),
		CheckNumber_: 789,
		Inputs_: []Utxo{{
			TxnId:  ids.TransactionId{},
			Amount: 20,
		},
			{
				TxnId:  ids.TransactionId{},
				Amount: 30,
			},
		},
		Output_: SweepOutput{
			Dest_:   dst.WalletAddress(),
			Amount_: 50,
		},
	}

	buf, err := sweep.MarshalBinary()
	if err != nil {
		t.Fatalf("Marshal Binary failed: %s", err.Error())
	} else if len(buf) == 0 {
		t.Fatalf("Marshal Binary failed: buf was empty.")
	} else {
		fmt.Printf("MarshalBinary: %v => %x\n", sweep, buf)
	}
}

func TestSweepMarshalRoundTrip(t *testing.T) {

	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	sweep := Sweep{
		Src:          dst.WalletAddress(),
		CheckNumber_: 789,
		Inputs_: []Utxo{{
			TxnId:  ids.TransactionId{},
			Amount: 90,
		},
			{
				TxnId:  ids.TransactionId{},
				Amount: 10,
			},
		},
		Output_: SweepOutput{
			Dest_:   dst.WalletAddress(),
			Amount_: 100,
		},
	}

	buf, err := sweep.MarshalBinary()
	if err != nil {
		t.Fatalf("Marshal Binary failed: %s", err.Error())
	} else if len(buf) == 0 {
		t.Fatalf("Marshal Binary failed: buf was empty.")
	} else {
		fmt.Printf("MarshalBinary: %v => %x\n", sweep, buf)
	}

	sweep2 := Sweep{}
	err = sweep2.UnmarshalBinary(buf)
	if err != nil {
		t.Fatalf("Unmarshal Binary failed: %s", err.Error())
	}

	if bytes.Compare(sweep.Src.Address()[:], sweep2.Src.Address()[:]) != 0 {
		t.Fatalf("Unmarshal Binary: source public keys not equal: %x != %x\n",
			sweep.Src, sweep2.Src)
	}

	if sweep.CheckNumber_ != sweep2.CheckNumber_ {
		t.Fatalf("Unmarshal Binary failed: check numbers not equal: %d != %d\n",
			sweep.CheckNumber_, sweep2.CheckNumber_)
	}

	if len(sweep.Inputs_) != len(sweep2.Inputs_) {
		t.Fatalf("Unmarshal Binary failed: %d outputs != %d outputs",
			sweep.Inputs_, sweep2.Inputs_)
	}
}

func TestSweepMarshalRoundTrip2(t *testing.T) {

	src2, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	pmt2 := Payment{
		Src:          src2.WalletAddress(),
		CheckNumber_: 456,
		Outputs_: []PaymentOutput{{
			Amount_: 50,
			Dest_:   dst.WalletAddress(),
		}},
	}

	sweep := Sweep{
		Src:          dst.WalletAddress(),
		CheckNumber_: 789,
		Inputs_: []Utxo{{
			TxnId:  ids.TransactionId{},
			Amount: 25,
		},
			{
				TxnId:  ids.TransactionId{},
				Amount: 75,
			},
		},
		Output_: SweepOutput{
			Dest_:   dst.WalletAddress(),
			Amount_: 100,
		},
	}

	buf, err := sweep.MarshalBinary()
	if err != nil {
		t.Fatalf("Marshal Binary failed: %s", err.Error())
	} else if len(buf) == 0 {
		t.Fatalf("Marshal Binary failed: buf was empty.")
	} else {
		fmt.Printf("MarshalBinary: %v => %x\n", sweep, buf)
	}

	sweep2 := Sweep{}
	err = sweep2.UnmarshalBinary(buf)
	if err != nil {
		t.Fatalf("Unmarshal Binary failed: %s", err.Error())
	}

	if bytes.Compare(sweep.Src.Address()[:], sweep2.Src.Address()[:]) != 0 {
		t.Fatalf("Unmarshal Binary: source public keys not equal: %x != %x\n",
			sweep.Src, pmt2.Src)
	}

	if sweep2.CheckNumber_ != sweep.CheckNumber_ {
		t.Fatalf("Unmarshal Binary failed: check numbers not equal: %d != %d\n",
			sweep.CheckNumber_, sweep2.CheckNumber_)
	}

	if len(sweep2.Inputs_) != len(sweep.Inputs_) {
		t.Fatalf("Unmarshal Binary failed: %d outputs != %d outputs",
			sweep.Inputs_, sweep2.Inputs_)
	}

	for i, input := range sweep.Inputs_ {
		if sweep2.Inputs_[i].TxnId != input.TxnId {
			t.Fatalf("Unmarshal Binary failed: input txnhashes: %v != %v",
				sweep.Inputs_[i].TxnId, input.TxnId)
		}

		if sweep.Inputs_[i].Amount != input.Amount {
			t.Fatalf("Unmarshal Binary failed: input amounts: %d != %d",
				sweep.Inputs_[i].Amount, input.Amount)
		}
	}
}

func TestSweepMarshalSignRoundTrip(t *testing.T) {

	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	sweep := Sweep{
		Src:          dst.WalletAddress(),
		CheckNumber_: 789,
		Inputs_: []Utxo{{
			TxnId:  ids.TransactionId{},
			Amount: 100,
		},
			{
				TxnId:  ids.TransactionId{},
				Amount: 50,
			},
		},
		Output_: SweepOutput{
			Dest_:   dst.WalletAddress(),
			Amount_: 150,
		},
	}

	buf, err := sweep.MarshalSignedBinary(dst)
	if err != nil {
		t.Fatalf("Marshal Signed Binary failed: %s", err.Error())
	} else if len(buf) == 0 {
		t.Fatalf("Marshal Signed Binary failed: buf was empty.")
	} else if !dst.Verify(buf) {
		t.Fatalf("Marshal Signed Binary failed: invalid signature.")
	} else {
		fmt.Printf("MarshalSignedBinary: %v => %x\n", sweep, buf)
	}

	sweep2 := Sweep{}
	err = sweep2.UnmarshalSignedBinary(dst, buf)
	if err != nil {
		t.Fatalf("Unmarshal Binary failed: %s", err.Error())
	}

	if bytes.Compare(sweep.Src.Address()[:], sweep2.Src.Address()[:]) != 0 {
		t.Fatalf("Unmarshal Binary: source public keys not equal: %x != %x\n",
			sweep.Src, sweep2.Src)
	}

	if sweep2.CheckNumber_ != sweep.CheckNumber_ {
		t.Fatalf("Unmarshal Binary failed: check numbers not equal: %d != %d\n",
			sweep.CheckNumber_, sweep2.CheckNumber_)
	}

	if len(sweep2.Inputs_) != len(sweep.Inputs_) {
		t.Fatalf("Unmarshal Binary failed: %d outputs != %d outputs",
			sweep.Inputs_, sweep2.Inputs_)
	}

	for i, input := range sweep.Inputs_ {
		if sweep2.Inputs_[i].TxnId != input.TxnId {
			t.Fatalf("Unmarshal Binary failed: input txnhashes: %v != %v",
				sweep2.Inputs_[i].TxnId, input.TxnId)
		}

		if sweep2.Inputs_[i].Amount != input.Amount {
			t.Fatalf("Unmarshal Binary failed: input amounts: %d != %d",
				sweep2.Inputs_[i].Amount, input.Amount)
		}
	}
}
