package transaction

import (
	"math"
	"math/rand"
	"testing"
)

func RandMint() Transaction {
	src := RandWallet()

	mint := Mint{
		Src:          src.WalletAddress(),
		CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
		Output_: MintOutput{
			Amount_: uint64(rand.Intn(math.MaxInt)),
			Dest_:   src.WalletAddress(),
		},
	}
	return &mint
}

func VerifyTxn(txn, loadedTxn Transaction, t *testing.T) {
	if txn.Hash() != loadedTxn.Hash() {
		t.Fatalf("Loaded transaction hash didn't match original: %x != %x",
			loadedTxn.Hash(), txn.Hash())
	}
	if txn.ConflictDomain() != loadedTxn.ConflictDomain() {
		t.Fatalf("Loaded transaction CD didn't match original: %v != %v",
			loadedTxn.ConflictDomain(), txn.ConflictDomain())
	}
}

func TestMintRoundTrip(t *testing.T) {
	mint := RandMint()

	mintBytes, err := mint.MarshalBinary()
	if err != nil {
		t.Fatalf("Error marshaling mint to bytes: %s", err.Error())
	}
	loadedMint := Mint{}
	err = loadedMint.UnmarshalBinary(mintBytes)
	if err != nil {
		t.Fatalf("Error unmarshaling mint bytes: %s", err.Error())
	}
	VerifyTxn(mint, &loadedMint, t)
}
