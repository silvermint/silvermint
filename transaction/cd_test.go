package transaction

import (
	"math"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/silvermint/silvermint/crypto"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

func RandBytes(n int) []byte {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return []byte(string(b))
}
func RandWallet() crypto.Wallet {
	wal, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	return wal
}

func RandConflictDomain() CD {
	txnType := rand.Intn(3)
	wal := RandWallet()
	checknum := uint16(rand.Intn(math.MaxUint16))

	var cd CD
	switch txnType {
	case 0:
		cd = MintCD{
			Source_:      wal.WalletAddress(),
			CheckNumber_: checknum,
		}
	case 1:
		cd = PaymentCD{
			Source_:      wal.WalletAddress(),
			CheckNumber_: checknum,
		}
	case 2:
		cd = SweepCD{
			Source_:      wal.WalletAddress(),
			CheckNumber_: checknum,
		}
	}
	return cd
}

func VerifyCD(cd, loadedCD CD, t *testing.T) {
	if cd.Source() != loadedCD.Source() {
		t.Fatalf("Loaded CD source didn't match original: %x != %x",
			loadedCD.Source().Address(), cd.Source().Address())
	}
	if cd.CheckNumber() != loadedCD.CheckNumber() {
		t.Fatalf("Loaded CD check number didn't match original: %d != %d",
			loadedCD.CheckNumber(), cd.CheckNumber())
	}
}

func TestCDRoundTrip(t *testing.T) {
	cd := RandConflictDomain()

	cdbytes, err := cd.MarshalBinary()
	if err != nil {
		t.Fatalf("Error marshaling CD to bytes: %s", err.Error())
	}
	loadedCD, err := UnmarshalCD(cdbytes)
	if err != nil {
		t.Fatalf("Error unmarshaling CD bytes: %s", err.Error())
	}

	VerifyCD(cd, loadedCD, t)
}
