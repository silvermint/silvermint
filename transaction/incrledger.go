package transaction

import (
	"encoding/binary"
	"errors"
	"fmt"
	"math"
	"sort"
	"strings"
	"sync"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
)

func init() {
	pflag.Uint64("wallet_sequence_keybits", 12, "Number of keybits from the wallet address to use for sharding the walletstates map.")
}

type WalletStatesSequence struct {
	states map[crypto.WalletAddress]*walletState
	mu     sync.RWMutex
}

func newWalletStatesSequence(sz uint64) *WalletStatesSequence {
	return &WalletStatesSequence{
		states: make(map[crypto.WalletAddress]*walletState, sz),
	}
}

func (wss *WalletStatesSequence) Length() int {
	wss.mu.RLock()
	defer wss.mu.RUnlock()

	return len(wss.states)
}

func (wss *WalletStatesSequence) Get(wal crypto.WalletAddress) (*walletState, bool) {
	wss.mu.RLock()
	defer wss.mu.RUnlock()

	state, ok := wss.states[wal]
	return state, ok
}

func (wss *WalletStatesSequence) Add(wal crypto.WalletAddress, state *walletState) *walletState {
	wss.mu.Lock()
	defer wss.mu.Unlock()

	_, ok := wss.states[wal]
	if !ok {
		wss.states[wal] = state
	}
	return wss.states[wal]
}

type WalletStates struct {
	sequences []*WalletStatesSequence
	wbits     int
}

func newWalletStates() *WalletStates {
	wbits := viper.GetInt("wallet_sequence_keybits")
	nSequences := int(math.Pow(2, float64(wbits)))
	sequences := make([]*WalletStatesSequence, nSequences)

	for i := 0; i < nSequences; i++ {
		sequences[i] = newWalletStatesSequence(0)
	}

	ws := WalletStates{
		sequences: sequences,
		wbits:     wbits,
	}

	return &ws
}

func (ws *WalletStates) Index(wal crypto.WalletAddress) uint64 {
	ind := binary.LittleEndian.Uint64(wal.Address()[wal.Length()-8:])
	ind = ind >> (64 - ws.wbits)
	return ind
}

func (ws *WalletStates) Get(wal crypto.WalletAddress) (*walletState, bool) {
	return ws.sequences[ws.Index(wal)].Get(wal)
}

func (ws *WalletStates) Add(wal crypto.WalletAddress, state *walletState) *walletState {
	return ws.sequences[ws.Index(wal)].Add(wal, state)
}

func (ws *WalletStates) Length() int {
	sz := 0
	for _, seq := range ws.sequences {
		sz += seq.Length()
	}
	return sz
}

type ConcurrentTxnLog struct {
	txnLog []ids.TransactionId
	mu     sync.RWMutex
}

func newConcurrentTxnLog() *ConcurrentTxnLog {
	return &ConcurrentTxnLog{
		txnLog: []ids.TransactionId{},
	}
}

func (ctl *ConcurrentTxnLog) Add(tids ...ids.TransactionId) {
	ctl.mu.Lock()
	defer ctl.mu.Unlock()

	ctl.txnLog = append(ctl.txnLog, tids...)
}

type ConcurrentFinalized struct {
	finalized map[ids.TransactionId]Transaction
	mu        sync.RWMutex
}

func newConcurrentFinalized() *ConcurrentFinalized {
	return &ConcurrentFinalized{
		finalized: map[ids.TransactionId]Transaction{},
	}
}

func (cfin *ConcurrentFinalized) Add(txn Transaction) {
	cfin.mu.Lock()
	defer cfin.mu.Unlock()

	cfin.finalized[txn.TxnId()] = txn
}

func (cfin *ConcurrentFinalized) Range(f func(tid ids.TransactionId, txn Transaction) bool) {
	cfin.mu.Lock()
	defer cfin.mu.Unlock()

	for tid, txn := range cfin.finalized {
		if !f(tid, txn) {
			return
		}
	}
}

func (cfin *ConcurrentFinalized) Length() int {
	cfin.mu.RLock()
	defer cfin.mu.RUnlock()

	return len(cfin.finalized)
}

type IncrLedger struct {
	wstates    *WalletStates
	TxnLog_    *ConcurrentTxnLog
	Finalized_ *ConcurrentFinalized
	Parent_    Ledger
	Child_     Ledger
	mu         sync.RWMutex
	Wg         sync.WaitGroup
}

func NewIncrLedger(parent Ledger) *IncrLedger {
	ledger := new(IncrLedger)
	ledger.wstates = newWalletStates()
	ledger.TxnLog_ = newConcurrentTxnLog()
	ledger.Finalized_ = newConcurrentFinalized()
	ledger.Parent_ = parent
	ledger.Child_ = nil
	ledger.Wg = sync.WaitGroup{}
	return ledger
}

// NB(chuck): Use this function for reading only. If the returned *ledgerMap
// was found in THIS ledger, it COULD be written to - but for safety the
// writeableWalletState(..) function should be used instead if that's the intent.
func (ledger *IncrLedger) WalletState(wal crypto.WalletAddress) (*walletState, bool) {
	cstate, ok := ledger.wstates.Get(wal)
	if !ok {
		return ledger.Parent().WalletState(wal)
	}
	return cstate, true
}

// Obtains a ledger map that can be written to. This could be the one already
// in this ledger, or a copy of one from a previous ledger. If a copy is made,
// it will be entered into this ledger. If there's no entry for this wallet
// then an empty ledgerMap will be returned (but not entered into the ledger).
func (ledger *IncrLedger) writeableWalletState(wal crypto.WalletAddress) *walletState {
	state, ok := ledger.wstates.Get(wal)
	if !ok {
		state, ok = ledger.Parent().WalletState(wal)
		if ok {
			state = state.Copy()
		} else {
			state = newConcurrentWalletState()
		}
		state = ledger.wstates.Add(wal, state)
	}
	return state
}
func (ledger *IncrLedger) Utxos(walletAddr crypto.WalletAddress) []Utxo {
	state, ok := ledger.WalletState(walletAddr)
	if !ok {
		return []Utxo{}
	}

	// Make a copy and sort it
	sorted := state.Utxos()
	sort.Slice(sorted, func(i, j int) bool {
		return sorted[i].Amount < sorted[j].Amount
	})

	return sorted
}

func (ledger *IncrLedger) TxnLog() []ids.TransactionId {
	return ledger.TxnLog_.txnLog
}

func (ledger *IncrLedger) Parent() Ledger {
	ledger.mu.RLock()
	defer ledger.mu.RUnlock()

	return ledger.Parent_
}

func (ledger *IncrLedger) Child() Ledger {
	ledger.mu.RLock()
	defer ledger.mu.RUnlock()

	return ledger.Child_
}

func (ledger *IncrLedger) Balance(walletAddr crypto.WalletAddress) uint64 {
	state, ok := ledger.WalletState(walletAddr)
	if !ok {
		return 0
	}

	var balance uint64 = 0

	for _, utxo := range state.Utxos() {
		balance += utxo.Amount
	}

	return balance
}

func (ledger *IncrLedger) CheckNumber(walletAddr crypto.WalletAddress) uint16 {
	state, ok := ledger.WalletState(walletAddr)
	if !ok {
		return 0
	}
	return state.CheckNumber()
}

func (ledger *IncrLedger) ExpectedCheckNumber(walletAddr crypto.WalletAddress) uint16 {
	// The first check number is 1, not 0.
	state, ok := ledger.WalletState(walletAddr)
	if !ok {
		return 1
	}
	return state.CheckNumber() + 1
}

func (l *IncrLedger) inputsUnspent(txn Transaction) bool {
	state, ok := l.WalletState(txn.Source())
	for _, input := range txn.Inputs() {
		// NB(chuck): Only check if the ledgerMap exists once we're in this
		// loop. It's OK for the ledgerMap to not yet exist as long as the
		// transaction has no inputs (e.g. a mint).
		if !ok || !state.Has(input) {
			log.Warningf("\tUTXO %s didn't exist", input.String())
			return false
		}
	}
	return true
}

func (l *IncrLedger) paymentBalanceSufficient(pmt Payment) bool {
	txnValue := uint64(0)
	for _, output := range pmt.Value().Outputs_ {
		t := txnValue + output.Amount()
		if (t > txnValue) != (output.Amount() > 0) {
			// Overflow.
			return false
		}
		txnValue = t
	}
	return l.Balance(pmt.Source()) >= txnValue
}

func (ledger *IncrLedger) IsValid(txn Transaction) error {
	if !txn.IsValid() {
		log.Warningf("isValidTransaction: transaction %x rejected: invalid", txn.Hash())
		return fmt.Errorf("VALIDTXN-INVALID")
	}

	if !txn.HasValidOutput() {
		log.Warningf("isValidTransaction: transaction %x rejected: doesn't have valid output", txn.Hash())
		return fmt.Errorf("VALIDTXN-BAD_OUTPUT")
	}

	if ledger.ExpectedCheckNumber(txn.Source()) != txn.CheckNumber() {
		log.Warningf("isValidTransaction: transaction %x from swal %x rejected: incorrect check number (got %d expected %d)",
			txn.Hash(), txn.Source(), txn.CheckNumber(), ledger.ExpectedCheckNumber(txn.Source()))
		return fmt.Errorf("VALIDTXN-WRONG_CHECKNUMBER")
	}

	if !ledger.inputsUnspent(txn) {
		log.Warningf("isValidTransaction: transaction %x rejected: inputs aren't UNSPENT\n", txn.Hash())
		return fmt.Errorf("VALIDTXN-DOUBLE_SPEND")
	}

	switch txn.Type() {
	case PaymentTxn:
		pmt := txn.(*Payment)
		if !ledger.paymentBalanceSufficient(*pmt) {
			log.Warningf("isValidTransaction: payment %x rejected: insufficient balance", txn.Hash())
			return fmt.Errorf("VALIDTXN-INSUFFICIENT_BALANCE")
		}
		return nil
	case MintTxn:
		return nil
	case SweepTxn:
		return nil
	default:
		// NB(chuck): should never happen
		log.Errorf("isValidTransaction: transaction rejected: unrecognized type")
		return fmt.Errorf("VALIDTXN-UNRECOGNIZED_TYPE")
	}
}

func (ledger *IncrLedger) UpdateTxnLog(txns ...Transaction) {
	defer ledger.Wg.Done()

	tidList := make([]ids.TransactionId, len(txns))
	for i, txn := range txns {
		tidList[i] = txn.TxnId()
	}
	ledger.TxnLog_.Add(tidList...)
}

// *IncrLedger.QueueFinalized checks if the transactions in the provided list are valid, and if
// so attempts to process them. If an error is encountered in either of these processes, the
// transaction is queued to be re-attempted, as it may just need another finalized transaction
// to process first.
func (ledger *IncrLedger) QueueFinalized(txns ...Transaction) {
	defer ledger.Wg.Done()

	for _, txn := range txns {
		ledger.processTxn(txn)
	}
}

func (ledger *IncrLedger) doMint(mint *Mint) error {
	// Verify the check number first.
	exp := ledger.ExpectedCheckNumber(mint.Src)
	if exp != mint.CheckNumber_ {
		return fmt.Errorf("DoMint: ERROR: Expected check number %d next, you sent %d.",
			exp, mint.CheckNumber_)
	}

	utxo := Utxo{
		TxnId:  mint.TxnId(),
		Amount: mint.Output_.Amount_,
		Output: 0,
	}

	destAddr := mint.Output_.Dest()
	destState := ledger.writeableWalletState(destAddr)
	destState.Add(utxo)

	srcAddr := mint.Src
	srcState := ledger.writeableWalletState(srcAddr)
	srcState.SetCheckNumber(mint.CheckNumber_)

	return nil
}

func (ledger *IncrLedger) doSweep(sweep *Sweep) error {
	srcState := ledger.writeableWalletState(sweep.Src)

	// Verify the check number is what we expect.
	if sweep.CheckNumber_ != srcState.CheckNumber()+1 {
		return errors.New(fmt.Sprintf("DoSweep: Invalid check number: got %d, expected %d.",
			sweep.CheckNumber_, srcState.CheckNumber()+1))
	}

	// Verify the inputs are valid and the output matches them
	var total uint64 = 0
	utxos := sweep.Inputs_

	for _, utxo := range utxos {
		if !srcState.Has(utxo) {
			return fmt.Errorf("Cannot sweep missing UTXO: %s", utxo.String())
		}

		newTotal := total + utxo.Amount

		if (newTotal > total) != (utxo.Amount > 0) {
			// Overflow.
			return errors.New("Total outputs exceeds maximum integer value.")
		}

		total = newTotal
	}

	if total != sweep.Output_.Amount_ {
		return fmt.Errorf("Sweep %x output value didn't match inputs, %d != %d",
			sweep.Hash(), sweep.Output_.Amount_, total)
	}

	// Verify the destination wallet is the same as the source
	if sweep.Output_.Dest_ != sweep.Src {
		return fmt.Errorf("Sweep %x output dest didn't match src, %x != %x",
			sweep.Hash(), sweep.Output_.Dest_, sweep.Src)
	}

	// Delete the spent UTXOs
	for _, utxo := range utxos {
		srcState.Delete(utxo)
	}

	newutxo := Utxo{
		TxnId:  sweep.TxnId(),
		Amount: sweep.Output_.Amount_,
		Output: 0,
	}

	// Finally, update the ledger.
	srcState.SetCheckNumber(sweep.CheckNumber_)
	srcState.Add(newutxo)

	return nil
}

func (ledger *IncrLedger) doPayment(pmt *Payment) error {
	// Verify the check number is what we expect.
	checkNo := ledger.CheckNumber(pmt.Src) + 1

	if pmt.CheckNumber_ != checkNo {
		return fmt.Errorf("DoPayment: Invalid check number: got %d, expected %d",
			pmt.CheckNumber_, checkNo)
	}

	// NB(leaf): When we look up slices of things like Utxos in our maps, we get back
	// a new slice, so modifications to it don't affect the underlying map until we
	// reassign. We therefore compute all the new values and then assign them all at
	// once at the very end, which eliminates the possibility of leaving the Ledger in
	// a weird state.

	// Find the list of Utxos in the source wallet. The var spentUtxos contains those
	// that we spend here and walletUtxos contains everything that remains unspent.
	spentUtxos := []Utxo{}
	srcState := ledger.writeableWalletState(pmt.Src)

	// Find the input(s) and verify there's enough to cover the outputs.
	// for each input:
	//   - find the Utxo
	//   - generate list of all UTXOs to be spent
	//   - sum up the total value of the transaction
	var input_amount uint64 = 0
	for _, input := range pmt.Inputs_ {
		if !srcState.Has(input) {
			return fmt.Errorf("Invalid payment: UTXO %s not found", input.String())
		}
		spentUtxos = append(spentUtxos, input)

		input_amount += input.Amount
	}

	// We should know the input_amount and we should have an updated list of unspent Utxos and
	// a list of spent ones.

	// For each output:
	//   - sum up the total amount
	//   - create a Utxo
	var total_amount uint64 = 0
	type UtxoAndDest struct {
		utxo           Utxo
		destWalletAddr crypto.WalletAddress
	}
	tmpUtxoAndDest := make([]UtxoAndDest, 0)

	for i, output := range pmt.Outputs_ {
		// Don't create UTXOs with 0 value
		if output.Amount() == 0 {
			continue
		}

		newTotal := total_amount + output.Amount()

		if (newTotal > total_amount) != (output.Amount() > 0) {
			// Overflow.
			return errors.New("Total outputs exceeds maximum integer value.")
		}

		total_amount = newTotal

		utxoAndDest := UtxoAndDest{
			utxo: Utxo{
				TxnId:  pmt.TxnId(),
				Amount: output.Amount_,
				Output: uint16(i),
			},
			destWalletAddr: output.Dest(),
		}

		tmpUtxoAndDest = append(tmpUtxoAndDest, utxoAndDest)
	}

	if total_amount > input_amount {
		return fmt.Errorf("Insufficient balance: trying to pay out %d, inputs have only %d.",
			total_amount, input_amount)
	} else if total_amount < input_amount {
		// Don't let people stupidly burn their coins. Send a payment to address 0 if you want that.
		return fmt.Errorf("Payment would burn Silvermint: total paid: outputs=%d < inputs=%d.",
			total_amount, input_amount)
	}

	// NB(leaf): No errors beyond here. Everything must deterministically complete or panic.

	// Delete the spent UTXOs from the source wallet.
	for _, spent := range spentUtxos {
		srcState.Delete(spent)
	}

	// Update the check number.
	srcState.SetCheckNumber(pmt.CheckNumber_)

	// Update the Utxo states for the output Utxos.
	// Add the new Utxos to the wallets.
	for _, utxoAndDest := range tmpUtxoAndDest {
		destState := ledger.writeableWalletState(utxoAndDest.destWalletAddr)
		destState.Add(utxoAndDest.utxo)
	}

	return nil
}

func (ledger *IncrLedger) processTxn(txn Transaction) error {
	var err error
	if err = ledger.IsValid(txn); err != nil {
		ledger.Finalized_.Add(txn)
	} else {
		switch txn.Type() {
		case PaymentTxn:
			var pmt *Payment
			pmt = txn.(*Payment)
			TxnsOutputsTotalFinalized.Add(float64(len(pmt.Outputs_)))
			err = ledger.doPayment(pmt)
		case MintTxn:
			var mint *Mint
			mint = txn.(*Mint)
			TxnsOutputsTotalFinalized.Inc()
			err = ledger.doMint(mint)
		case SweepTxn:
			var sweep *Sweep
			sweep = txn.(*Sweep)
			TxnsOutputsTotalFinalized.Inc()
			err = ledger.doSweep(sweep)
		default:
			err := fmt.Errorf("transaction rejected: unrecognized "+
				"transaction type: %d",
				txn.Type())
			log.Errorln(err.Error())
			return err
		}
		if err != nil {
			log.Errorf("error processing txn %x: %s", txn.Hash(), err.Error())
			ledger.Finalized_.Add(txn)
		}
	}
	return err
}

func (ledger *IncrLedger) AllInputsExist(txn Transaction) bool {
	for _, input := range txn.Inputs() {
		lmap, ok := ledger.WalletState(txn.Source())
		if !ok || !lmap.Has(input) {
			return false
		}
	}
	return true
}

// *IncrLedger.ProcessQueued attempts to process all transactions that failed to process initially
// with *IncrLedger.QueueFinalized. Some or all of the transactions in the list may just require
// that they be processed in a particular order (e.g. sequential checknumbers from the same wallet,
// or a transaction that spends a UTXO generated by another transaction in the list). It continues
// re-attempting processing of each transaction until no more can be processed. At that point it
// returns a composite error message for each of the failed transactions.
func (ledger *IncrLedger) ProcessQueued() error {
	var rejected []Transaction
	var errs []error
	anyProcessed := true
	for ledger.Finalized_.Length() != 0 && anyProcessed {
		anyProcessed = false
		rejected = []Transaction{}
		errs = []error{}
		ledger.Finalized_.Range(func(tid ids.TransactionId, txn Transaction) bool {
			if txn.CheckNumber() == ledger.ExpectedCheckNumber(txn.Source()) &&
				ledger.AllInputsExist(txn) {
				err := ledger.IsValid(txn)
				if err != nil {
					rejected = append(rejected, txn)
					errs = append(errs, err)
				} else {
					err = ledger.processTxn(txn)
					if err != nil {
						rejected = append(rejected, txn)
						errs = append(errs, err)
					} else {
						anyProcessed = true
						delete(ledger.Finalized_.finalized, tid)
					}
				}
			}
			return true
		})
	}
	ledger.Finalized_ = nil

	if len(rejected) > 0 {
		rejStrs := []string{}
		for i := range rejected {
			rejStrs = append(rejStrs, fmt.Sprintf("unable to process txn %s with CD %s: %s",
				rejected[i].TxnId(), rejected[i].ConflictDomain(), errs[i].Error()))
		}
		errStr := strings.Join(rejStrs, ", ")
		return fmt.Errorf(errStr)
	}
	return nil
}

func (ledger *IncrLedger) SetParent(parent Ledger) {
	ledger.mu.Lock()
	defer ledger.mu.Unlock()

	ledger.Parent_ = parent
}

func (ledger *IncrLedger) SetChild(child Ledger) {
	ledger.mu.Lock()
	defer ledger.mu.Unlock()

	ledger.Child_ = child
}

// See the more comprehensive description of the Compact routine detailed for
// the SSLedger. In short, this function compacts the receiver, ledger, into
// the argument. The function is only defined for an argument of type SSLedger.
func (ledger *IncrLedger) Compact(compacted Ledger) {
	switch compacted := compacted.(type) {
	case *SSLedger:
		// Copy wallet / UTXOs / check numbers
		for _, seq := range ledger.wstates.sequences {
			for wal, state := range seq.states {
				compacted.states[wal] = state
			}
		}
	case *IncrLedger:
		log.Errorf("Attempted to compact IncrLedger into another IncrLedger")
	}
}

func (ledger *IncrLedger) HeaderLength() uint64 {
	var l uint64 = 8 // Length of byte data
	l += 1           // Ledger type
	// Parent and Child info isn't marshaled, but rather reconstructed on load.
	return l
}

func (ledger *IncrLedger) WalletMapsLength() uint64 {
	var l uint64 = 8 // Num Wallets
	for _, seq := range ledger.wstates.sequences {
		for wal, state := range seq.states {
			l += uint64(wal.Length())                    // Wallet address (includes type/length)
			l += 2                                       // Wallet checknumber
			l += 4                                       // Num UTXOs in wallet
			l += uint64(len(state.utxos)) * UtxoLength() // Len of UTXOs
		}
	}
	return l
}

func (ledger *IncrLedger) TxnLogLength() uint64 {
	// A uint32 should be able to hold 15 minutes' worth of finalized txns at max TPS
	var l uint64 = 4 // Num finalized
	l += uint64(len(ledger.TxnLog_.txnLog)) * ids.TransactionIdLength()
	return l
}

func (ledger *IncrLedger) Length() uint64 {
	l := ledger.HeaderLength()
	l += ledger.WalletMapsLength()
	l += ledger.TxnLogLength()
	return l
}

func (ledger *IncrLedger) MarshalBinary() ([]byte, error) {
	llen := ledger.Length()
	data := make([]byte, llen)
	offset := uint64(0)
	// Ledger length
	binary.LittleEndian.PutUint64(data[offset:offset+8], llen)
	offset += 8

	// Ledger type
	data[offset] = byte(INCRLEDGER)
	offset += 1

	// Wallet checknumbers and UTXOs
	binary.LittleEndian.PutUint64(data[offset:offset+8], uint64(ledger.wstates.Length()))
	offset += 8
	ulen := UtxoLength()
	for _, seq := range ledger.wstates.sequences {
		for wal, state := range seq.states {
			wlen := wal.Length()
			copy(data[offset:offset+uint64(wlen)], wal.Address())
			offset += uint64(wlen)
			binary.LittleEndian.PutUint16(data[offset:offset+2], state.checkNumber)
			offset += 2
			binary.LittleEndian.PutUint32(data[offset:offset+4], uint32(len(state.utxos)))
			offset += 4
			for _, utxo := range state.utxos {
				udat, err := utxo.MarshalBinary()
				if err != nil {
					return nil, err
				}
				copy(data[offset:offset+ulen], udat)
				offset += ulen
			}
		}
	}

	// Transaction log
	binary.LittleEndian.PutUint32(data[offset:offset+4], uint32(len(ledger.TxnLog_.txnLog)))
	offset += 4
	tlen := ids.TransactionIdLength()
	for _, tid := range ledger.TxnLog_.txnLog {
		tdat, err := tid.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(data[offset:offset+tlen], tdat)
		offset += tlen
	}

	if offset != llen {
		return nil, fmt.Errorf("Computed ledger byte length didn't "+
			"match marshaled ledger byte length: %d != %d", llen, offset)
	}

	// Parent and Child info isn't marshaled, but rather reconstructed on load.

	return data, nil
}

func (ledger *IncrLedger) UnmarshalBinary(data []byte) error {
	if len(data) == 0 {
		return fmt.Errorf("can't unmarshal empty byte array")
	}
	offset := uint64(0)

	llen := binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8
	if int(llen) != len(data) {
		return fmt.Errorf("Unmarshal failed: Ledger has length %d "+
			"bytes, but we have %d bytes of data.", llen, len(data))
	}

	// Skip ledger type (if we're here we already know it's an IncrLedger)
	offset += 1

	// Wallet UTXOs and CheckNumbers
	nWals := binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8
	ulen := UtxoLength()
	ledger.wstates = newWalletStates()
	for i := uint64(0); i < nWals; i++ {
		wlen := binary.LittleEndian.Uint16(data[offset+2 : offset+4])
		wal := crypto.NewWalletAddress(data[offset : offset+uint64(wlen)])
		offset += uint64(wlen)
		cn := binary.LittleEndian.Uint16(data[offset : offset+2])
		offset += 2
		state := newConcurrentWalletState()
		state.checkNumber = cn
		lutxos := binary.LittleEndian.Uint32(data[offset : offset+4])
		offset += 4
		utxos := make([]Utxo, lutxos)
		for i := range utxos {
			var err error
			utxos[i], err = UnmarshalUtxo(data[offset : offset+ulen])
			if err != nil {
				return err
			}
			offset += ulen
		}
		state.utxos = utxos
		ledger.wstates.Add(wal, state)
	}

	// Transaction log
	nTxns := binary.LittleEndian.Uint32(data[offset : offset+4])
	offset += 4
	tlen := ids.TransactionIdLength()
	ledger.TxnLog_ = newConcurrentTxnLog()
	for i := uint32(0); i < nTxns; i++ {
		tid, err := ids.UnmarshalTransactionId(data[offset : offset+tlen])
		if err != nil {
			return err
		}
		ledger.TxnLog_.txnLog = append(ledger.TxnLog_.txnLog, tid)
		offset += tlen
	}

	if offset != llen {
		return fmt.Errorf("Computed ledger byte length didn't "+
			"match unmarshaled ledger byte length: %d != %d", llen, offset)
	}

	// Parent and Child info isn't unmarshaled, but rather reconstructed on load.

	return nil
}

func (ledger *IncrLedger) Wait() {
	ledger.Wg.Wait()
}
