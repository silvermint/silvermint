package transaction

import (
	"encoding/binary"
	"fmt"
	"sort"
	"sync"

	//"time"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
)

type SSLedger struct {
	states      map[crypto.WalletAddress]*walletState
	TxnLog_     []ids.TransactionId
	SpentUtxos_ []Utxo
	Child_      Ledger
	Height_     uint64
	mu          sync.RWMutex
}

func NewSSLedger(height uint64) *SSLedger {
	//nowMicro := time.Now().UnixMicro()

	ledger := new(SSLedger)
	ledger.states = map[crypto.WalletAddress]*walletState{}
	ledger.TxnLog_ = []ids.TransactionId{}
	ledger.SpentUtxos_ = []Utxo{}
	ledger.Child_ = nil
	ledger.Height_ = height

	//LedgerMicroVec.WithLabelValues("NewSSLedger").Add(float64(time.Now().UnixMicro() - nowMicro))

	return ledger
}

func (l *SSLedger) WalletState(w crypto.WalletAddress) (*walletState, bool) {
	v, ok := l.states[w]
	return v, ok
}

func (l *SSLedger) Utxos(w crypto.WalletAddress) []Utxo {
	//nowMicro := time.Now().UnixMicro()
	var utxos []Utxo

	v, ok := l.states[w]
	if ok {
		//LedgerMicroVec.WithLabelValues("Utxos").Add(float64(time.Now().UnixMicro() - nowMicro))

		utxos = v.utxos
	}

	// Make a copy and sort it
	sorted := make([]Utxo, len(utxos))
	copy(sorted, utxos)
	sort.Slice(sorted, func(i, j int) bool {
		return sorted[i].Amount < sorted[j].Amount
	})
	//LedgerMicroVec.WithLabelValues("Utxos").Add(float64(time.Now().UnixMicro() - nowMicro))

	return sorted
}

func (l *SSLedger) TxnLog() []ids.TransactionId {
	return l.TxnLog_
}

func (l *SSLedger) Parent() Ledger {
	return nil
}

func (l *SSLedger) Child() Ledger {
	l.mu.RLock()
	defer l.mu.RUnlock()

	return l.Child_
}

func (l *SSLedger) Balance(w crypto.WalletAddress) uint64 {
	//nowMicro := time.Now().UnixMicro()

	v, ok := l.states[w]
	if !ok {

		return 0
	}

	var balance uint64 = 0

	for _, u := range v.utxos {
		balance += u.Amount
	}
	//LedgerMicroVec.WithLabelValues("Balance").Add(float64(time.Now().UnixMicro() - nowMicro))

	return balance
}

func (l *SSLedger) CheckNumber(w crypto.WalletAddress) uint16 {
	//nowMicro := time.Now().UnixMicro()
	v, ok := l.states[w]
	if !ok {
		//LedgerMicroVec.WithLabelValues("CheckNumber").Add(float64(time.Now().UnixMicro() - nowMicro))

		return 0
	}
	//LedgerMicroVec.WithLabelValues("CheckNumber").Add(float64(time.Now().UnixMicro() - nowMicro))

	return v.checkNumber
}

func (l *SSLedger) ExpectedCheckNumber(w crypto.WalletAddress) uint16 {
	//nowMicro := time.Now().UnixMicro()

	// The first check number is 1, not 0.
	prev, ok := l.states[w]

	if !ok {
		//LedgerMicroVec.WithLabelValues("ExpectedCheckNumber").Add(float64(time.Now().UnixMicro() - nowMicro))
		return 1
	}
	//LedgerMicroVec.WithLabelValues("ExpectedCheckNumber").Add(float64(time.Now().UnixMicro() - nowMicro))

	return prev.checkNumber + 1
}

func (l *SSLedger) inputsUnspent(txn Transaction) bool {
	state, ok := l.WalletState(txn.Source())
	for _, input := range txn.Inputs() {
		// NB(chuck): Only check if the walletState exists once we're in this
		// loop. It's OK for the walletState to not yet exist as long as the
		// transaction has no inputs (e.g. a mint).
		if !ok || !state.Has(input) {
			log.Warningf("\tUTXO %s didn't exist", input.String())
			return false
		}
	}
	return true
}

func (l *SSLedger) paymentBalanceSufficient(pmt Payment) bool {
	txnValue := uint64(0)
	for _, output := range pmt.Value().Outputs_ {
		t := txnValue + output.Amount()
		if (t > txnValue) != (output.Amount() > 0) {
			// Overflow.
			return false
		}
		txnValue = t
	}
	return l.Balance(pmt.Source()) >= txnValue
}
func (ledger *SSLedger) IsValid(txn Transaction) error {
	if !txn.IsValid() {
		log.Warningf("isValidTransaction: transaction %x rejected: invalid", txn.Hash())
		return fmt.Errorf("VALIDTXN-INVALID")
	}

	if !txn.HasValidOutput() {
		log.Warningf("isValidTransaction: transaction %x rejected: doesn't have valid output", txn.Hash())
		return fmt.Errorf("VALIDTXN-BAD_OUTPUT")
	}

	if ledger.ExpectedCheckNumber(txn.Source()) != txn.CheckNumber() {
		log.Warningf("isValidTransaction: transaction %x rejected: incorrect check number", txn.Hash())
		return fmt.Errorf("VALIDTXN-WRONG_CHECKNUMBER")
	}

	if !ledger.inputsUnspent(txn) {
		log.Warningf("isValidTransaction: transaction %x rejected: inputs aren't UNSPENT", txn.Hash())
		return fmt.Errorf("VALIDTXN-DOUBLE_SPEND")
	}

	switch txn.Type() {
	case PaymentTxn:
		pmt := txn.(*Payment)
		if !ledger.paymentBalanceSufficient(*pmt) {
			log.Warningf("isValidTransaction: payment %x rejected: insufficient balance", txn.Hash())
			return fmt.Errorf("VALIDTXN-INSUFFICIENT_BALANCE")
		}
		return nil
	case MintTxn:
		return nil
	case SweepTxn:
		return nil
	default:
		// NB(chuck): should never happen
		log.Errorf("isValidTransaction: transaction rejected: unrecognized type")
		return fmt.Errorf("VALIDTXN-UNRECOGNIZED_TYPE")
	}
}

func (ledger *SSLedger) QueueFinalized(_ ...Transaction) {
	log.Errorf("Not valid to use SSLedger's QueueFinalized function")
}

func (ledger *SSLedger) ProcessQueued() error {
	return fmt.Errorf("Not valid to use SSLedger's ProcessQueued function")
}

func (l *SSLedger) SetParent(_ Ledger) {
	log.Errorf("Not valid to use SSLedger's SetParent function")
}

func (l *SSLedger) SetChild(child Ledger) {
	l.mu.Lock()
	defer l.mu.Unlock()

	l.Child_ = child
}

// NB(chuck): There is some overloading of terminology here (hence the switch
// statement) that makes this confusing. There are two compaction "processes",
// one which is the full compaction (i.e., go back to the last SSLedger and
// compact all ledgers since then into one new SSLedger), and individual ledger
// compaction (i.e. compact this one ledger - whatever its type - into one
// SSLedger).
// The general usage of Compact:
//   - ssl.Compact(IncrLedger): Go back to the last SSLedger and compact
//     forward into the provided empty ssl (full compaction).
//   - ssl.Compact(SSLedger): Copy ssl into SSLedger.
//   - incr.Compact(SSLedger): Copy incr into SSLedger.
//   - incr.Compact(IncrLedger): undefined
//
// Thus, generally Compact works as source.Compact(target) with the exception
// of ssl.Compact(IncrLedger), which traverses back to the last SSLedger and
// runs individual ledger.Compact(ssl) on each ledger to merge them into ssl.
func (ssl *SSLedger) Compact(l Ledger) {
	switch l.(type) {
	// This is the full compaction, compacting forward from the last SSLedger
	// until it reaches the child of l. The result is placed in the receiver,
	// ssl.
	case *IncrLedger:
		// Find the last SSLedger
		ledger := l
		for ledger.Parent() != nil {
			ledger = ledger.Parent()
		}

		// Pre-allocate the new SSLedger's maps to be the size of the last SSLedger.
		prev := ledger.(*SSLedger)
		ssl.states = make(map[crypto.WalletAddress]*walletState, len(prev.states))

		// Compact the created SSLedger with all ledgers up to the latest one
		for ledger != l.Child() {
			ledger.Compact(ssl)
			ledger = ledger.Child()
		}

		// Make copies of the wallet states in the SSLedger, since the current
		// entries are direct references to other ledgers' states (and thus
		// prevents those ledgers from being GCed)
		for wal, state := range ssl.states {
			ssl.states[wal] = state.Copy()
		}

		NumWalletsInLedger.Set(float64(len(ssl.states)))
		n := uint64(0)
		for _, state := range ssl.states {
			n += uint64(len(state.utxos))
		}
		NumUtxosInWallets.Set(float64(n))

		// Assign its child as the provided ledger's and the child's parent
		// as the new SSLedger.
		if l.Child() == nil {
			l.SetChild(NewIncrLedger(ssl))
		}
		ssl.SetChild(l.Child())
		ssl.Child().SetParent(ssl)

		// Remove the IncrLedger's child pointer so that the ledgers can be
		// garbage collected.
		l.SetChild(nil)
	// This is the individual compaction, which places the values contained in
	// the receiver, ssl, into the argument ledger, l.
	case *SSLedger:
		l := l.(*SSLedger)
		// Copy wallet / UTXOs / check numbers
		for wal, state := range ssl.states {
			l.states[wal] = state
		}
	}
}

func (ledger *SSLedger) HeaderLength() uint64 {
	var l uint64 = 8 // Length of byte data
	l += 1           // Ledger type
	l += 8           // Height
	// Parent and Child info isn't marshaled, but rather reconstructed on load.
	return l
}

func (ledger *SSLedger) WalletMapsLength() uint64 {
	var l uint64 = 8 // Num Wallets
	for wal, state := range ledger.states {
		l += uint64(wal.Length())                    // Wallet address (includes type/length)
		l += 2                                       // Wallet checknumber
		l += 4                                       // Num UTXOs in wallet
		l += uint64(len(state.utxos)) * UtxoLength() // Len of UTXOs
	}
	return l
}

func (ledger *SSLedger) TxnLogLength() uint64 {
	// A uint32 should be able to hold 15 minutes' worth of finalized txns at max TPS
	var l uint64 = 4 // Num finalized
	l += uint64(len(ledger.TxnLog_)) * ids.TransactionIdLength()
	return l
}

func (ledger *SSLedger) Length() uint64 {
	l := ledger.HeaderLength()
	l += ledger.WalletMapsLength()
	l += ledger.TxnLogLength()
	return l
}

func (ledger *SSLedger) MarshalBinary() ([]byte, error) {
	llen := ledger.Length()
	data := make([]byte, llen)
	offset := uint64(0)
	// Ledger length
	binary.LittleEndian.PutUint64(data[offset:offset+8], llen)
	offset += 8

	// Ledger type
	data[offset] = byte(SSLEDGER)
	offset += 1

	// Ledger height
	binary.LittleEndian.PutUint64(data[offset:offset+8], ledger.Height_)
	offset += 8

	// Wallet checknumbers and UTXOs
	binary.LittleEndian.PutUint64(data[offset:offset+8], uint64(len(ledger.states)))
	offset += 8
	ulen := UtxoLength()
	for wal, state := range ledger.states {
		wlen := wal.Length()
		copy(data[offset:offset+uint64(wlen)], wal.Address())
		offset += uint64(wlen)
		binary.LittleEndian.PutUint16(data[offset:offset+2], state.checkNumber)
		offset += 2
		binary.LittleEndian.PutUint32(data[offset:offset+4], uint32(len(state.utxos)))
		offset += 4
		for _, utxo := range state.utxos {
			udat, err := utxo.MarshalBinary()
			if err != nil {
				return nil, err
			}
			copy(data[offset:offset+ulen], udat)
			offset += ulen
		}
	}

	// Transaction log
	binary.LittleEndian.PutUint32(data[offset:offset+4], uint32(len(ledger.TxnLog_)))
	offset += 4
	tlen := ids.TransactionIdLength()
	for _, tid := range ledger.TxnLog_ {
		tdat, err := tid.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(data[offset:offset+tlen], tdat)
		offset += tlen
	}

	if offset != llen {
		return nil, fmt.Errorf("Computed ledger byte length didn't "+
			"match marshaled ledger byte length: %d != %d", llen, offset)
	}

	// Parent and Child info isn't marshaled, but rather reconstructed on load.

	return data, nil
}

func (ledger *SSLedger) UnmarshalBinary(data []byte) error {
	if len(data) == 0 {
		return fmt.Errorf("can't unmarshal empty byte array")
	}
	offset := uint64(0)

	llen := binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8
	if int(llen) != len(data) {
		return fmt.Errorf("Unmarshal failed: Ledger has length %d "+
			"bytes, but we have %d bytes of data.", llen, len(data))
	}

	// Skip ledger type (if we're here we already know it's an SSLedger)
	offset += 1

	// Height
	ledger.Height_ = binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8

	// Wallet UTXOs and CheckNumbers
	nWals := binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8
	ulen := UtxoLength()
	ledger.states = make(map[crypto.WalletAddress]*walletState, nWals)
	for i := uint64(0); i < nWals; i++ {
		wlen := binary.LittleEndian.Uint16(data[offset+2 : offset+4])
		wal := crypto.NewWalletAddress(data[offset : offset+uint64(wlen)])
		offset += uint64(wlen)
		cn := binary.LittleEndian.Uint16(data[offset : offset+2])
		offset += 2
		ledger.states[wal] = new(walletState)
		ledger.states[wal].checkNumber = cn
		lutxos := binary.LittleEndian.Uint32(data[offset : offset+4])
		offset += 4
		utxos := make([]Utxo, lutxos)
		for i := range utxos {
			var err error
			utxos[i], err = UnmarshalUtxo(data[offset : offset+ulen])
			if err != nil {
				return err
			}
			offset += ulen
		}
		ledger.states[wal].utxos = utxos
	}

	// Transaction log
	nTxns := binary.LittleEndian.Uint32(data[offset : offset+4])
	offset += 4
	ledger.TxnLog_ = make([]ids.TransactionId, nTxns)
	tlen := ids.TransactionIdLength()
	for i := uint32(0); i < nTxns; i++ {
		var err error
		ledger.TxnLog_[i], err = ids.UnmarshalTransactionId(data[offset : offset+tlen])
		if err != nil {
			return err
		}
		offset += tlen
	}

	if offset != llen {
		return fmt.Errorf("Computed ledger byte length didn't "+
			"match unmarshaled ledger byte length: %d != %d", llen, offset)
	}

	// Parent and Child info isn't unmarshaled, but rather reconstructed on load.

	return nil
}

func (ledger *SSLedger) Wait() {
}
