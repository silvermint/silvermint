// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package transaction

import (
	"encoding/binary"
	"fmt"
	"reflect"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
)

type MintOutput struct {
	Amount_ uint64
	Dest_   crypto.WalletAddress
}

func (out MintOutput) Amount() uint64             { return out.Amount_ }
func (out MintOutput) Dest() crypto.WalletAddress { return out.Dest_ }

type MintCD struct {
	fmt.Stringer
	Source_      crypto.WalletAddress
	CheckNumber_ uint16
	Numeric      uint64
}

func (mint MintCD) isConflictDomain() {}
func (mint MintCD) Source() crypto.WalletAddress {
	return mint.Source_
}
func (mint MintCD) CheckNumber() uint16 {
	return mint.CheckNumber_
}
func (mint MintCD) String() string {
	return fmt.Sprintf("MintCD[chk=%d]:%x", mint.CheckNumber_, mint.Source_)
}
func (mint MintCD) Uint64() uint64 {
	return mint.Numeric
}

func (mint MintCD) Length() uint16 {
	var l uint16 = 2           // Length
	l += 2                     // Transaction type
	l += mint.Source_.Length() // Source
	l += 2                     // CheckNumber
	return l
}

func (mint MintCD) MarshalBinary() ([]byte, error) {
	tlen := mint.Length()
	data := make([]byte, tlen)
	var offset uint16 = 0

	// Length
	binary.LittleEndian.PutUint16(data[offset:offset+2], tlen)
	offset += 2

	// Transaction type
	binary.LittleEndian.PutUint16(data[offset:offset+2], MintTxn)
	offset += 2

	// Source
	addr := mint.Source_.Address()
	copy(data[offset:offset+uint16(len(addr))], addr)
	offset += uint16(len(addr))

	// CheckNumber
	binary.LittleEndian.PutUint16(data[offset:offset+2], mint.CheckNumber_)
	offset += 2

	if offset != tlen {
		return nil, fmt.Errorf("ERROR: Offset didn't match computed MintCD length, %d != %d",
			offset, tlen)
	}

	return data, nil
}

type Mint struct {
	// TODO(leaf): Document the wire format.
	Src             crypto.WalletAddress
	CheckNumber_    uint16
	Output_         MintOutput
	Hash_           *TransactionHash
	Bytes           []byte
	Sig             crypto.Signature
	ConflictDomain_ *MintCD
	TxnId_          ids.TransactionId
}

func (m Mint) isTransaction() {}

func (mint Mint) Type() uint16 {
	return MintTxn
}

func (mint *Mint) ConflictDomain() CD {
	if mint.ConflictDomain_ == nil {
		mint.ConflictDomain_ = &MintCD{
			Source_:      mint.Src,
			CheckNumber_: mint.CheckNumber_,
			Numeric:      mint.Src.Uint64(),
		}
	}
	return *mint.ConflictDomain_
}

func (mint Mint) Value() Mint {
	return mint
}

func (mint Mint) Output(n int) (TransactionOutput, error) {
	if n != 0 {
		return nil, fmt.Errorf("Invalid output index %d: Mint only has output=0.", n)
	}
	// TODO(leaf): Maybe we want to store ptrs to Outputs instead?
	output := mint.Output_
	return output, nil
}

func (pout MintOutput) Length() uint16 {
	return uint16(reflect.TypeOf(pout.Amount_).Size()) + pout.Dest().Length()
}

func (mint Mint) Length() uint16 {
	var i uint16 = 0

	// Len
	i += 2

	// Txn Type
	i += 2

	// Src
	i += mint.Src.Length()

	// Chk No
	i += 2

	// Output
	m := MintOutput{}
	if mint.Output_ != m {
		i += mint.Output_.Length()
	}

	// Sig
	i += crypto.SIGNATURE_SIZE

	return i
}

func (mint Mint) Data() []byte {
	data := make([]byte, len(mint.Bytes))
	copy(data, mint.Bytes)
	return data
}

func (mint Mint) Source() crypto.WalletAddress {
	return mint.Src
}

func (mint Mint) CheckNumber() uint16 {
	return mint.CheckNumber_
}

// TODO(chuck): Stub
func (mint Mint) IsValid() bool {
	return mint.Output_.Amount_ != 0
}

func (mint Mint) MarshalBinary() ([]byte, error) {
	data := make([]byte, mint.Length())
	var offset int = 0

	// Len
	binary.LittleEndian.PutUint16(data[offset:offset+2], mint.Length())
	offset += 2

	// Txn Type
	binary.LittleEndian.PutUint16(data[offset:offset+2], mint.Type())
	offset += 2

	// Src
	copy(data[offset:offset+int(mint.Src.Length())], mint.Src.Address()[:])
	offset += int(mint.Src.Length())

	// ChkNo
	binary.LittleEndian.PutUint16(data[offset:offset+2], mint.CheckNumber_)
	offset += 2

	// Output
	output, err := mint.Output(0)
	if err != nil || output.Dest() == nil {
		// This should never happen.
		return nil, fmt.Errorf("Mint transaction has no output?!?! Error: %v", err.Error())
	}
	binary.LittleEndian.PutUint64(data[offset:offset+8], output.Amount())
	offset += 8

	dstkey := output.Dest()
	copy(data[offset:offset+int(dstkey.Length())], dstkey.Address()[:])
	offset += int(dstkey.Length())

	// Signature
	copy(data[offset:offset+crypto.SIGNATURE_SIZE], mint.Sig[:])
	return data, nil
}

func (mint Mint) MarshalSignedBinary(wallet crypto.Wallet) ([]byte, error) {
	buf, err := mint.MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("Sign: Can't marshal payment for signing: %s", err.Error())
	}
	sig, err := wallet.Sign(buf[0 : len(buf)-crypto.SIGNATURE_SIZE])
	if err != nil {
		return nil, fmt.Errorf("Sign: Can't sign payment: %s", err.Error())
	}
	copy(buf[len(buf)-crypto.SIGNATURE_SIZE:], sig)
	return buf, nil
}

func (mint *Mint) UnmarshalBinary(data []byte) error {
	if len(data) == 0 {
		return fmt.Errorf("Mint: can't unmarshal empty byte array.")
	}

	mint.Bytes = data

	var offset int = 0
	mintLength := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	mintTyp := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	if mintTyp != MintTxn {
		return fmt.Errorf("Can't parse binary mint: expected txn type %d, got %d.",
			MintTxn, mintTyp)
	}

	srcWalletType := binary.LittleEndian.Uint16(data[offset : offset+2])
	srcLength := int(binary.LittleEndian.Uint16(data[offset+2 : offset+4]))
	mint.Src = crypto.NewWalletAddress(data[offset : offset+srcLength])
	if mint.Src == nil {
		return fmt.Errorf("walletType does not exist %d", srcWalletType)
	}
	offset += srcLength

	mint.CheckNumber_ = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	out := MintOutput{}

	// Amount
	out.Amount_ = binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8

	// Dest
	destWalletType := binary.LittleEndian.Uint16(data[offset : offset+2])
	destLength := int(binary.LittleEndian.Uint16(data[offset+2 : offset+4]))
	out.Dest_ = crypto.NewWalletAddress(data[offset : offset+destLength])
	if out.Dest_ == nil {
		return fmt.Errorf("walletType does not exist %d", destWalletType)
	}
	offset += destLength

	mint.Output_ = out

	var sig crypto.Signature
	copy(sig[:], data[offset:offset+crypto.SIGNATURE_SIZE])
	offset += crypto.SIGNATURE_SIZE
	mint.Sig = sig

	if mint.Length() != mintLength {
		return fmt.Errorf("Mint: can't unmarshal: declared length %d != "+
			"actual length %d.", mintLength, mint.Length())
	}

	return nil
}

func (mint *Mint) UnmarshalSignedBinary(wallet crypto.Wallet, data []byte) error {
	err := mint.UnmarshalBinary(data)
	if err != nil {
		return err
	}

	if wallet.Verify(data) {
		return nil
	}

	return fmt.Errorf("Binary decoded correctly, but signature is invalid.")
}

func (mint *Mint) Hash() TransactionHash {
	if mint.Hash_ == nil {
		data, err := mint.MarshalBinary()
		if err != nil {
			return TransactionHash{}
		}
		hash := TransactionHash(crypto.HashBytes(data[:len(data)-crypto.SIGNATURE_SIZE]))
		mint.Hash_ = &hash
	}
	return *mint.Hash_
}

func (mint *Mint) HasValidOutput() bool {
	return mint.Output_.Amount_ > 0
}

func (mint *Mint) Inputs() []Utxo {
	return []Utxo{}
}

func (mint *Mint) TxnId() ids.TransactionId {
	return mint.TxnId_
}

func (mint *Mint) SetId(tid ids.TransactionId) {
	mint.TxnId_ = tid
}

// A mint is slashable if it was never valid and never will be (unlike the check for if it's
// valid, which is a determination that might change with time)
func (mint *Mint) IsSlashable() bool {
	sourceWallet, err := crypto.UnmarshalBinary(mint.Source().Address())
	if err != nil {
		log.Warningf("Mint %s rejected: unable to create wallet from mint source: %s",
			mint.TxnId().String(), err.Error())
		return true
	}

	if !sourceWallet.Verify(mint.Data()) {
		log.Warningf("Mint %s rejected: bad signature", mint.TxnId().String())
		return true
	}
	return false
}
