// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package transaction

import (
	"encoding/binary"
	"fmt"
	"reflect"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
)

type PaymentOutput struct {
	Amount_ uint64
	Dest_   crypto.WalletAddress
}

func (out PaymentOutput) Amount() uint64             { return out.Amount_ }
func (out PaymentOutput) Dest() crypto.WalletAddress { return out.Dest_ }

type PaymentCD struct {
	fmt.Stringer
	Source_      crypto.WalletAddress
	CheckNumber_ uint16
	Numeric      uint64
}

func (pmt PaymentCD) isConflictDomain() {}
func (pmt PaymentCD) Source() crypto.WalletAddress {
	return pmt.Source_
}
func (pmt PaymentCD) CheckNumber() uint16 {
	return pmt.CheckNumber_
}

func (pmt PaymentCD) String() string {
	return fmt.Sprintf("PaymentCD:[chk=%d]:%x", pmt.CheckNumber_, pmt.Source_)
}
func (pmt PaymentCD) Uint64() uint64 {
	return pmt.Numeric
}

func (pmt PaymentCD) Length() uint16 {
	var l uint16 = 2          // Length
	l += 2                    // Transaction type
	l += pmt.Source_.Length() // Source
	l += 2                    // CheckNumber
	return l
}

func (pmt PaymentCD) MarshalBinary() ([]byte, error) {
	tlen := pmt.Length()
	data := make([]byte, tlen)
	var offset uint16 = 0

	// Length
	binary.LittleEndian.PutUint16(data[offset:offset+2], tlen)
	offset += 2

	// Transaction type
	binary.LittleEndian.PutUint16(data[offset:offset+2], PaymentTxn)
	offset += 2

	// Source
	addr := pmt.Source_.Address()
	copy(data[offset:offset+uint16(len(addr))], addr)
	offset += uint16(len(addr))

	// CheckNumber
	binary.LittleEndian.PutUint16(data[offset:offset+2], pmt.CheckNumber_)
	offset += 2

	if offset != tlen {
		return nil, fmt.Errorf("ERROR: Offset didn't match computed PaymentCD length, %d != %d",
			offset, tlen)
	}

	return data, nil
}

type Payment struct {
	// NB(leaf): The wire format here is as follows:
	//    Source:			32 bytes
	//    CheckNumber:		 2 bytes
	//    NumOutputs:  		 2 bytes
	//    Outputs:          40 bytes/ea
	Src          crypto.WalletAddress
	CheckNumber_ uint16

	Inputs_  []Utxo
	Outputs_ []PaymentOutput

	Hash_           *TransactionHash
	Bytes           []byte
	Sig             crypto.Signature
	ConflictDomain_ *PaymentCD
	TxnId_          ids.TransactionId
}

func (pmt Payment) isTransaction() {}

func (pmt Payment) Type() uint16 {
	return PaymentTxn
}

func (pmt *Payment) ConflictDomain() CD {
	if pmt.ConflictDomain_ == nil {
		pmt.ConflictDomain_ = &PaymentCD{
			Source_:      pmt.Src,
			CheckNumber_: pmt.CheckNumber_,
			Numeric:      pmt.Src.Uint64(),
		}
	}
	return *pmt.ConflictDomain_
}

func (pmt Payment) Value() Payment {
	return pmt
}

func (pmt Payment) Output(n int) (TransactionOutput, error) {
	if n >= len(pmt.Outputs_) || n < 0 {
		return nil, fmt.Errorf("Invalid output index %d: Payment has %d outputs.",
			n, len(pmt.Outputs_))
	}
	// TODO(leaf): Maybe we want to store ptrs to Outputs instead?
	output := pmt.Outputs_[n]
	return output, nil
}

func (pout PaymentOutput) Length() uint16 {
	return uint16(reflect.TypeOf(pout.Amount_).Size()) + pout.Dest().Length()
}

func (pmt Payment) Length() uint16 {
	var length uint16 = 0
	// Length (2 bytes)
	length += 2
	// Txn Type (2 bytes)
	length += 2
	// Source (32 bytes)
	length += pmt.Src.Length()
	// CheckNumber (2 bytes)
	length += 2
	// NumInputs
	length += 2
	// Inputs
	length += uint16(UtxoLength() * uint64(len(pmt.Inputs_)))
	// NumOutputs
	length += 2
	// Outputs (40 bytes/ea)
	// use this for Length() in case there aren't any. E.g., if you're burning tokens.
	for i := 0; i < len(pmt.Outputs_); i++ {
		length += pmt.Outputs_[i].Length()
	}
	// Signature (64 bytes)
	length += crypto.SIGNATURE_SIZE
	return length
}

func (pmt Payment) Data() []byte {
	data := make([]byte, len(pmt.Bytes))
	copy(data, pmt.Bytes)
	return data
}

func (pmt Payment) Source() crypto.WalletAddress {
	return pmt.Src
}

func (pmt Payment) CheckNumber() uint16 {
	return pmt.CheckNumber_
}

func (pmt Payment) IsValid() bool {
	for _, output := range pmt.Outputs_ {
		if output.Amount_ == 0 {
			return false
		}
	}
	return true
}

func (pmt Payment) MarshalBinary() ([]byte, error) {
	var d []byte

	var payment_sz uint16 = pmt.Length()
	d = make([]byte, payment_sz)

	// Length
	var offset int = 0
	binary.LittleEndian.PutUint16(d[offset:offset+2], payment_sz)
	offset += 2

	// Txn Type
	binary.LittleEndian.PutUint16(d[offset:offset+2], pmt.Type())
	offset += 2

	// Source PubKey
	for i := 0; i < int(pmt.Src.Length()); i++ {
		d[offset+i] = pmt.Src.Address()[i]
	}
	offset += int(pmt.Src.Length())

	// Check Number
	binary.LittleEndian.PutUint16(d[offset:offset+2], pmt.CheckNumber_)
	offset += 2

	// Num Inputs
	binary.LittleEndian.PutUint16(d[offset:offset+2], uint16(len(pmt.Inputs_)))
	offset += 2

	// Inputs
	ulen := int(UtxoLength())
	for _, input := range pmt.Inputs_ {
		udat, err := input.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(d[offset:offset+ulen], udat)
		offset += ulen
	}

	// Num Outputs
	binary.LittleEndian.PutUint16(d[offset:offset+2], uint16(len(pmt.Outputs_)))
	offset += 2

	// Outputs
	for _, output := range pmt.Outputs_ {
		destLength := len(output.Dest_.Address())
		binary.LittleEndian.PutUint64(d[offset:offset+8], output.Amount())
		offset += 8

		copy(d[offset:offset+destLength], output.Dest_.Address())
		offset += destLength
	}
	// Signature
	copy(d[offset:offset+crypto.SIGNATURE_SIZE], pmt.Sig[:])
	return d, nil
}

func (pmt Payment) MarshalSignedBinary(wallet crypto.Wallet) ([]byte, error) {
	buf, err := pmt.MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("Sign: Can't marshal payment for signing: %s", err.Error())
	}
	sig, err := wallet.Sign(buf[0 : len(buf)-crypto.SIGNATURE_SIZE])
	if err != nil {
		return nil, fmt.Errorf("Sign: Can't sign payment: %s", err.Error())
	}
	copy(buf[len(buf)-crypto.SIGNATURE_SIZE:], sig)
	return buf, nil
}

func (pmt *Payment) UnmarshalBinary(data []byte) error {
	if len(data) == 0 {
		return fmt.Errorf("Payment: can't unmarshal empty byte array.")
	}
	pmt.Bytes = data

	var offset int = 0
	pmtLength := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	pmtTyp := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	if pmtTyp != PaymentTxn {
		return fmt.Errorf("Can't parse binary payment: expected txn type %d, got %d.",
			PaymentTxn, pmtTyp)
	}

	srcWalletType := binary.LittleEndian.Uint16(data[offset : offset+2])
	srcLength := int(binary.LittleEndian.Uint16(data[offset+2 : offset+4]))
	pmt.Src = crypto.NewWalletAddress(data[offset : offset+srcLength])
	if pmt.Src == nil {
		return fmt.Errorf("walletType does not exist %d", srcWalletType)
	}

	offset += srcLength

	pmt.CheckNumber_ = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Num Inputs
	numInputs := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Inputs
	pmt.Inputs_ = make([]Utxo, numInputs)
	ulen := int(UtxoLength())
	for i := uint16(0); i < numInputs; i++ {
		var err error
		pmt.Inputs_[i], err = UnmarshalUtxo(data[offset : offset+ulen])
		if err != nil {
			return err
		}
		offset += ulen
	}

	num_outputs := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	var i uint16 = 0
	for ; i < num_outputs; i++ {
		out := PaymentOutput{}

		// Amount
		out.Amount_ = binary.LittleEndian.Uint64(data[offset : offset+8])
		offset += 8

		// Dest
		destWalletType := binary.LittleEndian.Uint16(data[offset : offset+2])
		destLength := int(binary.LittleEndian.Uint16(data[offset+2 : offset+4]))
		out.Dest_ = crypto.NewWalletAddress(data[offset : offset+destLength])
		if out.Dest_ == nil {
			return fmt.Errorf("walletType does not exist %d", destWalletType)
		}

		offset += destLength

		pmt.Outputs_ = append(pmt.Outputs_, out)
	}

	var sig crypto.Signature
	copy(sig[:], data[offset:offset+crypto.SIGNATURE_SIZE])
	offset += crypto.SIGNATURE_SIZE
	pmt.Sig = sig

	// NB(leaf): We can't do this check until we've decoded the outputs, because Length()
	// gives the wrong result.
	if pmt.Length() != pmtLength {
		return fmt.Errorf("Payment: can't unmarshal: declared length %d != "+
			"actual length %d.", pmtLength, pmt.Length())
	}

	return nil
}

func (pmt *Payment) UnmarshalSignedBinary(wallet crypto.Wallet, data []byte) error {
	err := pmt.UnmarshalBinary(data)
	if err != nil {
		return err
	}

	if wallet.Verify(data) {
		return nil
	}

	return fmt.Errorf("Binary decoded correctly, but signature is invalid.")
}

func (pmt *Payment) Hash() TransactionHash {
	if pmt.Hash_ == nil {
		data, err := pmt.MarshalBinary()
		if err != nil {
			return TransactionHash{}
		}
		hash := TransactionHash(crypto.HashBytes(data[:len(data)-crypto.SIGNATURE_SIZE]))
		pmt.Hash_ = &hash
	}

	return *pmt.Hash_
}

func (pmt *Payment) HasValidOutput() bool {
	if len(pmt.Outputs_) == 0 {
		return false
	}
	for _, o := range pmt.Outputs_ {
		if o.Amount_ <= 0 {
			return false
		}
	}
	return true
}

func (pmt *Payment) Inputs() []Utxo {
	return pmt.Inputs_
}

func (pmt *Payment) TxnId() ids.TransactionId {
	return pmt.TxnId_
}

func (pmt *Payment) SetId(tid ids.TransactionId) {
	pmt.TxnId_ = tid
}

// A payment is slashable if it was never valid and never will be (unlike the check for if it's
// valid, which is a determination that might change with time)
func (pmt *Payment) IsSlashable() bool {
	sourceWallet, err := crypto.UnmarshalBinary(pmt.Source().Address())
	if err != nil {
		log.Warningf("Mint %s rejected: unable to create wallet from mint source: %s",
			pmt.TxnId().String(), err.Error())
		return true
	}

	if !sourceWallet.Verify(pmt.Data()) {
		log.Warningf("Mint %s rejected: bad signature", pmt.TxnId().String())
		return true
	}
	return false
}
