package crypto

import (
	"crypto"
	"crypto/ed25519"
	"crypto/rand"

	"encoding/binary"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/silvermint/silvermint/log"
)

const (
	CREATOR_NAME_SIZE     = 10
	TEST_WALLET_ADDR_SIZE = PUBLIC_KEY_SIZE + CREATOR_NAME_SIZE + 4
)

type TestWalletAddress struct {
	Address_ [TEST_WALLET_ADDR_SIZE]byte
}

func (a TestWalletAddress) Address() []byte {
	return a.Address_[:]
}

func (a TestWalletAddress) Length() uint16 {
	return TEST_WALLET_ADDR_SIZE
}

func (a TestWalletAddress) Uint64() uint64 {
	return binary.LittleEndian.Uint64(a.Address_[a.Length()-UINT64_BYTES-1:])
}

type TestWallet struct {
	Type_        uint16
	Length_      uint16
	Filename_    string
	CreatorName_ string
	Public_      ed25519.PublicKey
	Private_     ed25519.PrivateKey
}

func (w TestWallet) Type() uint16                { return w.Type_ }
func (w TestWallet) Length() uint16              { return w.Length_ }
func (w TestWallet) Filename() string            { return w.Filename_ }
func (w TestWallet) Public() ed25519.PublicKey   { return w.Public_ }
func (w TestWallet) CreatorName() string         { return w.CreatorName_ }
func (w TestWallet) Private() ed25519.PrivateKey { return w.Private_ }

// WalletAddress returns Address which is the marshal binary of the wallet struct, this is
// the Public Address that is now used to represent a Wallet, not just the PublicKey
func (w TestWallet) WalletAddress() WalletAddress {
	//set address to make sure that the address is generated and set in the wallet struct
	Address, err := w.MarshalBinary()
	if err != nil {
		log.Errorf("Unable to create WalletAddress: %s", err.Error())
		return nil
	}
	var buf [TEST_WALLET_ADDR_SIZE]byte
	copy(buf[:], Address[:])
	a := TestWalletAddress{Address_: buf}
	return a
}

func (w TestWallet) PrivateBytes() [PRIVATE_KEY_SIZE]byte {
	var buf [PRIVATE_KEY_SIZE]byte
	copy(buf[:], w.Private_[:])
	return buf
}

func (w TestWallet) Save() error {
	return w.SaveKeys(w.Filename_)
}

func (w TestWallet) SaveKeys(filename string) error {
	// Private key file is just the filename. Public key has ".pub"
	// appended to it.
	privKeyFilename := filename
	pubKeyFilename := fmt.Sprintf("%s.pub", filename)

	privKeyDirectory := filepath.Dir(privKeyFilename)
	info, err := os.Stat(privKeyDirectory)
	if err != nil {
		return fmt.Errorf("Couldn't save private key to invalid directory: %s", err.Error())
	} else if !info.IsDir() {
		return fmt.Errorf("Couldn't save private key: not a directory: %s", privKeyDirectory)
	} else {
		mode := info.Mode() | fs.ModePerm
		if mode != 0700 {
			// This could optionally be an error if we want to be very persnickety.
			log.Warningf("saving private key to directory readable "+
				"by others (mode %o). Recommend setting directory permissions "+
				"mode to 0700, instead.", mode)
		}
	}

	f, err := os.OpenFile(privKeyFilename, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return fmt.Errorf("Couldn't save private key to file %s: %s",
			privKeyFilename, err.Error())
	}
	defer f.Close()

	s := fmt.Sprintf("%x", w.Private_)
	_, err = f.WriteString(s)
	if err != nil {
		return fmt.Errorf("Couldn't save private key: %s", err.Error())
	}

	g, err := os.OpenFile(pubKeyFilename, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return fmt.Errorf("Couldn't save public key to file %s: %s",
			pubKeyFilename, err.Error())
	}
	defer g.Close()

	//Saving the full marshal binary of the wallet, not just the public key
	addr, err := w.MarshalBinary()
	if err != nil {
		return fmt.Errorf("Couldn't marshal wallet bytes: %s", err.Error())
	}
	t := fmt.Sprintf("%x", addr)
	if _, err = g.WriteString(t); err != nil {
		return fmt.Errorf("Couldn't save public key: %s", err.Error())
	}

	log.Debugf("Keys saved to %s", filename)
	return nil
}

func (w TestWallet) Sign(message []byte) ([]byte, error) {
	sig, err := w.Private_.Sign(rand.Reader, message, crypto.Hash(0))
	if err != nil {
		return sig, fmt.Errorf("Couldn't sign message: %s", err.Error())
	}
	return sig, nil
}

func (w TestWallet) Verify(message []byte) bool {
	if len(message) <= SIGNATURE_SIZE {
		return false
	}

	msg := message[0 : len(message)-SIGNATURE_SIZE]
	signature := message[len(message)-SIGNATURE_SIZE:]
	return ed25519.Verify(w.Public_, msg, signature)
}

func NewTestWallet(addr []byte) TestWallet {
	w, err := UnmarshalBinary(addr)
	if err != nil {
		log.Errorf("Unable to create wallet: %s", err.Error())
		return TestWallet{}
	}
	return w.(TestWallet)
}

func (w TestWallet) MarshalBinary() ([]byte, error) {
	addr := make([]byte, TEST_WALLET_ADDR_SIZE)

	// Type
	var offset int = 0
	binary.LittleEndian.PutUint16(addr[offset:offset+2], w.Type_)
	offset += 2

	// Total length of address
	binary.LittleEndian.PutUint16(addr[offset:offset+2], w.Length_)
	offset += 2

	// CreatorName
	copy(addr[offset:offset+CREATOR_NAME_SIZE], w.CreatorName_[:])
	offset += CREATOR_NAME_SIZE

	// Source PubKey
	copy(addr[offset:offset+PUBLIC_KEY_SIZE], w.Public_[:])
	offset += PUBLIC_KEY_SIZE

	return addr, nil
}
