package crypto

import (
	"crypto"
	"crypto/ed25519"
	"crypto/rand"
	"encoding/binary"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/silvermint/silvermint/log"
)

const (
	BASIC_WALLET_ADDR_SIZE = PUBLIC_KEY_SIZE + 4
)

type BasicWalletAddress struct {
	Address_ [BASIC_WALLET_ADDR_SIZE]byte
}

func (sa BasicWalletAddress) Address() []byte {
	return sa.Address_[:]
}

func (sa BasicWalletAddress) Length() uint16 {
	return BASIC_WALLET_ADDR_SIZE
}

func (sa BasicWalletAddress) Uint64() uint64 {
	return binary.LittleEndian.Uint64(sa.Address_[sa.Length()-UINT64_BYTES-1:])
}

type BasicWallet struct {
	Type_     uint16
	Length_   uint16
	Filename_ string
	Public_   ed25519.PublicKey
	Private_  ed25519.PrivateKey
}

func (w BasicWallet) Type() uint16                { return w.Type_ }
func (w BasicWallet) Length() uint16              { return w.Length_ }
func (w BasicWallet) Filename() string            { return w.Filename_ }
func (w BasicWallet) Public() ed25519.PublicKey   { return w.Public_ }
func (w BasicWallet) Private() ed25519.PrivateKey { return w.Private_ }

// WalletAddress returns Address which is the marshal binary of the wallet struct, this is
// the Public Address that is now used to represent a Wallet, not just the PublicKey
func (w BasicWallet) WalletAddress() WalletAddress {
	//set address to make sure that the address is generated and set in the wallet struct
	addr, err := w.MarshalBinary()
	if err != nil {
		log.Errorf("Unable to create WalletAddress: %s", err.Error())
		return nil
	}
	var buf [BASIC_WALLET_ADDR_SIZE]byte
	copy(buf[:], addr[:])
	sa := BasicWalletAddress{Address_: buf}
	return sa
}

func (w BasicWallet) PrivateBytes() [PRIVATE_KEY_SIZE]byte {
	var buf [PRIVATE_KEY_SIZE]byte
	copy(buf[:], w.Private_[:])
	return buf
}

func (w BasicWallet) Save() error {
	return w.SaveKeys(w.Filename_)
}

func (w BasicWallet) SaveKeys(filename string) error {
	// Private key file is just the filename. Public key has ".pub"
	// appended to it.
	privKeyFilename := filename
	pubKeyFilename := fmt.Sprintf("%s.pub", filename)

	privKeyDirectory := filepath.Dir(privKeyFilename)
	info, err := os.Stat(privKeyDirectory)
	if err != nil {
		return fmt.Errorf("Couldn't save private key to invalid directory: %s", err.Error())
	} else if !info.IsDir() {
		return fmt.Errorf("Couldn't save private key: not a directory: %s", privKeyDirectory)
	} else {
		mode := info.Mode() | fs.ModePerm
		if mode != 0700 {
			// This could optionally be an error if we want to be very persnickety.
			log.Warningf("saving private key to directory readable "+
				"by others (mode %o). Recommend setting directory permissions "+
				"mode to 0700, instead.", mode)
		}
	}

	f, err := os.OpenFile(privKeyFilename, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return fmt.Errorf("Couldn't save private key to file %s: %s",
			privKeyFilename, err.Error())
	}
	defer f.Close()

	s := fmt.Sprintf("%x", w.Private_)
	_, err = f.WriteString(s)
	if err != nil {
		return fmt.Errorf("Couldn't save private key: %s", err.Error())
	}

	g, err := os.OpenFile(pubKeyFilename, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return fmt.Errorf("Couldn't save public key to file %s: %s",
			pubKeyFilename, err.Error())
	}
	defer g.Close()

	//Saving the full marshal binary of the wallet, not just the public key
	addr, err := w.MarshalBinary()
	if err != nil {
		return fmt.Errorf("Couldn't marshal wallet bytes: %s", err.Error())
	}
	t := fmt.Sprintf("%x", addr)
	if _, err = g.WriteString(t); err != nil {
		return fmt.Errorf("Couldn't save public key: %s", err.Error())
	}

	log.Debugf("Keys saved to %s", filename)
	return nil
}

func (w BasicWallet) Sign(message []byte) ([]byte, error) {
	sig, err := w.Private_.Sign(rand.Reader, message, crypto.Hash(0))
	if err != nil {
		return sig, fmt.Errorf("Couldn't sign message: %s", err.Error())
	}
	return sig, nil
}

func (w BasicWallet) Verify(message []byte) bool {
	if len(message) <= SIGNATURE_SIZE {
		return false
	}

	msg := message[0 : len(message)-SIGNATURE_SIZE]
	signature := message[len(message)-SIGNATURE_SIZE:]
	return ed25519.Verify(w.Public_, msg, signature)
}

func NewSilvermintWallet(addr []byte) BasicWallet {
	w, err := UnmarshalBinary(addr)
	if err != nil {
		log.Errorf("Unable to create wallet: %s", err.Error())
		return BasicWallet{}
	}
	return w.(BasicWallet)
}

func NewSilvermintWalletFromPrivKey(privKey []byte) BasicWallet {
	w := BasicWallet{
		Type_:    BASIC_WALLET,
		Length_:  BASIC_WALLET_ADDR_SIZE,
		Public_:  make([]byte, PUBLIC_KEY_SIZE),
		Private_: make([]byte, PRIVATE_KEY_SIZE),
	}
	copy(w.Private_, privKey)
	pubkeyBytes, _ := PubKeyToBytes(w.Private_.Public())
	copy(w.Public_, pubkeyBytes[:PUBLIC_KEY_SIZE])
	return w
}

func PubKeyToBytes(pub crypto.PublicKey) ([]byte, error) {
	switch pub.(type) {
	case ed25519.PublicKey:
		var edpubkey ed25519.PublicKey = pub.(ed25519.PublicKey)
		bytes := make([]byte, ed25519.PublicKeySize)
		copy(bytes, edpubkey)
		return bytes, nil
	default:
		return nil, fmt.Errorf("Unsupported gob of goo.")
	}
}

func (wal BasicWallet) MarshalBinary() ([]byte, error) {
	addr := make([]byte, BASIC_WALLET_ADDR_SIZE)

	// Type
	var offset int = 0
	binary.LittleEndian.PutUint16(addr[offset:offset+2], wal.Type_)
	offset += 2

	// Total length of address
	binary.LittleEndian.PutUint16(addr[offset:offset+2], wal.Length_)
	offset += 2

	// Source PubKey
	copy(addr[offset:offset+PUBLIC_KEY_SIZE], wal.Public_[:])

	return addr, nil
}
