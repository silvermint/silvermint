package crypto

import (
	"crypto/ed25519"
	"encoding/binary"
	"fmt"
)

// List of wallet types
const (
	BASIC_WALLET uint16 = 0
	TEST_WALLET  uint16 = 1
)

const UINT64_BYTES = 8

type WalletAddress interface {
	Address() []byte
	Length() uint16
	Uint64() uint64
}

func NewWalletAddress(address []byte) WalletAddress {
	walletType := binary.LittleEndian.Uint16(address[0:2])
	if walletType == BASIC_WALLET {
		var tmpAddr [BASIC_WALLET_ADDR_SIZE]byte
		copy(tmpAddr[:], address[:BASIC_WALLET_ADDR_SIZE])
		return BasicWalletAddress{Address_: tmpAddr}
	}
	if walletType == TEST_WALLET {
		var tmpAddr [TEST_WALLET_ADDR_SIZE]byte
		copy(tmpAddr[:], address[:TEST_WALLET_ADDR_SIZE])
		return TestWalletAddress{Address_: tmpAddr}
	}
	return nil
}

type Wallet interface {
	Type() uint16
	Length() uint16
	Filename() string
	Public() ed25519.PublicKey
	Private() ed25519.PrivateKey

	WalletAddress() WalletAddress
	PrivateBytes() [PRIVATE_KEY_SIZE]byte
	Save() error
	SaveKeys(filename string) error
	Sign(message []byte) ([]byte, error)
	Verify(message []byte) bool

	MarshalBinary() ([]byte, error)
}

func CompareWalletAddress(l WalletAddress, r WalletAddress) bool {
	if len(l.Address()) != len(r.Address()) {
		return false
	}
	for i := 0; i < len(l.Address()); i++ {
		if l.Address()[i] != r.Address()[i] {
			return false
		}
	}
	return true
}

// Checks if it is a wallet type
func IsWalletType(WalletType uint16) bool {
	return WalletType == BASIC_WALLET || WalletType == TEST_WALLET
}

func NewWallet(filename string, walletType uint16) (Wallet, error) {
	k, p, err := ed25519.GenerateKey(nil)
	if err != nil {
		return nil, fmt.Errorf("Couldn't generate keypair: %s", err.Error())
	}
	//check if walletType is an actual wallet type
	if !IsWalletType(walletType) {
		return nil, fmt.Errorf("WalletType does not exist: %d", walletType)
	}

	var w Wallet
	switch walletType {
	case BASIC_WALLET:
		w = BasicWallet{
			Type_:     BASIC_WALLET,
			Length_:   BASIC_WALLET_ADDR_SIZE,
			Filename_: filename,
			Public_:   k,
			Private_:  p,
		}
	case TEST_WALLET:
		w = TestWallet{
			Type_:        TEST_WALLET,
			Length_:      TEST_WALLET_ADDR_SIZE,
			Filename_:    filename,
			CreatorName_: "Default",
			Public_:      k,
			Private_:     p,
		}
	default:
		return nil, fmt.Errorf("WalletType does not exist: %d", walletType)
	}

	if len(filename) != 0 {
		err = w.SaveKeys(filename)
		if err != nil {
			return nil, fmt.Errorf("Couldn't save keypair: %s", err.Error())
		}
	}

	return w, nil
}

func GenerateKey() (ed25519.PublicKey, ed25519.PrivateKey, error) {
	return ed25519.GenerateKey(nil)
}

func LoadWalletFromFile(filename string) (Wallet, error) {
	privKeyFilename := filename
	pubKeyFilename := fmt.Sprintf("%s.pub", filename)

	var returnW Wallet

	var err error
	Private, err := LoadKeyFromFile(privKeyFilename)
	if err != nil {
		return returnW, err
	}

	Address, err := LoadKeyFromFile(pubKeyFilename)
	if err != nil {
		return returnW, err
	}

	Type := binary.LittleEndian.Uint16(Address[0:2])

	w, err := UnmarshalBinary(Address)
	if err != nil {
		return nil, fmt.Errorf("Unable to unmarshal wallet from "+
			"address: %s", err.Error())
	}
	switch Type {
	case BASIC_WALLET:
		bw := w.(BasicWallet)
		bw.Private_ = Private
		bw.Filename_ = filename
		w = bw
	case TEST_WALLET:
		tw := w.(TestWallet)
		tw.Private_ = Private
		tw.Filename_ = filename
		w = tw
	default:
		return nil, fmt.Errorf("WalletType does not exist: %d", Type)
	}

	return w, nil
}

// NB(chuck): Kinda dumb, but we can't have an UnmarshalBinary([]byte) error
// interface function for a Wallet. That would require changing the Wallet
// interface from being an interface to {BasicWallet, TestWallet} to being an
// interface to {*BasicWallet, *TestWallet} (since UnmarshalBinary must have
// a pointer receiver). This changes all the Ledger stuff because we could no
// longer readily store/lookup wallets in maps or check them for equality, due
// to the fact that we'd be comparing pointers instead of structs.
func UnmarshalBinary(keybytes []byte) (Wallet, error) {
	if len(keybytes) < PUBLIC_KEY_SIZE {
		return BasicWallet{}, fmt.Errorf(
			"Can't load wallet from bytes: need at least %d bytes, have %d",
			PUBLIC_KEY_SIZE, len(keybytes))
	}

	var offset uint16 = 0
	wtype := binary.LittleEndian.Uint16(keybytes[offset : offset+2])
	offset += 2
	wlen := binary.LittleEndian.Uint16(keybytes[offset : offset+2])
	offset += 2

	var w Wallet
	switch wtype {
	case BASIC_WALLET:
		wal := BasicWallet{
			Type_:     BASIC_WALLET,
			Length_:   BASIC_WALLET_ADDR_SIZE,
			Filename_: "",
		}
		if len(keybytes) != BASIC_WALLET_ADDR_SIZE {
			return nil, fmt.Errorf(
				"Can't load wallet from bytes: need %d bytes, have %d",
				BASIC_WALLET_ADDR_SIZE, len(keybytes))
		}

		// Source PubKey
		wal.Public_ = keybytes[offset : offset+PUBLIC_KEY_SIZE]
		offset += PUBLIC_KEY_SIZE

		if wlen != offset {
			return nil, fmt.Errorf(
				"Wallet reported size of %d, actual size was %d",
				wlen, offset)
		}
		w = wal
	case TEST_WALLET:
		wal := TestWallet{
			Type_:     TEST_WALLET,
			Length_:   TEST_WALLET_ADDR_SIZE,
			Filename_: "",
		}
		if len(keybytes) != TEST_WALLET_ADDR_SIZE {
			return nil, fmt.Errorf(
				"Can't load wallet from bytes: need %d bytes, have %d",
				TEST_WALLET_ADDR_SIZE, len(keybytes))
		}

		// Creator Name
		wal.CreatorName_ = string(keybytes[offset : offset+CREATOR_NAME_SIZE])
		offset += CREATOR_NAME_SIZE

		// Source PubKey
		wal.Public_ = keybytes[offset : offset+PUBLIC_KEY_SIZE]
		offset += PUBLIC_KEY_SIZE

		if wlen != offset {
			return nil, fmt.Errorf(
				"Wallet reported size of %d, actual size was %d",
				wlen, offset)
		}
		w = wal
	default:
		return nil, fmt.Errorf("Wallet Type does not exist, type: %d", wtype)
	}

	return w, nil
}
