// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package crypto

import (
	"crypto/ed25519"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"os"
)

// AddressSize represents the new WalletAddress size.
// PUBLIC_KEY_SIZE is now only used to represent the size of the public key, not the WalletAddress.
const (
	PUBLIC_KEY_SIZE  = ed25519.PublicKeySize
	PRIVATE_KEY_SIZE = ed25519.PrivateKeySize
	SIGNATURE_SIZE   = ed25519.SignatureSize
	HASH_SIZE        = sha256.Size
)

// WalletAddress is now a marshal binary containing: Type, Length, and PublicKey.
type WalletKey [PRIVATE_KEY_SIZE]byte
type Signature [SIGNATURE_SIZE]byte

func Compare(l WalletAddress, r WalletAddress) int {
	for i := 0; i < len(l.Address()); i++ {
		if l.Address()[i] < r.Address()[i] {
			return -1
		}
		if l.Address()[i] > r.Address()[i] {
			return 1
		}
	}
	return 0
}

func HashBytes(buf []byte) [HASH_SIZE]byte {
	return sha256.Sum256(buf)
}

func CompareHashes(l [HASH_SIZE]byte, r [HASH_SIZE]byte) int {
	for i := 0; i < HASH_SIZE; i++ {
		if l[i] == r[i] {
			continue
		}
		if l[i] < r[i] {
			return -1
		}
		if l[i] > r[i] {
			return 1
		}
	}
	return 0
}

func LoadKeyFromFile(filename string) ([]byte, error) {

	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("Couldn't open key file: %s", err.Error())
	}
	key := make([]byte, len(data)/2)
	_, err = hex.Decode(key, data)
	if err != nil {
		return nil, fmt.Errorf("Couldn't decode key's hex: %s", err.Error())
	}
	return key, nil
}

func LoadKeyFromHex(key string) ([]byte, error) {
	data, err := hex.DecodeString(key)
	if err != nil {
		return nil, fmt.Errorf("Couldn't decode key's hex: %s", err.Error())
	}
	return data, nil
}

func UnmarshalPublicKeyFromHex(hex string) (Wallet, error) {
	pubKey, err := LoadKeyFromHex(hex)
	if err != nil {
		return nil, fmt.Errorf("Couldn't load wallet from hex: %s", err.Error())
	}

	wallet, err := UnmarshalBinary(pubKey)
	if err != nil {
		return nil, fmt.Errorf("Couldn't load wallet from hex: %s", err.Error())
	}
	return wallet, nil
}
