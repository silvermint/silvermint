// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package crypto

import (
	"testing"
)

type Foo struct {
	Buf []byte
}

func (f Foo) Bytes() []byte {
	return f.Buf
}

func TestBytes(t *testing.T) {
	mybuf := []byte{1, 2, 3, 4, 5}
	foo := Foo{Buf: mybuf}

	if foo.Buf[0] != 1 {
		t.Fatalf("foo.Buf not initialized correctly")
	}
	mybuf[0] = 99
	if foo.Buf[0] != 99 {
		t.Fatalf("foo.Buf didn't change when mybuf did.")
	}

	newbuf := foo.Bytes()
	if newbuf[0] != foo.Buf[0] {
		t.Fatalf("foo.Buf.Bytes() returned corrupt data.")
	}
	newbuf[0] = 101
	if newbuf[0] != foo.Buf[0] {
		t.Fatalf("foo.Buf and newbuf aren't the same buffer.")
	}
}
