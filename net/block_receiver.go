// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package net

import (
	"fmt"
	"io"
	"net"
	"strings"
	"time"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/prometheus/client_golang/prometheus"
	prom "github.com/prometheus/client_golang/prometheus"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	LISTENER_TYPE = "tcp4"
)

var (
	blocksReceived = prom.NewCounterVec(prom.CounterOpts{
		Name: "node_net_blocks_received_total"},
		[]string{"id", "addr"},
	)

	transactionsReceived = prom.NewCounterVec(
		prom.CounterOpts{Name: "node_net_txns_received_total"},
		[]string{"id", "addr"},
	)

	invalidBlocks = prom.NewCounterVec(
		prom.CounterOpts{Name: "node_net_blocks_invalid_total"},
		[]string{"id", "addr"},
	)

	blockBytesReceived = prom.NewCounterVec(
		prom.CounterOpts{Name: "node_net_block_bytes_received_total"},
		[]string{"id", "addr"},
	)

	RecvThreads = prom.NewGauge(prom.GaugeOpts{
		Name: "node_net_receiver_threads",
	})
	BlockReceiverMicroVec = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "block_receiver_usec_total",
		Help: "Total microseconds spent in block receiver subprocesses.",
	},
		[]string{"function"},
	)
)

func init() {
	prom.MustRegister(blocksReceived)
	prom.MustRegister(invalidBlocks)
	prom.MustRegister(blockBytesReceived)
	prom.MustRegister(transactionsReceived)
	prom.MustRegister(RecvThreads)
	prom.MustRegister(BlockReceiverMicroVec)

	pflag.Uint32("block_receiver_workers", 1000,
		"Number of worker threads in the block receiver pool.")
	pflag.Uint32("block_receiver_channels", 100,
		"Number of channels in the block receiver pool.")
}

func makeBlockRecvWorkerFunc(casa *casanova.Casanova) func(chan *net.TCPConn) {
	return func(conns chan *net.TCPConn) {
		RecvThreads.Inc()
		for conn := range conns {
			ReceiveMessageFromConnection(casa, conn)
		}
		RecvThreads.Dec()
	}
}

func ReceiveBlocks(cas *casanova.Casanova, hostport string) {
	laddr, err := net.ResolveTCPAddr(LISTENER_TYPE, hostport)
	if err != nil {
		log.Fatalf("Invalid listen address for block receiver: %s", err.Error())
	}

	l, err := net.ListenTCP(LISTENER_TYPE, laddr)

	if err != nil {
		log.Fatalf("Can't bind BlockReceiver on TCP address: %s", laddr)
	}

	defer l.Close()
	log.Debugf("Block receiver listening on TCP address: %s", laddr)

	workers := viper.GetUint("block_receiver_workers")
	channels := viper.GetUint("block_receiver_channels")
	log.Debugf("Block receiver will have %d workers across %d channels",
		workers, channels)

	workerFunc := makeBlockRecvWorkerFunc(cas)

	// silvermint#140 TODO(leaf): This will block the main thread if the queue fills up, e.g. if we are
	// getting blocks faster than we can process them. A better approach would be to use
	// a synchronized queue, but that's a more complicated implementation.
	conns := make(chan *net.TCPConn, int(channels))
	for x := uint(0); x < workers; x++ {
		go workerFunc(conns)
	}

	for {
		conn, err := l.AcceptTCP()
		if err != nil {
			log.Errorf("Error accepting connection: %s", err.Error())
			continue
		}

		log.Debugf("Accepted connection from %v", conn.RemoteAddr())
		conns <- conn
	}
}

func ReceiveMessageFromConnection(cas *casanova.Casanova, conn *net.TCPConn) {
	// silvermint#141 TODO(leaf): We currently expect a single block per TCP connection, which
	// is a very simple implementation, but will be pretty slow in practice. We
	// really want this to handle lots and lots of blocks. We'll need some sort
	// of wire protocol for that.
	defer conn.Close()

	ipMatch := false
	validators := validator.GetGlobalRegistrations().Validators()
	for _, val := range validators {
		if val.Active {
			valIp := strings.Split(val.Hostport, ":")[0]
			incommingIp := strings.Split(conn.RemoteAddr().String(), ":")[0]
			if valIp == incommingIp {
				ipMatch = true
				break
			}
		}
	}

	if !ipMatch {
		log.Errorf("Connection refused from %s: Not a known validator.", conn.RemoteAddr().String())
		return
	}

	data, err := io.ReadAll(conn)
	if err != nil {
		log.Errorf("Can't read block from TCP: %s", err.Error())
		return
	}

	if len(data) == 0 {
		log.Errorf("Could't read 0 bytes from connection with %v",
			conn.RemoteAddr())
		return
	}

	newlineIndex := 0
	for i := 0; i < len(data); i++ {
		if data[i] == 0x0A { // 0x0A is new line ascii.
			newlineIndex = i
			break
		}
	}

	messageType := string(data[0:newlineIndex])
	message := data[newlineIndex+1:]

	log.Debugf("Received message of type %s, it's %d bytes long.",
		messageType, len(message))

	switch messageType {
	case "BLOCK":
		ReceiveBlock(cas, message)

	case "CATCHUP":
		ReceiveCatchup(cas, message)

	default:
		log.Errorf("Unknown message type. Data was: %v", message)
		return
	}
	log.Debugf("ReceiveMessageFromConnection: message from %v processed.",
		conn.RemoteAddr())
	return
}

func ReceiveBlock(cas *casanova.Casanova, block_data []byte) {
	nowMicro := time.Now().UnixMicro()
	blk := block.NewSilvermintBlock()
	err := blk.UnmarshalBinary(block_data)
	if err != nil {
		log.Errorf("Unable to unmarshal block data: %s", err.Error())
		return
	}

	if blk.Creator_ == cas.Self_ {
		log.Debugf("Ignoring block received from self.")
		return
	}

	idStr := fmt.Sprint(blk.Creator().Id)
	addr := blk.Creator().Hostport

	blockBytesReceived.WithLabelValues(idStr, addr).Add(float64(len(block_data)))
	transactionsReceived.WithLabelValues(idStr, addr).Add(float64(len(blk.Transactions())))
	blocksReceived.WithLabelValues(idStr, addr).Inc()

	if err != nil {
		invalidBlocks.WithLabelValues(idStr, addr).Inc()
		log.Errorf("Invalid block data: %s", err.Error())
		return
	}

	log.Debugf("ReceiveBlockFromConnection: received block %s, %d bytes, from validator %v.",
		blk.BlockId(), len(block_data), blk.Creator().Id)
	BlockReceiverMicroVec.WithLabelValues("ReceiveBlock").Add(float64(time.Now().UnixMicro() - nowMicro))
	log.Debugf("ReceiveBlock: %s, took: %f seconds",
		blk.BlockId(), float64(time.Now().UnixMicro()-nowMicro)*0.000001)

	cas.Add(blk)
}
