# Block Exchange Protocol

# Server Messages

The server may send the following messages, in ASCII, followed by two newline
characters.

  * OK
    * The client may commence to block sending.
  * BYE
    * The client should disconnect and try to re-establish a connection. The
      server may close its connection immediately.


# Client Messages

The client will initiate the protocol by sending a message stating that it is
ready to beging sending blocks, containing its identity, and additional
information the server may need to know:

```
READY <validator_public_key>\n
LAST_BLOCK: <block_id> <block_hash>\n\n
```

The server will respond with either `OK` or `BYE`.

If the server replies `OK`, then the client will beging sending blocks in the
following format:

```
[ total_msg_length | block_contents ]
```

After each block, the server should write, `OK`, allowing the client to send
another message, or `BYE` causing disconnect.

