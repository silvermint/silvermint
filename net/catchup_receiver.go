// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package net

import (
	"fmt"
	"time"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/log"

	prom "github.com/prometheus/client_golang/prometheus"
)

const CATCHUP_RECEIVER_HOSTPORT_DEFAULT string = "0.0.0.0:901"

var (
	CatchupRequestsReceived = prom.NewCounterVec(prom.CounterOpts{
		Name: "node_net_catchup_requests_received_total"},
		[]string{"id", "addr"},
	)
	CatchupRecvThreads = prom.NewGauge(prom.GaugeOpts{
		Name: "node_net_catchup_receiver_threads",
	})
)

func init() {
	prom.MustRegister(CatchupRequestsReceived)
}

func SendCatchupBlock(cas *casanova.Casanova, catchup block.CatchupRequest) {
	nowMicro := time.Now().UnixMicro()
	val := catchup.Creator() // who originated the request and will recv the block
	bcd := catchup.Missing() // the BlockId representing the missing block

	blk := cas.GetBlockParentFromValidator(bcd)
	if blk == nil {
		// TODO(leaf): Should we retry this more than once? Maybe we'll have it in
		// a second or something?
		log.Errorf("Can't get block %s for catchup with validator %d.", bcd, val.Id)
		log.Errorf("Catchup Request was: %v", catchup)
		return
	}

	// SendToValidator returns an error but it logs everything and there's nothing
	// for us to do if we tried, we tried.
	log.Debugf("SendCatchupBlock: Found catchup block %s, sending to validator %d",
		bcd, val.Id)

	data, err := blk.MarshalSignedBinary(cas.Wallet)
	if err != nil {
		log.Errorln(err.Error())
	}
	SendToValidator(val, blk, data)

	casanova.CatchupUsecTotal.WithLabelValues("SendCatchupBlock").Add(float64(time.Now().UnixMicro() - nowMicro))
}

func ReceiveCatchup(cas *casanova.Casanova, data []byte) {
	nowMicro := time.Now().UnixMicro()
	catchup := block.NewSilvermintCatchup()
	err := catchup.UnmarshalBinary(data)
	if err != nil {
		log.Errorf("Unable to unmarshal catchup data: %s", err.Error())
		return
	} else if catchup.Creator().Id == cas.Self_.Id {
		log.Debugf("We are validator %d, catchup creator is %d. Ignoring "+
			"request that appears to come from ourselves.", cas.Self_.Id, catchup.Creator().Id)
		return
	}

	idStr := fmt.Sprint(catchup.Creator().Id)
	addr := catchup.Creator().Hostport

	CatchupRequestsReceived.WithLabelValues(idStr, addr).Inc()
	log.Debugf("ReceiveCatchup: received %d byte catchup request from %v/%d for block %s",
		len(data), catchup.Creator().Hostport, catchup.Creator().Id, catchup.Missing())
	go SendCatchupBlock(cas, catchup)
	casanova.CatchupUsecTotal.WithLabelValues("ReceiveCatchup").Add(float64(time.Now().UnixMicro() - nowMicro))
}
