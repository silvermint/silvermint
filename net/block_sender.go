// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package net

import (
	"errors"
	"fmt"
	"net"
	"time"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	metricNetNumValidators = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "node_net_num_validators",
		},
	)

	metricNetBlocksSent = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "node_net_blocks_sent_total",
			Help: "Count of blocks sent on the network by validator.",
		},
		[]string{"id", "addr"},
	)

	metricNetBlockBytes = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "node_net_block_bytes_total",
		Help: "Count of bytes in the blocks we have sent.",
	})

	metricNetTxnsSent = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "node_net_txns_sent_total",
		Help: "Count of transactions included in blocks we've sent.",
	})
)

func init() {
	prometheus.MustRegister(metricNetBlockBytes)
	prometheus.MustRegister(metricNetBlocksSent)
	prometheus.MustRegister(metricNetNumValidators)
	prometheus.MustRegister(metricNetTxnsSent)

	pflag.Int("max_block_sender_retires", 3,
		"Number of retries to send block to validator on timeout error. If empty: 3.")
}

func SendBlocks(cas *casanova.Casanova) {
	// This is a string that contains a duration string like "1s".
	casanova_cycle_duration, err := time.ParseDuration(cas.CycleDuration())
	if err != nil {
		log.Fatalf("Can't parse Casanova cycle time: %s", err.Error())
	}

	// The last timestamp when a block was produced.
	last_block, err := cas.LastBlock()
	last_time := time.Now()
	if err != nil {
		log.Errorf("Error getting last block: %s", err.Error())
	} else if last_block != nil {
		last_time = last_block.Time()
	}

	// Sleep until the next block is due.
	next_time := last_time.Add(casanova_cycle_duration)
	if next_time.UnixMilli() < time.Now().UnixMilli() {
		next_time = time.Now().Add(casanova_cycle_duration)
	}

	for {
		time.Sleep(time.Until(next_time))

		b := cas.Get()

		// Increase the total transaction sent metric.
		txnCount := len(b.Transactions())
		metricNetTxnsSent.Add(float64(txnCount))
		metricNetBlockBytes.Add(float64(b.Length()))

		v := validator.GetGlobalRegistrations().Active()
		s := cas.Self_
		go SendToValidators(v, s, b, cas.Wallet)
		next_time = next_time.Add(casanova_cycle_duration)
	}
}

func SendToValidators(active map[validator.Validator]bool,
	self validator.Validator, b block.Block, wal crypto.Wallet) {

	if b == nil {
		log.Warningln("block was nil. Not sending.")
		return
	}

	metricNetNumValidators.Set(float64(len(active)))

	var block_bytes []byte
	var err error
	if wal != nil {
		block_bytes, err = b.MarshalSignedBinary(wal)
	} else {
		block_bytes, err = b.MarshalBinary()
	}
	if err != nil {
		e := fmt.Errorf("Can't construct block bytes: %s", err.Error())
		log.Errorln(e.Error())
	}
	for v := range active {
		if v == self {
			continue
		}
		go SendToValidator(v, b, block_bytes)
	}
}

func SendToValidator(v validator.Validator, b block.Block, block_bytes []byte) error {
	// Maybe resolve the TCP address.
	if v.RemoteAddr == nil {
		log.Debugf("SendToValidator: Resolving %s...", v.Hostport)
		raddr, err := net.ResolveTCPAddr("tcp4", v.Hostport)
		if err != nil {
			e := fmt.Errorf("SendToValidator: Resolve failed: %s.", err.Error())
			log.Errorln(e.Error())
			return e
		}
		v.RemoteAddr = raddr
	}

	// Maybe connect to the server.

	// silvermint#137 TODO(leaf): I don't think we're handling reconnects in any sort of
	// graceful way here. So, we should do that.

	log.Debugf("Validator.Connect: Connecting to %s", v.Hostport)
	var c net.Conn
	var err error
	// silvermint#138 TODO(leaf): In this error condition, we probably need to queue the
	// block somewhere so that we can retry a few times.
	for retries := viper.GetInt("max_block_sender_retires"); retries > 0; retries-- {

		c, err = net.DialTimeout("tcp4", v.RemoteAddr.String(), time.Second*5)

		var timeoutError net.Error
		if err == nil {
			//if err is nil we succeded and break loop
			break
		} else if !errors.As(err, &timeoutError) || !timeoutError.Timeout() {
			//if error is not net error, or if it is net error but not caused by timeout return error
			e := fmt.Errorf("Validator.Connect: Can't connect to remote validator: %s", err.Error())
			log.Errorln(e.Error())
			return e
		} else if retries == 1 {
			//if we have reached the end of the for loop, the maxium number of tries, we return error
			e := fmt.Errorf("Validator.Connect: Can't connect to remote validator: %s Maximum retries reached",
				v.RemoteAddr.String())
			log.Errorln(e.Error())
			return e
		}
		log.Warningf("Validator.Connect: Retry #: %d, Validator: %d", retries, v.Id)
	}

	defer c.Close()

	_, err = c.Write([]byte("BLOCK\n"))
	if err != nil {
		e := fmt.Errorf("Write failed: %s", err.Error())
		log.Errorln(e.Error())
		return e
	}

	bytes, err := c.Write(block_bytes)
	if err != nil {
		e := fmt.Errorf("Write failed: %s", err.Error())
		log.Errorln(e.Error())
		return e
	}

	if bytes != len(block_bytes) {
		e := fmt.Errorf("Short write: %d written, expected %d",
			bytes, block_bytes)
		log.Errorln(e.Error())
		return e
	}

	metricNetBlocksSent.WithLabelValues(fmt.Sprint(v.Id), v.Hostport).Inc()

	log.Debugf("SendToValidator: Wrote block %s to %d: %d bytes, timestamp: %s",
		b.BlockId(), v.Id, bytes, b.Time().UTC())
	return nil
}
