// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package net

import (
	"fmt"
	"time"

	"net"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	NetCatchupsSent = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "node_net_catchups_sent_total",
			Help: "Count of catchups sent on the network by validator.",
		},
		[]string{"id", "addr"},
	)
)

func init() {
	prometheus.MustRegister(NetCatchupsSent)
}

func SendCatchupFactory(cas *casanova.Casanova) casanova.CatchupRequestor {
	return func(missing ids.BlockId) {
		nowMicro := time.Now().UnixMicro()
		val := validator.GetGlobalRegistrations().Get(missing.ValId())
		if !val.IsActive() {
			log.Errorf("SendCatchup: can't find validator %d in Casanova Validator Set.", missing.ValId())
			return
		}

		// Maybe resolve the TCP address.
		if val.RemoteAddr == nil {
			log.Warningf("SendCatchup: Resolving %s...", val.Hostport)
			raddr, err := net.ResolveTCPAddr("tcp4", val.Hostport)
			if err != nil {
				log.Errorf("SendCatchup: Resolve failed: %s.", err.Error())
				return
			}
			val.RemoteAddr = raddr
		}

		// Maybe connect to the server.

		// silvermint#137 TODO(leaf): I don't think we're handling reconnects in any sort of
		// graceful way here. So, we should do that.

		log.Debugf("SendCatchup: Connecting to %s", val.Hostport)
		c, err := net.DialTCP("tcp4", nil, val.RemoteAddr)
		if err != nil {
			log.Errorf("SendCatchup: Can't connect to remote validator: %s", err.Error())
			return
		}

		defer c.Close()

		catchup := block.SilvermintCatchupRequest{
			Creator_: cas.Self_,
			Missing_: missing,
		}

		catchup_bytes, err := catchup.MarshalBinary()
		if err != nil {
			log.Errorf("SendCatchup: Can't construct catchup bytes: %s", err.Error())
			return
		}

		_, err = c.Write([]byte("CATCHUP\n"))
		if err != nil {
			log.Errorf("Write failed: %s", err.Error())
			return
		}

		bytes, err := c.Write(catchup_bytes)
		if err != nil {
			log.Errorf("Write failed: %s", err.Error())
			return
		}

		if bytes != len(catchup_bytes) {
			log.Errorf("SendCatchup: short write: %d written, expected %d",
				bytes, len(catchup_bytes))
			return
		}

		NetCatchupsSent.WithLabelValues(fmt.Sprint(val.Id), val.Hostport).Inc()
		log.Debugf("SendCatchup: Wrote catchup request to %d for block %s.", missing.ValId(), missing)
		casanova.CatchupUsecTotal.WithLabelValues("SendCatchupWorker").Add(float64(time.Now().UnixMicro() - nowMicro))
	}
}
