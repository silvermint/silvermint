// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package genesis

import (
	"io/ioutil"
	"path/filepath"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/log"
)

// Simply tries to unmarshal the file
func Load(silvermint_genesis_file string) *block.Block {
	if silvermint_genesis_file == "" {
		return nil
	}
	contents, err := ioutil.ReadFile(filepath.Join(silvermint_genesis_file))
	if err != nil {
		log.Warningf("Genesis Load failed: %s", err.Error())
	}

	log.Warningf("Attempting to unmarshal Genesis block file: %v",
		silvermint_genesis_file)
	return UnmarshalBinary(contents)
}

// Casanova#176 TODO(chuck): This should actually return a GenesisBlock type, assuming we
// come up with a distinct type for the Genesis block
func UnmarshalBinary(file []byte) *block.Block {
	// Stub: attempt to load a Genesis block / do some checks to verify that it's not corrupted.
	return nil
}
