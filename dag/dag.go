// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package dag

import (
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/ids"
)

type Dag interface {
	Blocks() *BlockCache
	Roots() block.Heights
	Tip(uint16) ids.BlockId

	Add(block.Block) bool
	Get(ids.BlockId) (block.Block, bool)
	Has(ids.BlockId) bool
	Size() uint64

	Pending() *pendingQueue

	InterpretDeltas(block.Block) bool

	CatchUpChannel() chan block.Block

	GetDot() string
}
