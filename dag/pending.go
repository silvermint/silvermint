// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package dag

import (
	"sync"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/validator"
	"golang.org/x/exp/slices"
)

// The pendingQueue is the structure used by the DAG and its catchup algorithm.
// The blockQueue contains blocks that we've received that couldn't be added to
// the DAG, but weren't rejected as invalid. E.g., we don't have all the parents.
// The parentsQueue maps the BlockHashes/Blocks in the blockQueue to each
// validator from which we are missing a parent for that Block.
//
// The pendingQueue is not exactly thread safe and should be synchronized by the
// DAG itself, as needed.
type pendingQueue struct {
	// The DAG that this pendingQueue serves
	dag *SilvermintDag
	// The map from a pending BlockId to the pending Block itself
	Blocks map[ids.BlockId]block.Block
	// The map from a pending Block to the list of ValidatorID/Height pairs that are missing
	BlockParents map[block.Block][]ids.BlockId
	// The map from a missing ValidatorID/Height pair to the pending Blocks that require it
	ParentBlocks map[ids.BlockId][]block.Block
	// The queue's mutex
	mu sync.RWMutex
}

func newPendingQueue(dag *SilvermintDag) *pendingQueue {
	return &pendingQueue{
		dag:          dag,
		Blocks:       map[ids.BlockId]block.Block{},
		BlockParents: map[block.Block][]ids.BlockId{},
		ParentBlocks: map[ids.BlockId][]block.Block{},
	}
}

func (pq *pendingQueue) Len() int {
	pq.mu.RLock()
	defer pq.mu.RUnlock()
	return pq.lenParentsInternal() + pq.lenBlocksInternal()
}

func (pq *pendingQueue) LenParents() int {
	pq.mu.RLock()
	defer pq.mu.RUnlock()
	return pq.lenParentsInternal()
}

func (pq *pendingQueue) lenParentsInternal() int {
	return len(pq.ParentBlocks)
}

func (pq *pendingQueue) LenBlocks() int {
	pq.mu.RLock()
	defer pq.mu.RUnlock()
	return pq.lenBlocksInternal()
}

func (pq *pendingQueue) lenBlocksInternal() int {
	return len(pq.BlockParents)
}

// Checks to see if the provided Block has any association in the pendingQueue.
// This read-only operation is a precursor to a more onerous write-locking that
// would be needed to update the queue on addition of a Block to the DAG.
func (pq *pendingQueue) Has(blk block.Block) bool {
	pq.mu.RLock()
	defer pq.mu.RUnlock()

	if _, ok := pq.Blocks[blk.BlockId()]; ok {
		return true
	}
	if _, ok := pq.BlockParents[blk]; ok {
		return true
	}
	if _, ok := pq.ParentBlocks[blk.BlockId()]; ok {
		return true
	}
	return false
}

func (pq *pendingQueue) HasBlockId(bid ids.BlockId) bool {
	pq.mu.RLock()
	defer pq.mu.RUnlock()

	return pq.HasBlockIdInternal(bid)
}

func (pq *pendingQueue) HasBlockIdInternal(bid ids.BlockId) bool {
	for pbid := range pq.Blocks {
		if pbid == bid {
			return true
		}
	}

	return false
}

// Adds a Block blk to the pendingQueue, along with the associated missing
// parents for that Block given by the BlockIds bids,
func (pq *pendingQueue) PendingBlock(blk block.Block, bids []ids.BlockId) {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	pq.Blocks[blk.BlockId()] = blk
	pq.BlockParents[blk] = bids
	for _, bid := range bids {
		if _, exists := pq.ParentBlocks[bid]; !exists {
			pq.ParentBlocks[bid] = []block.Block{blk}
		} else {
			pq.ParentBlocks[bid] = append(pq.ParentBlocks[bid], blk)
		}
	}
}

// Deletes the provided BlockId as a missing parent. If this was the last parent
// any pending Block was waiting for, return a list of all such Blocks.
func (pq *pendingQueue) deleteParentInternal(bid ids.BlockId) []block.Block {
	addableBlocks := []block.Block{}

	// Iterate over each block that had this BlockId as a missing parent
	for _, pendingBlk := range pq.ParentBlocks[bid] {
		// Remove this parent from their list of missing parents
		for i, missingParent := range pq.BlockParents[pendingBlk] {
			if bid == missingParent {
				pq.BlockParents[pendingBlk] = slices.Delete(pq.BlockParents[pendingBlk], i, i+1)
				break
			}
		}
		// If this was the last missing parent for that block, mark the block
		// as addable
		if len(pq.BlockParents[pendingBlk]) == 0 {
			addableBlocks = append(addableBlocks, pendingBlk)
			delete(pq.BlockParents, pendingBlk)
		}
	}
	delete(pq.ParentBlocks, bid)

	return addableBlocks
}

// Deletes the provided Block from the pending queue. Ensures there are no
// parents for that Block still listed as missing. If this was the last Block
// that any other pending Block was waiting for, return a list of all such
// Blocks.
func (pq *pendingQueue) DeleteBlock(blk block.Block) []block.Block {
	pq.mu.Lock()
	defer pq.mu.Unlock()

	addableBlocks := []block.Block{}

	// Delete this as a missing block.
	bid := blk.BlockId()
	delete(pq.Blocks, bid)

	// Since we're deleting this block from the queue, it should've been added
	// to the DAG. Ensure we don't have any pending parents listed for this
	// block still. (This should be redundant)
	if _, ok := pq.BlockParents[blk]; ok {
		for _, bid := range pq.BlockParents[blk] {
			addableBlocks = append(addableBlocks, pq.deleteParentInternal(bid)...)
		}
	}
	delete(pq.BlockParents, blk)

	// Delete this block's BlockId as a missing parent for anyone
	addableBlocks = append(addableBlocks, pq.deleteParentInternal(bid)...)

	return addableBlocks
}

// Returns an array of counts of pending Blocks indexed on block Creator ID.
func (pq *pendingQueue) PendingByCreator() [validator.N_MAX_VALIDATORS]int {
	pq.mu.RLock()
	defer pq.mu.RUnlock()

	missing := [validator.N_MAX_VALIDATORS]int{}

	for _, pending := range pq.Blocks {
		missing[pending.Creator().Id]++
	}
	return missing
}

// Read ranges over the pq.ParentBlocks map.
// NB(chuck): The provided function MUST NOT mutate the map.
func (pq *pendingQueue) ParentRange(f func(ids.BlockId, []block.Block) bool) {
	pq.mu.RLock()
	defer pq.mu.RUnlock()

	for bid, blks := range pq.ParentBlocks {
		if !f(bid, blks) {
			return
		}
	}
}
