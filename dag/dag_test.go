// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package dag

import (
	"testing"

	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
)

type BlockHashToBlock map[block.BlockHash]block.Block
type BlockToBlockHash map[block.Block]block.BlockHash

func countChildren(parent block.Block, dag *SilvermintDag, regs []validator.Validator) int {
	l := 0
	for _, val := range regs {
		if val.IsActive() {
			dag.blocks.Range(val.Id, func(blk block.Block) bool {
				if blk.Hash() != parent.Hash() {
					genesisAdded := false
					for _, pval := range regs {
						if pval.IsActive() {
							if blk.Parents().BlockId(pval.Id).IsGenesis() && parent.BlockId().IsGenesis() {
								if !genesisAdded {
									genesisAdded = true
									l++
								}
							} else if blk.Parents().BlockId(pval.Id) == parent.BlockId() {
								l++
							}
						}
					}
				}
				return true
			})
		}
	}
	return l
}

func TestHasAllGenesis(t *testing.T) {
	viper.Set("save_dir", "/tmp")
	va := validator.Validator{Id: 0}
	regs := [validator.N_MAX_VALIDATORS]validator.Validator{}
	regs[va.Id] = va
	genesisBlock := block.Genesis(regs)

	roots := [validator.N_MAX_VALIDATORS]block.Block{}
	for i := range roots {
		roots[i] = genesisBlock
	}
	dag := Build(genesisBlock, roots, nil)

	blk := block.NewSilvermintBlock()
	blk.Deltas_ = block.NewHeightDeltaArray()

	t.Run("Missing parents empty", func(t *testing.T) {
		ok := dag.InterpretDeltas(blk)
		if !ok {
			t.Errorf("Block parent is genesis, but has missing parents")
		}
	})

	t.Run("Roots has Genesis", func(t *testing.T) {
		roots := dag.Roots()
		found := false
		for _, v := range roots {
			if genesisBlock.Height() == v {
				found = true
			}

			if !found {
				t.Errorf("Genesis missing from roots with hash %s\n", genesisBlock.BlockId())
			}
		}
	})

	t.Run("Genesis is a tip", func(t *testing.T) {
		tip := dag.blocks.Tip(va.Id)
		if tip.Height() != genesisBlock.Height() {
			t.Error("Got false, expected true")
		}
	})

	t.Run("one block", func(t *testing.T) {
		if l := dag.Size(); l != 1 {
			t.Errorf("Got %d, expected 1", l)
		}
	})

	t.Run("Genesis among blocks", func(t *testing.T) {
		if !dag.Has(genesisBlock.BlockId()) {
			t.Error("Got false, expected true")
		}
	})

	t.Run("Genesis has no children", func(t *testing.T) {
		if !dag.Has(genesisBlock.BlockId()) {
			t.Errorf("Got false, expected true")
		} else {
			l := countChildren(genesisBlock, dag, regs[:])
			if l != 0 {
				t.Errorf("Got %d, expected 0", l)
			}
		}
	})
}

func TestHasAllFirstBlock(t *testing.T) {
	viper.Set("save_dir", "/tmp")
	va := validator.Validator{Id: 0, Hostport: "node0.silvermint.net:900"}
	regs := [validator.N_MAX_VALIDATORS]validator.Validator{}
	regs[va.Id] = va
	genesisBlock := block.Genesis(regs)
	h := genesisBlock.BlockId()
	roots := [validator.N_MAX_VALIDATORS]block.Block{}
	for i := range roots {
		roots[i] = genesisBlock
	}
	dag := Build(genesisBlock, roots, nil)

	t.Run("dag has Genesis", func(t *testing.T) {
		_, ok := dag.Get(h)
		if !ok {
			t.Error("Got false, expected true")
		}
	})
	t.Run("Genesis has no children", func(t *testing.T) {
		if !dag.Has(h) {
			t.Errorf("Got false, expected true")
		} else {
			l := countChildren(genesisBlock, dag, regs[:])
			if l != 0 {
				t.Errorf("Got %d, expected 0", l)
			}
		}
	})
}

func TestAddFirstBlock(t *testing.T) {
	viper.Set("save_dir", "/tmp")
	viper.Set("block_cache_size", 100)
	va := validator.Validator{Id: 0, Hostport: "node0.silvermint.net:900", Active: true}
	regs := [validator.N_MAX_VALIDATORS]validator.Validator{}
	regs[va.Id] = va
	genesisBlock := block.Genesis(regs)
	hg := genesisBlock.BlockId()
	roots := [validator.N_MAX_VALIDATORS]block.Block{}
	for i := range roots {
		roots[i] = genesisBlock
	}
	dag := Build(genesisBlock, roots, nil)
	ba1Parents := block.Heights{}
	ba1Parents[va.Id] = hg.Height()
	ba1 := block.FromParents(
		va,
		ba1Parents,
		[]transaction.Transaction{},
		1, genesisBlock.Registrations())
	ha1 := ba1.BlockId()
	dag.Add(ba1)

	t.Run("dag has Genesis and ba1", func(t *testing.T) {
		_, gok := dag.Get(hg)
		_, ba1ok := dag.Get(ha1)
		if !gok {
			t.Error("Got false, expected true")
		}
		if !ba1ok {
			t.Error("Got false, expected true")
		}
	})
	t.Run("Roots has Genesis", func(t *testing.T) {
		roots := dag.Roots()
		found := false
		for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
			if hg == roots.BlockId(i) {
				found = true
			}
		}
		if !found {
			t.Error("Got false, expected true")
		}
	})
	t.Run("ba1 is a tip", func(t *testing.T) {
		tip := dag.blocks.Tip(va.Id)
		if tip.BlockId() != ha1 {
			t.Errorf("Got %x, expected %x", tip, ha1)
		}
	})
	t.Run("Genesis has one child", func(t *testing.T) {
		if !dag.Has(hg) {
			t.Errorf("Got false, expected true")
		} else {
			l := countChildren(genesisBlock, dag, regs[:])
			if l != 1 {
				t.Errorf("Got %d, expected 1", l)
			}
		}
	})
}

func TestAddChildWithoutParents(t *testing.T) {
	viper.Set("save_dir", "/tmp")
	viper.Set("block_cache_size", 100)
	va := validator.Validator{Id: 0, Hostport: "www.r_u_for_real.com:900", Active: true}
	vb := validator.Validator{Id: 1, Hostport: "somewhere.awesome:900", Active: true}

	regs := [validator.N_MAX_VALIDATORS]validator.Validator{}
	regs[va.Id] = va
	regs[vb.Id] = vb
	genesisBlock := block.Genesis(regs)
	hg := genesisBlock.BlockId()
	roots := [validator.N_MAX_VALIDATORS]block.Block{}
	for i := range roots {
		roots[i] = genesisBlock
	}
	dag := Build(genesisBlock, roots, nil)
	ba1Parents := block.Heights{}
	ba1Parents[va.Id] = hg.Height()
	bb1Parents := block.Heights{}
	bb1Parents[vb.Id] = hg.Height()
	ba1 := block.FromParents(va, ba1Parents, []transaction.Transaction{}, 1, genesisBlock.Registrations())
	bb1 := block.FromParents(vb, bb1Parents, []transaction.Transaction{}, 1, genesisBlock.Registrations())
	ha1 := ba1.BlockId()
	hb1 := bb1.BlockId()
	ba2Parents := block.Heights{}
	ba2Parents[va.Id] = ha1.Height()
	ba2Parents[vb.Id] = hb1.Height()
	ba2 := block.FromParents(va, ba2Parents, []transaction.Transaction{}, 2, genesisBlock.Registrations())
	ba2.(*block.SilvermintBlock).Deltas_.(*block.SilvermintHeightDeltaArray)[0] = block.HeightDelta{
		Delta:  1,
		Length: 1,
	}

	t.Run("adding should return false", func(t *testing.T) {
		if dag.Add(ba2) {
			t.Error("Got true, expected false")
		}
	})
	t.Run("Genesis has no children", func(t *testing.T) {
		if !dag.Has(hg) {
			t.Errorf("Got false, expected true")
		} else {
			l := countChildren(genesisBlock, dag, regs[:])
			if l != 0 {
				t.Errorf("Got %d, expected 0", l)
			}
		}
	})
}

func TestAddChildWithParents(t *testing.T) {
	viper.Set("save_dir", "/tmp")
	viper.Set("block_cache_size", 100)
	va := validator.Validator{Id: 0, Hostport: "www.ibm.com:900", Active: true}
	vb := validator.Validator{Id: 1, Hostport: "amazong.io:900", Active: true}

	regs := [validator.N_MAX_VALIDATORS]validator.Validator{}
	regs[va.Id] = va
	regs[vb.Id] = vb
	genesisBlock := block.Genesis(regs)
	hg := genesisBlock.BlockId()
	//fmt.Printf("TestAddChildWithParents: genesis := %s\n", hg)

	roots := [validator.N_MAX_VALIDATORS]block.Block{}
	for i := range roots {
		roots[i] = genesisBlock
	}
	dag := Build(genesisBlock, roots, nil)

	ba1Parents := block.Heights{}
	ba1Parents[va.Id] = hg.Height()
	ba1 := block.FromParents(va, ba1Parents,
		[]transaction.Transaction{}, 1, genesisBlock.Registrations())
	// Validator 0 has only seen ba1
	ba1.(*block.SilvermintBlock).Deltas_.(*block.SilvermintHeightDeltaArray)[0] = block.HeightDelta{
		Delta:  0,
		Length: 1,
	}
	bb1Parents := block.Heights{}
	bb1Parents[vb.Id] = hg.Height()
	bb1 := block.FromParents(vb, bb1Parents,
		[]transaction.Transaction{}, 1, genesisBlock.Registrations())
	// Validator 1 has only seen bb1
	bb1.(*block.SilvermintBlock).Deltas_.(*block.SilvermintHeightDeltaArray)[1] = block.HeightDelta{
		Delta:  0,
		Length: 1,
	}
	ha1 := ba1.BlockId()
	hb1 := bb1.BlockId()

	ba2Parents := block.Heights{}
	ba2Parents[va.Id] = ha1.Height()
	ba2Parents[vb.Id] = hb1.Height()
	ba2 := block.FromParents(va, ba2Parents,
		[]transaction.Transaction{}, 2, genesisBlock.Registrations())
	// Validator 0 has now seen ba1 and bb1
	ba2.(*block.SilvermintBlock).Deltas_.(*block.SilvermintHeightDeltaArray)[0] = block.HeightDelta{
		Delta:  1,
		Length: 1,
	}
	ba2.(*block.SilvermintBlock).Deltas_.(*block.SilvermintHeightDeltaArray)[1] = block.HeightDelta{
		Delta:  1,
		Length: 1,
	}
	ha2 := ba2.BlockId()

	dag.Add(ba1)
	dag.Add(bb1)
	success := dag.Add(ba2)

	t.Run("adding should return true", func(t *testing.T) {
		if !success {
			t.Error("Got false, expected true")
		}
	})
	t.Run("Roots has Genesis", func(t *testing.T) {
		roots := dag.Roots()
		found := false
		for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
			if hg == roots.BlockId(i) {
				found = true
			}
		}
		if !found {
			t.Error("Got false, expected true")
		}
	})
	t.Run("two tips", func(t *testing.T) {
		l := 0
		for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
			tblk := dag.blocks.Tip(i)
			if !block.IsGenesis(tblk) {
				l++
			}
		}
		if l != 2 {
			dag.PrintDag()
			dag.PrintTips()
			t.Errorf("Got %d, expected 2", l)
		}
	})
	t.Run("bb1 is a tip", func(t *testing.T) {
		tip := dag.blocks.Tip(vb.Id)
		if tip.BlockId() != hb1 {
			t.Errorf("Got %x, expected %x", tip, hb1)
		}
	})
	t.Run("ba2 is a tip", func(t *testing.T) {
		tip := dag.blocks.Tip(va.Id)
		if tip.BlockId() != ha2 {
			t.Errorf("Got %x, expected %x", tip, ha2)
		}
	})
	t.Run("Genesis has two children", func(t *testing.T) {
		if !dag.Has(hg) {
			t.Errorf("Got false, expected true")
		} else {
			l := countChildren(genesisBlock, dag, regs[:])
			if l != 2 {
				t.Errorf("Got %d, expected 2", l)
			}
		}
	})
	t.Run("ba1 has one child", func(t *testing.T) {
		if !dag.Has(hg) {
			t.Errorf("Got false, expected true")
		} else {
			l := countChildren(ba1, dag, regs[:])
			if l != 1 {
				t.Errorf("Got %d, expected 1", l)
			}
		}
	})
	t.Run("bb1 has one child", func(t *testing.T) {
		if !dag.Has(hg) {
			t.Errorf("Got false, expected true")
		} else {
			l := countChildren(bb1, dag, regs[:])
			if l != 1 {
				t.Errorf("Got %d, expected 1", l)
			}
		}
	})
	t.Run("ba2 has no children", func(t *testing.T) {
		if !dag.Has(hg) {
			t.Errorf("Got false, expected true")
		} else {
			l := countChildren(ba2, dag, regs[:])
			if l != 0 {
				t.Errorf("Got %d, expected 0", l)
			}
		}
	})
}
