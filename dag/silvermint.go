// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package dag

import (
	"fmt"
	"math"
	"strings"
	"time"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/utils"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	dagBlocks = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_dag_blocks_total",
		Help: "Number of blocks added to the DAG.",
	})
	dagValidatorBlocks = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "casanova_validator_dag_blocks_total",
			Help: "Number of blocks added to the DAG from each validator.",
		},
		[]string{"id"},
	)
	DagBlocksRejected = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_dag_blocks_rejected_total",
		Help: "Number of blocks rejected.",
	})
	dagDeletedBlocks = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_dag_blocks_deleted",
		Help: "Number of blocks deleted.",
	})
	DagMicroVec = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "dag_usec_total",
		Help: "Total microseconds spent in DAG subprocesses.",
	},
		[]string{"function"},
	)
	TotalBytesWritten = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "consensus_bytes_total",
		Help: "Total bytes written to disk for various objects.",
	},
		[]string{"object"},
	)
)

func init() {
	prometheus.MustRegister(dagBlocks)
	prometheus.MustRegister(DagBlocksRejected)
	prometheus.MustRegister(dagDeletedBlocks)
	prometheus.MustRegister(dagValidatorBlocks)
	prometheus.MustRegister(DagMicroVec)
	prometheus.MustRegister(TotalBytesWritten)

	pflag.Uint64("catchup_listener_buffer", 100, "Buffer size for the catchup listener channel.")
	pflag.Uint64("block_cache_size", 20, "Number of blocks to keep from each validator.")
}

type SilvermintDag struct {
	roots   block.Heights
	blocks  *BlockCache
	pending *pendingQueue
	catchup chan block.Block
}

func (dag *SilvermintDag) Blocks() *BlockCache                     { return dag.blocks }
func (dag *SilvermintDag) Roots() block.Heights                    { return dag.roots }
func (dag *SilvermintDag) Get(bid ids.BlockId) (block.Block, bool) { return dag.blocks.Get(bid) }
func (dag *SilvermintDag) Has(bid ids.BlockId) bool                { return dag.blocks.Seen(bid) }
func (dag *SilvermintDag) Pending() *pendingQueue                  { return dag.pending }
func (dag *SilvermintDag) CatchUpChannel() chan block.Block        { return dag.catchup }

func Build(genesis block.Block, roots [validator.N_MAX_VALIDATORS]block.Block, bcache *BlockCache) *SilvermintDag {
	dag := new(SilvermintDag)

	dag.roots = block.Heights{}
	var err error
	if bcache == nil {
		bcache, err = NewBlockCache(genesis)
		if err != nil {
			log.Errorf("unable to create block cache: %s", err.Error())
		}
	}
	dag.blocks = bcache
	for i, root := range roots {
		if root != nil {
			// Add the block's height to the roots list
			dag.roots[i] = roots[i].Height()
		}
	}

	dag.pending = newPendingQueue(dag)
	dag.catchup = newCatchupChannel()

	return dag
}

func Load(genesis block.Block,
	roots [validator.N_MAX_VALIDATORS]block.Block,
	blks [validator.N_MAX_VALIDATORS][]block.Block,
	bcache *BlockCache,
) (*SilvermintDag, error) {
	// Add each block to the blocks map
	// NB(chuck): The blocks are already sorted by ascending height and should
	// be added in that order to correctly set the tips / cache.
	addedList := []string{}
	for vid := uint16(0); vid < validator.N_MAX_VALIDATORS; vid++ {
		for i := 0; i < len(blks[vid]); i++ {
			bid := blks[vid][i].BlockId()
			if block.IsGenesis(blks[vid][i]) {
				bid = block.GenesisId(vid)
			}
			bcache.Set(bid, blks[vid][i])
			addedList = append(addedList, blks[vid][i].BlockId().String())
		}
	}

	// Create a new DAG using the provided roots and populated cache
	d := Build(genesis, roots, bcache)

	log.Debugf("Loaded blocks %s to DAG", strings.Join(addedList, ", "))

	return d, nil
}

// Adds a block to this dag, mutating it.
// Adds each transaction to the given map from hashes to transactions
// Returns true if the block is no longer pending (added or duplicate), false otherwise.
func (dag *SilvermintDag) Add(blk block.Block) bool {
	nowMicro := time.Now().UnixMicro()

	// If the block is already in the DAG, then we have no work to do.
	if dag.Has(blk.BlockId()) {
		go func() { dag.CatchUpChannel() <- blk }()
		return true
	}

	// Ensure all parents have been interpreted from HeightDeltas (this should
	// be redundant)
	if !dag.InterpretDeltas(blk) {
		log.Debugf("DAG rejected block: failed to interpret parents for %s", blk.BlockId())
		DagBlocksRejected.Inc()
		return false
	}

	pheights := make([]string, 0)
	for val := range blk.Registrations() {
		if val.IsActive() {
			pheights = append(pheights, blk.Parents().BlockId(val.Id).String())
		}
	}
	log.Debugf("Dag accepted block: parents=%s", strings.Join(pheights, ", "))

	// Block accepted into the DAG. We can't fail from here on.
	dagBlocks.Inc()

	// Add the block to the maps.
	dag.blocks.Set(blk.BlockId(), blk)
	log.Debugf("Block %s added to blocks map", blk.BlockId())
	log.Debugf("Dag accepted block: %s", blk.BlockId())

	// Remove the block from any pending status
	go func() { dag.CatchUpChannel() <- blk }()

	dagValidatorBlocks.WithLabelValues(fmt.Sprintf("%d", blk.Creator().Id)).Inc()

	log.Debugf("Dag has %d blocks in it, %d pending blocks, %d missing parents.",
		dag.blocks.Size(), dag.Pending().LenBlocks(), dag.Pending().LenParents())
	DagMicroVec.WithLabelValues("Add").Add(float64(time.Now().UnixMicro() - nowMicro))
	log.Debugf("DAG: %s, Add: %f seconds",
		blk.BlockId(), float64(time.Now().UnixMicro()-nowMicro)*0.000001)

	return true
}

// Interprets and assigns block Parents from block's HeightDeltas, mutating it.
// Returns true if successful or false if there were missing parents.
func (dag *SilvermintDag) InterpretDeltas(blk block.Block) bool {
	nowMicro := time.Now().UnixMicro()

	// Load the previous parent's parent heights and add this block's deltas
	// to them to determine this block's parent heights
	prevBlkId := blk.BlockId().Prev()
	prevBlk, pbok := dag.blocks.Get(prevBlkId)
	if !pbok {
		// We need at least all blocks between the tip+1 for this validator and
		// the (known missing) direct parent
		tip := dag.blocks.Tip(blk.Creator().Id)
		// The tip might've updated since we first checked pbok, so double
		// check that the tip height is still less than the parent block height
		if tip.Height() < blk.Height() {
			missing, err := prevBlkId.IntervalBlockId(tip.BlockId().Next())
			if err != nil {
				log.Errorf("Unable to get interval of missing block IDs: %s",
					err.Error())
				dag.Pending().PendingBlock(blk, []ids.BlockId{prevBlkId})
			} else {
				dag.Pending().PendingBlock(blk, missing)
			}
		} else {
			dag.Pending().PendingBlock(blk, []ids.BlockId{prevBlkId})
		}
		return false
	}
	prevHeights := prevBlk.Parents()
	parentHeights := prevHeights.Add(blk.Deltas())
	blk.SetParents(parentHeights)

	// Check if all parents are in the DAG
	missingParents := []ids.BlockId{}
	for val := range blk.Registrations() {
		pbid := parentHeights.BlockId(val.Id)
		if !dag.Has(pbid) {
			// We need at least all blocks between the tip+1 for this validator
			// and the (known missing) parent
			tip := dag.blocks.Tip(val.Id)
			missing, err := pbid.IntervalBlockId(tip.BlockId().Next())
			if err != nil {
				log.Errorf("Error getting interval of missing block IDs: %s",
					err.Error())
				missingParents = append(missingParents, pbid)
			} else {
				missingParents = append(missingParents, missing...)
			}
		}
	}

	// If the block was missing any parents, add it to the pendingQueue.
	if len(missingParents) != 0 {
		dag.Pending().PendingBlock(blk, missingParents)
		// Now that the block is in the pending queue, double check if any of
		// the parents we observed as missing have since been added.
		// If any such parents are found, notify the pending queue that they're
		// no longer missing.
		for _, missingParent := range missingParents {
			if dag.Has(missingParent) {
				missingBlk, _ := dag.Get(missingParent)
				dag.CatchUpChannel() <- missingBlk
			}
		}

		missingHeights := []string{}
		for _, missingParent := range missingParents {
			missingHeights = append(missingHeights, missingParent.String())
		}

		log.Warningf("Queued %s as pending with %d missing parents:[%s]\n",
			blk.BlockId(), len(missingParents), missingHeights)
	}

	DagMicroVec.WithLabelValues("InterpretDeltas").Add(float64(time.Now().UnixMicro() - nowMicro))
	log.Debugf("DAG: %s, InterpretDeltas: %f seconds",
		blk.BlockId(), float64(time.Now().UnixMicro()-nowMicro)*0.000001)

	return len(missingParents) == 0
}

func newCatchupChannel() chan block.Block {
	sz := viper.GetUint64("catchup_listener_buffer")
	c := make(chan block.Block, sz)
	return c
}

func (dag *SilvermintDag) GetDot() string {
	out := "strict digraph {\n"

	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		curr := dag.blocks.Tip(i)

		id := curr.Creator().Id

		for {
			// Print the digraph edges.
			phts := curr.Parents()
			for i, pht := range phts {
				if pht == 0 {
					continue
				}
				out += fmt.Sprintf("x%s -> x%s", curr.BlockId(), phts.BlockId(uint16(i)))
			}

			nextBcd := phts.BlockId(id)
			if nextBcd.Height() == 0 {
				continue
			}

			next, ok := dag.Get(nextBcd)
			if !ok {
				log.Warningf("Couldn't get block for non-empty hash: %s", nextBcd)
				continue
			}

			// Keep going only if it's still an in-memory block.
			curr, ok = next.(*block.SilvermintBlock)
			if !ok {
				break
			}
			log.Debugf("Block %s is SilvermintBlock, traversing.", nextBcd)
		}
	}

	out += "}\n"
	return out
}

func (dag *SilvermintDag) Tip(vid uint16) ids.BlockId {
	return dag.blocks.Tip(vid).BlockId()
}

func (dag *SilvermintDag) Size() uint64 {
	// Initialize count to one for the Genesis block
	n := uint64(1)
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		n += dag.blocks.Tip(i).Height()
	}
	return n
}

// TODO(chuck): This function isn't being used currently, but it can be used
// for getting the blocks needed for a snapshot to send to another validator.
// Determines the minimum, non-deletable height for each validator.
// The parents of tips can be built on still, so they aren't deletable.
func (dag *SilvermintDag) LeastUndeletableHeights(
	regs map[validator.Validator]bool,
) block.Heights {
	// Initialize
	minHeights := block.Heights{}
	for val := range regs {
		minHeights[val.Id] = math.MaxUint64
	}
	for val := range regs {
		tip, _ := dag.Get(dag.Tip(val.Id))
		// If Genesis is any of the tips we can't delete anything, return 0s
		if block.IsGenesis(tip) {
			return [validator.N_MAX_VALIDATORS]uint64{}
		}
		ps := tip.Parents()
		// Get the height of each parent and save it if it's smaller than the
		// current minHeight
		for pVal := range regs {
			pblk, _ := dag.Get(ps.BlockId(pVal.Id))
			minHeights[pVal.Id] = utils.Min(pblk.Height(), minHeights[pVal.Id])
		}
	}
	return minHeights
}
