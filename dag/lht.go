// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package dag

import (
	"fmt"

	"gitlab.com/silvermint/silvermint/transaction"
)

type LinearHashTable struct {
	table   []transaction.Transaction
	keybits uint16
	size    uint32
}

func NewLHT(keybits uint16) (*LinearHashTable, error) {
	lht := new(LinearHashTable)
	lht.size = (1 << keybits)
	lht.keybits = keybits
	lht.table = make([]transaction.Transaction, lht.size)
	for i := range lht.table {
		lht.table[i] = nil
	}
	return lht, nil
}

func (tbl *LinearHashTable) Reset() {
	for i := range tbl.table {
		tbl.table[i] = nil
	}
}

func (tbl *LinearHashTable) Key(value uint64) uint64 {
	return uint64(value >> (64 - tbl.keybits))
}

func (tbl *LinearHashTable) Get(key uint64) transaction.Transaction {
	if key >= uint64(tbl.size) {
		return nil // this is not a valid value really.
	}
	return tbl.table[key]
}

func (tbl *LinearHashTable) GetTxn(value uint64, hash transaction.TransactionHash,
) (transaction.Transaction, bool) {
	sz := uint64(tbl.size)
	key := tbl.Key(value)
	i := (key + sz - 1) % sz
	for tbl.table[key] != nil && tbl.table[key].Hash() != hash && key != i {
		key = (key + 1) % sz
	}
	if tbl.table[key] != nil && tbl.table[key].Hash() == hash {
		return tbl.table[key], true
	}
	return nil, false
}

// Inserts the value into the table. Returns a bool indicating whether
// there was another (duplicate) element with the same value. If
// successfully inserted, error is nil.
func (tbl *LinearHashTable) Insert(txn transaction.Transaction) (bool, error) {
	sz := uint64(tbl.size)
	key := tbl.Key(txn.Source().Uint64())
	i := (key + sz - 1) % sz
	for tbl.table[key] != nil &&
		transaction.Compare(tbl.table[key].ConflictDomain(), txn.ConflictDomain()) != 0 &&
		key != i {
		key = (key + 1) % sz
	}

	if tbl.table[key] == nil {
		tbl.table[key] = txn
		return false, nil
	}

	p := txn
	dup := false
	for tbl.table[key] != nil {
		q := tbl.table[key]
		if transaction.Compare(q.ConflictDomain(), txn.ConflictDomain()) == 0 {
			// TODO(chuck): Is this check right? What if the exact same
			// transaction got added to multiple blocks?
			if q.Hash() != txn.Hash() {
				dup = true
			}
		}
		if i == key {
			return dup, fmt.Errorf("This isn't the Hilbert hotel.")
		}
		tbl.table[key] = p
		p = q
		key = (key + 1) % sz
	}

	tbl.table[key] = p
	return dup, nil
}

// Gets all dups of txn
func (tbl *LinearHashTable) GetDups(txn transaction.Transaction,
) ([]transaction.Transaction, error) {
	sz := uint64(tbl.size)
	key := tbl.Key(txn.Source().Uint64())
	i := (key + sz - 1) % sz
	for tbl.table[key] != nil &&
		transaction.Compare(tbl.table[key].ConflictDomain(), txn.ConflictDomain()) != 0 &&
		key != i {
		key = (key + 1) % sz
	}

	if tbl.table[key] == nil {
		return []transaction.Transaction{}, nil
	}

	dups := []transaction.Transaction{}
	for tbl.table[key] != nil {
		if transaction.Compare(tbl.table[key].ConflictDomain(), txn.ConflictDomain()) == 0 {
			if tbl.table[key].Hash() != txn.Hash() {
				dups = append(dups, tbl.table[key])
			}
		}
		if i == key {
			return dups, fmt.Errorf("This isn't the Hilbert hotel.")
		}
		key = (key + 1) % sz
	}

	return dups, nil
}

func (tbl LinearHashTable) Print() {
	for i := 0; i < cap(tbl.table); i++ {
		fmt.Printf("%08x: %016x [%04x]\n", i, tbl.table[i], tbl.Key(tbl.table[i].Source().Uint64()))
	}
}
