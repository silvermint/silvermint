// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package dag

import (
	"fmt"
	"strings"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/validator"
)

// This is an insanely inefficient way of getting children, but these functions are unused currently
func getChildren(parent block.Block, dag *SilvermintDag, regs validator.ValidatorSet) map[uint16][]block.Block {
	children := map[uint16][]block.Block{}
	for val := range regs {
		children[val.Id] = []block.Block{}
	}
	for val := range regs {
		if val.IsActive() {
			dag.blocks.Range(val.Id, func(blk block.Block) bool {
				if blk.Hash() != parent.Hash() {
					genesisAdded := false
					for pval := range regs {
						if pval.IsActive() {
							if blk.Parents().BlockId(pval.Id).IsGenesis() && parent.BlockId().IsGenesis() {
								if !genesisAdded {
									genesisAdded = true
									children[pval.Id] = append(children[pval.Id], blk)
								}
							} else if blk.Parents().BlockId(pval.Id) == parent.BlockId() {
								children[pval.Id] = append(children[pval.Id], blk)
							}
						}
					}
				}
				return true
			})
		}
	}
	return children
}

func (dag *SilvermintDag) PrintDag() {
	roots := dag.Roots()
	for vid := range roots {
		root := roots.BlockId(uint16(vid))
		blk, _ := dag.Get(root)
		fmt.Printf("root: %s: ", root)
		if !dag.Has(root) {
			fmt.Printf("not in DAG...\n")
		} else {
			children := getChildren(blk, dag, blk.Registrations())
			nChildren := 0
			for val := range blk.Registrations() {
				nChildren += len(children[val.Id])
			}
			fmt.Printf("%d children...\n", len(children))
			dag.PrintChildren(2, root)
		}
	}
}

func (dag *SilvermintDag) PrintChildMap(indent int) {
	str := ""
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		dag.blocks.Range(i, func(blk block.Block) bool {
			str += fmt.Sprintf("%s: ", blk.BlockId())
			for _, children := range getChildren(blk, dag, blk.Registrations()) {
				for _, child := range children {
					str += fmt.Sprintf("%s, ", child.BlockId())
				}
			}
			str += "], "
			return true
		})
	}
	fmt.Printf("%sChildMap: %s\n", strings.Repeat(" ", indent), str)
}

func (dag *SilvermintDag) PrintChildren(indent int, bid ids.BlockId) {
	blk, _ := dag.Get(bid)

	for _, children := range getChildren(blk, dag, blk.Registrations()) {
		for _, child := range children {
			fmt.Printf("%s`-->> %s\n",
				strings.Repeat(" ", indent),
				child.BlockId(),
			)
			dag.PrintChildren(indent+2, child.BlockId())
		}
	}
}

func (dag *SilvermintDag) PrintTips() {
	fmt.Printf("\ndag tips: ----\n")
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		blk := dag.Blocks().Tip(i)
		b, _ := blk.MarshalBinary()
		fmt.Printf("    %s: %b\n", blk.BlockId(), b)
	}
	fmt.Printf("dag tips: ----\n")
}
