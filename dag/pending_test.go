// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package dag

import (
	"testing"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/validator"
)

func TestEnqueueBlock(t *testing.T) {
	pq := newPendingQueue(nil)
	blk := block.NewSilvermintBlock()
	blk.Deltas_ = block.NewHeightDeltaArray()
	pq.PendingBlock(blk, []ids.BlockId{{ValId_: 0, Height_: 1}})
	if pq.LenBlocks() != 1 {
		t.Fatalf("enqueue failed: expected 1, got %d", len(pq.Blocks))
	}
}

func TestEnqueueBlockParent(t *testing.T) {
	log.Debugf("Starting TestEnqueueBlockParent...")
	pq := newPendingQueue(nil)
	blk := block.NewSilvermintBlock()
	blk.Deltas_ = block.NewHeightDeltaArray()
	missingParent := ids.BlockId{ValId_: blk.Creator().Id + 1, Height_: blk.Height()}
	pq.PendingBlock(blk, []ids.BlockId{missingParent})
	if pq.LenBlocks() != 1 {
		t.Fatalf("enqueue failed: expected 1, got %d", pq.LenBlocks())
	}
	if pq.LenParents() != 1 {
		t.Fatalf("enqueue failed: expected 1, got %d", pq.LenParents())
	}

	pq.DeleteBlock(blk)
	_, inBlocks := pq.Blocks[blk.BlockId()]
	if inBlocks {
		t.Fatalf("Block %s was supposed to be deleted but was in pq.Blocks",
			blk.BlockId())
	}
	_, inParentBlocks := pq.ParentBlocks[missingParent]
	if inParentBlocks {
		t.Fatalf("ValidatorID/Height %d/%d was supposed to be deleted from "+
			"pq.ParentBlocks after adding Block %s",
			missingParent.ValId_, missingParent.Height_, blk.BlockId())
	}
	_, inBlockParents := pq.BlockParents[blk]
	if inBlockParents {
		t.Fatalf("Block %s was supposed to be deleted but was in pq.BlockParents",
			blk.BlockId())
	}

	for i := 0; i < 10000; i++ {
		blk := new(block.SilvermintBlock)
		blk.Deltas_ = block.NewHeightDeltaArray()
		blk.Timestamp_ = int64(i)
		blk.Creator_ = validator.Validator{Id: uint16(i)}
		log.Debugf("Adding Block/Parent %s/%d to pending...",
			blk.BlockId(), blk.Creator().Id)
		vhs := []ids.BlockId{{ValId_: blk.Creator_.Id + 1, Height_: 0}}
		pq.PendingBlock(blk, vhs)
		if pq.LenBlocks() != i+1 {
			t.Fatalf("Failed to add block to pending: "+
				"BlockId %s, creator %d. There are %d pending.",
				blk.BlockId(), blk.Creator().Id, pq.LenBlocks())
		}
		if pq.Len() != 2*(i+1) {
			t.Fatalf("Failed to add parent to pending: "+
				"BlockId %s, creator %d. There are %d missing parents.",
				blk.BlockId(), blk.Creator().Id, pq.LenParents())
		}
	}
}
