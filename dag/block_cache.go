package dag

import (
	"fmt"
	"path/filepath"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/blockdb"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/utils"
	"gitlab.com/silvermint/silvermint/validator"
)

var (
	SaveMicroVec = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "save_usec_total",
		Help: "Total microseconds spent writing to disk for various objects.",
	},
		[]string{"object"},
	)
)

func init() {
	prometheus.MustRegister(SaveMicroVec)
}

type BlockCache struct {
	bcache utils.Cache[block.Block]
	bslabs [validator.N_MAX_VALIDATORS]blockdb.SlabDir
	pslabs [validator.N_MAX_VALIDATORS]blockdb.SlabDir
	queue  []*utils.ConcurrentSortedSlice[block.Block]
}

func NewBlockCache(genesis block.Block) (*BlockCache, error) {
	sz := viper.GetUint64("block_cache_size")
	save_dir := viper.GetString("save_dir")

	// Initialize the Block cache
	bcache := *utils.NewCache[block.Block](sz)
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		bcache.Set(block.GenesisId(i), genesis)
	}

	bdat, err := genesis.MarshalBinary()
	if err != nil {
		return nil, err
	}
	pdat, err := genesis.Parents().MarshalBinary()
	if err != nil {
		return nil, err
	}

	// Initialize the Block/Parent slabs
	bslabs := [validator.N_MAX_VALIDATORS]blockdb.SlabDir{}
	pslabs := [validator.N_MAX_VALIDATORS]blockdb.SlabDir{}
	for i := 0; i < validator.N_MAX_VALIDATORS; i++ {
		dir := filepath.Join(save_dir, fmt.Sprint(i))
		err := utils.MaybeCreateDir(dir, 0755)
		if err != nil {
			return nil, err
		}
		blocks, err := blockdb.NewSlabDir(filepath.Join(dir, "blocks"))
		if err != nil {
			return nil, err
		}
		n, curr := blocks.Current()
		if n == 0 && curr.Index().Last() == 0 {
			curr.Append(bdat)
		}
		bslabs[i] = blocks

		parents, err := blockdb.NewSlabDir(filepath.Join(dir, "parents"))
		if err != nil {
			return nil, err
		}
		n, curr = parents.Current()
		if n == 0 && curr.Index().Last() == 0 {
			curr.Append(pdat)
		}
		pslabs[i] = parents
	}

	bc := &BlockCache{
		bcache: bcache,
		bslabs: bslabs,
		pslabs: pslabs,
		queue:  utils.NewConcurrentSortedSliceArray[block.Block](validator.N_MAX_VALIDATORS),
	}
	return bc, nil
}

func (bc BlockCache) GetTxn(tid ids.TransactionId) (transaction.Transaction, bool) {
	blk, ok := bc.Get(tid.BlockId)
	if ok {
		if uint16(len(blk.Transactions())) > tid.Index {
			return blk.Transactions()[tid.Index], true
		}
	}
	log.Warningf("Transaction lookup for %s failed", tid.String())
	return nil, false
}

// TODO(chuck): We'll probably want separate functions for loading Blocks/Parents.
// There will be distinct instances where we'll want the parent info (e.g. a
// validator came back online and is talking about a block we haven't needed in a
// while) and when we won't need parent info (e.g. when someone requests an old
// block for catchup). For now, always loading both is the more comprehensive but
// inefficient choice
func (bc BlockCache) Get(bid ids.BlockId) (block.Block, bool) {
	blk, ok := bc.bcache.Get(bid)
	if !ok {
		// If we've never seen the block/parents, we can't get/load it
		if !bc.bcache.Seen(bid) {
			log.Warningf("No Block or Parents for %s in cache or on disk", bid.String())
			return nil, false
		}
		// Attempt to load the block from disk
		log.Warningf("Block %s not found in cache, attempting to load", bid.String())
		var err error
		blk, err = bc.Load(bid)
		if err != nil {
			log.Errorf("Unable to load Block %s from disk: %s", bid.String(), err.Error())
			return nil, false
		}
	}

	return blk, true
}

func (bc BlockCache) Set(bid ids.BlockId, blk block.Block) {
	bc.bcache.Set(bid, blk)
}

func (bc BlockCache) Has(bid ids.BlockId) bool {
	return bc.bcache.Has(bid)
}

func (bc BlockCache) Seen(bid ids.BlockId) bool {
	return bc.bcache.Seen(bid)
}

func (bc BlockCache) Size() uint64 {
	return bc.bcache.Size()
}

func (bc BlockCache) Tip(vid uint16) block.Block {
	blk := bc.bcache.Tip(vid)
	if blk == nil {
		_, bslab := bc.bslabs[vid].Current()
		// NB(chuck): bslab.Index().Last() returns an index corresponding to an
		// empty element. Use -1 to get the last populated element in the slab.
		bid := ids.BlockId{
			ValId_:  vid,
			Height_: bslab.Index().Last() - 1,
		}
		var err error
		blk, err = bc.Load(bid)
		if err != nil {
			log.Errorf("unable to load tip Block: %s", err.Error())
		}
	}
	return blk
}

func (bc BlockCache) Range(vid uint16, f func(block.Block) bool) {
	blk := bc.Tip(vid)
	for {
		if !f(blk) {
			return
		}
		if blk.BlockId().Height() == 0 {
			break
		}
		bid := blk.BlockId().Prev()
		blk, _ = bc.Get(bid)
	}
}

func (bc BlockCache) LoadBlock(bid ids.BlockId) (block.Block, error) {
	_, bslab := bc.bslabs[bid.ValId()].Current()
	// If it's not in the current slab, iterate over them until it's found
	if bid.Height() < bslab.Index().Initial() {
		for _, bslab = range bc.bslabs[bid.ValId()].Slabs() {
			if bid.Height() >= bslab.Index().Initial() &&
				bid.Height() < bslab.Index().Last() {
				break
			}
		}
	}
	// Block of Height 0 is stored at index 0
	bdata, err := bslab.Get(bid.Height())
	if err != nil {
		return nil, err
	}

	blk := block.NewSilvermintBlock()
	err = blk.UnmarshalBinary(bdata)
	if err != nil {
		return nil, err
	}
	return blk, nil
}

func (bc BlockCache) LoadParents(bid ids.BlockId) (block.Heights, error) {
	_, pslab := bc.pslabs[bid.ValId()].Current()
	// If it's not in the current slab, iterate over them until it's found
	if bid.Height() < pslab.Index().Initial() {
		for _, pslab = range bc.pslabs[bid.ValId()].Slabs() {
			if bid.Height() >= pslab.Index().Initial() &&
				bid.Height() < pslab.Index().Last() {
				break
			}
		}
	}
	ps := block.Heights{}
	// Block of Height 0 is stored at index 0
	pdata, err := pslab.Get(bid.Height())
	if err != nil {
		return block.Heights{}, err
	}
	err = ps.UnmarshalBinary(pdata)
	if err != nil {
		return block.Heights{}, err
	}

	return ps, nil
}

func (bc BlockCache) Load(bid ids.BlockId) (block.Block, error) {
	// Load the block
	blk, err := bc.LoadBlock(bid)
	if err != nil {
		return nil, err
	}
	// Load the block parents and set those
	ps, err := bc.LoadParents(bid)
	if err != nil {
		return nil, err
	}
	blk.SetParents(ps)

	return blk, nil
}

func (bc BlockCache) Save(blk block.Block) error {
	_, bslab := bc.bslabs[blk.Creator().Id].Current()
	_, pslab := bc.pslabs[blk.Creator().Id].Current()
	// NB(chuck): Checking this ensures we only add the same Block/Parents once,
	// and that they're only added in order. This keeps the indexing correct.
	if blk.Height() == bslab.Index().Last() {
		bdata, err := blk.MarshalBinary()
		if err != nil {
			return err
		}
		pdata, err := blk.Parents().MarshalBinary()
		if err != nil {
			return err
		}
		nowMicro := time.Now().UnixMicro()
		err = bslab.Append(bdata)
		if err != nil {
			return err
		}
		err = pslab.Append(pdata)
		if err != nil {
			return err
		}
		mus := float64(time.Now().UnixMicro() - nowMicro)
		SaveMicroVec.WithLabelValues("block").Add(mus)
		log.Debugf("Block: %s, Save: %f seconds", blk.BlockId().String(), mus*0.000001)
		TotalBytesWritten.WithLabelValues("block").Add(float64(len(bdata)))
		TotalBytesWritten.WithLabelValues("parents").Add(float64(len(pdata)))
		next, ok := bc.queue[blk.Creator().Id].Pop()
		if ok {
			bc.Save(next)
		}
	} else {
		bc.queue[blk.Creator().Id].Add(blk.BlockId(), blk)
	}

	return nil
}

func LoadBlockCache() (*BlockCache, error) {
	sz := viper.GetUint64("block_cache_size")
	save_dir := viper.GetString("save_dir")

	// Open/assign our block and parents files
	bslabs := [validator.N_MAX_VALIDATORS]blockdb.SlabDir{}
	pslabs := [validator.N_MAX_VALIDATORS]blockdb.SlabDir{}
	for i := 0; i < validator.N_MAX_VALIDATORS; i++ {
		dir := filepath.Join(save_dir, fmt.Sprint(i))

		var err error
		bslabs[i], err = blockdb.NewSlabDir(filepath.Join(dir, "blocks"))
		if err != nil {
			return nil, err
		}

		pslabs[i], err = blockdb.NewSlabDir(filepath.Join(dir, "parents"))
		if err != nil {
			return nil, err
		}
	}
	// Create the BlockCache
	bc := &BlockCache{
		bcache: *utils.NewCache[block.Block](sz),
		bslabs: bslabs,
		pslabs: pslabs,
	}
	return bc, nil
}
