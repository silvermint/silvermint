// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package dag

import (
	"fmt"
	"math"
	"math/rand"
	"testing"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/transaction"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

func RandBytes(n int) []byte {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return []byte(string(b))
}
func RandWallet() crypto.Wallet {
	wal, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	return wal
}

func RandTransaction() transaction.Transaction {
	ttype := rand.Intn(3)
	src := RandWallet()

	var txn transaction.Transaction
	switch ttype {
	case 0:
		mint := transaction.Mint{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Output_: transaction.MintOutput{
				Amount_: uint64(rand.Intn(math.MaxInt)),
				Dest_:   src.WalletAddress(),
			},
		}
		txn = &mint
	case 1:
		th := transaction.TransactionHash{}
		copy(th[:], RandBytes(crypto.HASH_SIZE))
		pmt := transaction.Payment{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Inputs_: []transaction.Utxo{
				{
					TxnId:  ids.TransactionId{},
					Amount: rand.Uint64(),
				},
			},
			Outputs_: []transaction.PaymentOutput{
				{
					Amount_: uint64(rand.Intn(math.MaxInt)),
					Dest_:   src.WalletAddress(),
				},
			},
		}
		txn = &pmt
	case 2:
		th := transaction.TransactionHash{}
		copy(th[:], RandBytes(crypto.HASH_SIZE))
		sweep := transaction.Sweep{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Inputs_: []transaction.Utxo{
				{
					TxnId:  ids.TransactionId{},
					Amount: rand.Uint64(),
				},
			},
			Output_: transaction.SweepOutput{
				Amount_: uint64(rand.Intn(math.MaxInt)),
				Dest_:   src.WalletAddress(),
			},
		}
		txn = &sweep
	}
	return txn
}

func TestDuplicateEntry(t *testing.T) {
	keybits := uint16(24)
	tab, _ := NewLHT(keybits)

	wal := RandWallet()
	mint1 := transaction.Mint{
		Src:          wal.WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 100,
			Dest_:   wal.WalletAddress(),
		},
	}
	mint2 := transaction.Mint{
		Src:          wal.WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 200,
			Dest_:   wal.WalletAddress(),
		},
	}
	dup1, _ := tab.Insert(RandTransaction())
	dup2, _ := tab.Insert(RandTransaction())
	dup3, _ := tab.Insert(&mint1)
	dup4, _ := tab.Insert(&mint2)

	if !dup1 && !dup2 && !dup3 && !dup4 {
		// Hrm, we should have at least one.
		key := tab.Key(0xdeadbeefdeadbeef)
		for i := key - 0x10; i < key+0x20; i++ {
			fmt.Printf("%x: 0x%016x\n", i, tab.Get(i))
		}
	}

	if !dup1 && !dup2 && !dup3 && !dup4 {
		t.Fatalf("Error - should have had duplicate detected for 0xdeadbeefdeadbeef")
	}
}
