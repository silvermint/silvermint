// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package console

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"os/exec"
	"reflect"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/log"
)

func render(c *gin.Context, data gin.H, templateName string) {
	switch c.Request.Header.Get("Accept") {
	case "application/json":
		c.JSON(http.StatusOK, data["payload"])
	default:
		c.HTML(http.StatusOK, templateName, data)
	}
}

// Index produces the index html page.
func Index(c *gin.Context) {
	render(c,
		gin.H{"title": "Welcome to Silvermint",
			"payload": "Blocks unchained."},
		"index.html")
}

// Config produces the configuration web page.
func Config(c *gin.Context) {
	render(c,
		gin.H{"title": "Config",
			"debug":                     viper.Get("debug"),
			"gin_mode":                  viper.Get("gin_mode"),
			"port":                      viper.Get("port"),
			"management_console_ipaddr": viper.Get("management_console_ipaddr"),
			"api_port":                  viper.Get("api_port"),
			"api_ipaddr":                viper.Get("api_ipaddr"),
			"templates_path":            viper.Get("templates_path"),
			"block_channel_bufsz":       viper.Get("block_channel_bufsz"),
			"block_receiver_hostport":   viper.Get("block_receiver_hostport"),
			"validator_id":              viper.Get("validator_id"),
			"validators":                viper.Get("validators"),
			"data_dir":                  viper.Get("data_dir"),
			"log_dir":                   viper.Get("log_dir"),
			"save_dir":                  viper.Get("save_dir"),
			"genesis_file":              viper.Get("genesis_dir"),
			"casanova_testing":          viper.Get("casanova_testing"),
		},
		"config.html")
}

// Save handles saving an updated configuration.
func Save(w http.ResponseWriter, r *http.Request) {
	for _, field := range viper.AllKeys() {
		val := r.FormValue(field)
		if val != "" {
			// NB(chuck): I am using a map instead of viper.Set(...) because
			// the Set() call overrides values saved when we call WriteConfig.
			// This causes an unexpectedly weird interaction between the web
			// interface and configuration file.
			var err error
			m := make(map[string]interface{})
			m[field], err = strToX(val, viper.Get(field))
			if err == nil {
				viper.MergeConfigMap(m)
			}
		}
	}
	viper.WriteConfig()

	http.Redirect(w, r, "/config", http.StatusSeeOther)
}

func strToX(sv string, tv interface{}) (interface{}, error) {
	var err error
	var cnv interface{}

	switch reflect.ValueOf(tv).Kind() {
	case reflect.Bool:
		cnv, err = strconv.ParseBool(sv)
	case reflect.Interface:
		cnv = sv
	case reflect.Int:
		cnv, err = strconv.Atoi(sv)
	case reflect.Float32:
		cnv, err = strconv.ParseFloat(sv, 32)
	case reflect.Float64:
		cnv, err = strconv.ParseFloat(sv, 64)
	case reflect.String:
		cnv = sv
	case reflect.Array, reflect.Slice:
		s := strings.Split(strings.Trim(sv, "[]"), ",")
		cnvs := make([]interface{}, len(s))
		for i, val := range s {
			cnvs[i], err = strToX(strings.Trim(val, " "), tv.([]interface{})[0])
			if err != nil {
				break
			}
		}
		cnv = cnvs
	default:
		err = errors.New("unknown type")
	}
	if err != nil {
		log.Warningf("skipping input \"%v\" for configuration variable of type %T", sv, tv)
	}
	return cnv, err
}

func DagDot(context *gin.Context) {
	context.Header("Content-Type", "text/plain; charset=utf-8")
	writer := context.Writer
	fmt.Fprintf(writer, "%s", casptr.Consensus.Dag.GetDot())
}

func Dag(context *gin.Context) {
	context.Header("Content-Type", "image/png")
	context.Header("Refresh", "2")

	cmd := exec.Command("dot", "-Tpng")
	dot := casptr.Consensus.Dag.GetDot()
	cmd.Stdin = strings.NewReader(dot)
	cmd.Stdout = context.Writer
	var stderr bytes.Buffer
	cmd.Stderr = bufio.NewWriter(&stderr)
	err := cmd.Run()
	if err != nil {
		log.Warningf("dag render failed: %s", err.Error())
		str := stderr.String()
		if len(str) != 0 {
			log.Warningf("dag render error was: %s", str)
		}
		log.Warningf("dag render dot input was: %s", dot)
	}
}
