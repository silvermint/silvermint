// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package console

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func initRoutes(router *gin.Engine) {
	// Route: index.html
	router.GET("/", Index)

	// Configuration options will live here
	router.GET("/config", Config)
	router.POST("/config", gin.WrapH(http.HandlerFunc(Save)))

	// Metrics are served here for Prometheus to scrape
	router.GET("/metrics", gin.WrapH(promhttp.Handler()))

	router.GET("/dag.dot", DagDot)
	router.GET("/dag", Dag)
}
