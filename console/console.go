// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package console

import (
	"fmt"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"

	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/log"
)

var totalRequestsAndStatus = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_request_and_status_totals",
		Help: "Number of get requests with response status.",
	},
	[]string{"path", "status"},
)

var casptr *casanova.Casanova

func init() {
	prometheus.MustRegister(totalRequestsAndStatus)
	pflag.String("management_console_ipaddr", "0.0.0.0",
		"The IP address for the Management Console to listen on.")
	pflag.Int("port", 8888,
		"The port on which the management console HTTP server listens.")

}

// Run runs the management console, duh.
//
// The management console is a web server that serves whatever goo a systems
// admin might need to manage or interrogate the node software. E.g., it
// should serve whatever monitoring goop is being used, a configuration
// interface so that people don't have to modify config files if they don't
// want to, and so forth.
//
// silvermint#143 TODO(leaf): Currently references various global pointers, probably should
// take these as params instead to be more functional.
func Run(templates []string, casanova_ptr *casanova.Casanova) {
	casptr = casanova_ptr

	ipAddress := viper.GetString("management_console_ipaddr")
	port := viper.GetInt("port")
	if ipAddress == "" || port == 0 {
		log.Fatalln("Management console requires an IP address and API port.")
	}

	consoleAddress := fmt.Sprintf("%s:%d", ipAddress, port)
	log.Debugf("Console listening on %s", consoleAddress)

	var router *gin.Engine = gin.Default()
	router.SetTrustedProxies(nil)

	for _, path := range templates {
		glob := filepath.Join(path, "*")
		router.LoadHTMLGlob(glob)
		log.Debugf("Console loaded HTML Glob: %s", glob)
	}

	router.Use(IncrRequestAndStatus)
	initRoutes(router)
	router.Run(consoleAddress)
}

// IncrRequestAndStatus is a wrapper for incrementing the prometheus variable
// for a given console URI.
func IncrRequestAndStatus(c *gin.Context) {
	path := c.FullPath()
	status := strconv.Itoa(c.Writer.Status())

	totalRequestsAndStatus.WithLabelValues(path, status).Inc()
}
