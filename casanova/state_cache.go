package casanova

import (
	"fmt"
	"path/filepath"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/blockdb"
	"gitlab.com/silvermint/silvermint/dag"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/utils"
	"gitlab.com/silvermint/silvermint/validator"
)

type StateCache struct {
	cache utils.Cache[StateData]
	slabs [validator.N_MAX_VALIDATORS]blockdb.SlabDir
	queue []*utils.ConcurrentSortedSlice[StateData]
}

func NewStateCache(genesisState StateData) (*StateCache, error) {
	sz := viper.GetUint64("block_cache_size")
	save_dir := viper.GetString("save_dir")

	// Initialize the StateData cache
	cache := *utils.NewCache[StateData](sz)
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		cache.Set(block.GenesisId(i), genesisState)
	}

	gdata, err := genesisState.MarshalBinary()
	if err != nil {
		return nil, err
	}

	// Initialize the StateData slabs
	sslabs := [validator.N_MAX_VALIDATORS]blockdb.SlabDir{}
	for i := 0; i < validator.N_MAX_VALIDATORS; i++ {
		dir := filepath.Join(save_dir, fmt.Sprint(i), "state")

		state, err := blockdb.NewSlabDir(dir)
		if err != nil {
			return nil, err
		}

		n, curr := state.Current()
		if n == 0 && curr.Index().Last() == 0 {
			curr.Append(gdata)
		}

		sslabs[i] = state
	}

	sc := &StateCache{
		cache: cache,
		slabs: sslabs,
		queue: utils.NewConcurrentSortedSliceArray[StateData](validator.N_MAX_VALIDATORS),
	}

	return sc, nil
}

func (sc StateCache) Get(bid ids.BlockId) (StateData, error) {
	sd, ok := sc.cache.Get(bid)
	if !ok {
		// If we've never seen the StateData, we can't get/load it
		if !sc.cache.Seen(bid) {
			return StateData{}, fmt.Errorf("no StateData for %s in cache or on disk",
				bid.String())
		}
		// Attempt to load the StateData from disk
		log.Warningf("StateData for %s not found in cache, attempting to load", bid.String())
		var err error
		sd, err = sc.Load(bid)
		if err != nil {
			return StateData{}, fmt.Errorf("unable to load StateData for %s from disk: %s",
				bid.String(), err.Error())
		}
	}
	return sd, nil
}

func (sc StateCache) Set(bid ids.BlockId, sd StateData) {
	sc.cache.Set(bid, sd)
}

func (sc StateCache) Load(bid ids.BlockId) (StateData, error) {
	sd := emptyStateData(CasanovaExtra{})
	_, sslab := sc.slabs[bid.ValId()].Current()
	// If it's not in the current slab, iterate over them until it's found
	if bid.Height() < sslab.Index().Initial() {
		for _, sslab = range sc.slabs[bid.ValId()].Slabs() {
			if bid.Height() >= sslab.Index().Initial() &&
				bid.Height() < sslab.Index().Last() {
				break
			}
		}
	}
	// Block of Height 0 is stored at index 0
	sdata, err := sslab.Get(bid.Height())
	if err != nil {
		return StateData{}, err
	}
	err = sd.UnmarshalBinary(sdata)
	if err != nil {
		return StateData{}, err
	}

	// Update the TransactionIds for the StateData's AddedTxns
	for i := range sd.AddedTxns {
		tid := ids.TransactionId{
			BlockId: bid,
			Index:   uint16(i),
		}
		sd.AddedTxns[i].SetId(tid)
	}

	// Update the BlockId field
	sd.BlockId = bid

	return sd, nil
}

func (sc StateCache) Save(sd StateData) error {
	_, sslab := sc.slabs[sd.BlockId.ValId()].Current()
	// NB(chuck): Checking this ensures we only add the same StateData once,
	// and that they're only added in order. This keeps the indexing correct.
	if sd.BlockId.Height() == sslab.Index().Last() {
		data, err := sd.MarshalBinary()
		if err != nil {
			return err
		}
		nowMicro := time.Now().UnixMicro()
		err = sslab.Append(data)
		if err != nil {
			return err
		}
		mus := float64(time.Now().UnixMicro() - nowMicro)
		dag.SaveMicroVec.WithLabelValues("state").Add(mus)
		log.Debugf("StateData: %s, Save: %f seconds", sd.BlockId.String(), mus*0.000001)
		dag.TotalBytesWritten.WithLabelValues("state").Add(float64(len(data)))
		next, ok := sc.queue[sd.BlockId.ValId()].Pop()
		if ok {
			sc.Save(next)
		}
	} else {
		sc.queue[sd.BlockId.ValId()].Add(sd.BlockId, sd)
	}

	return nil
}

func (sc StateCache) Tip(vid uint16) StateData {
	sd := sc.cache.Tip(vid)
	if sd.IsEmpty() {
		_, sslab := sc.slabs[vid].Current()
		// NB(chuck): sslab.Index().Last() returns an index corresponding to an
		// empty element. Use -1 to get the last populated element in the slab.
		bid := ids.BlockId{
			ValId_:  vid,
			Height_: sslab.Index().Last() - 1,
		}
		var err error
		sd, err = sc.Load(bid)
		if err != nil {
			log.Errorf("unable to load tip StateData: %s", err.Error())
		}
	}
	return sd
}

func LoadStateCache() (*StateCache, error) {
	sz := viper.GetUint64("block_cache_size")
	save_dir := viper.GetString("save_dir")

	// Open/assign our state slab files
	sslabs := [validator.N_MAX_VALIDATORS]blockdb.SlabDir{}
	for i := 0; i < validator.N_MAX_VALIDATORS; i++ {
		dir := filepath.Join(save_dir, fmt.Sprint(i), "state")

		var err error
		sslabs[i], err = blockdb.NewSlabDir(dir)
		if err != nil {
			return nil, err
		}
	}
	// Create the StateCache
	sc := &StateCache{
		cache: *utils.NewCache[StateData](sz),
		slabs: sslabs,
	}
	return sc, nil
}
