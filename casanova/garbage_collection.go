package casanova

import (
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/log"
)

func init() {
	pflag.Bool("delete_blocks", false, "Enables Delete Blocks Routine.")
	pflag.Int("delete_blocks_frequency", 0,
		"The amount of time to wait between deleting blocks (seconds).")
	pflag.Bool("delete_txns", true, "Enables Delete Transactions Routine.")
	pflag.Int("delete_txns_frequency", 5,
		"The amount of time to wait between deleting transactions (seconds).")
}

func (cas *Casanova) BlockGCThread() {
	if viper.GetBool("delete_blocks") {
		log.Warningln("the delete_blocks flag is deprecated.")
	}
	if viper.GetInt("delete_blocks_frequency") != 0 {
		log.Warningln("the delete_blocks_frequency flag is deprecated")
	}
}
