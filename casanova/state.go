// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"encoding/binary"
	"fmt"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/dag"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/status"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
)

var (
	ConsensusStateMicroVec = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "consensus_block_state_usec_total",
		Help: "Total microseconds spent in BlockState and its subprocesses.",
	},
		[]string{"function", "self"},
	)
	ReducerMicroVec = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "consensus_reducer_usec_total",
		Help: "Total microseconds spent in reducer and its subprocesses.",
	},
		[]string{"function"},
	)
	NumConflictDomains = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "consensus_num_conflict_domains",
		Help: "Number of Conflict Domains considered by latest BlockState calculation.",
	})
	NumUnarchivedBlocks = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "consensus_num_unarchived_blocks",
		Help: "Number of unarchived blocks considered by latest BlockState calculation.",
	})
	NumTxnsInMap = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "consensus_num_transactions_in_map",
		Help: "Number of transactions in the TxnMap.",
	})
	NumTxnsInLocationMap = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "consensus_num_transactions_in_location_map",
		Help: "Number of transactions in the location map.",
	})
	NumTxnsInWalletsMap = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "consensus_num_transactions_in_wallets_map",
		Help: "Number of transactions in the wallets map.",
	})
	BlockStateThreads = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "consensus_num_block_state_threads",
		Help: "Number of active BlockState calculations currently.",
	})
)

func init() {
	prometheus.MustRegister(ConsensusStateMicroVec)
	prometheus.MustRegister(ReducerMicroVec)
	prometheus.MustRegister(NumConflictDomains)
	prometheus.MustRegister(NumUnarchivedBlocks)
	prometheus.MustRegister(NumTxnsInMap)
	prometheus.MustRegister(NumTxnsInLocationMap)
	prometheus.MustRegister(NumTxnsInWalletsMap)
	prometheus.MustRegister(BlockStateThreads)
}

type CombineExtraFunc func(u1 CasanovaExtra, u2 CasanovaExtra) CasanovaExtra
type CheckTxnsFunc func(block.Block, CasanovaExtra) ([]transaction.Transaction, error)
type CheckRegistrationsFunc func(validator.ValidatorSet, CasanovaExtra) validator.ValidatorSet
type StateDataReducerFunc func(sds []StateData) StateData

type StateData struct {
	BlockId    ids.BlockId
	Bvs        BVS
	Cdvs       CDVS
	AddedTxns  []transaction.Transaction
	Validators validator.ValidatorSet
	Slashed    validator.ValidatorSet
	Ftm        uint16
	Height     map[validator.Validator]uint64
	Extra      CasanovaExtra
}

func emptyStateData(extra CasanovaExtra) StateData {
	return StateData{
		BlockId:    ids.BlockId{},
		Bvs:        BVS{},
		Cdvs:       CDVS{},
		AddedTxns:  []transaction.Transaction{},
		Validators: validator.ValidatorSet{},
		Slashed:    validator.ValidatorSet{},
		Ftm:        0,
		Height:     map[validator.Validator]uint64{},
		Extra:      extra,
	}
}

// Provides an empty StateData object where the elements are pre-allocated
// according to the size of the template StateData provided.
// The AddedTxns field is pre-allocated according to the block Transactions
// for the provided templateBlk.
func preallocateStateData(template StateData) StateData {
	return StateData{
		Bvs:        make(BVS, len(template.Bvs)),
		Cdvs:       make(CDVS, len(template.Cdvs)),
		AddedTxns:  make([]transaction.Transaction, len(template.AddedTxns)),
		Validators: make(validator.ValidatorSet, len(template.Validators)),
		Slashed:    make(validator.ValidatorSet, len(template.Slashed)),
		Ftm:        0,
		Height:     make(map[validator.Validator]uint64, len(template.Height)),
		Extra:      CasanovaExtra{},
	}
}

// TODO(chuck): This is pretty hokey. We might consider changing the StateCache
// to store *StateData instead of StateData to resolve the ambiguity of an
// empty return value.
func (sd StateData) IsEmpty() bool {
	return sd.Height == nil
}

func (sd StateData) TxnsLength() uint64 {
	var l uint64 = 2 // Number of txns
	for _, txn := range sd.AddedTxns {
		l += uint64(txn.Length()) // Txn length
	}
	return l
}

func (sd StateData) ValidatorLength() uint64 {
	var l uint64 = 2 // Num validators
	for val := range sd.Validators {
		l += uint64(val.Length()) // Length of the Validator
	}
	return l
}

func (sd StateData) SlashedLength() uint64 {
	var l uint64 = 2 // Num validators
	for val := range sd.Slashed {
		l += uint64(val.Length()) // Length of the Validator
	}
	return l
}

func (sd StateData) FtmLength() uint64 {
	var l uint64 = 2 // FTM
	return l
}

func (sd StateData) HeightsLength() uint64 {
	var l uint64 = 2                // Num validators
	l += uint64(len(sd.Height) * 2) // Validator IDs
	l += uint64(len(sd.Height) * 8) // Heights
	return l
}

func (sd StateData) Length() uint64 {
	var l uint64 = 8          // Length of byte data
	l += sd.Bvs.Length()      // BVS
	l += sd.Cdvs.Length()     // CDVS
	l += sd.TxnsLength()      // AddedTxns
	l += sd.ValidatorLength() // Validators
	l += sd.SlashedLength()   // Slashed
	l += sd.FtmLength()       // FTM
	l += sd.HeightsLength()   // Height
	l += sd.Extra.Length()    // IncrLedger
	return l
}

func (sd StateData) MarshalBinary() ([]byte, error) {
	slen := sd.Length()
	data := make([]byte, slen)
	var offset uint64 = 0

	// Length
	binary.LittleEndian.PutUint64(data[offset:offset+8], slen)
	offset += 8

	// BVS
	bvsBytes, err := sd.Bvs.MarshalBinary()
	if err != nil {
		return nil, err
	}
	copy(data[offset:offset+uint64(len(bvsBytes))], bvsBytes)
	offset += uint64(len(bvsBytes))

	// CDVS
	cdvsBytes, err := sd.Cdvs.MarshalBinary()
	if err != nil {
		return nil, err
	}
	copy(data[offset:offset+uint64(len(cdvsBytes))], cdvsBytes)
	offset += uint64(len(cdvsBytes))

	// AddedTxns
	binary.LittleEndian.PutUint16(data[offset:offset+2], uint16(len(sd.AddedTxns)))
	offset += 2
	for _, txn := range sd.AddedTxns {
		txnBytes, err := txn.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(data[offset:offset+uint64(len(txnBytes))], txnBytes)
		offset += uint64(len(txnBytes))
	}

	// Validators
	binary.LittleEndian.PutUint16(data[offset:offset+2], uint16(len(sd.Validators)))
	offset += 2
	for val := range sd.Validators {
		valBytes, err := val.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(data[offset:offset+uint64(len(valBytes))], valBytes)
		offset += uint64(len(valBytes))
	}

	// Slashed
	binary.LittleEndian.PutUint16(data[offset:offset+2], uint16(len(sd.Slashed)))
	offset += 2
	for val := range sd.Slashed {
		valBytes, err := val.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(data[offset:offset+uint64(len(valBytes))], valBytes)
		offset += uint64(len(valBytes))
	}

	// FTM
	binary.LittleEndian.PutUint16(data[offset:offset+2], sd.Ftm)
	offset += 2

	// Heights
	binary.LittleEndian.PutUint16(data[offset:offset+2], uint16(len(sd.Height)))
	offset += 2
	for val, height := range sd.Height {
		binary.LittleEndian.PutUint16(data[offset:offset+2], val.Id)
		offset += 2
		binary.LittleEndian.PutUint64(data[offset:offset+8], height)
		offset += 8
	}

	// Ledger
	if sd.Extra.Ledger != nil {
		ldat, err := sd.Extra.Ledger.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(data[offset:offset+uint64(len(ldat))], ldat)
		offset += uint64(len(ldat))
	}

	if offset != slen {
		return nil, fmt.Errorf("Computed StateData byte length didn't "+
			"match marshaled StateData byte length: %d != %d", slen, offset)
	}

	return data, nil
}

func (sd *StateData) UnmarshalBinary(data []byte) error {
	if len(data) == 0 {
		return fmt.Errorf("Can't unmarshal empty byte array")
	}
	var offset uint64 = 0

	// Length
	slen := binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8
	if slen != uint64(len(data)) {
		return fmt.Errorf("StateData has length %d bytes, "+
			"but we have %d bytes of data", slen, len(data))
	}

	// BVS
	bvslen := binary.LittleEndian.Uint64(data[offset : offset+8])
	err := sd.Bvs.UnmarshalBinary(data[offset : offset+bvslen])
	if err != nil {
		return err
	}
	offset += bvslen

	// CDVS
	cdvslen := binary.LittleEndian.Uint64(data[offset : offset+8])
	err = sd.Cdvs.UnmarshalBinary(data[offset : offset+cdvslen])
	if err != nil {
		return err
	}
	offset += cdvslen

	// AddedTxns
	ntxns := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	for i := uint16(0); i < ntxns; i++ {
		tlen := binary.LittleEndian.Uint16(data[offset : offset+2])
		txn, err := transaction.UnmarshalTxn(data[offset : offset+uint64(tlen)])
		if err != nil {
			return err
		}
		sd.AddedTxns = append(sd.AddedTxns, txn)
		offset += uint64(tlen)
	}

	// Validators
	nvals := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	for i := uint16(0); i < nvals; i++ {
		vlen := binary.LittleEndian.Uint16(data[offset : offset+2])
		val := validator.Validator{}
		err = val.UnmarshalBinary(data[offset : offset+uint64(vlen)])
		if err != nil {
			return err
		}
		offset += uint64(vlen)
		sd.Validators[val] = true
	}

	// Slashed
	nslashed := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	for i := uint16(0); i < nslashed; i++ {
		vlen := binary.LittleEndian.Uint16(data[offset : offset+2])
		val := validator.Validator{}
		err = val.UnmarshalBinary(data[offset : offset+uint64(vlen)])
		if err != nil {
			return err
		}
		offset += uint64(vlen)
		sd.Slashed[val] = true
	}

	// FTM
	sd.Ftm = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Heights
	nheights := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	for i := uint16(0); i < nheights; i++ {
		vid := binary.LittleEndian.Uint16(data[offset : offset+2])
		offset += 2
		height := binary.LittleEndian.Uint64(data[offset : offset+8])
		offset += 8
		found := false
		for val := range sd.Validators {
			if vid == val.Id {
				found = true
				sd.Height[val] = height
				break
			}
		}
		if !found {
			return fmt.Errorf("Unable to associate Validator ID %d with "+
				"Validator object for Height map", vid)
		}
	}

	// Extra / Ledger
	sd.Extra = CasanovaExtra{}
	// NB(chuck): After unmarshaling a ledger, the pointers will need to be set
	if offset != slen {
		llen := binary.LittleEndian.Uint64(data[offset : offset+8])
		ledger, err := transaction.UnmarshalLedger(data[offset : offset+llen])
		if err != nil {
			return err
		}
		sd.Extra.Ledger = ledger
		offset += llen
	}

	if offset != slen {
		return fmt.Errorf("Computed StateData byte length didn't "+
			"match marshaled StateData byte length: %d != %d", slen, offset)
	}

	return nil
}

// Computes the information necessary for Casanova + changes in the validator
// set. Assumes the block has already been checked for validity.
func (cas *Casanova) BlockState(blk block.Block) (StateData, error) {
	BlockStateThreads.Inc()
	defer BlockStateThreads.Dec()

	log.Debugf("Computing blockstate for %s", blk.BlockId())

	start := time.Now().UnixMicro()
	nowMicro := time.Now().UnixMicro()

	stateDataCache := cas.StateCache
	consensus := cas.Consensus

	// Base case: The Genesis Block.
	if block.IsGenesis(blk) {
		state := CasanovaGenesisState(blk.Registrations(), cas.GenesisExtra)
		return state, nil
	}

	// Try the cache first
	cached, err := stateDataCache.Get(blk.BlockId())
	if err == nil {
		return cached, nil
	}

	blockCreator := blk.Creator()
	selfStr := "other"
	if blockCreator == cas.Self_ {
		selfStr = "self"
	}

	elapsed := float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("preamble", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, preamble: %f seconds", blk.BlockId(), elapsed*0.000001)

	// Get Parent state data, then reduce it into the new state.
	nowMicro = time.Now().UnixMicro()
	regs := blk.Registrations()
	parentStates, prevParentState, err := GetParentStates(cas, blk, regs)
	if err != nil {
		return StateData{}, err
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("GetParentStates", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, GetParentStates: %f seconds", blk.BlockId(), elapsed*0.000001)

	// Assemble a StateData with latest assertions
	// from each parent validator about each conflict domain
	nowMicro = time.Now().UnixMicro()
	result := reducer(parentStates, blk.BlockId())
	result.BlockId = blk.BlockId()

	// Update the height of the block creator to include this block
	result.Height[blockCreator]++
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("reducer", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, reducer: %f seconds to reduce %d StateData for len(BVS) %d",
		blk.BlockId(), elapsed*0.000001, len(parentStates), len(result.Bvs))

	// Only blocks created by this validator need a ledger
	nowMicro = time.Now().UnixMicro()
	if blk.Creator() == cas.Self_ {
		// Either get the existing IncrLedger that was preset as the previous
		// ledger's child, or create a new one if it wasn't set (genesis).
		iledger := prevParentState.Extra.Ledger.Child()
		if iledger == nil {
			iledger = transaction.NewIncrLedger(prevParentState.Extra.Ledger)
			prevParentState.Extra.Ledger.SetChild(iledger)
		}
		// Create an empty IncrLedger and set it as this one's child
		iledger.SetChild(transaction.NewIncrLedger(iledger))

		// Assign the ledger
		result.Extra.Ledger = iledger
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("ledgerAllocation", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, ledgerAllocation: %f seconds", blk.BlockId(), elapsed*0.000001)

	// Delete any CDs archived by this host from the CDVS
	nowMicro = time.Now().UnixMicro()
	result.Cdvs.DeleteOldCDs()
	ConsensusStateMicroVec.WithLabelValues("DeleteOldCDs", selfStr).Add(float64(time.Now().UnixMicro() - nowMicro))
	log.Debugf("StateAtBlock: %s, DeleteOldCDs: %f seconds",
		blk.BlockId(), float64(time.Now().UnixMicro()-nowMicro)*0.000001)

	// Delete fully archived blocks from the BVS
	nowMicro = time.Now().UnixMicro()
	result.Bvs.DeleteOldBlockIds()
	ConsensusStateMicroVec.WithLabelValues("DeleteOldBlockIds", selfStr).Add(float64(time.Now().UnixMicro() - nowMicro))
	log.Debugf("StateAtBlock: %s, DeleteOldBlockIds: %f seconds",
		blk.BlockId(), float64(time.Now().UnixMicro()-nowMicro)*0.000001)

	// Gets the LHT for all "active" (unarchived) transactions known
	// to this block, NOT including the transactions added BY this block
	nowMicro = time.Now().UnixMicro()
	activeTxns, detectedConflicts, err := consensus.PopulateActiveTxnTable(result, cas.StateCache, blk, cas, selfStr)
	if err != nil {
		return StateData{}, err
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("populateActiveTxnTable", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, populateActiveTxnTable: %f seconds", blk.BlockId(), elapsed*0.000001)

	// Update CDVS with any conflicts found in the active txns list
	nowMicro = time.Now().UnixMicro()
	updateConflicts(result.Bvs, result.Cdvs, detectedConflicts, cas.Consensus.Dag.Blocks())
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("updateConflicts", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, updateConflicts: %f seconds", blk.BlockId(), elapsed*0.000001)

	// If the block was created by this validator, determine what transactions
	// are valid to add to it and do so. Otherwise, simply verify that the
	// transactions already contained in it are valid / non-slashable.
	nowMicro = time.Now().UnixMicro()
	var validTxns []transaction.Transaction
	if blk.Creator() == cas.Self_ {
		sb := blk.(*block.SilvermintBlock)
		header := sb.LengthWithoutTransactions()
		available := cas.MAX_BLOCK_DATA - uint64(header)

		// Only pop transactions from the queue while there's enough space in
		// the block for them (and there are still transactions in the queue)
		var ind uint16 = 0
		for available > cas.Consensus.Waiting.NextLength() &&
			cas.Consensus.Waiting.Size() > 0 {
			txn, length := cas.Consensus.Waiting.Pop()
			// Only add the queued transaction to the block if:
			// 1) It's valid
			// 2) It doesn't conflict with any other active transaction
			if result.Extra.Ledger.IsValid(txn) == nil {
				dup, err := activeTxns.table.Insert(txn)
				if err != nil {
					log.Warningf("Couldn't insert transaction into LHT: %s",
						err.Error())
					break
				}
				// If it was reported as a dup when inserting into the LHT it
				// could be a false positive. Check it with verifyConflicts.
				if !dup {
					available -= length
					tid := ids.TransactionId{
						BlockId: ids.BlockId{
							ValId_:  blk.Creator().Id,
							Height_: blk.Height(),
						},
						Index: ind,
					}
					txn.SetId(tid)
					ind++
					sb.Transactions_ = append(sb.Transactions_, txn)
					validTxns = append(validTxns, txn)
				}
			}
		}
		// After we've finished adding transactions to the block, reset
		// the Data_ and Hash_ fields so that they'll be recomputed
		sb.Data_ = []byte{}
		sb.Hash_ = nil

		// The LHT used above is now populated with the parents' transactions
		// and the transactions from this block. Assign it as the table we'll
		// use for checking incoming transactions from the Client API. Re-use
		// the table that was previously there for our next BlockState call.
		prev := consensus.ActiveTxns.Table()
		consensus.ActiveTxns.Set(activeTxns)
		activeTxns = prev
	} else {
		// Validate transaction conflict domains. This checks that the block
		// creator is not attempting to add transactions to a CD that is already
		// populated.
		latest, _ := cas.StateCache.Get(cas.Consensus.Dag.Tip(cas.Self_.Id))
		slashableTxns, err := getSlashableTxns(activeTxns, blk, latest.Extra.Ledger)
		if err != nil {
			return StateData{}, err
		}
		// Validate transactions
		// Validating the transactions themselves is client-
		// dependent, so client needs to be a parameter. E.g. Pyr8
		// checks to see that the balance is sufficient.
		// Validate as of our latest ledger.
		validTxns = make([]transaction.Transaction, len(blk.Transactions()))
		copy(validTxns, blk.Transactions())

		// NB(chuck): Until we're actually slashing validators for trying to
		// populate an existing CD, we should at least discard the offending
		// transaction instead of sending it to consensus.
		validTxns = removeSlashableTxns(validTxns, slashableTxns)
	}

	// We no longer need the activeTxns/LHT for this BlockState calculation,
	// so launch a go-routine to zero it out
	go activeTxns.Reset(consensus.TableListener, selfStr)

	// Store only the valid transactions for the block in the StateData
	result.AddedTxns = validTxns

	// Only add this block to the BVS if it introduces one or more valid txns
	if len(result.AddedTxns) > 0 {
		vs := VS{}
		vs[blockCreator.Id] = status.UNCONTESTED
		result.Bvs[blk.BlockId()] = &vs
		err := ChangeTxnStatus(blockCreator, status.UNCONTESTED, result,
			cas.Self_, cas.Consensus.Dag.Blocks(), nil)
		if err != nil {
			return StateData{}, err
		}
	}

	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("validTransactions+", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, validTransactions+: %f seconds", blk.BlockId(), elapsed*0.000001)

	// Generate a BSNT for the uncontested transactions from the BVS
	nowMicro = time.Now().UnixMicro()
	bsnt, err := result.Bvs.ConvertToStatusTotals()
	if err != nil {
		return StateData{}, err
	}
	NumUnarchivedBlocks.Set(float64(len(bsnt)))
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	log.Debugf("StateAtBlock: %s updating %d unarchived blocks", blk.BlockId(), len(bsnt))
	ConsensusStateMicroVec.WithLabelValues("BlockConvertToStatusTotals", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, BlockConvertToStatusTotals: %f seconds", blk.BlockId(), elapsed*0.000001)

	// For each unarchived block, update result with the decision
	nowMicro = time.Now().UnixMicro()
	err = updateUnarchivedBlockStatus(cas, result, blk, blockCreator, bsnt)
	if err != nil {
		return StateData{}, err
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("updateBlockVs", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, updateBlockVs: %f seconds", blk.BlockId(), elapsed*0.000001)

	// Generate a CDTSNT from the CDVS for the contested transactions
	nowMicro = time.Now().UnixMicro()
	cdtsnt, err := result.Cdvs.ConvertToStatusTotals()
	if err != nil {
		return StateData{}, err
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("ContestedConvertToStatusTotals", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, ContestedConvertToStatusTotals: %f seconds",
		blk.BlockId(), elapsed*0.000001)

	// Sort the contested CDs in case there are sequential checknumbers
	nowMicro = time.Now().UnixMicro()
	orderedCDs := consensus.SortCDs(cdtsnt)
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	NumConflictDomains.Set(float64(len(orderedCDs)))
	ConsensusStateMicroVec.WithLabelValues("sortCDs", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s sortCDs: %f seconds for %d CDs",
		blk.BlockId(), elapsed*0.000001, len(orderedCDs))

	// For each contested conflict domain, update result with the decision
	nowMicro = time.Now().UnixMicro()
	err = updateContestedCDStatus(cas, result, blk, blockCreator, orderedCDs, cdtsnt, cas.Self_)
	if err != nil {
		return StateData{}, err
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("updateContestedVs", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, updateContestedVs: %f seconds", blk.BlockId(), elapsed*0.000001)

	// Process all finalized and archived transactions to the ledger
	nowMicro = time.Now().UnixMicro()
	if blk.Creator() == cas.Self_ {
		// Finish processing all transactions launched as go-routines before re-attempting the
		// ones that were unprocessable
		result.Extra.Ledger.Wait()
		elapsed = float64(time.Now().UnixMicro() - nowMicro)
		ConsensusStateMicroVec.WithLabelValues("WaitForTxns", selfStr).Add(elapsed)
		log.Debugf("StateAtBlock: %s, WaitForTxns: %f seconds", blk.BlockId(), elapsed*0.000001)
		// Re-attempt the unprocessable ones
		nowMicro = time.Now().UnixMicro()
		err := result.Extra.Ledger.ProcessQueued()
		if err != nil {
			// TODO(chuck): If we got an error because a transaction was
			// invalid, we should slash the validator, maybe stop processing
			// the rest of its transactions, and then recover so that we
			// process the other finalized transactions.
			// For now don't return the error, just reject the transaction(s)
			// and print it.
			log.Error(err.Error())
		}
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("processTxns", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, processTxns: %f seconds", blk.BlockId(), elapsed*0.000001)

	// Validate registrations.
	// Validating registrations is client-dependent, so
	// needs to be a parameter. E.g. Pyr8 requires
	// a bonding transaction before accepting a registration.
	nowMicro = time.Now().UnixMicro()
	regs = ValidRegistrations(blk.Registrations(), result.Extra)

	// Add valid registrations to validators
	for reg := range regs {
		result.Validators[reg] = true
	}

	result.Ftm = ftm(result.Validators)

	stateDataCache.Set(blk.BlockId(), result)
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("post", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, post: %f seconds", blk.BlockId(), elapsed*0.000001)

	elapsed = float64(time.Now().UnixMicro() - start)
	ConsensusStateMicroVec.WithLabelValues("BlockState", selfStr).Add(elapsed)
	log.Debugf("INFO: StateAtBlock: %s, (%d@%d), took %f seconds to compute state",
		blk.BlockId(), blk.Creator().Id, blk.Height(), elapsed*0.000001)

	return result, nil
}

// Casanova#163 TODO: detect equivocation
func reducer(parentStates []StateData, bid ids.BlockId) StateData {
	// Casanova#163 TODO(stay): To find equivocation, find block issued by
	// block.creator among ancestors or self of each parent,
	// then sort by height. There should not be two of the
	// same height, and there should be a path via parent
	// links passing through all of them.
	nowMicro := time.Now().UnixMicro()
	result := preallocateStateData(parentStates[0])
	elapsed := float64(time.Now().UnixMicro() - nowMicro)
	log.Debugf("StateAtBlock: %s, reducer: preallocateStateData: %f seconds", bid, elapsed*0.000001)
	ReducerMicroVec.WithLabelValues("preallocateStateData").Add(elapsed)

	nowMicro = time.Now().UnixMicro()
	// Merge the validators data from all parents
	for i := range parentStates {
		for val := range parentStates[i].Validators {
			result.Validators[val] = true
		}
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	log.Debugf("StateAtBlock: %s, reducer: mergeValidators: %f seconds", bid, elapsed*0.000001)
	ReducerMicroVec.WithLabelValues("mergeValidators").Add(elapsed)

	// Determine which of the provided StateData has the max height
	// for each validator.
	// Assign the largest to the result.Heights for each validator.
	nowMicro = time.Now().UnixMicro()
	maxHeightParents := [validator.N_MAX_VALIDATORS]int{}
	for reg := range result.Validators {
		for i := range parentStates {
			if result.Height[reg] < parentStates[i].Height[reg] {
				result.Height[reg] = parentStates[i].Height[reg]
				maxHeightParents[reg.Id] = i
			}
		}
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	log.Debugf("StateAtBlock: %s, reducer: getMaxHeightParents: %f seconds", bid, elapsed*0.000001)
	ReducerMicroVec.WithLabelValues("getMaxHeightParents").Add(elapsed)

	// Create an empty VS for each unique BVS entry across all StateData
	nowMicro = time.Now().UnixMicro()
	for i := range parentStates {
		for bid := range parentStates[i].Bvs {
			if _, ok := result.Bvs[bid]; !ok {
				result.Bvs[bid] = &VS{}
			}
		}
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	log.Debugf("StateAtBlock: %s, template block had BVS length: %d", bid, len(parentStates[0].Bvs))
	log.Debugf("StateAtBlock: %s, reducer: preallocateBVS: %f seconds", bid, elapsed*0.000001)
	ReducerMicroVec.WithLabelValues("preallocateBVS").Add(elapsed)

	// For each BVS entry, and for each registered validator, populate the VS
	// with statuses from the parent StateData that had the largest height for
	// that validator
	nowMicro = time.Now().UnixMicro()
	for bid, resultvs := range result.Bvs {
		for val := range result.Validators {
			maxHeightParent := parentStates[maxHeightParents[val.Id]]
			if _, ok := maxHeightParent.Bvs[bid]; ok {
				resultvs[val.Id] = maxHeightParent.Bvs[bid][val.Id]
			}
		}
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	log.Debugf("StateAtBlock: %s, reducer: populateBVS: %f seconds", bid, elapsed*0.000001)
	ReducerMicroVec.WithLabelValues("populateBVS").Add(elapsed)

	// Create an empty VS for each unique CDVS entry across all StateData
	nowMicro = time.Now().UnixMicro()
	for i := range parentStates {
		cur := parentStates[i]
		for cd := range cur.Cdvs {
			if _, ok := result.Cdvs[cd]; !ok {
				result.Cdvs[cd] = &ContestedVS{}
			}
		}
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	log.Debugf("StateAtBlock: %s, reducer: preallocateCDVS: %f seconds", bid, elapsed*0.000001)
	ReducerMicroVec.WithLabelValues("preallocateCDVS").Add(elapsed)

	// For each CDVS entry, and for each registered validator, populate the VS
	// with statuses from the parent StateData that had the largest height for
	// that validator
	nowMicro = time.Now().UnixMicro()
	for cd, resultvs := range result.Cdvs {
		for val := range result.Validators {
			maxHeightParent := parentStates[maxHeightParents[val.Id]]
			if _, ok := maxHeightParent.Cdvs[cd]; ok {
				resultvs[val.Id] = maxHeightParent.Cdvs[cd][val.Id]
			}
		}
	}
	elapsed = float64(time.Now().UnixMicro() - nowMicro)
	log.Debugf("StateAtBlock: %s, reducer: populateCDVS: %f seconds", bid, elapsed*0.000001)
	ReducerMicroVec.WithLabelValues("populateCDVS").Add(elapsed)

	result.Ftm = ftm(result.Validators)

	return result
}

// The idea of this function is to reduce the provided list of parentBlocks by
// removing parents whose StateData was already merged into some other parent's
// StateData. For example:
//
//	Block P parents: [GP1, GP2, GP3, ..., GPN]
//	Block B parents: [P,   GP2, GP3, ..., GPN]
//
// Then in computing block B's state, the parents shared with block P don't
// need to be merged into the StateData computed for block B, because their
// information is already contained in the merged StateData that was computed
// for B's parent P. Thus, in the above situation, the parents list for block B
// can be shrunk down to simply [P] for the purposes of computing B's StateData.
func shrinkParentList(
	parentBlocks [validator.N_MAX_VALIDATORS]block.Block,
	vals map[validator.Validator]bool,
) (
	[validator.N_MAX_VALIDATORS]block.Block,
	int,
) {
	// Determine which parent block shares the most of its parents with the
	// provided list of parentHashes, and set it as the optimal parent.
	var optimalParent block.Block
	sharedParents := 0
	for vOuter := range vals {
		if parentBlocks[vOuter.Id] != nil {
			n := 0
			grandparents := parentBlocks[vOuter.Id].Parents()
			for vInner := range vals {
				if parentBlocks[vInner.Id] != nil {
					if grandparents[vInner.Id] >= parentBlocks[vInner.Id].Height() {
						n++
					}
				}
			}
			if n >= sharedParents {
				sharedParents = n
				optimalParent = parentBlocks[vOuter.Id]
			}
		}
	}
	// Take the optimal parent and remove all of its parents from
	// the list of parentBlocks
	reduction := 0
	optimalGrandparents := optimalParent.Parents()
	for val := range vals {
		if parentBlocks[val.Id] != nil {
			parentHeight := optimalGrandparents[val.Id]
			if parentHeight == parentBlocks[val.Id].Height() {
				reduction++
				parentBlocks[val.Id] = nil
			}
		}
	}

	return parentBlocks, reduction
}

func GetParentStates(
	cas *Casanova,
	blk block.Block,
	regs validator.ValidatorSet,
) ([]StateData, StateData, error) {

	parentStates := make([]StateData, 0)

	// Load all the block parents
	phs := blk.Parents()
	pbs := [validator.N_MAX_VALIDATORS]block.Block{}
	nParents := 0
	genesisIsParent := false
	var prevParentState StateData
	for val := range regs {
		pid := phs.BlockId(val.Id)
		pb, pbok := cas.Consensus.Dag.Get(pid)
		if !pbok {
			return nil, prevParentState, fmt.Errorf("Parent height not in "+
				"Dag: block=%s parent=%s", blk.BlockId(), pid)
		}
		if val == cas.Self_ {
			// We return the parent StateData from THIS validator for ALL
			// blocks to ensure their StateData has a pointer to a ledger.
			var err error
			prevParentState, err = cas.StateCache.Get(pid)
			if err != nil {
				return nil, prevParentState, fmt.Errorf("Couldn't find "+
					"parent %s for block %s in StateCache",
					pid, blk.BlockId())
			}
		}
		if !block.IsGenesis(pb) {
			pbs[val.Id] = pb
			nParents++
		} else if !genesisIsParent {
			// Only add the genesis block to the parents list once (if at all)
			genesisIsParent = true
			pbs[val.Id] = pb
			nParents++
		}
	}

	// Initialization values
	nShrunk := nParents
	// Iterate until we can't reduce the parents list further
	for nShrunk != 0 && nParents != 1 {
		pbs, nShrunk = shrinkParentList(pbs, regs)
		nParents -= nShrunk
	}

	for _, pb := range pbs {
		if pb != nil {
			state, err := cas.StateCache.Get(pb.BlockId())
			if err != nil {
				return nil, StateData{}, fmt.Errorf("Parent StateData not found in StateCache,"+
					" block=%s, parent=%s", blk.BlockId(), pb.BlockId())
			}
			parentStates = append(parentStates, state)
		}
	}

	return parentStates, prevParentState, nil
}

// Provided a Transaction txn, the Block, blk, that introduced txn, and the BVS
// for some block whose StateData is being computed, returns a status that is
// the same as blk's status in the BVS, but with status.Txn set to txn.
func adoptBlockStatusForTxn(
	blk block.Block,
	txn transaction.Transaction,
	bvs BVS,
) status.ContestedStatus {
	blkStatus := bvs[blk.BlockId()]
	blkCreatorStatus := blkStatus[blk.Creator().Id]
	var txnStatus status.ContestedStatus
	// TODO(chuck): I'm not sure if checks for FINALIZED/ARCHIVED are needed.
	// If a Block was FINALIZED/ARCHIVED and this validator either: 1) Didn't
	// have a CDVS entry for the transaction's conflict domain, or 2) Didn't
	// have a status for that conflict domain, then the transaction should have
	// been FINALIZED/ARCHIVED with the Block. In that case, I'm not sure if
	// it's possible for the CD to become contested without somebody getting
	// slashed... maybe in some weird partition case. So in the interest of
	// BlockState not failing but instead hopefully continuing on to slash the
	// offending validator or at least reject the non-FINALIZED/ARCHIVED
	// transaction, I assign the relevant status rather than returning an error.
	switch blkCreatorStatus {
	case status.UNCONTESTED:
		txnStatus = status.Uncontested{TxnId_: txn.TxnId()}
	case status.UNCONTESTEDFTM:
		txnStatus = status.UncontestedFTM{TxnId_: txn.TxnId()}
	case status.FINALIZED:
		txnStatus = status.Finalized{TxnId_: txn.TxnId()}
	case status.ARCHIVED:
		txnStatus = status.Archived{TxnId_: txn.TxnId()}
	}
	return txnStatus
}

// Given a list of lists of transactions that conflict with each other, creates
// an entry to the CDVS (if one doesn't exist) for each conflicted CD. Updates
// the corresponding VS by adopting the Block status from the BVS for the Block
// that introduced each conflicting transaction.
func updateConflicts(
	bvs BVS,
	cdvs CDVS,
	detectedConflicts map[transaction.CD][]transaction.Transaction,
	cache *dag.BlockCache,
) {
	for _, conflict := range detectedConflicts {
		conflictedCD := conflict[0].ConflictDomain()
		// If the conflicted CD exists already, add any new transactions to it
		// using V: txn's creator, S: the containing block's status.
		if vs, ok := cdvs[conflictedCD]; ok {
			for _, txn := range conflict {
				blk, _ := cache.Get(txn.TxnId().BlockId)
				val := blk.Creator()
				// If there's no status, create it with the status that the
				// containing block had.
				if vs[val.Id] == nil {
					txnStatus := adoptBlockStatusForTxn(blk, txn, bvs)
					vs[val.Id] = txnStatus
				} else {
					// TODO(chuck): Figure out if in every case it's correct to do
					// nothing here... I think it is. The creator of a transaction
					// that contends with another transaction for a conflict domain
					// shouldn't already have a status for that conflict domain. If
					// it did, it would mean it was knowingly adding a transaction
					// to an existing CD and should be slashed (elsewhere).
					// If the status already exists for this transaction's creator,
					// assume that this is an old (already known) conflict.
				}
			}
		} else {
			// If there's no conflicted CD entry for this, we create a new one
			// with the creator status for the containing blocks.
			vs := ContestedVS{}
			for _, txn := range conflict {
				blk, _ := cache.Get(txn.TxnId().BlockId)
				val := blk.Creator()
				txnStatus := adoptBlockStatusForTxn(blk, txn, bvs)
				vs[val.Id] = txnStatus
			}
			cdvs[conflictedCD] = &vs
		}
	}
}

// Given a list of the "active" (unarchived) transactions and a block that is
// being built on them, verifies that the transactions contained in blk aren't
// attempting to populate an extant conflict domain. Returns a list of any such
// transactions that would lead to a slashing offence for blockCreator.
func getSlashableTxns(
	activeTxns *TableWithIndex,
	blk block.Block,
	ledger transaction.Ledger,
) ([]transaction.Transaction, error) {
	slashableTxns := []transaction.Transaction{}
	for _, txn := range blk.Transactions() {
		dup, err := activeTxns.table.Insert(txn)
		if err != nil {
			return nil, err
		}
		if dup {
			slashableTxns = append(slashableTxns, txn)
			log.Debugf("StateAtBlock: block %s with creator %d attempted to populate an existing "+
				"conflict domain, txn was: %x with CD %v", blk.BlockId().String(),
				blk.Creator().Id, txn.Hash(), txn.ConflictDomain())

			// delete(r.Validators, blockCreator)
			//
			// Casanova#170 TODO(stay) do we really want to return r here?
			// Where do we slash validators that build on blocks
			// issued by slashed validators?
			//
			// TxnsRejected.WithLabelValues("CONSENSUS-EXISTING_CD").Add(float64(len(blk.Transactions())))
			// dag.DagBlocksRejected.Inc()
			// return maybe.Just[StateData[U]]{Data: r}
		} else if txn.IsSlashable() {
			slashableTxns = append(slashableTxns, txn)
			log.Debugf("StateAtBlock: block %s with creator %d attempted to add a slashable txn, "+
				"txn was: %x with CD %v", blk.BlockId().String(),
				blk.Creator().Id, txn.Hash(), txn.ConflictDomain())
		}
	}
	return slashableTxns, nil
}

// Removes the provided slashableTxns from the provided list of validTxns.
func removeSlashableTxns(
	validTxns []transaction.Transaction,
	slashableTxns []transaction.Transaction,
) []transaction.Transaction {
	for _, slashableTxn := range slashableTxns {
		for i, validTxn := range validTxns {
			if slashableTxn.Hash() == validTxn.Hash() {
				validTxns[i] = validTxns[len(validTxns)-1]
				validTxns = validTxns[:len(validTxns)-1]
				break
			}
		}
	}
	return validTxns
}

// Updates statuses for all Blocks contained in the bsnt
// as of Block blk, created by Validator creator, with StateData sd.
func updateUnarchivedBlockStatus(
	cas *Casanova,
	sd StateData,
	blk block.Block,
	creator validator.Validator,
	bsnt BlkStatusNumTotal,
) error {
	for activeBlockId, snt := range bsnt {
		vs := sd.Bvs[activeBlockId]
		creatorStatus := vs[creator.Id]
		err := updateVsUncontested(cas, vs, activeBlockId, snt, creatorStatus, creator, sd)
		if err != nil {
			return err
		}
	}
	return nil
}

// Updates the statuses for all contested CDs in cdtsnt
// as of Block blk, created by Validator creator, with StateData sd.
func updateContestedCDStatus(
	cas *Casanova,
	sd StateData,
	blk block.Block,
	creator validator.Validator,
	orderedCDs []transaction.CD,
	cdtsnt CDTxnStatusNumTotal,
	self validator.Validator,
) error {
	for _, cd := range orderedCDs {
		tsnt := cdtsnt[cd]
		vs := sd.Cdvs[cd]
		creatorStatus := vs[creator.Id]
		err := updateVsContested(vs, tsnt, creatorStatus, creator, blk, sd, cas)
		if err != nil {
			return err
		}
	}
	return nil
}
