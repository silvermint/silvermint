// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"errors"
	"fmt"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/blockdb"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/dag"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/status"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/utils"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const SSLEDGER_FOLDER_NAME = "ledgers"

var (
	nTxnsReceived = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_txns_received_total",
		Help: "Number of transactions received by the Casanova API.",
	})
	TxnsSucceeded = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_dag_txns_succeeded_total",
		Help: "Number of transactions that were completed successfully.",
	})
	ParentsBytes = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_parents_bytes_total",
		Help: "Number of bytes used for DAG parents information across all blocks" +
			"produced by this node.",
	})
	TxnsTotalTimeToFinalize = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_txns_finalization_time_total",
		Help: "Total time waiting to finalize for each transaction.",
	})
	TxnsTotalFinalized = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_txns_finalized_total",
		Help: "Total transactions finalized.",
	})
	DeletableTxns = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "deletable_txns",
		Help: "Total deletable transactions found.",
	})
	BlocksProduced = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_blocks_produced_total",
		Help: "Total blocks produced.",
	})
	TxnsProduced = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "casanova_txns_produced_total",
		Help: "Total transactions produced.",
	})
	CasanovaMicroVec = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "casanova_usec_total",
		Help: "Total microseconds spent in Casanova processes.",
	},
		[]string{"function"},
	)
	AddedBlockSource = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "casanova_added_block_source",
		Help: "Total blocks added by source.",
	},
		[]string{"source"},
	)
)

func init() {
	prometheus.MustRegister(BlocksProduced)
	prometheus.MustRegister(DeletableTxns)
	prometheus.MustRegister(CasanovaMicroVec)
	prometheus.MustRegister(nTxnsReceived)
	prometheus.MustRegister(ParentsBytes)
	prometheus.MustRegister(TxnsProduced)
	prometheus.MustRegister(TxnsSucceeded)
	prometheus.MustRegister(TxnsTotalFinalized)
	prometheus.MustRegister(TxnsTotalTimeToFinalize)
	prometheus.MustRegister(AddedBlockSource)

	// TODO(leaf): Set this back to 65534 when we're ready to scale out.
	pflag.Uint64("max_block_size", 256000, "Max block size [bytes].")
	pflag.String("cycle_duration", "1s", "Block cycle duration.")
	pflag.Bool("save_blocks", true, "Enable/disable saving blocks to disk.")
	pflag.Bool("save_state", true, "Enable/disable saving block state to disk.")
}

type CasanovaExtra struct {
	Ledger transaction.Ledger
}

func (ce CasanovaExtra) Length() uint64 {
	if ce.Ledger == nil {
		return 0
	}
	return ce.Ledger.Length()
}

type CatchupRequestor func(ids.BlockId)
type CatchupRequestorFactory func(*Casanova) CatchupRequestor

type Casanova struct {
	ValidatorId uint16
	// Casanova#150 TODO(leaf): We need to enforce that the Validator Ids in the
	// ValidatorSet are unique. We don't do that at the moment and
	// eventually it'll be a DAG property, but we should keep an eye
	// on it.
	Self_     validator.Validator
	Wallet    crypto.Wallet
	Consensus *Consensus

	GenesisExtra CasanovaExtra
	StateCache   StateCache

	catchupRequestor CatchupRequestor
	MAX_BLOCK_DATA   uint64
	InProgress       InProgressMap

	SSLedgers blockdb.SlabDir
}

func (c *Casanova) Self() validator.Validator { return c.Self_ }

func (cas *Casanova) Add(blk block.Block) bool {

	log.Debugf("Casanova: block received: validator=%d timestamp=%s blockid=%s",
		blk.Creator().Id, blk.Time().UTC(), blk.BlockId())

	// Verify the block signature
	wal := crypto.NewSilvermintWallet(blk.Pubkey().Address())
	bdat, err := blk.MarshalBinary()
	if err != nil {
		log.Errorf("unable to get block bytes: %s", err.Error())
		return false
	}
	if !wal.Verify(bdat) {
		log.Errorf("binary decoded correctly, but signature is invalid wal: %x data: %x",
			wal.Public(), bdat)
		return false
	}

	return cas.AddBlockFromSource(blk, "received")
}

func (cas *Casanova) AddBlockFromSource(blk block.Block, source string) bool {
	added := false
	// NB(chuck): Don't set a block as in-progress until InterpretDeltas
	// succeeds. Otherwise the in-progress status during an unsuccessful
	// InterpretDeltas could foil an otherwise successful call to
	// InterpretDeltas (for an edge case where the unsuccesful call adds
	// the block to pending, but before the in-progress status is removed the
	// missing parent is added to the DAG, resulting in this function being
	// called again from the pending queue).
	if !cas.Consensus.Dag.Has(blk.BlockId()) {
		if cas.Consensus.Dag.InterpretDeltas(blk) {
			if cas.InProgress.SetInProgress(blk.BlockId()) {
				defer cas.InProgress.RemoveInProgress(blk.BlockId())
				// For blocks that aren't created by us or loaded from disk,
				// this is the point at which we can save their parent info.
				// NB(chuck): It is important not to (attempt to) save parent
				// info for created blocks here, as the hash for the block may
				// change during BlockState.
				if viper.GetBool("save_blocks") && source != "created" {
					go cas.Consensus.Dag.Blocks().Save(blk)
				}
				result, err := cas.BlockState(blk)
				if err != nil {
					log.Errorf("AddBlockFromSource(): BlockState returned error: %s",
						err.Error())
					dag.DagBlocksRejected.Inc()
					return false
				}
				added = cas.Consensus.Dag.Add(blk)
				if viper.GetBool("save_state") {
					// StateData is immutable, so now that it's computed we'll write it out.
					go cas.StateCache.Save(result)
				}
			}
		}
	} else {
		// If we receive a Block for which there's already a BlockId in the DAG,
		// it could be equivocation. Check for this first.
		// TODO(chuck): If we can't find/load the block for some reason, we're just trusting that
		// they're not attempting to equivocate (although we don't do anything with the received
		// block except send it to the catchup listener). OK?
		dblk, ok := cas.Consensus.Dag.Get(blk.BlockId())
		if ok {
			if dblk.Hash() != blk.Hash() {
				log.Errorf("Validator %d equivocated by issuing block %x with ID %s"+
					" when we've already added block %x that had ID %s",
					blk.Creator().Id, blk.Hash(), blk.BlockId().String(), dblk.Hash(),
					dblk.BlockId().String())
				// TODO(chuck): Slash.
			}
		} else {
			dblk = blk
		}
		// NB(chuck): We may have requested a catchup block that was already in
		// our DAG (due to an edge case where InterpretDeltas sees a parent
		// missing as that parent is being added to the DAG). Ensure it's
		// removed from the list of missing blocks.
		// We can't send the received block to the channel since it doesn't
		// have the height set, so we load the copy from the DAG.
		log.Debugf("Received blk %s but it was already in the DAG, sending to catchup listener",
			blk.BlockId())
		go func() { cas.Consensus.Dag.CatchUpChannel() <- dblk }()
	}
	if added {
		AddedBlockSource.WithLabelValues(source).Inc()
	}
	return added
}

func (cas *Casanova) Get() block.Block {
	start := time.Now()
	log.Debugf("Trying to generate new block in Casanova...")
	nowMicro := time.Now().UnixMicro()

	b := new(block.SilvermintBlock)
	b.Creator_ = cas.Self()
	b.Timestamp_ = time.Now().UnixMilli()

	// Registrations
	b.Registrations_ = []validator.Validator{}
	for v := range validator.GetGlobalRegistrations().Active() {
		b.Registrations_ = append(b.Registrations_, v)
	}

	// Tips
	parentHeights := make([]string, 0)
	b.Parents_ = block.Heights{}
	for _, reg := range b.Registrations_ {
		tid := cas.Consensus.Dag.Tip(reg.Id)
		b.Parents_[reg.Id] = tid.Height()
		parentHeights = append(parentHeights, tid.String())
	}

	// Height
	prevBlkId := cas.Consensus.Dag.Tip(cas.Self_.Id)
	b.Height_ = prevBlkId.Height() + 1

	// Parents
	prevBlk, _ := cas.Consensus.Dag.Get(prevBlkId)
	b.Deltas_, _ = b.Parents_.Sub(prevBlk.Parents())

	// Validator / Wallet
	b.Validator_ = b.Creator_.Id
	copy(b.Pubkey_[:], b.Creator_.Pubkey.Address())

	ParentsBytes.Add(float64(b.ParentsLength()))
	ParentsBytes.Add(float64(b.HeaderLength()))

	BlocksProduced.Inc()

	// TODO(leaf): Talk to Mike about how to handle this. This method really
	// can't fail.
	if !cas.AddBlockFromSource(b, "created") {
		log.Error("Unable to compute StateData / add to DAG for created block")
		finMicro := time.Now().UnixMicro()
		CasanovaMicroVec.WithLabelValues("Get").Add(float64(finMicro - nowMicro))
		return b
	}

	b.Length_ = b.Length()

	log.Debugf("INFO: Generated new block %s [%d bytes] with parents: %s in %f seconds",
		b.BlockId(), b.Length(), strings.Join(parentHeights, ", "), time.Since(start).Seconds())

	// NB(chuck): We have to ensure this block and its parents have been saved
	// to disk before sending it to anyone else so that we won't equivocate on
	// a crash / reload. We must do this after computing StateData due to the
	// block's transactions / bytes / hash being mutated by that function.
	bdat, err := b.MarshalSignedBinary(cas.Wallet)
	if err != nil {
		log.Errorf("Unable to MarshalBinary for created block: %s", err.Error())
	}
	b.Data_ = bdat
	if viper.GetBool("save_blocks") {
		cas.Consensus.Dag.Blocks().Save(b)
	}

	TxnsProduced.Add(float64(len(b.Transactions_)))
	finMicro := time.Now().UnixMicro()
	CasanovaMicroVec.WithLabelValues("Get").Add(float64(finMicro - nowMicro))
	return b
}

func ChangeTxnStatus(
	vtor validator.Validator,
	txnStatus status.Status,
	state StateData,
	self validator.Validator,
	cache *dag.BlockCache,
	txns ...transaction.Transaction,
) error {
	if txnStatus == status.FINALIZED && vtor == self && len(txns) > 0 {
		// Only process a transaction if this validator is the blk creator
		ledger := state.Extra.Ledger
		creatorBlk, ok := cache.Get(txns[0].TxnId().BlockId)
		if !ok {
			err := fmt.Errorf("unable to get transaction %s from Block cache",
				txns[0].TxnId().String())
			log.Errorln(err.Error())
			return err
		}
		// Don't let these block BlockState
		// TODO(chuck): Make these calls less ugly.
		ledger.(*transaction.IncrLedger).Wg.Add(2)
		go ledger.QueueFinalized(txns...)
		go ledger.(*transaction.IncrLedger).UpdateTxnLog(txns...)

		nowMillis := time.Now().UnixMilli()
		blkMillis := creatorBlk.Timestamp()
		if blkMillis >= nowMillis {
			txnHashes := []string{}
			for _, txn := range txns {
				txnHashes = append(txnHashes, fmt.Sprintf("%x", txn.Hash()))
			}
			log.Warningf("Instantaneous or negative finalization time for "+
				"transaction %x is impossible.", strings.Join(txnHashes, ", "))
		}
		finalMillis := nowMillis - blkMillis
		log.Debugf("Transaction(s) for block %s finalized in %d milliseconds.",
			txns[0].TxnId().BlockId.String(), finalMillis)
		nTxns := float64(len(txns))
		totalTime := nTxns * float64(finalMillis)
		if totalTime < 0 {
			totalTime = 0
		}
		TxnsTotalTimeToFinalize.Add(totalTime)
		TxnsTotalFinalized.Add(nTxns)

		TxnsSucceeded.Add(nTxns)
	}
	return nil

}

func (cas *Casanova) AddTxn(data []byte) error {
	nowMicro := time.Now().UnixMicro()
	nTxnsReceived.Inc()

	txn, err := transaction.UnmarshalTxn(data)
	if err != nil {
		errMessage := fmt.Sprintf("AddTxn: transaction rejected: unable to unmarshal data: %s",
			err.Error())
		log.Errorln(errMessage)
		TxnsRejected.WithLabelValues("ADDTXN-BAD_UNMARSHAL").Inc()
		return fmt.Errorf(errMessage)
	}

	// Verify transaction is valid before adding it
	latest, _ := cas.StateCache.Get(cas.Consensus.Dag.Tip(cas.Self_.Id))
	err = latest.Extra.Ledger.IsValid(txn)
	if err != nil {
		TxnsRejected.WithLabelValues(err.Error()).Inc()
		return fmt.Errorf("AddTxn: transaction %x rejected: %s", txn.Hash(), err.Error())
	}
	// Verify the transaction isn't slashable before adding it
	if txn.IsSlashable() {
		TxnsRejected.WithLabelValues("SLASHABLE").Inc()
		return fmt.Errorf("AddTxn: transaction %x rejected: slashable", txn.Hash())
	}
	// TODO(chuck): This is kinda hokey, but due to changes in what goes into the CDVS
	// it's a straightforward way to ensure we aren't populating an existing conflict domain
	vsAlreadyThere := cas.Consensus.ActiveTxns.CDExists(txn)
	if vsAlreadyThere {
		TxnsRejected.WithLabelValues("ADDTXN-EXISTING_CD").Inc()
		msg := fmt.Sprintf("AddTxn: transaction %x rejected: Conflict Domain already populated: %v",
			txn.Hash(), txn.ConflictDomain())
		log.Warningln(msg)
		return fmt.Errorf(msg)
	}

	cas.Consensus.Waiting.Enqueue(txn)
	TxnsPending.Inc()
	CasanovaMicroVec.WithLabelValues("AddTxn").Add(float64(time.Now().UnixMicro() - nowMicro))

	return nil
}

func (cas *Casanova) LastBlock() (block.Block, error) {
	blk, blok := cas.Consensus.Dag.Get(cas.Consensus.Dag.Tip(cas.Self_.Id))
	if !blok {
		return nil, errors.New("no blocks in our local chain")
	}
	return blk, nil
}

func (cas *Casanova) CycleDuration() string {
	return viper.GetString("cycle_duration")
}

// If a transaction is found in the CDVS it means it is contested. The more
// likely scenario is that it will NOT be found in the CDVS. This requires
// considering several scenarios applicable to uncontested transactions:
//  1. The transaction was rejected before being added to a Block or
//     never actually existed in the first place (TRANSACTION NOT FOUND)
//  2. The transaction was submitted but not yet added to a block. Check
//     the pending queue. (Either PENDING or TRANSACTION NOT FOUND)
//  3. (Rare) The transaction was submitted and added to a Block, but that
//     Block's StateData has not yet been computed to add the transaction
//     to the TxnMap. (TRANSACTION NOT FOUND)
//  4. The transaction was submitted and added to a Block but the
//     transaction was found to be invalid (TRANSACTION NOT FOUND)
//  5. The transaction was submitted and added to a Block, and the
//     transaction was found to be valid. Check the Block's entry in the
//     CDVS and adopt the Block's status for this transaction.
//     (any UNCONTESTED status)
func (cas *Casanova) GetPaymentState(
	hash transaction.TransactionHash,
	checknum uint16,
	swal crypto.WalletAddress,
) (string, error) {
	var casstr string

	if cas.Consensus.Waiting.Has(hash) {
		casstr = "PENDING"
	} else {
		latest := cas.StateCache.Tip(cas.Self_.Id)
		ledger := latest.Extra.Ledger

		stat, ok := cas.Consensus.ActiveTxns.GetStatusFor(cas.Self_.Id,
			hash, swal, latest)
		if ok {
			switch stat {
			case status.UNSEEN:
				casstr = "TRANSACTION NOT FOUND"
			case status.UNCONTESTED:
				casstr = "UNCONTESTED"
			case status.UNCONTESTEDFTM:
				casstr = "UNCONTESTED-FTM"
			case status.CONTESTEDLEASTHASH:
				casstr = "CONTESTED-LEASTHASH"
			case status.CONTESTEDFTM:
				casstr = "CONTESTED-FTM"
			case status.FINALIZED:
				casstr = "FINALIZED"
			case status.ARCHIVED:
				casstr = "ARCHIVED"
			}
		} else {
			if ledger.CheckNumber(swal) < checknum {
				// TODO(chuck): It might have been rejected, or we might have
				// never seen it, or it might be in a block we're currently
				// computing state for. Might need finer-grain here.
				casstr = "TRANSACTION NOT FOUND"
			} else {
				// TODO(chuck): SOMETHING archived for this CD. There's no
				// guarantee it's the transaction that the user wants though,
				// because CDs can be populated by >1 transaction. We have to
				// find something better long-term.
				casstr = "ARCHIVED"
			}
		}
	}

	return casstr, nil
}

func (cas *Casanova) loadKeys() (crypto.Wallet, error) {
	valWalletDir := filepath.Join(viper.GetString("config_dir"), "keys")
	walletFile := filepath.Join(valWalletDir, "nodekey")
	wal, err := crypto.LoadWalletFromFile(walletFile)
	if err != nil {
		err := utils.MaybeCreateDir(valWalletDir, 0700)
		if err != nil {
			log.Fatalf("Error creating Validator wallet save directory: %s\n", err.Error())
			return nil, err
		}
		wal, err = crypto.NewWallet(walletFile, crypto.BASIC_WALLET)
		if err != nil {
			log.Fatalf("Error creating Validator wallet: %s\n", err.Error())
			return nil, err
		}
	}
	return wal, nil
}

func NewCasanova(
	id uint16,
	vals [validator.N_MAX_VALIDATORS]validator.Validator,
	catchupFactory CatchupRequestorFactory,
) (*Casanova, error) {

	maxBlockSize := viper.GetUint64("max_block_size")

	cas := new(Casanova)

	cas.ValidatorId = id
	cas.Self_ = vals[id]

	var err error
	cas.Wallet, err = cas.loadKeys()
	if err != nil {
		return nil, err
	}

	for vid, val := range vals {
		if val.IsActive() {
			validator.GetGlobalRegistrations().Set(uint16(vid), val)
		}
	}

	// Create slabdir for SSLedgers
	save_dir := viper.GetString("save_dir")
	sledgers, err := blockdb.NewSlabDir(filepath.Join(save_dir, fmt.Sprint(id), SSLEDGER_FOLDER_NAME))
	if err != nil {
		return nil, err
	}
	cas.SSLedgers = sledgers

	genesis := block.Genesis(vals)
	log.Debugf("Casanova initializing with Genesis Block %s", genesis.Hash().Short())

	cas.GenesisExtra = CasanovaExtra{
		Ledger: transaction.NewSSLedger(genesis.Height()),
	}

	cas.catchupRequestor = nil
	if catchupFactory == nil {
		log.Warningln("Casanova initialized without a Catchup Requestor Factory: " +
			"Catchup is disabled.")
	} else {
		cas.catchupRequestor = catchupFactory(cas)
	}

	cas.Consensus = Build(genesis, nil)

	cas.MAX_BLOCK_DATA = maxBlockSize
	cas.InProgress = InProgressMap{
		inprogress: map[ids.BlockId]bool{},
		mu:         *new(sync.RWMutex),
	}

	genesisState, err := cas.BlockState(genesis)
	if err != nil {
		err := fmt.Errorf("failed to load Genesis state: %s", err.Error())
		return nil, err
	}

	sc, err := NewStateCache(genesisState)
	if err != nil {
		return nil, err
	}
	cas.StateCache = *sc

	// Set our on-disk initial saved ledger as Genesis'
	ldat, err := genesisState.Extra.Ledger.MarshalBinary()
	if err != nil {
		err := fmt.Errorf("failed to marshal Genesis ledger binary: %s", err.Error())
		return nil, err
	}

	_, ssledgerslab := cas.SSLedgers.Current()
	ssledgerslab.Append(ldat)

	return cas, nil
}

func ValidRegistrations(regs validator.ValidatorSet, extra CasanovaExtra) validator.ValidatorSet {
	return regs
}

func (cas *Casanova) GetBlockParentFromValidator(bid ids.BlockId) block.Block {
	// TODO(leaf): Do nil ptr checks.
	blk, ok := cas.Consensus.Dag.Get(bid)
	if !ok {
		log.Warningf("Couldn't find block %s for catchup request", bid)
	}
	return blk
}

func Load(id uint16, vals [validator.N_MAX_VALIDATORS]validator.Validator,
	catchupFactory CatchupRequestorFactory,
) (*Casanova, error) {
	// Create an empty casanova instance and assign the easy stuff
	cas := new(Casanova)

	cas.ValidatorId = id
	cas.Self_ = vals[id]

	var err error
	cas.Wallet, err = cas.loadKeys()
	if err != nil {
		return nil, err
	}

	for vid, val := range vals {
		if val.IsActive() {
			validator.GetGlobalRegistrations().Set(uint16(vid), val)
		}
	}

	cas.catchupRequestor = nil
	if catchupFactory == nil {
		log.Warningln("Casanova initialized without a Catchup Requestor Factory: " +
			"Catchup is disabled.")
	} else {
		cas.catchupRequestor = catchupFactory(cas)
	}

	cas.MAX_BLOCK_DATA = viper.GetUint64("max_block_size")
	cas.InProgress = InProgressMap{
		inprogress: map[ids.BlockId]bool{},
		mu:         *new(sync.RWMutex),
	}

	// Load the SSLedger slab
	sdir := viper.GetString("save_dir")
	cas.SSLedgers, err = blockdb.NewSlabDir(filepath.Join(sdir, fmt.Sprint(id), SSLEDGER_FOLDER_NAME))
	if err != nil {
		return nil, err
	}

	// Load the BlockCache
	bc, err := dag.LoadBlockCache()
	if err != nil {
		return nil, err
	}

	// Load the StateCache
	sc, err := LoadStateCache()
	if err != nil {
		return nil, err
	}
	cas.StateCache = *sc

	// Get the tip block for this validator
	// TODO(chuck): If the tip is non-zero in height and we encounter an error
	// at any point to follow, we should crash. Otherwise we would start up
	// and start issuing blocks that equivocate on our earlier ones. The TODO
	// is to find a more graceful solution.
	tip := bc.Tip(id)
	if tip == nil {
		return nil, fmt.Errorf("no tip block found")
	}

	// The root is going to be the latest block with an SSLedger
	_, curr := cas.SSLedgers.Current()
	ind := curr.Index().Last() - 1
	lastSSL, err := cas.LoadSSLedgerAt(ind)
	if err != nil {
		if tip.Height() > 0 {
			log.Fatalf(err.Error())
		}
		return nil, err
	}
	rootHeights := block.Heights{}
	rootHeights[ind] = lastSSL.Height_

	// The rest of the roots are the root block's parents
	rid := ids.BlockId{
		ValId_:  id,
		Height_: lastSSL.Height_,
	}
	rootParents, err := bc.LoadParents(rid)
	if err != nil {
		if tip.Height() > 0 {
			log.Fatalf(err.Error())
		}
		return nil, err
	}
	for i := uint64(0); i < validator.N_MAX_VALIDATORS; i++ {
		if i == ind {
			continue
		}
		rootHeights[i] = rootParents[i]
	}

	// // Load the Genesis block
	genesis, err := bc.Load(block.GenesisId(cas.Self_.Id))
	if err != nil {
		return nil, err
	}

	// Determine the max and min height blocks to load
	maxHeights := tip.Parents()
	if maxHeights[cas.Self_.Id] != tip.Height() {
		maxHeights[tip.Creator().Id]++ // for the tip itself
	}
	minHeights := block.Heights{}
	cacheSize := viper.GetUint64("block_cache_size")
	nToLoad := cacheSize - 1
	for vid := 0; vid < validator.N_MAX_VALIDATORS; vid++ {
		if maxHeights[vid] >= nToLoad {
			minHeights[vid] = utils.Min(maxHeights[vid]-nToLoad, rootHeights[vid])
		} else {
			minHeights[vid] = 0
		}
	}

	// Get BlockIds for all blocks ranging from minHeights to maxHeights
	bids, err := maxHeights.IntervalBlockIds(minHeights, validator.GetGlobalRegistrations().Active())
	if err != nil {
		if tip.Height() > 0 {
			log.Fatalf(err.Error())
		}
		return nil, err
	}

	// Load the blocks from disk corresponding to the BlockIds above
	var roots [validator.N_MAX_VALIDATORS]block.Block
	var blks [validator.N_MAX_VALIDATORS][]block.Block
	for _, bid := range bids {
		blk, err := bc.Load(bid)
		if err != nil {
			if tip.Height() > 0 {
				log.Fatalf(err.Error())
			}
			return nil, err
		}
		if bid.Height() == rootHeights[bid.ValId()] {
			roots[bid.ValId()] = blk
		}
		blks[bid.ValId()] = append(blks[bid.ValId()], blk)
	}

	// Create a new DAG, loading all the blocks above to the cache
	d, err := dag.Load(genesis, roots, blks, bc)
	if err != nil {
		if tip.Height() > 0 {
			log.Fatalf(err.Error())
		}
		return nil, err
	}
	cas.Consensus = Build(genesis, d)

	// Load each cached block's StateData to the StateCache
	failed := map[block.Block]bool{}
	for vid := uint16(0); vid < validator.N_MAX_VALIDATORS; vid++ {
		for _, blk := range blks[vid] {
			bid := blk.BlockId()
			if block.IsGenesis(blk) {
				bid = block.GenesisId(vid)
			}
			sd, err := cas.StateCache.Load(bid)
			if err != nil {
				log.Warningf("Failed to load state for %s: %s",
					blk.BlockId().String(), err.Error())
				failed[blk] = true
			} else {
				cas.StateCache.Set(bid, sd)
			}
		}
	}

	// Some of the blocks' StateData may not have saved prior to the crash.
	// Recompute StateData for any such blocks.
	for len(failed) != 0 {
		anyAdded := false
		for blk := range failed {
			// NB(chuck): Don't need to add to the cache or save if successful;
			// BlockState should do that on its own
			_, err := cas.BlockState(blk)
			if err != nil {
				continue
			}
			anyAdded = true
			delete(failed, blk)
		}
		if !anyAdded {
			bids := []string{}
			for blk := range failed {
				bids = append(bids, blk.BlockId().String())
			}
			err = fmt.Errorf("unable to compute missing StateData for loaded blocks %s",
				strings.Join(bids, ", "))
			if tip.Height() > 0 {
				log.Fatalf(err.Error())
			}
			return nil, err
		}
	}

	// The latest ledger should always have an empty IncrLedger as its child.
	bid := tip.BlockId()
	sd, err := cas.StateCache.Get(bid)
	if err != nil {
		return nil, err
	}
	sd.Extra.Ledger.SetChild(transaction.NewIncrLedger(sd.Extra.Ledger))
	sd.Extra.Ledger.Child().SetParent(sd.Extra.Ledger)

	// Assign parent/child pointers for each ledger in the cache. When a height
	// is reached for which an SSLedger is available, use that instead of the
	// IncrLedger.
	for bid.Height() > minHeights[id] {
		sd, err := cas.StateCache.Get(bid)
		if err != nil {
			if tip.Height() > 0 {
				log.Fatalf(err.Error())
			}
			return nil, err
		}
		bid = bid.Prev()
		prev, err := cas.StateCache.Get(bid)
		if err != nil {
			if tip.Height() > 0 {
				log.Fatalf(err.Error())
			}
			return nil, err
		}
		if bid.Height() == lastSSL.Height_ {
			sd.Extra.Ledger.SetParent(lastSSL)
			lastSSL.SetChild(sd.Extra.Ledger)
			if bid.Height() > minHeights[id] {
				if ind > 0 {
					ind--
					lastSSL, err = cas.LoadSSLedgerAt(ind)
					if err != nil {
						if tip.Height() > 0 {
							log.Fatalf(err.Error())
						}
						return nil, err
					}
				} else {
					err = fmt.Errorf("SSLedger prior to index %d unavailable"+
						" (last SSLedger was at height %d)", ind, lastSSL.Height_)
					if tip.Height() > 0 {
						log.Fatalf(err.Error())
					}
					return nil, err
				}
			}
		} else {
			sd.Extra.Ledger.SetParent(prev.Extra.Ledger)
			prev.Extra.Ledger.SetChild(sd.Extra.Ledger)
		}
	}

	// The minHeight ledger will have a nil parent pointer once the loop above
	// completes. This is only allowed if that ledger was an SSLedger. If it was
	// an IncrLedger, continue loading ledgers and assigning parent/child
	// pointers until we reach the previous SSLedger.
	for bid.Height() > lastSSL.Height_ {
		sd, err := cas.StateCache.Load(bid)
		if err != nil {
			if tip.Height() > 0 {
				log.Fatalf(err.Error())
			}
			return nil, err
		}
		bid = bid.Prev()
		prev, _ := cas.StateCache.Load(bid)
		if bid.Height() == lastSSL.Height_ {
			sd.Extra.Ledger.SetParent(lastSSL)
			lastSSL.SetChild(sd.Extra.Ledger)
			break
		} else {
			sd.Extra.Ledger.SetParent(prev.Extra.Ledger)
			prev.Extra.Ledger.SetChild(sd.Extra.Ledger)
		}
	}

	// Update Consensus' walletTxns map with all active transactions
	tstate, err := cas.StateCache.Get(tip.BlockId())
	if err != nil {
		return nil, err
	}
	for bid := range tstate.Bvs {
		state, err := cas.StateCache.Get(bid)
		if err != nil {
			return nil, err
		}
		for _, txn := range state.AddedTxns {
			cas.Consensus.ActiveTxns.active.table.Insert(txn)
		}
	}

	// TODO(chuck): This does nothing to recreate the transaction cache.
	// That will have to be done once we have the on-disk storage figured out
	// and linked up. Until then, restarting a node will only work as long as
	// there haven't been any transactions.

	return cas, nil
}

// This loads the SSLedger at the specified index in the ssledgerSlab.
// The height of the ledger is unknown until it is loaded.
func (cas *Casanova) LoadSSLedgerAt(index uint64) (*transaction.SSLedger, error) {
	_, ssledgerSlab := cas.SSLedgers.Current()

	ssl := transaction.NewSSLedger(0)
	ssdata, err := ssledgerSlab.Get(index)
	if err != nil {
		return nil, err
	}
	err = ssl.UnmarshalBinary(ssdata)
	if err != nil {
		return nil, err
	}
	return ssl, nil
}
