// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"fmt"
	"math"
	"sort"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/dag"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/status"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
)

var (
	TxnsRejected = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "casanova_txns_rejected_total",
		Help: "Number of transactions that were rejected by the Casanova API.",
	},
		[]string{"reason"})
	TxnsPending = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "casanova_txns_pending",
		Help: "Number of transactions in the pending queue waiting to be added to a block.",
	})
)

func init() {
	prometheus.MustRegister(TxnsRejected)
	prometheus.MustRegister(TxnsPending)
	pflag.Uint16("lht_keybits", 22, "Keybits to use for the LinearHashTable data structure.")
	pflag.Int("num_lhts", 500, "Number of LHTs to make available for consensus.")
	pflag.Int("lht_pool_buffer", 100, "Buffer size for the LHT get/done channels.")
}

// transaction -> status kind -> number of validators with that status
// transaction -> "TOTAL" -> number of validators that know about the txn
// Assumes total number of validators less than 65536, which is roughly
// 25x the planned size.
type StatusNumTotal struct {
	StatusNum map[status.Status]uint16
	Total     uint16
}

type TxnStatusNumTotal map[ids.TransactionId]StatusNumTotal
type CDTxnStatusNumTotal map[transaction.CD]TxnStatusNumTotal
type BlkStatusNumTotal map[ids.BlockId]StatusNumTotal

type activeTxns struct {
	active *TableWithIndex
	mu     sync.RWMutex
}

func (ats *activeTxns) Table() *TableWithIndex {
	ats.mu.RLock()
	defer ats.mu.RUnlock()

	return ats.active
}

func (ats *activeTxns) Set(lht *TableWithIndex) {
	ats.mu.Lock()
	defer ats.mu.Unlock()

	ats.active = lht
}

// *activeTxns.CDExists checks to see if there's a match in the LHT corresponding to an active
// transaction from the provided Transaction's source wallet. If so, iterates over all the active
// transactions from that source wallet to determine if the txn's CD is already populated.
func (ats *activeTxns) CDExists(
	txn transaction.Transaction,
) bool {
	ats.mu.RLock()
	defer ats.mu.RUnlock()

	dups, err := ats.active.table.GetDups(txn)
	if err != nil {
		log.Errorf("Error checking transaction CD conflicts: %s", err.Error())
		return true
	}
	return len(dups) != 0
}

// *activeTxns.GetStatusFor checks if the active transaction table has an active transaction from
// the provided source wallet. If it does, it then checks the activeTxnsByWallet map to see if the
// TransactionHash matches one of that wallet's active transactions. If there is a match, it then
// checks the provided StateData's CDVS and BVS to get the status of that Transaction. Returns the
// status of the transaction (if it was found), and a boolean indicating whether it was found.
func (ats *activeTxns) GetStatusFor(
	myid uint16,
	hash transaction.TransactionHash,
	swal crypto.WalletAddress,
	latest StateData,
) (status.Status, bool) {
	ats.mu.RLock()
	defer ats.mu.RUnlock()

	if txn, ok := ats.active.table.GetTxn(swal.Uint64(), hash); ok {
		if _, ok := latest.Cdvs[txn.ConflictDomain()]; ok {
			// It's in the CDVS - contested
			// TODO(chuck): This probably isn't right. What if this is the status for the other
			// contested txn in this CD?
			return latest.Cdvs[txn.ConflictDomain()][myid].Kind(), true
		} else {
			if _, ok := latest.Bvs[txn.TxnId().BlockId]; ok {
				return latest.Bvs[txn.TxnId().BlockId][myid], true
			}
			// This would be a weird case... somehow the txn is in the active transaction
			// table, but there's no entry for it either in the CDVS or BVS. Still, handle it.
			return status.UNSEEN, false
		}
	}
	return status.UNSEEN, false
}

type TablePool struct {
	LHTs      []*dag.LinearHashTable
	Available map[int]bool
	Done      chan int
	Get       chan *TableRequest
	Close     chan bool
}

func NewTablePool() *TablePool {
	kbits := viper.GetUint16("lht_keybits")
	nTables := viper.GetInt("num_lhts")
	buffSize := viper.GetInt("lht_pool_buffer")
	pool := &TablePool{
		LHTs:      make([]*dag.LinearHashTable, nTables),
		Available: make(map[int]bool, nTables),
		Done:      make(chan int, buffSize),
		Get:       make(chan *TableRequest, buffSize),
		Close:     make(chan bool, 1),
	}
	for i := range pool.LHTs {
		var err error
		pool.LHTs[i], err = dag.NewLHT(kbits)
		if err != nil {
			log.Errorf("Unable to create LHT %d: %s", i, err.Error())
		} else {
			pool.Available[i] = true
		}
	}
	return pool
}

type TableWithIndex struct {
	id    int
	table *dag.LinearHashTable
}

func (twi *TableWithIndex) Reset(tp *TablePool, selfStr string) {
	nowMicro := time.Now().UnixMicro()
	twi.table.Reset()
	elapsed := float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("TableReset", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: TableReset: %f seconds", elapsed*0.000001)
	tp.Done <- twi.id
}

type TableRequest struct {
	resp chan *TableWithIndex
}

func (tp *TablePool) Listen() {
	for {
		select {
		case msg := <-tp.Done:
			tp.Available[msg] = true
		case msg := <-tp.Get:
			if len(tp.Available) == 0 {
				tp.Get <- msg
			} else {
				for i := range tp.Available {
					delete(tp.Available, i)
					msg.resp <- &TableWithIndex{
						id:    i,
						table: tp.LHTs[i],
					}
					break
				}
			}
		case <-tp.Close:
			return
		}
	}
}

type Consensus struct {
	// The dag of blocks
	Dag dag.Dag
	// List of transactions to add to next block
	Waiting TransactionQueue
	// Unarchived transactions known to this validator
	ActiveTxns *activeTxns
	// TableListener listens for requests for clean LHTs and provides them from the TablePool
	TableListener *TablePool
}

func Build(genesis block.Block, d dag.Dag) *Consensus {
	pool := NewTablePool()
	req := &TableRequest{
		resp: make(chan *TableWithIndex, 1),
	}
	go pool.Listen()

	pool.Get <- req
	active := &activeTxns{
		active: <-req.resp,
	}

	if d == nil {
		roots := [validator.N_MAX_VALIDATORS]block.Block{}
		for i := range roots {
			roots[i] = genesis
		}
		d = dag.Build(genesis, roots, nil)
	}

	return &Consensus{
		Dag: d,
		Waiting: TransactionQueue{
			txns: []transaction.Transaction{},
		},
		ActiveTxns:    active,
		TableListener: pool,
	}
}

func ftm(vs validator.ValidatorSet) uint16 {
	numFaulty := len(vs) / 3
	return uint16((len(vs) + numFaulty + 1) / 2)
}

func CasanovaGenesisState(vs validator.ValidatorSet, extra CasanovaExtra) StateData {
	heights := map[validator.Validator]uint64{}
	for vtor := range vs {
		heights[vtor] = 0
	}

	return StateData{
		Bvs:        BVS{},
		Cdvs:       CDVS{},
		AddedTxns:  []transaction.Transaction{},
		Validators: vs,
		Slashed:    validator.ValidatorSet{},
		Ftm:        ftm(vs),
		Height:     heights,
		Extra:      extra,
	}
}

// Only calls ChangeTxnStatus on the uncontested transactions associated with the finalized block
// IF AND ONLY IF: 1) the block for which we're computing state is one issued by this validator
// (since we only maintain ledgers for this validator) and 2) the status was FINALIZED for the
// block in question.
func ChangeBlockStatus(cas *Casanova,
	vtor validator.Validator,
	stat status.Status,
	state StateData,
	snt StatusNumTotal,
	activeBlockId ids.BlockId,
) error {
	// Don't do anything if the status is other than FINALIZED or the block that changed the status
	// to FINALIZED wasn't issued by this validator
	if vtor == cas.Self_ && stat == status.FINALIZED {
		blkState, _ := cas.StateCache.Get(activeBlockId)
		if len(state.Cdvs) == 0 {
			// If we have no contested transactions, don't bother making a copy of the AddedTxns
			// list and checking it against the CDVS
			return ChangeTxnStatus(vtor, stat, state, cas.Self_, cas.Consensus.Dag.Blocks(),
				blkState.AddedTxns...)
		} else {
			// Otherwise, only process transactions that aren't in our CDVS (uncontested)
			unarchivedTxns := make([]transaction.Transaction, 0, len(blkState.AddedTxns))
			for _, txn := range blkState.AddedTxns {
				if _, ok := state.Cdvs[txn.ConflictDomain()]; !ok {
					unarchivedTxns = append(unarchivedTxns, txn)
				}
			}
			return ChangeTxnStatus(vtor, stat, state, cas.Self_, cas.Consensus.Dag.Blocks(),
				unarchivedTxns...)
		}
	}
	return nil
}

// As of a block, Block A, updates the status for Block A's creator in vs,
// where vs is the Validator-Status map pertaining to some unarchived block,
// Block B.
// The status for B is a proxy for the status for all uncontested transactions
// introduced by Block B.
// "I" and "me" in comments below refer to the Block creator.
func updateVsUncontested(
	cas *Casanova,
	vs *VS,
	activeBlockId ids.BlockId,
	snt StatusNumTotal,
	creatorStatus status.Status,
	creator validator.Validator,
	r StateData,
) error {

	ftm := r.Ftm

	if creatorStatus == status.UNSEEN {
		// same as UNCONTESTED
		if snt.StatusNum[status.UNCONTESTED]+
			snt.StatusNum[status.UNCONTESTEDFTM]+
			snt.StatusNum[status.FINALIZED] >= ftm {
			stat := status.UNCONTESTEDFTM
			vs[creator.Id] = stat
			return ChangeBlockStatus(cas, creator, stat, r, snt, activeBlockId)
		} else {
			stat := status.UNCONTESTED
			vs[creator.Id] = stat
			return ChangeBlockStatus(cas, creator, stat, r, snt, activeBlockId)
		}
	} else {
		// Usually we'd switch on creatorStatus.(type), but since
		// Kind() already exists, we use it
		switch creatorStatus {
		case status.ARCHIVED:
			// NB(leaf): Transactions need to hang around until their last output
			// is spent, so we can't delete them from memory until that happens.
			// And, we should track that in the on-finalize code.
		case status.FINALIZED:
			// If everyone else is at least FINALIZED, go to ARCHIVED
			// Otherwise stay FINALIZED
			if snt.StatusNum[status.FINALIZED]+snt.StatusNum[status.ARCHIVED] == uint16(len(r.Validators)) {
				stat := status.ARCHIVED
				vs[creator.Id] = stat
				return ChangeBlockStatus(cas, creator, stat, r, snt, activeBlockId)
			}
		case status.UNCONTESTEDFTM:
			// If I see at least FTM with at least UNCONTESTED-FTM, go to FINALIZED
			// Otherwise stay UNCONTESTED-FTM
			if snt.StatusNum[status.UNCONTESTEDFTM]+snt.StatusNum[status.FINALIZED] >= ftm {
				stat := status.FINALIZED
				vs[creator.Id] = stat
				return ChangeBlockStatus(cas, creator, stat, r, snt, activeBlockId)
			}
		case status.UNCONTESTED:
			// If I see at least FTM with at least UNCONTESTED, go to UNCONTESTED-FTM
			// Otherwise stay UNCONTESTED
			if snt.StatusNum[status.UNCONTESTED]+
				snt.StatusNum[status.UNCONTESTEDFTM]+
				snt.StatusNum[status.FINALIZED] >= ftm {
				stat := status.UNCONTESTEDFTM
				vs[creator.Id] = stat
				ChangeBlockStatus(cas, creator, stat, r, snt, activeBlockId)
			}
		case status.CONTESTEDLEASTHASH:
			return fmt.Errorf("Contested state with only one transaction!")
		case status.CONTESTEDFTM:
			return fmt.Errorf("Contested state with only one transaction!")
		default:
			return fmt.Errorf("Unrecognized status!")
		}
	}
	return nil
}

func isNewRound(d uint64) bool {
	// New rounds start when d is of the form
	// r(r+3)/2 for some natural r.
	// r = (-3 +/- sqrt(9+8d))/2 is natural
	// when 9+8d is a perfect square.
	s := math.Sqrt(float64(9 + 8*d))
	return s == math.Floor(s)
}

func leastHashAndMostVoted(
	tsnt TxnStatusNumTotal,
	bcache *dag.BlockCache,
) (ids.TransactionId, ids.TransactionId, StatusNumTotal, error) {

	var lhTxn, mvTxn transaction.TransactionHash
	var lhTid, mvTid ids.TransactionId
	var mvSnt StatusNumTotal

	for tid, snt := range tsnt {
		tblk, ok := bcache.Get(tid.BlockId)
		if !ok {
			err := fmt.Errorf("Block not found for txn %s\n", tid.String())
			log.Errorf(err.Error())
			return lhTid, mvTid, mvSnt, err
		}
		th := tblk.Transactions()[tid.Index].Hash()
		if lhTxn.Greater(th) || lhTxn.IsEmpty() {
			lhTxn = th
			lhTid = tid
		}
		if snt.Total >= mvSnt.Total || mvTxn.IsEmpty() {
			mvTxn = th
			mvSnt = snt
			mvTid = tid
		}
	}

	return lhTid, mvTid, mvSnt, nil
}

// Updates the status of block.creator in vs
// based on the statuses of the other validators
// when there are multiple transactions in the
// conflict domain.
// "I" and "me" in comments below refer to the block creator.
func updateVsContested(
	vs *ContestedVS,
	tsnt TxnStatusNumTotal,
	creatorStatus status.ContestedStatus,
	creator validator.Validator,
	blk block.Block,
	r StateData,
	cas *Casanova,
) error {
	ftm := r.Ftm
	if creatorStatus == nil {
		// Find least hash and most-voted-for in tsnt
		lhTid, mvTid, mvSnt, err := leastHashAndMostVoted(tsnt, cas.Consensus.Dag.Blocks())
		if err != nil {
			return err
		}

		var tid ids.TransactionId
		var stat status.ContestedStatus
		if mvSnt.Total >= ftm {
			tid = mvTid
			// Check whether the ftm votes are
			// themselves *-FTM or FINALIZED votes
			// so we can go to FINALIZED
			ftmVotes := mvSnt.StatusNum[status.UNCONTESTEDFTM] +
				mvSnt.StatusNum[status.CONTESTEDFTM] +
				mvSnt.StatusNum[status.FINALIZED]
			if ftmVotes >= ftm {
				stat = status.Finalized{TxnId_: tid}
			} else {
				stat = status.ContestedFTM{TxnId_: tid, Idx: r.Height[creator]}
			}
		} else {
			tid = lhTid
			stat = status.ContestedLeastHash{TxnId_: tid, Idx: r.Height[creator]}
		}
		vs[creator.Id] = stat
		tblk, ok := cas.Consensus.Dag.Blocks().Get(tid.BlockId)
		if !ok {
			return fmt.Errorf("No block for transaction %s", tid.String())
		}
		txn := tblk.Transactions()[tid.Index]
		err = ChangeTxnStatus(creator, stat.Kind(), r, cas.Self_, cas.Consensus.Dag.Blocks(), txn)
		if err != nil {
			log.Errorf("updateVsContested: ChangeTxnStatus error: %s", err.Error())
		}
	} else {
		tid := creatorStatus.TxnId()
		snt := tsnt[tid]
		tblk, ok := cas.Consensus.Dag.Blocks().Get(tid.BlockId)
		if !ok {
			return fmt.Errorf("No block for transaction %s", tid.String())
		}
		txn := tblk.Transactions()[tid.Index]

		// Contention
		// Usually we'd switch on creatorStatus.(type), but since
		// Kind() already exists, we use it for uncontested cases
		switch creatorStatus.Kind() {
		case status.ARCHIVED:
			if len(tsnt) != 1 {
				return fmt.Errorf("Archived state with multiple transactions!")
			} else {
				// NB(leaf): Transactions need to hang around until their last output
				// is spent, so we can't delete them from memory until that happens.
				// And, we should track that in the on-finalize code.
			}
		case status.FINALIZED:
			if len(tsnt) != 1 {
				// The conflict came too late for me to care.
				// Therefore stay FINALIZED.
			} else {
				// If everyone else is at least FINALIZED, go to ARCHIVED
				// Otherwise stay FINALIZED
				if snt.StatusNum[status.FINALIZED]+
					snt.StatusNum[status.ARCHIVED] == uint16(len(r.Validators)) {
					stat := status.Archived{TxnId_: tid}
					vs[creator.Id] = stat
					err := ChangeTxnStatus(creator, stat.Kind(), r, cas.Self_, cas.Consensus.Dag.Blocks(), txn)
					if err != nil {
						return err
					}
				}
			}
		case status.UNCONTESTEDFTM:
			// This is the first evidence I've seen of conflict.

			// If I see at least FTM for my transaction *now*
			// (doesn't matter that I saw it before)
			// with either UncontestedFTM or ContestedFTM or Finalized,
			// switch to Finalized, since the conflict was too late.
			// Otherwise, take a lock on this transaction,
			// start the round counter, and switch to ContestedFTM.
			if snt.StatusNum[status.UNCONTESTEDFTM]+
				snt.StatusNum[status.CONTESTEDFTM]+
				snt.StatusNum[status.FINALIZED] >= ftm {
				stat := status.Finalized{TxnId_: tid}
				vs[creator.Id] = stat
				TxnsRejected.WithLabelValues("CONSENSUS").Add(float64(len(tsnt) - 1))
				err := ChangeTxnStatus(creator, stat.Kind(), r, cas.Self_, cas.Consensus.Dag.Blocks(), txn)
				if err != nil {
					return err
				}
			} else {
				stat := status.ContestedFTM{TxnId_: tid, Idx: r.Height[creator]}
				vs[creator.Id] = stat
				err := ChangeTxnStatus(creator, stat.Kind(), r, cas.Self_, cas.Consensus.Dag.Blocks(), txn)
				if err != nil {
					return err
				}
			}
		case status.UNCONTESTED:
			// This is the first I've seen of the conflict.
			// If there's an FTM for some transaction, take a lock on it
			// and change to CONTESTED-FTM.
			// Otherwise, take a lock on the least hash option and
			// change to CONTESTED-LEASTHASH.
			var ftms []ids.TransactionId
			for t, snt2 := range tsnt {
				if snt2.Total >= ftm {
					ftms = append(ftms, t)
				}
			}
			// Casanova#163 TODO: handle equivocation by validators
			// while (ftms.length > 1) {
			//   slash and recalculate the sizes
			// }
			if len(ftms) > 0 {
				newtid := ftms[0]
				newtblk, ok := cas.Consensus.Dag.Blocks().Get(newtid.BlockId)
				if !ok {
					return fmt.Errorf("No block for transaction %s", newtid.String())
				}
				newtxn := newtblk.Transactions()[newtid.Index]
				stat := status.ContestedFTM{TxnId_: newtid, Idx: r.Height[creator]}
				vs[creator.Id] = stat
				err := ChangeTxnStatus(creator, stat.Kind(), r, cas.Self_, cas.Consensus.Dag.Blocks(), newtxn)
				if err != nil {
					return err
				}
			} else {
				found := false
				var least transaction.TransactionHash
				var leastTid ids.TransactionId

				// Find least hash transaction
				for tid := range tsnt {
					tblk, ok := cas.Consensus.Dag.Blocks().Get(tid.BlockId)
					if !ok {
						return fmt.Errorf("No block for transaction %s", tid.String())
					}
					t := tblk.Transactions()[tid.Index].Hash()
					if !found || t.Lesser(least) {
						found = true
						least = t
						leastTid = tid
					}
				}
				if !found {
					return fmt.Errorf("No transaction for hash")
				}
				newtblk, ok := cas.Consensus.Dag.Blocks().Get(leastTid.BlockId)
				if !ok {
					return fmt.Errorf("No block for transaction %s", leastTid.String())
				}
				newtxn := newtblk.Transactions()[leastTid.Index]
				stat := status.ContestedLeastHash{TxnId_: leastTid, Idx: r.Height[creator]}
				vs[creator.Id] = stat
				err := ChangeTxnStatus(creator, stat.Kind(), r, cas.Self_, cas.Consensus.Dag.Blocks(), newtxn)
				if err != nil {
					return err
				}
			}
		case status.CONTESTEDFTM:
			switch cs := creatorStatus.(type) {
			case status.ContestedFTM:
				// If it is a new round
				//   If I see at least FTM for my transaction (with either
				//       UncontestedFTM or ContestedFTM or Finalized),
				//     switch to Finalized.
				//   Otherwise, if I see at least FTM for a different
				//       transaction (with any status)
				//     switch to ContestedFTM for that transaction
				// Otherwise, stay ContestedFTM for my transaction
				if isNewRound(r.Height[creator] - cs.Index()) {
					if snt.StatusNum[status.UNCONTESTEDFTM]+
						snt.StatusNum[status.CONTESTEDFTM]+
						snt.StatusNum[status.FINALIZED] >= ftm {
						stat := status.Finalized{TxnId_: tid}
						TxnsRejected.WithLabelValues("CONSENSUS").Add(float64(len(tsnt) - 1))
						vs[creator.Id] = stat
						err := ChangeTxnStatus(creator, stat.Kind(), r, cas.Self_, cas.Consensus.Dag.Blocks(), txn)
						if err != nil {
							return err
						}
					} else {
						var ftms []ids.TransactionId
						for t, snt2 := range tsnt {
							if snt2.Total >= ftm {
								ftms = append(ftms, t)
							}
						}
						// Casanova#163 TODO: handle equivocation by validators
						// while (ftms.length > 1) {
						//   slash and recalculate the sizes
						// }
						if len(ftms) > 0 {
							tid := ftms[0]
							tblk, ok := cas.Consensus.Dag.Blocks().Get(tid.BlockId)
							if !ok {
								return fmt.Errorf("No block for transaction %s", tid.String())
							}
							txn := tblk.Transactions()[tid.Index]
							stat := status.ContestedFTM{TxnId_: tid, Idx: r.Height[creator]}
							vs[creator.Id] = stat
							err := ChangeTxnStatus(creator, stat.Kind(), r, cas.Self_, cas.Consensus.Dag.Blocks(), txn)
							if err != nil {
								return err
							}
						}
					}
				}
			default:
				return fmt.Errorf("Status has wrong kind!")
			}
		case status.CONTESTEDLEASTHASH:
			switch cs := creatorStatus.(type) {
			case status.ContestedLeastHash:

				// If it is a new round
				//   If I see at least FTM for my transaction (with any status),
				//     switch to CONTESTED-FTM.
				// Otherwise, stay CONTESTED-LEASTHASH for my transaction
				if isNewRound(r.Height[creator] - cs.Index()) {
					if snt.Total >= ftm {
						stat := status.ContestedFTM{TxnId_: tid, Idx: r.Height[creator]}
						vs[creator.Id] = stat
						err := ChangeTxnStatus(creator, stat.Kind(), r, cas.Self_, cas.Consensus.Dag.Blocks(), txn)
						if err != nil {
							return err
						}
					}
				}
			default:
				return fmt.Errorf("Status has wrong kind: expected contested-leasthash.")
			}
		default:
			return fmt.Errorf("Unrecognized status: %d", creatorStatus.Kind())
		}
	}
	return nil
}

// Determines the "active" (unarchived) blocks and the valid transactions
// contained in each. Inserts these into one sorted LHT keyed on the
// numeric representation of the active transactions' CDs.
func (consensus *Consensus) PopulateActiveTxnTable(
	result StateData,
	stateCache StateCache,
	blk block.Block,
	cas *Casanova,
	selfStr string,
) (*TableWithIndex, map[transaction.CD][]transaction.Transaction, error) {
	nowMicro := time.Now().UnixMicro()
	tr := &TableRequest{
		resp: make(chan *TableWithIndex, 1),
	}
	consensus.TableListener.Get <- tr
	lht := <-tr.resp
	elapsed := float64(time.Now().UnixMicro() - nowMicro)
	ConsensusStateMicroVec.WithLabelValues("WaitForTable", selfStr).Add(elapsed)
	log.Debugf("StateAtBlock: %s, WaitForTable: %f seconds", blk.BlockId(), elapsed*0.000001)

	dups := map[transaction.CD][]transaction.Transaction{}
	txnCount := 0
	for bid, vs := range result.Bvs {
		stat := vs[cas.Self_.Id]
		if stat == status.FINALIZED || stat == status.ARCHIVED {
			continue
		}
		unarchivedState, err := stateCache.Get(bid)
		if err != nil {
			return nil, nil, err
		}
		txnCount += len(unarchivedState.AddedTxns)
		for _, addedTxn := range unarchivedState.AddedTxns {
			dup, err := lht.table.Insert(addedTxn)
			if err != nil {
				return nil, nil, err
			}
			if dup {
				newDups, err := lht.table.GetDups(addedTxn)
				if err != nil {
					return nil, nil, err
				}
				dups[addedTxn.ConflictDomain()] = append(newDups, addedTxn)
			}
		}
	}
	log.Debugf("BlockState: %s added %d txns to the activeTxnTable", blk.BlockId(), txnCount)
	return lht, dups, nil
}

func (consensus *Consensus) SortCDs(cdtsnt CDTxnStatusNumTotal) []transaction.CD {
	cdsMap := map[transaction.CD]bool{}
	for _, tsnt := range cdtsnt {
		for tid := range tsnt {
			tblk, ok := consensus.Dag.Blocks().Get(tid.BlockId)
			if ok {
				txn := tblk.Transactions()[tid.Index]
				cdsMap[txn.ConflictDomain()] = true
			}
		}
	}

	cdsList := make([]transaction.CD, len(cdsMap))
	i := 0
	for cd := range cdsMap {
		cdsList[i] = cd
		i++
	}

	sort.Slice(cdsList, func(i, j int) bool {
		return transaction.Compare(cdsList[i], cdsList[j]) < 0
	})

	return cdsList
}
