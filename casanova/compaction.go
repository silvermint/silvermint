package casanova

import (
	"time"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/transaction"
)

func init() {
	pflag.Int("compaction_frequency", 15,
		"The amount of time to wait between compacting ledgers (seconds).")
	pflag.Bool("save_ledgers", true, "Enable/disable saving ledgers to disk.")
}

// Given the initial user-provided time to wait between function executions,
// the latest wait time between function executions, and the actual time that
// the function took to execute, returns how long the wait should be before the
// next execution. Never returns a smaller wait time than the one provided by
// the user.
func ExponentialBackoff(initWait, currWait, execTime time.Duration) time.Duration {
	if currWait < execTime {
		currWait = currWait * 2
	} else if currWait > initWait && currWait/2 > execTime {
		currWait = currWait / 2
	}
	return currWait
}

func (cas *Casanova) CompactionThread() {
	initFreq := viper.GetInt("compaction_frequency")
	initDur := time.Duration(initFreq) * time.Second
	currDur := initDur

	last_time := time.Now()
	next_time := last_time.Add(currDur)

	for {
		time.Sleep(time.Until(next_time))

		nowMicro := time.Now().UnixMicro()
		cas.CompactLatest()
		compactionTime := time.Now().UnixMicro() - nowMicro
		compactionDur := time.Duration(compactionTime) * time.Microsecond

		CasanovaMicroVec.WithLabelValues("CompactLedgers").Add(float64(compactionTime))
		log.Debugf("CompactLedgers: %f seconds", float64(compactionTime)*0.000001)

		currDur = ExponentialBackoff(initDur, currDur, compactionDur)
		next_time = next_time.Add(currDur)
	}
}

func (cas *Casanova) CompactLatest() {
	// NB(chuck): We compact as of Tip-1 because the BlockState computation for
	// a new block we're issuing assigns it the tip ledger's child ledger.
	// Compaction unassigns the provided ledger's child pointer (in order to
	// break the ledger pointer chain starting at Genesis), thus we should
	// compact as of a ledger whose child pointer is no longer needed.
	bid := cas.Consensus.Dag.Tip(cas.Self_.Id)
	sd, _ := cas.StateCache.Get(bid.Prev())

	log.Debugf("Compacting ledgers as of block %s", bid)

	// Get the compacted ledger
	compacted := transaction.NewSSLedger(bid.Height())
	compacted.Compact(sd.Extra.Ledger)

	if viper.GetBool("save_ledgers") {
		ledgerBytes, err := compacted.MarshalBinary()
		if err != nil {
			log.Error("Unable to marshal SSLedger binary: %s", err.Error())
			return
		}
		_, curr := cas.SSLedgers.Current()
		curr.Append(ledgerBytes)
	}
}
