package casanova

import (
	"sync"

	"gitlab.com/silvermint/silvermint/ids"
)

// Blocks are registered as in-progress when the process of computing their
// state and adding them to the DAG begins. They are removed from in-progress
// status when the process of attempting to compute their state / add them to
// the DAG concludes.
// The in-progress status is registered according to the block's creator ID and
// height pair (dag.ValHeight). This allows pending blocks whose parent hashes
// are not yet known (but whose parent ValHeights are known) to discern whether
// they need to request that parent via catchup.
type InProgressMap struct {
	inprogress map[ids.BlockId]bool
	mu         sync.RWMutex
}

func (ip *InProgressMap) Has(vh ids.BlockId) bool {
	ip.mu.RLock()
	defer ip.mu.RUnlock()

	return ip.inprogress[vh]
}

// Sets the provided Block as in-progress, returns whether it was successfully set.
func (ip *InProgressMap) SetInProgress(bid ids.BlockId) bool {
	ip.mu.Lock()
	defer ip.mu.Unlock()

	isInProgress := ip.inprogress[bid]
	ip.inprogress[bid] = true
	return !isInProgress
}

// Removes the Block from the in-progress maps.
func (ip *InProgressMap) RemoveInProgress(bid ids.BlockId) {
	ip.mu.Lock()
	defer ip.mu.Unlock()

	delete(ip.inprogress, bid)
}
