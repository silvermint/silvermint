// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/status"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/spf13/pflag"
)

// Creates a StateData with a BVS populated using the provided list of Blocks with random statuses assigned
// for each of the provided list of Validators.
func makeRandomStateData(blks []block.Block, vals []validator.Validator) StateData {
	state := StateData{
		Bvs:        BVS{},
		Validators: map[validator.Validator]bool{},
		Height:     map[validator.Validator]uint64{},
	}

	// Create statuses for each hash and assign to BVS
	for i := range blks {
		vs := VS{}
		for _, v := range vals {
			switch rand.Intn(4) {
			case 0:
				vs[v.Id] = status.ARCHIVED
			case 1:
				vs[v.Id] = status.FINALIZED
			case 2:
				vs[v.Id] = status.UNCONTESTEDFTM
			case 3:
				vs[v.Id] = status.UNCONTESTED
			}
		}
		state.Bvs[blks[i].BlockId()] = &vs
	}

	for i := range vals {
		state.Validators[vals[i]] = true
		state.Height[vals[i]] = rand.Uint64()
	}

	return state
}

// The original reducer function as written by Mike Stay.
func simpleReducer(prev StateData, cur StateData) StateData {
	result := StateData{
		Bvs:        prev.Bvs,
		Cdvs:       prev.Cdvs,
		AddedTxns:  prev.AddedTxns,
		Validators: prev.Validators,
		Slashed:    prev.Slashed,
		Ftm:        prev.Ftm,
		Height:     prev.Height,
		Extra:      prev.Extra,
	}

	result.Slashed = validator.ValidatorSet{}

	// for each blk in cur
	for bh, curvs := range cur.Bvs {
		resultvs, resultvsok := result.Bvs[bh]
		// if prev doesn't have info about
		// this conflict domain, use cur's
		if !resultvsok {
			result.Bvs[bh] = curvs.Copy()
		} else {
			// for each validator
			for vtor := range cur.Validators {
				curstatus := curvs[vtor.Id]
				if resultvs[vtor.Id] == status.UNSEEN {
					// if prev doesn't have info about
					// this validator, use cur's info
					resultvs[vtor.Id] = curstatus
				} else if cur.Height[vtor] > result.Height[vtor] {
					// otherwise use the latest info
					// from this validator
					resultvs[vtor.Id] = curstatus
				}
			}
		}
	}

	// for each cd in cur
	for cd, curvs := range cur.Cdvs {
		resultvs, resultvsok := result.Cdvs[cd]
		// if prev doesn't have info about
		// this conflict domain, use cur's
		if !resultvsok {
			result.Cdvs[cd] = curvs.Copy()
		} else {
			// for each validator
			for vtor := range cur.Validators {
				curstatus := curvs[vtor.Id]
				if resultvs[vtor.Id] == nil {
					// if prev doesn't have info about
					// this validator, use cur's info
					resultvs[vtor.Id] = curstatus
				} else if cur.Height[vtor] > result.Height[vtor] {
					// otherwise use the latest info
					// from this validator
					resultvs[vtor.Id] = curstatus
				}
			}
		}
	}

	// Union the two sets of validators and update
	// the height.
	for vtor := range cur.Validators {
		result.Validators[vtor] = true
		resultHeight := result.Height[vtor]
		curHeight := cur.Height[vtor]
		if resultHeight > curHeight {
			result.Height[vtor] = resultHeight
		} else {
			result.Height[vtor] = curHeight
		}
	}

	result.Ftm = ftm(result.Validators)
	return result
}

func TestReducer(t *testing.T) {
	vals := pflag.Int("vals", 10, "Number of validators in the network.")
	block_depth := pflag.Int("block_depth", 10, "Number of unfinalized blocks per validator.")
	pflag.Parse()

	// Create casanova object
	casanovas, validators, wallets := setupNValidators(*vals, t)
	cas := casanovas[0]

	nBlks := *vals * *block_depth

	// Create a list of Blocks with random data, which will be used to populate the BVS
	blks := make([]block.Block, nBlks)
	for i := range blks {
		vid := rand.Intn(*vals)
		sb := block.SilvermintBlock{
			Creator_:   validators[vid],
			Height_:    rand.Uint64(),
			Validator_: uint16(vid),
			Timestamp_: rand.Int63(),
			Parents_:   block.Heights{},
			Deltas_:    block.NewHeightDeltaArray(),
		}
		for _, v := range validators {
			sb.Parents_[v.Id] = rand.Uint64()
		}
		data, _ := sb.MarshalSignedBinary(wallets[vid])
		sb.Data_ = data
		blks[i] = &sb
	}

	// Create a list of fake parent StateData for reducer to merge
	parentStates := make([]StateData, *vals)
	for i := range parentStates {
		parentStates[i] = makeRandomStateData(blks, validators)
	}

	// Reduce the parent StateData using the simpleReducer defined above.
	t1 := time.Now()
	var simpleResult StateData
	if len(parentStates) > 0 {
		simpleResult = preallocateStateData(parentStates[0])
	} else {
		simpleResult = emptyStateData(cas.GenesisExtra)
	}
	for _, pm := range parentStates {
		simpleResult = simpleReducer(simpleResult, pm)
	}
	t2 := time.Now()

	// Reduce the parent StateData using the reducer defined in state.go
	result := reducer(parentStates, ids.BlockId{})
	t3 := time.Now()

	// Verify that the StateData resulting from each of the reducers above
	// created an identical BVS
	i := 0
	j := 0
	for bh, simpleVS := range simpleResult.Bvs {
		vs, ok := result.Bvs[bh]
		if !ok {
			t.Fatalf("ERROR: Blockhash %x not found in new BVS\n", bh)
		}
		i++
		for vid, simpleStat := range simpleVS {
			stat := vs[vid]
			if !ok {
				t.Fatalf("ERROR: Blockhash %x didn't have status for validator %d\n", bh, vid)
			}
			if simpleStat != stat {
				t.Fatalf("ERROR: Blockhash %x had wrong status for validator %d: wanted %d, had %d\n", bh, vid, simpleStat, stat)
			}
			j++
		}
	}

	fmt.Printf("Checked %d blocks and %d statuses\n", i, j)

	fmt.Printf("Time to reduce %d StateData (%d active blocks, %d total statuses) using simpleReducer: %d ms.\n",
		*vals, *vals**block_depth, *vals**vals**block_depth, t2.Sub(t1).Milliseconds())
	fmt.Printf("Time to reduce %d StateData (%d active blocks, %d total statuses) using reducer: %d ms.\n",
		*vals, *vals**block_depth, *vals**vals**block_depth, t3.Sub(t2).Milliseconds())
}
