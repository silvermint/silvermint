// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"encoding/binary"
	"fmt"

	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/status"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
)

// block -> validator -> status
// This assumes that different blocks
// (wrt ==) don't conflict.
type BVS map[ids.BlockId]*VS

// TODO(chuck): Figure out whether we actually need ALL validators to have
// archived a transaction or if it's enough that this validator has.
func (bvs BVS) DeleteOldBlockIds() {
	for bid, vs := range bvs {
		if vs.AllHaveStatus(status.ARCHIVED) {
			delete(bvs, bid)
		}
	}
}

// Given a map from block hash to validator to status,
// invert status.block and take cardinality to get
// result: bid -> (statuskind or total) -> #
func (bvs BVS) ConvertToStatusTotals() (BlkStatusNumTotal, error) {
	bsnt := make(BlkStatusNumTotal, len(bvs))
	for bid, vs := range bvs {
		for _, stat := range vs {
			if stat == status.UNSEEN {
				continue
			}
			snt, sntok := bsnt[bid]
			if !sntok {
				snt = StatusNumTotal{map[status.Status]uint16{}, 0}
			}
			snt.StatusNum[stat]++
			bsnt[bid] = snt
		}
	}
	for i, stats := range bsnt {
		var total uint16 = 0
		for _, count := range stats.StatusNum {
			total += count
		}
		stats.Total = total
		bsnt[i] = stats
	}
	return bsnt, nil
}

func (bvs BVS) Length() uint64 {
	var l uint64 = 8                                       // Length
	l += uint64(len(bvs) * (2 + 8))                        // BlockIds
	l += uint64(len(bvs) * validator.N_MAX_VALIDATORS * 1) // Statuses
	return l
}

func (bvs BVS) MarshalBinary() ([]byte, error) {
	vlen := bvs.Length()
	data := make([]byte, vlen)
	var offset uint64 = 0

	// Length
	binary.LittleEndian.PutUint64(data[offset:offset+8], vlen)
	offset += 8

	// BCDs / VS
	for bid, vs := range bvs {
		binary.LittleEndian.PutUint16(data[offset:offset+2], bid.ValId_)
		offset += 2
		binary.LittleEndian.PutUint64(data[offset:offset+8], bid.Height_)
		offset += 8
		for _, s := range vs {
			data[offset] = uint8(s)
			offset += 1
		}
	}

	if offset != vlen {
		return nil, fmt.Errorf("ERROR: Offset didn't match computed BVS length, %d != %d",
			offset, vlen)
	}

	return data, nil
}

func (bvs BVS) UnmarshalBinary(data []byte) error {
	if len(data) == 0 {
		return fmt.Errorf("ERROR: Can't unmarshal empty byte array")
	}

	// Length
	var offset uint64 = 0
	blen := binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8

	if blen != uint64(len(data)) {
		return fmt.Errorf("ERROR: BVS has length %d bytes, "+
			"but we have %d bytes of data", blen, len(data))
	}

	// BlockIds / VS
	for offset < blen {
		bid := ids.BlockId{}
		bid.ValId_ = binary.LittleEndian.Uint16(data[offset : offset+2])
		offset += 2
		bid.Height_ = binary.LittleEndian.Uint64(data[offset : offset+8])
		offset += 8
		vs := VS{}
		for i := 0; i < validator.N_MAX_VALIDATORS; i++ {
			vs[i] = status.Status(data[offset])
			offset += 1
		}
		bvs[bid] = &vs
	}

	if offset != blen {
		return fmt.Errorf("ERROR: Offset didn't match computed BVS length, %d != %d",
			offset, blen)
	}

	return nil
}

type VS [validator.N_MAX_VALIDATORS]status.Status

func (vs VS) AllHaveStatus(stat status.Status) bool {
	allHaveStatus := true
	for _, s := range vs {
		if s != status.UNSEEN && s != stat {
			allHaveStatus = false
			break
		}
	}
	return allHaveStatus
}

func (vs VS) AnyHaveStatus(stat status.Status) bool {
	for _, s := range vs {
		if s == stat {
			return true
		}
	}
	return false
}

func (vs VS) Copy() *VS {
	result := vs
	return &result
}

type ContestedVS [validator.N_MAX_VALIDATORS]status.ContestedStatus

func (vs ContestedVS) AllHaveStatus(stat status.Status) bool {
	allHaveStatus := true
	for _, s := range vs {
		if s != nil && s.Kind() != stat {
			allHaveStatus = false
			break
		}
	}
	return allHaveStatus
}

func (vs ContestedVS) Copy() *ContestedVS {
	result := vs
	return &result
}

func (cvs ContestedVS) Length() uint32 {
	var l uint32 = 4 // Length
	for _, s := range cvs {
		if s != nil {
			l += 2                        // Validator ID
			l += uint32(status.Length(s)) // Status
		}
	}
	return l
}

func (cvs ContestedVS) MarshalBinary() ([]byte, error) {
	clen := cvs.Length()
	data := make([]byte, clen)

	var offset uint32 = 0

	// Length
	binary.LittleEndian.PutUint32(data[offset:offset+4], clen)
	offset += 4

	// Validator -> Status
	for v, s := range cvs {
		if s != nil {
			binary.LittleEndian.PutUint16(data[offset:offset+2], uint16(v))
			offset += 2
			sbytes, err := status.MarshalBinary(s)
			if err != nil {
				return nil, fmt.Errorf("ERROR: Unable to marshal ContestedStatus: %s",
					err.Error())
			}
			copy(data[offset:offset+uint32(len(sbytes))], sbytes)
			offset += uint32(len(sbytes))
		}
	}

	return data, nil
}

func (cvs *ContestedVS) UnmarshalBinary(data []byte) error {
	var offset uint32 = 0

	// Length
	clen := binary.LittleEndian.Uint32(data[offset : offset+4])
	offset += 4
	if int(clen) != len(data) {
		return fmt.Errorf("ERROR: Unmarshal failed: ContestedVS has length %d "+
			"bytes, but we have %d bytes of data.", clen, len(data))
	}

	// Validator -> Status
	for offset < clen {
		vid := binary.LittleEndian.Uint16(data[offset : offset+2])
		offset += 2
		slen := uint32(data[offset])
		stat, err := status.UnmarshalBinary(data[offset : offset+slen])
		if err != nil {
			return fmt.Errorf("ERROR: Unable to unmarshal ContestedStatus: %s",
				err.Error())
		}
		cvs[vid] = stat
		offset += uint32(slen)
	}

	return nil
}

// conflict domain -> validator -> status
// This assumes that different conflict
// domain objects (wrt ==) don't conflict.
type CDVS map[transaction.CD]*ContestedVS

// TODO(chuck): Figure out whether we actually need ALL validators to have
// archived a block or if it's enough that this validator has.
func (c CDVS) DeleteOldCDs() {
	for cd, vs := range c {
		if vs.AllHaveStatus(status.ARCHIVED) {
			delete(c, cd)
		}
	}
}

// Given a map from conflict domain to validator to status,
// invert status.txn and take cardinality to get
// result: cd -> txn -> (statuskind or total) -> #
func (c CDVS) ConvertToStatusTotals() (CDTxnStatusNumTotal, error) {
	result := make(CDTxnStatusNumTotal, len(c))
	for cd, vs := range c {
		tsnt := TxnStatusNumTotal{}
		for _, stat := range vs {
			if stat == nil {
				continue
			}
			tid := stat.TxnId()
			snt, sntok := tsnt[tid]
			if !sntok {
				snt = StatusNumTotal{map[status.Status]uint16{}, 0}
			}
			snt.StatusNum[stat.Kind()]++
			tsnt[tid] = snt
		}

		for i, stats := range tsnt {
			var total uint16 = 0
			for _, count := range stats.StatusNum {
				total += count
			}
			stats.Total = total
			tsnt[i] = stats
		}
		if len(tsnt) > 0 {
			result[cd] = tsnt
		}
	}

	return result, nil
}

func (cdvs CDVS) Length() uint64 {
	var l uint64 = 8 // Length
	for cd, vs := range cdvs {
		l += uint64(cd.Length()) // Length of the CD
		l += uint64(vs.Length()) // Length of the VS
	}
	return l
}

func (cdvs CDVS) MarshalBinary() ([]byte, error) {
	clen := cdvs.Length()
	data := make([]byte, clen)
	var offset uint64 = 0

	// Length
	binary.LittleEndian.PutUint64(data[offset:offset+8], clen)
	offset += 8

	// CDVS
	for cd, vs := range cdvs {
		cdBytes, err := cd.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(data[offset:offset+uint64(len(cdBytes))], cdBytes)
		offset += uint64(len(cdBytes))
		vsBytes, err := vs.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(data[offset:offset+uint64(len(vsBytes))], vsBytes)
		offset += uint64(len(vsBytes))
	}

	if offset != clen {
		return nil, fmt.Errorf("ERROR: Offset didn't match computed CDVS length, %d != %d",
			offset, clen)
	}

	return data, nil
}

func (cdvs CDVS) UnmarshalBinary(data []byte) error {
	if len(data) == 0 {
		return fmt.Errorf("ERROR: Can't unmarshal empty byte array")
	}

	// Length
	var offset uint64 = 0
	clen := binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8

	if clen != uint64(len(data)) {
		return fmt.Errorf("ERROR: CDVS has length %d bytes, "+
			"but we have %d bytes of data", clen, len(data))
	}

	// CDs / VS
	for offset < clen {
		cdlen := binary.LittleEndian.Uint16(data[offset : offset+2])
		cd, err := transaction.UnmarshalCD(data[offset : offset+uint64(cdlen)])
		offset += uint64(cdlen)
		if err != nil {
			return err
		}
		vslen := binary.LittleEndian.Uint32(data[offset : offset+4])
		vs := ContestedVS{}
		err = vs.UnmarshalBinary(data[offset : offset+uint64(vslen)])
		offset += uint64(vslen)
		if err != nil {
			return err
		}
		cdvs[cd] = &vs
	}

	if offset != clen {
		return fmt.Errorf("ERROR: Offset didn't match computed CDVS length, %d != %d",
			offset, clen)
	}

	return nil
}
