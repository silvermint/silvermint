package casanova

import (
	"math"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/status"
	"gitlab.com/silvermint/silvermint/transaction"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

func RandBytes(n int) []byte {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return []byte(string(b))
}

func RandBlockId() ids.BlockId {
	return ids.BlockId{
		ValId_:  uint16(rand.Intn(math.MaxUint16)),
		Height_: rand.Uint64(),
	}
}

func RandUncontestedStatus() uint8 {
	return uint8(rand.Intn(7))
}

func RandContestedStatus(tid ids.TransactionId) status.ContestedStatus {

	txnType := status.Status(1 + rand.Intn(6))
	index := uint64(rand.Intn(math.MaxInt))

	var cstat status.ContestedStatus
	switch txnType {
	case status.UNCONTESTED:
		cstat = status.Uncontested{
			TxnId_: tid,
		}
	case status.UNCONTESTEDFTM:
		cstat = status.UncontestedFTM{
			TxnId_: tid,
		}
	case status.CONTESTEDLEASTHASH:
		cstat = status.ContestedLeastHash{
			TxnId_: tid,
			Idx:    index,
		}
	case status.CONTESTEDFTM:
		cstat = status.ContestedFTM{
			TxnId_: tid,
			Idx:    index,
		}
	case status.FINALIZED:
		cstat = status.Finalized{
			TxnId_: tid,
		}
	case status.ARCHIVED:
		cstat = status.Archived{
			TxnId_: tid,
		}
	}
	return cstat
}

func RandWallet() crypto.Wallet {
	wal, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	return wal
}

func RandConflictDomain() transaction.CD {
	txnType := rand.Intn(3)
	wal := RandWallet()
	checknum := uint16(rand.Intn(math.MaxUint16))

	var cd transaction.CD
	switch txnType {
	case 0:
		cd = transaction.MintCD{
			Source_:      wal.WalletAddress(),
			CheckNumber_: checknum,
		}
	case 1:
		cd = transaction.PaymentCD{
			Source_:      wal.WalletAddress(),
			CheckNumber_: checknum,
		}
	case 2:
		cd = transaction.SweepCD{
			Source_:      wal.WalletAddress(),
			CheckNumber_: checknum,
		}
	}
	return cd
}

func RandBVS(nBlocks int) BVS {
	bvs := BVS{}

	for i := 0; i < nBlocks; i++ {
		vs := VS{}
		for j := range vs {
			vs[j] = status.Status(RandUncontestedStatus())
		}
		bvs[RandBlockId()] = &vs
	}
	return bvs
}

func RandCDVS(nTxns int) CDVS {
	cdvs := CDVS{}

	for i := 0; i < nTxns; i++ {
		vs := ContestedVS{}

		for j := range vs {
			tid := RandTxnId()
			vs[j] = RandContestedStatus(tid)
		}
		cd := RandConflictDomain()
		cdvs[cd] = &vs
	}
	return cdvs
}

func VerifyBVS(bvs, loadedBvs BVS, t *testing.T) {
	for bh, vs := range bvs {
		loadedVS, ok := loadedBvs[bh]
		if !ok {
			t.Fatalf("Loaded BVS didn't have hash %x", bh)
		}
		for v, s := range vs {
			if loadedVS[v] != s {
				t.Fatalf("Loaded status for validator %d didn't match original (%d != %d) for block %x",
					v, s, loadedVS[v], bh)
			}
		}
	}
}

func VerifyCDVS(cdvs, loadedCdvs CDVS, t *testing.T) {
	for cd, vs := range cdvs {
		loadedVS, ok := loadedCdvs[cd]
		if !ok {
			t.Fatalf("Loaded CDVS didn't have CD %v", cd)
		}
		for v, s := range vs {
			loadedS := loadedVS[v]

			if s.Kind() != loadedS.Kind() {
				t.Fatalf("Loaded status for validator %d didn't match original (%d != %d) for CD %v",
					v, s.Kind(), loadedS.Kind(), cd)
			}
			if s.TxnId() != loadedS.TxnId() {
				t.Fatalf("Loaded txnhash for validator %d didn't match original (%x != %x) for CD %v",
					v, s.TxnId(), loadedS.TxnId(), cd)
			}

			switch s := s.(type) {
			case status.ContestedFTM:
				loadedS := loadedS.(status.ContestedFTM)
				if s.Index() != loadedS.Index() {
					t.Fatalf("Loaded index for validator %d didn't match original (%d != %d) for CD %v",
						v, s.Index(), loadedS.Index(), cd)
				}
			case status.ContestedLeastHash:
				loadedS := loadedS.(status.ContestedLeastHash)
				if s.Index() != loadedS.Index() {
					t.Fatalf("Loaded index for validator %d didn't match original (%d != %d) for CD %v",
						v, s.Index(), loadedS.Index(), cd)
				}
			}

		}
	}

}

func TestBVSRoundTrip(t *testing.T) {
	nBlocks := 100
	bvs := RandBVS(nBlocks)

	bvsBytes, err := bvs.MarshalBinary()
	if err != nil {
		t.Fatalf("Error marshaling BVS to bytes: %s", err.Error())
	}
	loadedBvs := BVS{}
	err = loadedBvs.UnmarshalBinary(bvsBytes)
	if err != nil {
		t.Fatalf("Error unmarshaling BVS from bytes: %s", err.Error())
	}

	VerifyBVS(bvs, loadedBvs, t)
}

func TestCDVSRoundTrip(t *testing.T) {
	nTxns := 100
	cdvs := RandCDVS(nTxns)

	cdvsBytes, err := cdvs.MarshalBinary()
	if err != nil {
		t.Fatalf("Error marshaling CDVS to bytes: %s", err.Error())
	}
	loadedCdvs := CDVS{}
	err = loadedCdvs.UnmarshalBinary(cdvsBytes)
	if err != nil {
		t.Fatalf("Error unmarshaling CDVS from bytes: %s", err.Error())
	}

	VerifyCDVS(cdvs, loadedCdvs, t)
}
