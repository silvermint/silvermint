// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"sync"

	"gitlab.com/silvermint/silvermint/transaction"
)

type TransactionQueue struct {
	txns []transaction.Transaction
	lens []uint64
	mu   sync.RWMutex
}

func (tq *TransactionQueue) Enqueue(txn transaction.Transaction) {
	tq.mu.Lock()
	defer tq.mu.Unlock()

	tq.txns = append(tq.txns, txn)
	tq.lens = append(tq.lens, uint64(txn.Length()))
}

func (tq *TransactionQueue) Pop() (transaction.Transaction, uint64) {
	tq.mu.Lock()
	defer tq.mu.Unlock()

	if len(tq.txns) > 0 && len(tq.lens) > 0 {
		txn := tq.txns[0]
		length := tq.lens[0]
		tq.txns = tq.txns[1:]
		tq.lens = tq.lens[1:]
		TxnsPending.Dec()
		return txn, length
	}
	return nil, 0
}

func (tq *TransactionQueue) Size() int {
	tq.mu.RLock()
	defer tq.mu.RUnlock()

	return len(tq.txns)
}

func (tq *TransactionQueue) NextLength() uint64 {
	tq.mu.RLock()
	defer tq.mu.RUnlock()

	if len(tq.lens) > 0 {
		return tq.lens[0]
	}
	return 0
}

func (tq *TransactionQueue) Has(txnHash transaction.TransactionHash) bool {
	tq.mu.RLock()
	defer tq.mu.RUnlock()

	for i := range tq.txns {
		if tq.txns[i].Hash() == txnHash {
			return true
		}
	}
	return false
}
