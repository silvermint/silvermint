// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"testing"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/transaction"
)

// Mints 100 silvermint to wallet 0, attempts to send 75 of
// it to wallet 1 on a network of only one validator,
// verifying that the stages are:
// Block 0: UNCONTESTED-FTM
// Block 1: FINALIZED
// Wallet 0 then attempts to send the remaining 25 to wallet 1,
// using the same check number. Verifies this proceeds as:
// Block 2: ARCHIVED
// since this is the same conflict domain as the original FINALIZED
// transaction. Lastly, checks to ensure that the balances remained
// the same (transaction rejected).
func TestBlockTxnProcessed(t *testing.T) {
	viper.Set("log_level", 5)
	viper.Set("compaction_frequency", 3)
	log.InitLogs()
	nValidators := 2

	casanovas, validators, wallets := setupNValidators(nValidators, t)

	// Create mint transaction to wallet 0
	mintTxn := transaction.Mint{
		Src:          wallets[0].WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 100,
			Dest_:   wallets[0].WalletAddress(),
		},
		Bytes: []byte{},
		Sig:   crypto.Signature{},
	}
	mb, err := mintTxn.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for mint txn: %s", err.Error())
	}
	mintTxn.Bytes = mb
	copy(mintTxn.Sig[:], mb[len(mb)-crypto.SIGNATURE_SIZE:])

	// Mint submitted to Validator 0
	casanovas[0].AddTxn(mb)

	// Go through consensus for the mint transaction
	blk0m := casanovas[0].Get()
	blk0mbytes, err := blk0m.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk0m: %s", err.Error())
	}
	blk1m := block.NewSilvermintBlock()
	err = blk1m.UnmarshalBinary(blk0mbytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}
	expect(0, &mintTxn, "UNCONTESTED-FTM", blk0m, casanovas, validators, t)
	casanovas[1].Add(blk1m)
	blk2m := casanovas[1].Get()
	blk2mbytes, err := blk2m.MarshalSignedBinary(wallets[1])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk2: %s", err.Error())
	}
	blk3m := block.NewSilvermintBlock()
	err = blk3m.UnmarshalBinary(blk2mbytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}
	expect(1, &mintTxn, "UNCONTESTED-FTM", blk2m, casanovas, validators, t)
	casanovas[0].Add(blk3m)

	blk4m := casanovas[0].Get()
	blk4mbytes, err := blk4m.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk4: %s", err.Error())
	}
	blk5m := block.NewSilvermintBlock()
	err = blk5m.UnmarshalBinary(blk4mbytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}
	expect(0, &mintTxn, "FINALIZED", blk4m, casanovas, validators, t)
	casanovas[1].Add(blk5m)
	blk6m := casanovas[1].Get()
	blk6mbytes, err := blk6m.MarshalSignedBinary(wallets[1])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk6: %s", err.Error())
	}
	blk7m := block.NewSilvermintBlock()
	err = blk7m.UnmarshalBinary(blk6mbytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}
	expect(1, &mintTxn, "FINALIZED", blk6m, casanovas, validators, t)
	casanovas[0].Add(blk7m)

	// The mint transaction has now FINALIZED with both validators -
	// we should be clear to use the UTXO it generated

	// Wallet 0 creates payment transaction P0 to wallet 1
	// with checknumber 2
	latest, _ := casanovas[0].StateCache.Get(casanovas[0].Consensus.Dag.Tip(casanovas[0].Self_.Id))
	utxos := latest.Extra.Ledger.Utxos(wallets[0].WalletAddress())
	amt := uint64(0)
	for _, utxo := range utxos {
		amt += utxo.Amount
	}
	p0 := transaction.Payment{
		Src:          wallets[0].WalletAddress(),
		CheckNumber_: 2,
		Inputs_:      utxos,
		Outputs_: []transaction.PaymentOutput{
			{
				Amount_: amt * 3 / 4,
				Dest_:   wallets[1].WalletAddress(),
			},
			{
				Amount_: amt / 4,
				Dest_:   wallets[0].WalletAddress(),
			},
		},
		Bytes: []byte{},
		Sig:   crypto.Signature{},
	}
	p0b, err := p0.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal payment 0 to signed binary, %s", err.Error())
	}
	p0.Bytes = p0b
	copy(p0.Sig[:], p0b[len(p0b)-crypto.SIGNATURE_SIZE:])

	// P0 submitted to Validator 0
	casanovas[0].AddTxn(p0b)

	// Generate block from Validator 0, add to its DAG, check status
	blk0 := casanovas[0].Get()
	blk0bytes, err := blk0.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk0: %s", err.Error())
	}
	blk1 := block.NewSilvermintBlock()
	err = blk1.UnmarshalBinary(blk0bytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}
	expect(0, &p0, "UNCONTESTED-FTM", blk0, casanovas, validators, t)
	casanovas[1].Add(blk1)
	blk2 := casanovas[1].Get()
	blk2bytes, err := blk2.MarshalSignedBinary(wallets[1])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk2: %s", err.Error())
	}
	blk3 := block.NewSilvermintBlock()
	err = blk3.UnmarshalBinary(blk2bytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}
	expect(1, &p0, "UNCONTESTED-FTM", blk2, casanovas, validators, t)
	casanovas[0].Add(blk3)

	// Check balances
	latest, _ = casanovas[0].StateCache.Get(casanovas[0].Consensus.Dag.Tip(casanovas[0].Self_.Id))
	b0 := latest.Extra.Ledger.Balance(wallets[0].WalletAddress())
	b1 := latest.Extra.Ledger.Balance(wallets[1].WalletAddress())

	if b0 != amt {
		t.Fatalf("Wallet 0 didn't have correct balance after blocks 0/2: %d instead of %d\n",
			b0, amt)
	}

	if b1 != 0 {
		t.Fatalf("Wallet 1 didn't have correct balance after blocks 0/2: %d instead of %d\n",
			b1, 0)
	}

	// Generate block from Validator 0, add to its Dag, check status
	time.Sleep(time.Millisecond)

	blk4 := casanovas[0].Get()
	blk4bytes, err := blk4.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk4: %s", err.Error())
	}
	blk5 := block.NewSilvermintBlock()
	err = blk5.UnmarshalBinary(blk4bytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}
	expect(0, &p0, "FINALIZED", blk4, casanovas, validators, t)
	casanovas[1].Add(blk5)
	blk6 := casanovas[1].Get()
	blk6bytes, err := blk6.MarshalSignedBinary(wallets[1])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk6: %s", err.Error())
	}
	blk7 := block.NewSilvermintBlock()
	err = blk7.UnmarshalBinary(blk6bytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}
	expect(1, &p0, "FINALIZED", blk6, casanovas, validators, t)
	casanovas[0].Add(blk7)

	// Check balances
	latest, _ = casanovas[0].StateCache.Get(casanovas[0].Consensus.Dag.Tip(casanovas[0].Self_.Id))
	b0 = latest.Extra.Ledger.Balance(wallets[0].WalletAddress())
	b1 = latest.Extra.Ledger.Balance(wallets[1].WalletAddress())

	if b0 != amt/4 {
		t.Fatalf("Wallet 0 didn't have correct balance after blocks 4/6: %d instead of %d\n",
			b0, amt/4)
	}

	if b1 != amt*3/4 {
		t.Fatalf("Wallet 1 didn't have correct balance after blocks 4/6: %d instead of %d\n",
			b1, amt*3/4)
	}

	// Try to send payment with same check number for remaining amount
	p1 := transaction.Payment{
		Src:          wallets[0].WalletAddress(),
		CheckNumber_: 2,
		Inputs_:      utxos,
		Outputs_: []transaction.PaymentOutput{
			{
				Amount_: amt / 4,
				Dest_:   wallets[1].WalletAddress(),
			},
		},
		Bytes: []byte{},
		Sig:   crypto.Signature{},
	}
	p1b, err := p1.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal payment 1 to signed binary, %s", err.Error())
	}
	p1.Bytes = p1b
	copy(p1.Sig[:], p1b[len(p1b)-crypto.SIGNATURE_SIZE:])

	// P1 submitted to Validator 0
	err = casanovas[0].AddTxn(p1b)
	if err == nil {
		t.Fatalf("Should have rejected this (double spend) txn, but it got added\n")
	}

	// Generate block from Validator 0, add to its DAG, check status
	blk8 := casanovas[0].Get()
	blk8bytes, err := blk8.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk8: %s", err.Error())
	}
	blk9 := block.NewSilvermintBlock()
	err = blk9.UnmarshalBinary(blk8bytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}

	// TODO(chuck): This is wrong for now. Status should be "REJECTED" or "TRANSACTION NOT FOUND".
	// This is waiting on a better solution in the cas.GetPaymentState(..) function for rejected
	// transactions.
	expect(0, &p1, "ARCHIVED", blk8, casanovas, validators, t)

	casanovas[1].Add(blk9)
	blk10 := casanovas[1].Get()
	blk10bytes, err := blk10.MarshalSignedBinary(wallets[1])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk10: %s", err.Error())
	}
	blk11 := block.NewSilvermintBlock()
	err = blk11.UnmarshalBinary(blk10bytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}
	casanovas[0].Add(blk11)

	if b0 != amt/4 {
		t.Fatalf("Wallet 0 didn't have correct balance after blocks 8/10: %d instead of %d\n",
			b0, amt/4)
	}

	if b1 != amt*3/4 {
		t.Fatalf("Wallet 1 didn't have correct balance after blocks 8/10: %d instead of %d\n",
			b1, amt*3/4)
	}

	// Issue a few additional blocks so that the blocks that ARCHIVED the payment (and thus
	// marked the mint UTXO and transaction as deletable) are found to be deletable.
	// After two compactions the transaction should be deleted from the map.
	for j := 0; j < 5; j++ {
		for i := 0; i < nValidators; i++ {
			blk := casanovas[i].Get()
			bbytes, err := blk.MarshalSignedBinary(wallets[i])
			if err != nil {
				t.Fatalf(err.Error())
			}
			for k := 0; k < nValidators; k++ {
				if k == i {
					continue
				}
				blkm := block.NewSilvermintBlock()
				err = blkm.UnmarshalBinary(bbytes)
				if err != nil {
					t.Fatalf(err.Error())
				}
				casanovas[k].Add(blkm)
			}
			if j%viper.GetInt("compaction_frequency") == 0 {
				casanovas[i].CompactLatest()
			}
		}
	}

	anyCDVSDeleted := false
	anyUTXODeleted := false
	for i := 0; i < nValidators; i++ {
		sd, err := casanovas[i].StateCache.Get(casanovas[i].Consensus.Dag.Tip(casanovas[i].Self_.Id))
		if err == nil {
			if _, ok := sd.Cdvs[mintTxn.ConflictDomain()]; !ok {
				anyCDVSDeleted = true
			}
			if sd.Extra.Ledger != nil {
				mintUtxo := transaction.Utxo{
					TxnId: ids.TransactionId{
						BlockId: blk0m.BlockId(),
						Index:   0,
					},
					Amount: mintTxn.Output_.Amount_,
				}
				state, _ := sd.Extra.Ledger.WalletState(wallets[0].WalletAddress())
				if !state.Has(mintUtxo) {
					anyUTXODeleted = true
				}
			}
		}
	}
	if !anyCDVSDeleted {
		t.Fatalf("Expected Mint transaction CD to be deleted from CDVS")
	}
	if !anyUTXODeleted {
		t.Fatalf("Expected Mint transaction output UTXO to be deleted from ledger")
	}

}
