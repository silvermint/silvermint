// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"crypto/ed25519"
	"strconv"
	"testing"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/status"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
)

func setupNValidators(nValidators int, t *testing.T) ([]*Casanova, []validator.Validator, []crypto.Wallet) {
	viper.Set("lht_keybits", 10)
	viper.Set("save_dir", "/tmp")
	viper.Set("config_dir", "/tmp")
	viper.Set("block_cache_size", 100)
	viper.Set("num_lhts", 100)
	viper.Set("lht_pool_buffer", 100)
	var wallets []crypto.Wallet
	var validators []validator.Validator
	valsMap := map[validator.Validator]bool{}
	valIdsMap := [validator.N_MAX_VALIDATORS]validator.Validator{}
	for i := 0; i < nValidators; i++ {
		// Create wallets
		pub, priv, _ := ed25519.GenerateKey(nil)

		wallets = append(wallets, crypto.BasicWallet{
			Type_:     crypto.BASIC_WALLET,
			Length_:   crypto.BASIC_WALLET_ADDR_SIZE,
			Filename_: "",
			Public_:   pub,
			Private_:  priv,
		})

		// Create validators
		validators = append(validators,
			validator.Validator{
				Id:         uint16(i),
				Active:     true,
				RemoteAddr: nil,
				Hostport:   "0.0.0.0:" + strconv.Itoa(8888+i),
				Conn:       nil,
				Pubkey:     wallets[i].WalletAddress(),
			})
		valsMap[validators[i]] = true
		valIdsMap[uint16(i)] = validators[i]
	}

	var casanovas []*Casanova
	for i := 0; i < nValidators; i++ {
		// Create casanova instances
		cas, err := NewCasanova(validators[i].Id, valIdsMap, nil)
		if err != nil {
			t.Fatalf("Error creating new casanova, %s", err.Error())
		}
		casanovas = append(casanovas, cas)
	}
	return casanovas, validators, wallets
}

func simulateSendBlock(creator int, blk block.Block, casanovas []*Casanova, wallets []crypto.Wallet, t *testing.T) {
	// "Send" the blocks by marshaling byte data
	byteData, err := blk.MarshalSignedBinary(wallets[creator])
	if err != nil {
		t.Fatalf("Error marshaling signed block binary, %s", err.Error())
	}

	// Add the "received" (unmarshaled) block to the other validators' DAGs
	for i := 0; i < len(casanovas); i++ {
		if i != creator {
			unmarshaledBlk := block.NewSilvermintBlock()
			err := unmarshaledBlk.UnmarshalBinary(byteData)
			if err != nil {
				t.Fatalf("Error unmarshaling block binary, %s", err.Error())
			}
			casanovas[i].Add(unmarshaledBlk)
		}
	}
}

// This function accepts a block, "blk", previously issued by
// a validator, "creator", and asserts a "status" for that block
func expect(creator int, txn transaction.Transaction,
	status string, blk block.Block, casanovas []*Casanova,
	vals []validator.Validator, t *testing.T) {

	cas := casanovas[creator]
	// Verify the status for the creator is as expected
	casstatus, err := cas.GetPaymentState(txn.Hash(), txn.CheckNumber(), txn.Source())
	if err != nil {
		t.Fatalf("Error getting transaction status: %s", err.Error())
	}
	if status == "ALL-ARCHIVED" {
		// If block deletion is disabled, don't expect anything to be deleted.
		if viper.GetBool("delete_blocks") && casstatus != "TRANSACTION NOT FOUND" {
			t.Fatalf("Expected status %s, got %s,", status, casstatus)
		}
		return
	} else {
		if casstatus != status {
			t.Fatalf("Expected status %s, got %s", status, casstatus)
		}
	}
}

// This function accepts a validator, "creator". It issues a block
// from that validator, submits it to the other validators from the
// list, "vals", and then asserts a status for the creator.
func expectStatus(creator int, pmt transaction.Payment,
	status status.Status, casanovas []*Casanova,
	vals []validator.Validator, wallets []crypto.Wallet,
	t *testing.T) {

	// Create a new block and add it to the creator's DAG
	cas := casanovas[creator]
	time.Sleep(time.Millisecond)
	blk := cas.Get()

	simulateSendBlock(creator, blk, casanovas, wallets, t)

	// Verify the status for the creator is as expected
	val := vals[creator]
	casstate, _ := cas.StateCache.Get(blk.BlockId())
	casvs := casstate.Cdvs[pmt.ConflictDomain()]
	casstatus := casvs[val.Id]
	if casstatus.Kind() != status {
		t.Fatalf("Expected status %d, got %d", status, casstatus.Kind())
	}
}

func TestStateData(t *testing.T) {
	nValidators := 5
	casanovas, validators, wallets := setupNValidators(nValidators, t)

	// Issue a block for each validator so they have an IncrLedger
	for i := 0; i < nValidators; i++ {
		blk := casanovas[i].Get()
		// "Send" the blocks by marshaling byte data
		simulateSendBlock(i, blk, casanovas, wallets, t)
	}

	// Create mint transaction to wallet 0
	mintTxn := transaction.Mint{
		Src:          wallets[0].WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 100,
			Dest_:   wallets[0].WalletAddress(),
		},
		Bytes: []byte{},
		Sig:   crypto.Signature{},
	}
	mb, err := mintTxn.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for mint txn: %s", err.Error())
	}
	mintTxn.Bytes = mb
	copy(mintTxn.Sig[:], mb[len(mb)-crypto.SIGNATURE_SIZE:])

	// Add the mint transaction to validator 0
	err = casanovas[0].AddTxn(mb)
	if err != nil {
		t.Fatalf("Unable to add mint txn: %s", err.Error())
	}
	// Issue enough blocks from everyone that the mint txn finalizes
	i := 0
	finalized := make([]bool, nValidators)
	for {
		i = i % nValidators
		stat, err := casanovas[i].GetPaymentState(mintTxn.Hash(), mintTxn.CheckNumber(), mintTxn.Source())
		if err != nil {
			t.Fatalf("Error getting mint state: %s", err.Error())
		}
		if stat != "FINALIZED" {
			blk := casanovas[i].Get()
			simulateSendBlock(i, blk, casanovas, wallets, t)
		} else {
			finalized[i] = true
		}
		stop := true
		for j := range finalized {
			stop = stop && finalized[j]
		}
		if stop {
			break
		}
		i++
	}

	// Wallet 0 creates payment transaction P0 to wallet 1
	// with checknumber 2
	latest0, _ := casanovas[0].StateCache.Get(casanovas[0].Consensus.Dag.Tip(casanovas[0].ValidatorId))
	latest1, _ := casanovas[1].StateCache.Get(casanovas[1].Consensus.Dag.Tip(casanovas[1].ValidatorId))
	l0 := latest0.Extra.Ledger
	l1 := latest1.Extra.Ledger

	p0 := transaction.Payment{
		Src:          wallets[0].WalletAddress(),
		CheckNumber_: 2,
		Inputs_:      l0.Utxos(wallets[0].WalletAddress()),
		Outputs_: []transaction.PaymentOutput{
			{
				Amount_: 100,
				Dest_:   wallets[1].WalletAddress(),
			},
		},
		Bytes: []byte{},
		Sig:   crypto.Signature{},
	}
	p0b, err := p0.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal payment 0 to signed binary, %s", err.Error())
	}
	p0.Bytes = p0b
	copy(p0.Sig[:], p0b[len(p0b)-crypto.SIGNATURE_SIZE:])

	// Wallet 0 attempts double-spend by submitting payment
	// transaction P1 to wallet 2 with checknumber 2
	p1 := transaction.Payment{
		Src:          wallets[0].WalletAddress(),
		CheckNumber_: 2,
		Inputs_:      l1.Utxos(wallets[0].WalletAddress()),
		Outputs_: []transaction.PaymentOutput{
			{
				Amount_: 100,
				Dest_:   wallets[2].WalletAddress(),
			},
		},
		Bytes: []byte{},
		Sig:   crypto.Signature{},
	}
	p1b, err := p1.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Unable to marshal payment 1 to signed binary, %s", err.Error())
	}
	p1.Bytes = p1b
	copy(p1.Sig[:], p1b[len(p1b)-crypto.SIGNATURE_SIZE:])

	// P0 submitted to Validator 0
	casanovas[0].AddTxn(p0b)

	// P1 submitted to Validator 1
	casanovas[1].AddTxn(p1b)

	// Generate blocks from Validator 0 and Validator 1, add to their own DAGs
	blk0 := casanovas[0].Get()
	time.Sleep(time.Millisecond)
	blk1 := casanovas[1].Get()

	// Verify initial state for blocks 0 and 1
	expect(0, &p0, "UNCONTESTED", blk0, casanovas, validators, t)

	expect(1, &p1, "UNCONTESTED", blk1, casanovas, validators, t)

	// "Send" the blocks by marshaling byte data
	byteData0, err := blk0.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Error marshaling signed block binary, %s", err.Error())
	}

	byteData1, err := blk1.MarshalSignedBinary(wallets[0])
	if err != nil {
		t.Fatalf("Error marshaling signed block binary, %s", err.Error())
	}

	// Add the "received" (unmarshaled) 0 block to the other validators
	for i := 0; i < nValidators; i++ {
		if uint16(i) != blk0.Creator().Id {
			unmarshaledBlk0 := block.NewSilvermintBlock()
			err := unmarshaledBlk0.UnmarshalBinary(byteData0)
			if err != nil {
				t.Fatalf("Error unmarshaling block binary, %s", err.Error())
			}
			casanovas[i].Add(unmarshaledBlk0)
		}
	}

	// Add the "received" (unmarshaled) 1 block to the other validators
	for i := 0; i < nValidators; i++ {
		if uint16(i) != blk1.Creator().Id {
			unmarshaledBlk1 := block.NewSilvermintBlock()
			err := unmarshaledBlk1.UnmarshalBinary(byteData1)
			if err != nil {
				t.Fatalf("Error unmarshaling block binary, %s", err.Error())
			}
			casanovas[i].Add(unmarshaledBlk1)
		}
	}

	// 2 issues block
	expectStatus(2, p0, status.CONTESTEDLEASTHASH, casanovas, validators, wallets, t)

	// 3 issues block
	expectStatus(3, p0, status.CONTESTEDLEASTHASH, casanovas, validators, wallets, t)

	// 4 has traffic issues, doesn't issue block

	// Second block cycle
	// 0 issues block
	//   Check: transaction is CONTESTED-FTM for 0
	//          because 0 or 1 had the least hash and
	//          2, 3 chose the one with least hash, so
	//          three votes
	expectStatus(0, p0, status.CONTESTEDFTM, casanovas, validators, wallets, t)

	// 1 issues block
	//   Check: transaction is CONTESTED-FTM for 1
	//          because 0 or 1 had the least hash and
	//          2, 3 chose the one with least hash, so
	//          three votes outweigh the 1 or 0 that
	//          had the larger hash
	expectStatus(1, p0, status.CONTESTEDFTM, casanovas, validators, wallets, t)

	// 2 issues block
	//   Check: transaction is CONTESTED-LEASTHASH for 2
	//          Doesn't change because it's not a new round yet
	expectStatus(2, p0, status.CONTESTEDLEASTHASH, casanovas, validators, wallets, t)

	// 3 issues block
	//   Check: transaction is CONTESTED-LEASTHASH for 3
	//          Doesn't change because it's not a new round yet
	expectStatus(3, p0, status.CONTESTEDLEASTHASH, casanovas, validators, wallets, t)

	// 4 does nothing

	// Third block cycle

	casanovas[0].GetPaymentState(p0.Hash(), p0.CheckNumber(), p0.Source())
	casanovas[0].GetPaymentState(p1.Hash(), p1.CheckNumber(), p1.Source())
	// 0 issues block
	//   Check: transaction is CONTESTED-FTM for 0
	//          because it's not a new round yet
	expectStatus(0, p0, status.CONTESTEDFTM, casanovas, validators, wallets, t)

	// 1 issues block
	//   Check: transaction is CONTESTED-FTM for 1
	//          because it's not a new round yet
	expectStatus(1, p0, status.CONTESTEDFTM, casanovas, validators, wallets, t)

	// 2 issues block
	//   Check: transaction is CONTESTED-FTM for 2
	//          because it's not a new round yet
	expectStatus(2, p0, status.CONTESTEDFTM, casanovas, validators, wallets, t)

	// 3 issues block
	//   Check: transaction is CONTESTED-FTM for 3
	//          because it's not a new round yet
	expectStatus(3, p0, status.CONTESTEDFTM, casanovas, validators, wallets, t)

	// 4 does nothing

	// Fourth block cycle

	// 0 issues block
	//   Check: transaction is FINALIZED for 0
	expectStatus(0, p0, status.FINALIZED, casanovas, validators, wallets, t)

	// 1 issues block
	//   Check: transaction is FINALIZED for 1
	expectStatus(1, p0, status.FINALIZED, casanovas, validators, wallets, t)

	// 2 issues block
	//   Check: transaction is CONTESTED-FTM for 2
	//          because it's not a new round yet
	expectStatus(2, p0, status.CONTESTEDFTM, casanovas, validators, wallets, t)

	// 3 issues block
	//   Check: transaction is CONTESTED-FTM for 3
	//          because it's not a new round yet
	expectStatus(3, p0, status.CONTESTEDFTM, casanovas, validators, wallets, t)

	// 4 finally issues block.
	//   Check: transaction is FINALIZED for 4 because it sees FTM of FTMs
	expectStatus(4, p0, status.FINALIZED, casanovas, validators, wallets, t)

	// Fifth block cycle

	// 0 issues block
	//   Check: transaction is FINALIZED for 0
	expectStatus(0, p0, status.FINALIZED, casanovas, validators, wallets, t)

	// 1 issues block
	//   Check: transaction is FINALIZED for 1
	expectStatus(1, p0, status.FINALIZED, casanovas, validators, wallets, t)

	// 2 issues block
	//   Check: transaction is FINALIZED for 2
	expectStatus(2, p0, status.FINALIZED, casanovas, validators, wallets, t)

	// 3 issues block
	//   Check: transaction is FINALIZED for 3
	expectStatus(3, p0, status.FINALIZED, casanovas, validators, wallets, t)

	// 4 issues block.
	//   Check: transaction is ARCHIVED for 4 because everyone is finalized
	expectStatus(4, p0, status.ARCHIVED, casanovas, validators, wallets, t)

	if p0.Hash().Equal(p1.Hash()) {
		t.Fatalf("Transaction 0 and 1 had equal hashes!?!?")
	}

	for _, cas := range casanovas {
		bals := make([]uint64, nValidators)
		latest, _ := cas.StateCache.Get(cas.Consensus.Dag.Tip(cas.ValidatorId))
		for i := 0; i < nValidators; i++ {
			bals[i] = latest.Extra.Ledger.Balance(wallets[i].WalletAddress())
		}
		if bals[0] != 0 {
			t.Fatalf("Wrong final balance for wallet 0")
		}
		if p0.Hash().Lesser(p1.Hash()) {
			if bals[1] != p0.Outputs_[0].Amount_ {
				t.Fatalf("Wrong final balance for wallet 1")
			}
			if bals[2] != 0 {
				t.Fatalf("Wrong final balance for wallet 2")
			}
		} else {
			if bals[1] != 0 {
				t.Fatalf("Wrong final balance for wallet 1")
			}
			if bals[2] != p1.Outputs_[0].Amount_ {
				t.Fatalf("Wrong final balance for wallet 2")
			}
		}
		if bals[3] != 0 {
			t.Fatalf("Wrong final balance for wallet 3")
		}
		if bals[4] != 0 {
			t.Fatalf("Wrong final balance for wallet 4")
		}

	}
}
