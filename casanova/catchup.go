// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"fmt"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/validator"
)

var (
	dagPending = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "casanova_dag_blocks_pending",
		Help: "Number of blocks/parents in the pending queue.",
	})

	dagPendingByVal = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "casanova_dag_blocks_pending_by_val",
		Help: "Number of blocks in the pending queue by creator.",
	},
		[]string{"creator"},
	)
	CatchupUsecTotal = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "catchup_usec_total",
		Help: "Total number of microseconds spent in the catchup protocol.",
	},
		[]string{"function"},
	)
)

func init() {
	prometheus.MustRegister(dagPending)
	prometheus.MustRegister(dagPendingByVal)
	prometheus.MustRegister(CatchupUsecTotal)
}

// Requests missing parents for blocks in the pending queue.
func (cas *Casanova) CatchUp() int {
	nowMicro := time.Now().UnixMicro()
	var parents_requested int = 0

	dagPending.Set(float64(cas.Consensus.Dag.Pending().Len()))

	activeVals := validator.GetGlobalRegistrations().Active()
	for val := range activeVals {
		dagPendingByVal.WithLabelValues(fmt.Sprintf("%d", val.Id)).Set(0)
	}

	pendingByCreator := cas.Consensus.Dag.Pending().PendingByCreator()

	for val := range activeVals {
		dagPendingByVal.WithLabelValues(
			fmt.Sprintf("%d", val.Id)).Add(float64(pendingByCreator[val.Id]))
	}

	// Request any parents that aren't found in either the DAG, the pending
	// queue, or the in-progress map
	cas.Consensus.Dag.Pending().ParentRange(func(bid ids.BlockId, _ []block.Block) bool {
		if !cas.Consensus.Dag.Pending().HasBlockIdInternal(bid) &&
			!cas.InProgress.Has(bid) {
			go cas.catchupRequestor(bid)
			parents_requested++
		}
		return true
	})

	CatchupUsecTotal.WithLabelValues("CatchUp").Add(float64(time.Now().UnixMicro() - nowMicro))
	if parents_requested > 0 {
		log.Debugf("CatchUp: requested %d parents in %f seconds.",
			parents_requested, float64(time.Now().UnixMicro()-nowMicro)*0.000001)
	}

	return parents_requested
}

func (cas *Casanova) CatchUpThread() {
	dag := cas.Consensus.Dag
	for {
		if dag == nil {
			continue
		}

		if dag.Pending().Len() == 0 {
			time.Sleep(time.Second / 2)
		}

		cas.CatchUp()
		time.Sleep(time.Second)
	}
}

// Receives blocks as they're added to the DAG.
// Removes them from the pending queue and determines if there are any newly
// addable blocks that resulted from that block's addition.
// Attempts to add any newly addable blocks to the DAG.
func (cas *Casanova) CatchUpListener() {
	for {
		blk := <-cas.Consensus.Dag.CatchUpChannel()
		if cas.Consensus.Dag.Pending().Has(blk) {
			addableBlks := cas.Consensus.Dag.Pending().DeleteBlock(blk)
			log.Debugf("Block %s received by CatchUpListener, found %d newly addable blocks",
				blk.BlockId(), len(addableBlks))
			if len(addableBlks) > 0 {
				for _, addableBlk := range addableBlks {
					go cas.AddBlockFromSource(addableBlk, "catchup")
				}
			}
		}
	}
}
