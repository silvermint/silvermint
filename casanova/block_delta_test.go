package casanova

import (
	"testing"
	"time"

	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/crypto"
)

func testMarshalUnmarshalBlock(blk block.Block, wallet crypto.Wallet, cas *Casanova, t *testing.T) block.Block {
	blkBytes, err := blk.MarshalSignedBinary(wallet)
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for blk: %s", err.Error())
	}
	newBlk := block.NewSilvermintBlock()
	err = newBlk.UnmarshalBinary(blkBytes)
	if err != nil {
		t.Fatalf("Error unmarshaling block binary, %s", err.Error())
	}
	return newBlk
}

func TestBlockDeltas(t *testing.T) {
	nValidators := 2

	casanovas, _, wallets := setupNValidators(nValidators, t)

	blk0_0 := casanovas[0].Get()
	time.Sleep(time.Millisecond)
	blk0_1 := casanovas[0].Get()
	time.Sleep(time.Millisecond)
	blk0_2 := casanovas[0].Get()
	time.Sleep(time.Millisecond)
	blk0_3 := casanovas[0].Get()
	time.Sleep(time.Millisecond)
	blk0_4 := casanovas[0].Get()
	time.Sleep(time.Millisecond)

	blk1_0 := casanovas[1].Get()
	time.Sleep(time.Millisecond)

	casanovas[0].Add(testMarshalUnmarshalBlock(blk1_0, wallets[1], casanovas[0], t))

	blk0_5 := casanovas[0].Get()
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_5, wallets[0], casanovas[1], t))

	valId := 0
	delta := 0
	if blk0_0.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_0 should have delta %d for validator %d, got %d",
			delta, valId, blk0_0.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 0
	if blk0_0.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_0 should have delta %d for validator %d, got %d",
			delta, valId, blk0_0.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 0
	delta = 1
	if blk0_1.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_1 should have delta %d for validator %d, got %d",
			delta, valId, blk0_1.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 0
	if blk0_1.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_1 should have delta %d for validator %d, got %d",
			delta, valId, blk0_1.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 0
	delta = 1
	if blk0_2.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_2 should have delta %d for validator %d, got %d",
			delta, valId, blk0_2.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 0
	if blk0_2.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_2 should have delta %d for validator %d, got %d",
			delta, valId, blk0_2.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 0
	delta = 1
	if blk0_3.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_3 should have delta %d for validator %d, got %d",
			delta, valId, blk0_3.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 0
	if blk0_3.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_3 should have delta %d for validator %d, got %d",
			delta, valId, blk0_3.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 0
	delta = 1
	if blk0_4.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_4 should have delta %d for validator %d, got %d",
			delta, valId, blk0_4.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 0
	if blk0_4.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_4 should have delta %d for validator %d, got %d",
			delta, valId, blk0_4.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 0
	delta = 1
	if blk0_5.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_5 should have delta %d for validator %d, got %d",
			delta, valId, blk0_5.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 1
	if blk0_5.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_5 should have delta %d for validator %d, got %d",
			delta, valId, blk0_5.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 0
	delta = 0
	if blk1_0.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk1_0 should have delta %d for validator %d, got %d",
			delta, valId, blk1_0.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 0
	if blk1_0.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk1_0 should have delta %d for validator %d, got %d",
			delta, valId, blk1_0.Deltas().Get(uint16(valId)).Delta)
	}

	blk1_1 := casanovas[1].Get()
	time.Sleep(time.Millisecond)
	casanovas[0].Add(testMarshalUnmarshalBlock(blk1_1, wallets[1], casanovas[0], t))
	blk0_6 := casanovas[0].Get()
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_6, wallets[0], casanovas[1], t))
	blk1_2 := casanovas[1].Get()
	time.Sleep(time.Millisecond)
	casanovas[0].Add(testMarshalUnmarshalBlock(blk1_2, wallets[1], casanovas[0], t))
	blk0_7 := casanovas[0].Get()
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_7, wallets[0], casanovas[1], t))

	valId = 0
	delta = 0
	if blk1_1.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk1_1 should have delta %d for validator %d, got %d",
			delta, valId, blk1_1.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 1
	if blk1_1.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk1_1 should have delta %d for validator %d, got %d",
			delta, valId, blk1_1.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 0
	delta = 1
	if blk0_6.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_6 should have delta %d for validator %d, got %d",
			delta, valId, blk0_6.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 1
	if blk0_6.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_6 should have delta %d for validator %d, got %d",
			delta, valId, blk0_6.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 0
	delta = 0
	if blk1_2.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk1_2 should have delta %d for validator %d, got %d",
			delta, valId, blk1_2.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 1
	if blk1_2.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk1_2 should have delta %d for validator %d, got %d",
			delta, valId, blk1_2.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 0
	delta = 1
	if blk0_7.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_7 should have delta %d for validator %d, got %d",
			delta, valId, blk0_7.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 1
	if blk0_7.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_7 should have delta %d for validator %d, got %d",
			delta, valId, blk0_7.Deltas().Get(uint16(valId)).Delta)
	}

	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_0, wallets[0], casanovas[1], t))
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_1, wallets[0], casanovas[1], t))
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_2, wallets[0], casanovas[1], t))
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_3, wallets[0], casanovas[1], t))
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_4, wallets[0], casanovas[1], t))
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_5, wallets[0], casanovas[1], t))
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_6, wallets[0], casanovas[1], t))
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_7, wallets[0], casanovas[1], t))
	time.Sleep(time.Millisecond)

	blk1_3 := casanovas[1].Get()
	time.Sleep(time.Millisecond)
	casanovas[0].Add(testMarshalUnmarshalBlock(blk1_3, wallets[1], casanovas[0], t))
	blk0_8 := casanovas[0].Get()
	time.Sleep(time.Millisecond)
	casanovas[1].Add(testMarshalUnmarshalBlock(blk0_8, wallets[0], casanovas[1], t))

	valId = 0
	delta = 8
	if blk1_3.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk1_3 should have delta %d for validator %d, got %d",
			delta, valId, blk1_3.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 1
	if blk1_3.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk1_3 should have delta %d for validator %d, got %d",
			delta, valId, blk1_3.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 0
	delta = 1
	if blk0_8.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_8 should have delta %d for validator %d, got %d",
			delta, valId, blk0_8.Deltas().Get(uint16(valId)).Delta)
	}
	valId = 1
	delta = 1
	if blk0_8.Deltas().Get(uint16(valId)).Delta != uint32(delta) {
		t.Fatalf("blk0_8 should have delta %d for validator %d, got %d",
			delta, valId, blk0_8.Deltas().Get(uint16(valId)).Delta)
	}
}
