package casanova

import (
	"fmt"
	"math"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func RandTxnId() ids.TransactionId {
	return ids.TransactionId{
		BlockId: RandBlockId(),
		Index:   uint16(rand.Intn(math.MaxUint16)),
	}
}

func RandTransaction() transaction.Transaction {
	ttype := rand.Intn(3)
	src := RandWallet()

	var txn transaction.Transaction
	switch ttype {
	case 0:
		mint := transaction.Mint{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Output_: transaction.MintOutput{
				Amount_: uint64(rand.Intn(math.MaxInt)),
				Dest_:   src.WalletAddress(),
			},
		}
		txn = &mint
	case 1:
		pmt := transaction.Payment{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Inputs_: []transaction.Utxo{
				{
					TxnId:  RandTxnId(),
					Amount: rand.Uint64(),
				},
			},
			Outputs_: []transaction.PaymentOutput{
				{
					Amount_: uint64(rand.Intn(math.MaxInt)),
					Dest_:   src.WalletAddress(),
				},
			},
		}
		txn = &pmt
	case 2:
		th := transaction.TransactionHash{}
		copy(th[:], RandBytes(crypto.HASH_SIZE))
		sweep := transaction.Sweep{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Inputs_: []transaction.Utxo{
				{
					TxnId:  RandTxnId(),
					Amount: rand.Uint64(),
				},
			},
			Output_: transaction.SweepOutput{
				Amount_: uint64(rand.Intn(math.MaxInt)),
				Dest_:   src.WalletAddress(),
			},
		}
		txn = &sweep
	}
	return txn
}

func RandValidator(i int) validator.Validator {
	val := validator.Validator{
		Id:       uint16(i),
		Active:   true,
		Hostport: fmt.Sprintf("something:%d", i),
		Pubkey:   RandWallet().WalletAddress(),
	}
	return val
}

func VerifyTxns(txns, loadedTxns []transaction.Transaction, t *testing.T) {
	for i, txn := range txns {
		if txn.Hash() != loadedTxns[i].Hash() {
			t.Fatalf("Loaded transaction hash didn't match original: %x != %x",
				loadedTxns[i].Hash(), txn.Hash())
		}
		if txn.ConflictDomain() != loadedTxns[i].ConflictDomain() {
			t.Fatalf("Loaded transaction CD didn't match original: %v != %v",
				loadedTxns[i].ConflictDomain(), txn.ConflictDomain())
		}
	}
}

func VerifyValidators(vals, loadedVals validator.ValidatorSet, t *testing.T) {
	for val := range vals {
		_, ok := loadedVals[val]
		if !ok {
			t.Fatalf("Loaded validators list was missing validator %d", val.Id)
		}
	}
}

func VerifyHeights(heights, loadedHeights map[validator.Validator]uint64, t *testing.T) {
	for val, height := range heights {
		loadedHeight, ok := loadedHeights[val]
		if !ok {
			t.Fatalf("Loaded heights list was missing validator %d", val.Id)
		}
		if height != loadedHeight {
			t.Fatalf("Loaded validator height didn't match original: %d != %d",
				loadedHeight, height)
		}
	}
}

func TestStateDataRoundTrip(t *testing.T) {
	nBlocks := 100
	nTxns := 100
	nAdded := 100
	nValidators := 2000
	nSlashed := 5

	// Make StateData
	sd := StateData{}
	// Make BVS
	sd.Bvs = RandBVS(nBlocks)

	// Make CDVS
	sd.Cdvs = RandCDVS(nTxns)

	// Make AddedTxns
	sd.AddedTxns = []transaction.Transaction{}
	for i := 0; i < nAdded; i++ {
		sd.AddedTxns = append(sd.AddedTxns, RandTransaction())
	}

	// Make Validators
	sd.Validators = validator.ValidatorSet{}
	for i := 0; i < nValidators; i++ {
		sd.Validators[RandValidator(i)] = true
	}

	// Make Slashed
	sd.Slashed = validator.ValidatorSet{}
	for i := 0; i < nSlashed; i++ {
		sd.Slashed[RandValidator(i)] = true
	}

	// Make FTM
	sd.Ftm = uint16(rand.Intn(math.MaxUint16))

	// Make Height
	sd.Height = map[validator.Validator]uint64{}
	for v := range sd.Validators {
		sd.Height[v] = uint64(rand.Intn(math.MaxInt))
	}

	// Ignore extra (for now)

	sdBytes, err := sd.MarshalBinary()
	if err != nil {
		t.Fatalf("Unable to marshal StateData: %s", err.Error())
	}
	loadedSD := emptyStateData(CasanovaExtra{})
	err = loadedSD.UnmarshalBinary(sdBytes)
	if err != nil {
		t.Fatalf("Unable to unmarshal StateData: %s", err.Error())
	}

	VerifyBVS(sd.Bvs, loadedSD.Bvs, t)
	VerifyCDVS(sd.Cdvs, loadedSD.Cdvs, t)
	VerifyTxns(sd.AddedTxns, loadedSD.AddedTxns, t)
	VerifyValidators(sd.Validators, loadedSD.Validators, t)
	VerifyValidators(sd.Slashed, loadedSD.Slashed, t)
	if sd.Ftm != loadedSD.Ftm {
		t.Fatalf("Loaded FTM value didn't match original %d != %d",
			loadedSD.Ftm, sd.Ftm)
	}
	VerifyHeights(sd.Height, loadedSD.Height, t)
}
