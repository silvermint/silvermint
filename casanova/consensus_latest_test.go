// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"testing"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/status"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
)

type TestLatestTransaction struct {
	transaction.Transaction

	conflictDomain TestLatestCD
	value          string
}

type TestLatestCD struct {
	transaction.CD

	val bool
}

func (t TestLatestCD) isConflictDomain() {}

func (txn TestLatestTransaction) ConflictDomain() transaction.CD  { return txn.conflictDomain }
func (txn TestLatestTransaction) Value() string                   { return txn.value }
func (txn TestLatestTransaction) Length() uint16                  { return 0 }
func (txn TestLatestTransaction) MarshalBinary() ([]byte, error)  { return make([]byte, 0), nil }
func (txn *TestLatestTransaction) UnmarshalBinary(b []byte) error { return nil }

func NewTLT(cd bool, value string) *TestLatestTransaction {
	t := new(TestLatestTransaction)
	t.conflictDomain = TestLatestCD{val: cd}
	t.value = value
	return t
}

func TestSingleCDSingleValidator(t *testing.T) {
	initial := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})

	src, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	txn := transaction.Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 12345,
		Outputs_: []transaction.PaymentOutput{{
			Amount_: 1024,
			Dest_:   dst.WalletAddress(),
		}},
	}

	va := validator.Validator{Id: 0, Hostport: "somewhere.com:900"}
	stat := status.Uncontested{TxnId_: txn.TxnId()}
	cdvs := CDVS{
		txn.ConflictDomain(): &ContestedVS{
			0: stat,
		},
	}
	A := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})
	A.Validators = validator.ValidatorSet{va: true}
	A.Cdvs = cdvs
	A.Ftm = 1
	A.Height = map[validator.Validator]uint64{va: 1}
	result := reducer([]StateData{initial, A}, ids.BlockId{})

	t.Run("one validator", func(t *testing.T) {
		size := len(result.Validators)
		if size != 1 {
			t.Errorf("Got %d, expected 1", size)
		}
	})
	t.Run("va among validators", func(t *testing.T) {
		if !result.Validators[va] {
			t.Error("Got false, expected true")
		}
	})
	t.Run("empty slashed set", func(t *testing.T) {
		size := len(result.Slashed)
		if size != 0 {
			t.Errorf("Got %d, expected 0", size)
		}
	})
	t.Run("ftm = 1", func(t *testing.T) {
		if result.Ftm != 1 {
			t.Errorf("Got %d, expected 1", result.Ftm)
		}
	})
	t.Run("max height for va is 1", func(t *testing.T) {
		if h := result.Height[va]; h != 1 {
			t.Errorf("Got %d, expected 1", h)
		}
	})
}

func TestSingleCDTwoValidators(t *testing.T) {
	initial := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})

	src, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	txnA := transaction.Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 12345,
		Outputs_: []transaction.PaymentOutput{{
			Amount_: 1024,
			Dest_:   dst.WalletAddress(),
		}},
	}

	va := validator.Validator{Id: 0, Hostport: "somewhere.com:900"}
	statusA := status.Uncontested{TxnId_: txnA.TxnId()}
	cdvsA := CDVS{
		txnA.ConflictDomain(): &ContestedVS{
			0: statusA,
		},
	}
	A := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})
	A.Validators = validator.ValidatorSet{va: true}
	A.Cdvs = cdvsA
	A.Ftm = 1
	A.Height = map[validator.Validator]uint64{va: 1}
	afterA := reducer([]StateData{initial, A}, ids.BlockId{})

	txnB := transaction.Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 12346,
		Outputs_: []transaction.PaymentOutput{{
			Amount_: 1024,
			Dest_:   dst.WalletAddress(),
		}},
	}

	vb := validator.Validator{Id: 1, Hostport: "node1.pyrofex.io:900"}
	statusB := status.Uncontested{TxnId_: txnB.TxnId()}
	cdvsB := CDVS{
		txnB.ConflictDomain(): &ContestedVS{
			1: statusB,
		},
	}
	B := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})
	B.Validators = validator.ValidatorSet{vb: true}
	B.Cdvs = cdvsB
	B.Ftm = 1
	B.Height = map[validator.Validator]uint64{vb: 1}
	result := reducer([]StateData{afterA, B}, ids.BlockId{})

	t.Run("two validators", func(t *testing.T) {
		size := len(result.Validators)
		if size != 2 {
			t.Errorf("Got %d, expected 2", size)
		}
	})
	t.Run("va among validators", func(t *testing.T) {
		if !result.Validators[va] {
			t.Error("Got false, expected true")
		}
	})
	t.Run("vb among validators", func(t *testing.T) {
		if !result.Validators[vb] {
			t.Error("Got false, expected true")
		}
	})
	t.Run("empty slashed set", func(t *testing.T) {
		size := len(result.Slashed)
		if size != 0 {
			t.Errorf("Got %d, expected 0", size)
		}
	})
	t.Run("ftm = 1", func(t *testing.T) {
		if result.Ftm != 1 {
			t.Errorf("Got %d, expected 1", result.Ftm)
		}
	})
	t.Run("va sees cd as uncontested", func(t *testing.T) {
		vs, vsok := result.Cdvs[txnA.ConflictDomain()]
		if !vsok {
			t.Error("Got false, expected true")
		}
		s := vs[va.Id]
		if s == nil {
			t.Error("Got false, expected true")
		}
		kind := s.Kind()
		if kind != status.UNCONTESTED {
			t.Errorf("Got %d, expected %d", kind, status.UNCONTESTED)
		}
	})
	t.Run("vb sees cd as uncontested", func(t *testing.T) {
		vs, vsok := result.Cdvs[txnB.ConflictDomain()]
		if !vsok {
			t.Error("Got false, expected true")
		}
		s := vs[vb.Id]
		if s == nil {
			t.Error("Got false, expected true")
		}
		kind := s.Kind()
		if kind != status.UNCONTESTED {
			t.Errorf("Got %d, expected %d", kind, status.UNCONTESTED)
		}
	})
}

func TestSingleCDOneValidatorAtDifferentHeights(t *testing.T) {
	initial := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})

	src, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	txnA1 := transaction.Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 12345,
		Outputs_: []transaction.PaymentOutput{{
			Amount_: 1024,
			Dest_:   dst.WalletAddress(),
		}},
	}

	va := validator.Validator{Id: 0, Hostport: "somewhere.com:900"}
	statusA1 := status.Uncontested{TxnId_: txnA1.TxnId()}
	cdvsA1 := CDVS{
		txnA1.ConflictDomain(): &ContestedVS{0: statusA1},
	}
	A1 := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})
	A1.Validators = validator.ValidatorSet{va: true}
	A1.Cdvs = cdvsA1
	A1.Ftm = 1
	A1.Height = map[validator.Validator]uint64{va: 1}
	afterA1 := reducer([]StateData{initial, A1}, ids.BlockId{})

	txnA2 := transaction.Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 12346,
		Outputs_: []transaction.PaymentOutput{{
			Amount_: 1024,
			Dest_:   dst.WalletAddress(),
		}},
	}

	statusA2 := status.Uncontested{TxnId_: txnA2.TxnId()}
	cdvsA2 := CDVS{
		txnA2.ConflictDomain(): &ContestedVS{0: statusA2},
	}
	A2 := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})
	A2.Validators = validator.ValidatorSet{va: true}
	A2.Cdvs = cdvsA2
	A2.Ftm = 1
	A2.Height = map[validator.Validator]uint64{va: 2}
	result := reducer([]StateData{afterA1, A2}, ids.BlockId{})

	t.Run("one validators", func(t *testing.T) {
		size := len(result.Validators)
		if size != 1 {
			t.Errorf("Got %d, expected 1", size)
		}
	})
	t.Run("va among validators", func(t *testing.T) {
		if !result.Validators[va] {
			t.Error("Got false, expected true")
		}
	})
	t.Run("empty slashed set", func(t *testing.T) {
		size := len(result.Slashed)
		if size != 0 {
			t.Errorf("Got %d, expected 0", size)
		}
	})
	t.Run("ftm = 1", func(t *testing.T) {
		if result.Ftm != 1 {
			t.Errorf("Got %d, expected 1", result.Ftm)
		}
	})
	t.Run("max height for va is 2", func(t *testing.T) {
		if h := result.Height[va]; h != 2 {
			t.Errorf("Got %d, expected 2", h)
		}
	})
}

func TestTwoCDsTwoValidators(t *testing.T) {
	initial := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})

	src, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	txnA := transaction.Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 12345,
		Outputs_: []transaction.PaymentOutput{{
			Amount_: 1024,
			Dest_:   dst.WalletAddress(),
		}},
	}

	va := validator.Validator{Id: 0, Hostport: "somewhere.com:900"}
	statusA := status.Uncontested{TxnId_: txnA.TxnId()}
	cdvsA := CDVS{
		txnA.ConflictDomain(): &ContestedVS{0: statusA},
	}
	A := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})
	A.Validators = validator.ValidatorSet{va: true}
	A.Cdvs = cdvsA
	A.Ftm = 1
	A.Height = map[validator.Validator]uint64{va: 1}
	afterA := reducer([]StateData{initial, A}, ids.BlockId{})

	txnB := transaction.Payment{
		Src:          src.WalletAddress(),
		CheckNumber_: 12345,
		Outputs_: []transaction.PaymentOutput{{
			Amount_: 1024,
			Dest_:   dst.WalletAddress(),
		}},
	}

	vb := validator.Validator{Id: 1, Hostport: "somewhere-else.com:900"}
	statusB := status.Uncontested{TxnId_: txnB.TxnId()}
	cdvsB := CDVS{
		txnB.ConflictDomain(): &ContestedVS{1: statusB},
	}
	B := emptyStateData(CasanovaExtra{Ledger: transaction.NewSSLedger(0)})
	B.Validators = validator.ValidatorSet{vb: true}
	B.Cdvs = cdvsB
	B.Ftm = 1
	B.Height = map[validator.Validator]uint64{vb: 1}
	result := reducer([]StateData{afterA, B}, ids.BlockId{})

	t.Run("two validators", func(t *testing.T) {
		size := len(result.Validators)
		if size != 2 {
			t.Errorf("Got %d, expected 2", size)
		}
	})
	t.Run("va among validators", func(t *testing.T) {
		if !result.Validators[va] {
			t.Error("Got false, expected true")
		}
	})
	t.Run("vb among validators", func(t *testing.T) {
		if !result.Validators[vb] {
			t.Error("Got false, expected true")
		}
	})
	t.Run("empty slashed set", func(t *testing.T) {
		size := len(result.Slashed)
		if size != 0 {
			t.Errorf("Got %d, expected 0", size)
		}
	})
	t.Run("ftm = 1", func(t *testing.T) {
		if result.Ftm != 1 {
			t.Errorf("Got %d, expected 1", result.Ftm)
		}
	})
	t.Run("va sees cd as uncontested", func(t *testing.T) {
		vs, vsok := result.Cdvs[txnA.ConflictDomain()]
		if !vsok {
			t.Error("Got false, expected true")
		}
		s := vs[va.Id]
		if s == nil {
			t.Error("Got false, expected true")
		}
		kind := s.Kind()
		if kind != status.UNCONTESTED {
			t.Errorf("Got %d, expected %d", kind, status.UNCONTESTED)
		}
	})
	t.Run("vb sees cd as uncontested", func(t *testing.T) {
		vs, vsok := result.Cdvs[txnB.ConflictDomain()]
		if !vsok {
			t.Error("Got false, expected true")
		}
		s := vs[vb.Id]
		if s == nil {
			t.Error("Got false, expected true")
		}
		kind := s.Kind()
		if kind != status.UNCONTESTED {
			t.Errorf("Got %d, expected %d", kind, status.UNCONTESTED)
		}
	})
}
