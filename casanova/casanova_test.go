// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package casanova

import (
	"testing"

	"gitlab.com/silvermint/silvermint/transaction"
)

func TestOccupyExistingCD(t *testing.T) {
	nValidators := 1
	casanovas, _, wallets := setupNValidators(nValidators, t)

	cas := casanovas[0]
	wal := wallets[0]

	mint1 := transaction.Mint{
		Src:          wal.WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 100,
			Dest_:   wal.WalletAddress(),
		},
	}
	m1data, err := mint1.MarshalSignedBinary(wal)
	if err != nil {
		t.Fatalf("Unable to marshal mint1 bytes: %s", err.Error())
	}

	err = cas.AddTxn(m1data)
	if err != nil {
		t.Fatalf("Unable to add mint1 to queue: %s", err.Error())
	}

	// Issue a block to let the first mint go to consensus
	cas.Get()

	mint2 := transaction.Mint{
		Src:          wal.WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 200,
			Dest_:   wal.WalletAddress(),
		},
	}
	m2data, err := mint2.MarshalSignedBinary(wal)
	if err != nil {
		t.Fatalf("Unable to marshal mint2 bytes: %s", err.Error())
	}

	err = cas.AddTxn(m2data)
	if err == nil {
		t.Fatalf("Shouldn't have been able to add second mint transaction " +
			"(would've been slashable for occupying existing CD)")
	}
}
