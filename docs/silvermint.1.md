% silvermint(8)
% Nash E. Foster and Michael A. Stay
% March 2023


# NAME

silvermint - Silvermint daemon software (a.k.a. the "node")

# SYNOPSIS

silvermint [-h] <options>

# DESCRIPTION

Silvermint is a leaderless cryptocurrency network built on top of the Casanova
protocol, which was designed and build by Pyrofex Corporation starting in 2018.
To participate in the network. you need to run a "node" that has permission
(from the other nodes) to bond to the network and produce blocks. This man page
documents the Unix daemon written by Pyrofex as a reference implementation for
the network's node software.

Most configuration settings should be put into
`/etc/silvermint/silvermint.yaml`, but for operational convenience nearly all
settings are also available as command line options.

# OPTIONS

**-h**, **--help** : Prints cmd-line usage and exits.

**--api_ipaddr string**
: The IP address that the Client API listens on. (default "0.0.0.0")

**--api_port int**
: The port on which the client api HTTP server listens. (default 8880)

**--block_receiver_channels uint32**
: Number of channels in the block receiver pool. (default 100)

**--block_receiver_hostport string**
: TCP hostport the block receiver listens on. (default "0.0.0.0:900")

**--block_receiver_workers uint32**
: Number of worker threads in the block receiver pool. (default 1000)

**--catchup_listener_buffer uint**
: Buffer size for the catchup listener channel. (default 100)

**--compaction_frequency int**
: The amount of time to wait between compacting ledgers (seconds). (default 10)

**--config_dir string**
: Directory for private files that need root permisions to access. If empty:
/etc/silvermint. (default "/etc/silvermint")

**--cpuprof_interval string**
: Interval after which to write a CPU profile. (default "1m")

**--cpuprof_output string**
: CPU profile output filename. (default "silvermint_cpu.prof")

**--cycle_duration string**
: Block cycle duration. (default "1s")

**--debug**
: Enable debug messages.

**--delete_blocks**
: Enables Delete Blocks Routine. (default true)

**--delete_blocks_frequency int**
: The amount of time to wait between deleting blocks (seconds). (default 30)

**--delete_txns**
: Enables Delete Transactions Routine. (default true)

**--delete_txns_frequency int**
: The amount of time to wait between deleting transactions (seconds). (default 5)

**--enable_cpuprof**
: Enable CPU profiling.

**--enable_memprof**
: Enable memory profiling.

**--enable_mutexprof**
: Enable mutex profiling.

**--genesis_file string**
: Filepath to load a Genesis block from. Default is create new genesis.

**--gin_mode string**
: The mode in which the Gin Engine runs. (default "release")

**--lht_keybits uint16**
: Keybits to use for the LinearHashTable data structure. (default 24)

**--load_snapshot**
: Attempt to load snapshot files if they exist. (default true)

**--log_dir string**
: Directory for log files. If empty: /var/silvermint/log. (default "/var/silvermint/log")

**--log_level int**
: Level that determines what logs are recorded, default is 4. 0: Fatal, 1: Error, 2: Warning, 3: Info, 4: Debug, 5: Trace (default 4)

**--log_suffix string**
: Suffix to append to silvermint log filenames.

**--logtostdout**
: Enable to log to STDOUT instead of a log file.

**--management_console_ipaddr string**
: The IP address for the Management Console to listen on. (default "0.0.0.0")

**--max_block_sender_retires int**
: Number of retries to send block to validator on timeout error. If empty: 3. (default 3)

**--max_block_size uint**
: Max block size [bytes]. (default 256000)

**--memprof_interval string**
: Interval after which to write a memory profile. (default "1m")

**--memprof_output string**
: Memory profile output filename. (default "silvermint_mem.prof")

**--mutexprof_interval string**
: Interval after which to write a mutex profile. (default "1m")

**--mutexprof_output string**
: Mutex profile output filename. (default "silvermint_mutex.prof")

**--mutexprof_rate int**
: Mutex profiling rate. On average, every 1/N contention events are reported. (default 1)

**--port int**
: The port on which the management console HTTP server listens. (default 8888)

**--pprof_ipaddr string**
: The IP address that the PPROF data is served on. (default "0.0.0.0")

**--pprof_port int**
: The port on which the PPROF data is served. (default 8898)

**--save_dir string**
: Directory for dag snapshot save files. If empty: ~/save. (default "/var/silvermint/save")

**--save_state**
: Enable/disable saving block state to disk. (default true)

**--templates_path string**
: A list of potential locations to find HTML templates (separated by OS-specific path list separator). (default "/usr/lib/silvermint/templates:./templates")

**--txn_gc_buffer uint**
: Buffer size for the transaction GC channel. (default 100)

**--validator_id string**
: The validator ID. (default "0")

**--validators string**
: Comma separated list of validator specs. (default "0/127.0.0.1:900")


# FILES

**/etc/silvermint/silvermint.yaml**

The primary configuration file.

# SEE ALSO

simulate(1), wallet(1)

# AUTHORS

Silvermint is the work of Pyrofex Corporation. Original designed and
implemented by Nash E. Foster and Michael A. Stay, it has since received
important contributions from Charles Whitlock, Steve Werner, and Andrew
Johnson, among others.
