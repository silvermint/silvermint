// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package status

import (
	"encoding/binary"
	"fmt"

	"gitlab.com/silvermint/silvermint/ids"
)

type Status uint8

const (
	UNSEEN             Status = 0
	UNCONTESTED        Status = 1
	UNCONTESTEDFTM     Status = 2
	CONTESTEDLEASTHASH Status = 3
	CONTESTEDFTM       Status = 4
	FINALIZED          Status = 5
	ARCHIVED           Status = 6
)

type ContestedStatus interface {
	// For use in consensus.invert()
	Kind() Status
	TxnId() ids.TransactionId
	isStatus()
}

type Archived struct {
	TxnId_ ids.TransactionId
}

func (Archived) isStatus()                  {}
func (Archived) Kind() Status               { return ARCHIVED }
func (a Archived) TxnId() ids.TransactionId { return a.TxnId_ }

type Finalized struct {
	TxnId_ ids.TransactionId
}

func (Finalized) isStatus()                  {}
func (Finalized) Kind() Status               { return FINALIZED }
func (f Finalized) TxnId() ids.TransactionId { return f.TxnId_ }

type UncontestedFTM struct {
	TxnId_ ids.TransactionId
}

func (UncontestedFTM) isStatus()                  {}
func (UncontestedFTM) Kind() Status               { return UNCONTESTEDFTM }
func (u UncontestedFTM) TxnId() ids.TransactionId { return u.TxnId_ }

type Uncontested struct {
	TxnId_ ids.TransactionId
}

func (Uncontested) isStatus()                  {}
func (Uncontested) Kind() Status               { return UNCONTESTED }
func (u Uncontested) TxnId() ids.TransactionId { return u.TxnId_ }

type ContestedFTM struct {
	TxnId_ ids.TransactionId
	Idx    uint64
}

func (ContestedFTM) isStatus()                  {}
func (ContestedFTM) Kind() Status               { return CONTESTEDFTM }
func (c ContestedFTM) TxnId() ids.TransactionId { return c.TxnId_ }
func (c ContestedFTM) Index() uint64            { return c.Idx }

type ContestedLeastHash struct {
	TxnId_ ids.TransactionId
	Idx    uint64
}

func (ContestedLeastHash) isStatus()                  {}
func (ContestedLeastHash) Kind() Status               { return CONTESTEDLEASTHASH }
func (c ContestedLeastHash) TxnId() ids.TransactionId { return c.TxnId_ }
func (c ContestedLeastHash) Index() uint64            { return c.Idx }

func Length(s ContestedStatus) uint8 {
	var l uint8 = 1 // Length
	l += 1          // Status
	// TODO(chuck): Writing the transaction hash for each validator is
	// a pretty inefficient way to do things, but it's easy. We could
	// possibly come up with a fancier method of bunching validators
	// under the transactionhash that their status applies to, but we
	// would have to something similar to computing a CDTSNT - which
	// would need CPU. Since we don't expect many contested
	// transactions I think this is OK for now.
	l += uint8(ids.TransactionIdLength()) // Txn hash
	if s.Kind() == CONTESTEDLEASTHASH || s.Kind() == CONTESTEDFTM {
		l += 8 // Index
	}
	return l
}

func MarshalBinary(s ContestedStatus) ([]byte, error) {
	slen := Length(s)
	data := make([]byte, slen)
	var offset uint8 = 0

	// Length
	data[offset] = slen
	offset += 1
	// Status
	data[offset] = uint8(s.Kind())
	offset += 1
	// TransactionId
	tlen := uint8(ids.TransactionIdLength())
	tdat, err := s.TxnId().MarshalBinary()
	if err != nil {
		return nil, err
	}
	copy(data[offset:offset+tlen], tdat[:])
	offset += tlen
	// Index (ContestedLeastHash / ContestedFTM only)
	switch s := s.(type) {
	case ContestedLeastHash:
		binary.LittleEndian.PutUint64(data[offset:offset+8], s.Index())
		offset += 8
	case ContestedFTM:
		binary.LittleEndian.PutUint64(data[offset:offset+8], s.Index())
		offset += 8
	}
	return data, nil
}

func UnmarshalBinary(data []byte) (ContestedStatus, error) {
	if len(data) == 0 {
		return nil, fmt.Errorf("ERROR: Can't unmarshal empty byte array")
	}

	var offset uint8 = 0

	// Length
	slen := uint8(data[offset])
	offset += 1
	if uint8(len(data)) != slen {
		return nil, fmt.Errorf("ERROR: Status has length %d bytes, "+
			"but we have %d bytes of data", len(data), len(data))
	}

	// Status
	stat := Status(data[offset])
	offset += 1
	// TransactionId
	var err error
	tlen := uint8(ids.TransactionIdLength())
	tid, err := ids.UnmarshalTransactionId(data[offset : offset+tlen])
	if err != nil {
		return nil, err
	}
	offset += tlen
	// Index (ContestedLeastHash / ContestedFTM only)
	var cstat ContestedStatus
	switch stat {
	case UNSEEN:
		// This case shouldn't be possible
		cstat = nil
	case UNCONTESTED:
		cstat = Uncontested{
			TxnId_: tid,
		}
	case UNCONTESTEDFTM:
		cstat = UncontestedFTM{
			TxnId_: tid,
		}
	case CONTESTEDLEASTHASH:
		index := binary.LittleEndian.Uint64(data[offset : offset+8])
		offset += 8
		cstat = ContestedLeastHash{
			TxnId_: tid,
			Idx:    index,
		}
	case CONTESTEDFTM:
		index := binary.LittleEndian.Uint64(data[offset : offset+8])
		offset += 8
		cstat = ContestedFTM{
			TxnId_: tid,
			Idx:    index,
		}
	case FINALIZED:
		cstat = Finalized{
			TxnId_: tid,
		}
	case ARCHIVED:
		cstat = Archived{
			TxnId_: tid,
		}
	default:
		return nil, fmt.Errorf("ERROR: Unrecognized status %d", stat)
	}

	if offset != slen {
		return nil, fmt.Errorf("ERROR: Read bytes didn't match indicated "+
			"length, %d != %d", offset, slen)
	}

	return cstat, nil
}
