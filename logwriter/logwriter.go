// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package logwriter

import (
	"os"
	"path/filepath"

	"github.com/spf13/viper"
)

// For now LogWriter just wraps a single file.
type LogWriter struct {
	dir         string
	max_size_mb int
	fd          *os.File
}

func NewLogWriter(dir string) (*LogWriter, error) {
	lw := LogWriter{dir: dir}
	log_suffix := viper.GetString("log_suffix")
	filename := filepath.Join(dir, "silvermint_clientapi_access"+log_suffix+".log")
	fd, err := os.Create(filename)
	if err != nil {
		return nil, err
	}
	lw.fd = fd
	return &lw, nil
}

func (lw *LogWriter) Write(p []byte) (n int, err error) {
	return lw.fd.Write(p)
}
