#!/bin/bash
# Author: Nash E. Foster <leaf@pyrofex.net>
#
# A simple build script.
#

set -e

OS=Linux
case $(uname -s) in
    Linux*)     OS="Linux";;
    Darwin*)    OS="Mac"
        function realpath() {
            OURPWD=$PWD
            cd "$(dirname "$1")"
            LINK=$(readlink "$(basename "$1")")
            while [ "$LINK" ]; do
                cd "$(dirname "$LINK")"
                LINK=$(readlink "$(basename "$1")")
            done
            REALPATH="$PWD/$(basename "$1")"
            cd "$OURPWD"
            echo "$REALPATH"
        }
    ;;
    CYGWIN*)    OS="Cygwin";;
    MINGW*)     OS="MinGw";;
    *)          OS="UNKNOWN";;
esac
export OS
export PATH="${PATH}:$(realpath ~)/go/bin"

Usage() {
	echo "Usage: build.sh [OPTION]"
	echo ""
	echo "Build the silvermint node."
	echo
	echo "Options:"
	echo "  -h          Display this help message and exit."
	echo "  -l=LDFLAGS  Set the ldflags."
	echo "  -o=DIR      Set the build output directory to DIR."
	echo "  -r          Run the node after building."
	echo "  -T=PATH     Sets the HTML templates path to PATH."
	echo "  -t          Run tests after building."
	echo "  -v          Produce verbose output."
	echo "  -M          Run go mod tidy first."
	echo "  -G=DIR      Sets GOROOT to DIR and adds dir to the shell's PATH."
	echo "  -g=DIR      Sets GOPATH to DIR."
	echo "  -s          Enable swagger."
}

verbose() {
	if [ "${VERBOSE}" != "" ]; then {
		echo $* ;
	} ; fi
}

LDFLAGS="-X 'git.pyro.cloud/eng/silvermint.Version=$(git describe --tags)' -extldflags '-static' '-w'"
OUTPUT_DIR=""
RUN=""
TEST=""
TEMPLATES=""
VERBOSE=""
GO_MOD_TIDY=""
GOROOT="/usr/local/go"
GOPATH="$(realpath ~)/go"
SWAGGER="n"

while getopts "g:G:c:hMo:rT:tvs" option; do {
	case $option in
		g)
			GOPATH="${OPTARG}"
			export GOPATH
			;;
		G)
			GOROOT="${OPTARG}"
			PATH="${PATH}:${OPTARG}/bin:~/go/bin"
			export PATH GOROOT
			;;
		c)
			CLEAN_CACHE="y"
			;;
		h)
			Usage
			exit 0
			;;
		l)
			LDFLAGS="${OPTARG}"
			;;
		M)
			GO_MOD_TIDY=y
			;;
		o)
			OUTPUT_DIR="-o ${OPTARG}"
			;;
		r)
			RUN=y
			;;
		T)
			TEMPLATES="--templates_path=${OPTARG}"
			;;
		t)
			TEST=y
			;;
		v)
			VERBOSE=y
			;;
		s)
			SWAGGER=y
			;;
		*)
			echo "FATAL: Unknown option."
			Usage
			exit 1
			;;
	esac
}; done

# Now, build the binaries.
BIN_TARGETS=$(find . -name main.go -exec dirname {} \;)

if [ "${CLEAN_CACHE}" != ""  ]; then {
	go clean -cache -testcache
} ; fi

# Tidy up if requested.
if [ "${GO_MOD_TIDY}" != "" ] ; then {
	go mod tidy
}; fi

if [ "${SWAGGER}" == "y" ]; then {
	# Generate the swagger spec, -s was given skip generation
	swagger generate spec -o ./swagger.json
}; fi

for BINARY in ${BIN_TARGETS}; do {
	verbose "Building with args: -ldflags \"${LDFLAGS}\" ${BINARY}"
	go build -ldflags "${LDFLAGS}" -o ./ -v ${BINARY}
}; done

# Optionally, run all the tests.
if [ "${TEST}" != "" ]; then {
	go test ./...
}; fi

# Optionally, run the binary.
if [ "${RUN}" != "" ]; then {
	go run . ${TEMPLATES}
}; fi
