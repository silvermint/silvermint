package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/utils"
	"gitlab.com/silvermint/silvermint/validator"
)

func main() {
	version := pflag.Bool("version", false, "Print version and exit.")
	save_dir := pflag.String("save_dir", "/tmp/testmksnapshot", "Directory for test validators and txns folders to store binarys")
	num_validators := pflag.IntP("num_validators", "v", 1, "Total number of validators to test")
	num_txns := pflag.IntP("num_txns", "t", 0, "Total number of transactions to create")
	pflag.Parse()
	if *version {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	viper.BindPFlags(pflag.CommandLine)

	wallet_dir := filepath.Join(*save_dir, "keys")
	err := MakeValidators(*num_validators, wallet_dir)
	if err != nil {
		log.Fatal(err.Error())
	}

	err = MakeTransactions(*num_txns, wallet_dir)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func MakeValidators(num_validators int, wallet_dir string) error {
	admin_wallet_filepath := filepath.Join(wallet_dir, "adminkey")
	for i := 0; i < num_validators; i++ {
		admin_wallet_filename := fmt.Sprintf("%s_%d", admin_wallet_filepath, i)
		wallet, err := crypto.LoadWalletFromFile(admin_wallet_filename)
		if err != nil {
			wallet, err = crypto.NewWallet(admin_wallet_filename, crypto.BASIC_WALLET)
			if err != nil {
				return err
			}
		}
		val := validator.Validator{
			Id:       uint16(i),
			Hostport: fmt.Sprintf("localhost:90%d", i),
			Pubkey:   wallet.WalletAddress(),
			Active:   true,
		}
		utils.Save(val, "validators", fmt.Sprintf("val_%x.bin", wallet.WalletAddress().Address()))
	}

	return nil
}

func MakeTransactions(num_transactions int, wallet_dir string) error {
	admin_wallet_filepath := fmt.Sprintf("%s_%d", filepath.Join(wallet_dir, "adminkey"), 0)
	admin_wallet, err := crypto.LoadWalletFromFile(admin_wallet_filepath)
	if err != nil {
		admin_wallet, err = crypto.NewWallet(admin_wallet_filepath, crypto.BASIC_WALLET)
		if err != nil {
			return err
		}
	}

	for i := 0; i < num_transactions; i++ {
		wallet_filepath := fmt.Sprintf("%s_%d", filepath.Join(wallet_dir, "userkey"), i)
		wallet, err := crypto.NewWallet(wallet_filepath, crypto.BASIC_WALLET)
		if err != nil {
			return err
		}

		mint := new(transaction.Mint)
		mint.CheckNumber_ = uint16(i)
		mint.Src = admin_wallet.WalletAddress()
		mint.Output_ = transaction.MintOutput{Amount_: 100, Dest_: wallet.WalletAddress()}

		mintBytes, err := mint.MarshalSignedBinary(admin_wallet)
		if err != nil {
			return err
		}

		err = mint.UnmarshalBinary(mintBytes)
		if err != nil {
			return err
		}

		err = utils.Save(mint, "transactions", fmt.Sprintf("txn_%x.bin", wallet.WalletAddress().Address()))
		if err != nil {
			return err
		}
	}
	return nil
}
