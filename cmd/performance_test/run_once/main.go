// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"crypto/ed25519"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	logging "gitlab.com/silvermint/silvermint/log"

	"github.com/prometheus/client_golang/prometheus/testutil"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/net"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
	"golang.org/x/exp/slices"
)

const TO_MICRO = 1000000
const TO_NANO = 1000000000

func TestSetupNValidators(nValidators int) (
	[]*casanova.Casanova,
	[]validator.Validator,
	[]crypto.Wallet,
	error,
) {
	var wallets []crypto.Wallet
	var validators []validator.Validator
	valsMap := map[validator.Validator]bool{}
	valIdsMap := [validator.N_MAX_VALIDATORS]validator.Validator{}
	for i := 0; i < nValidators; i++ {
		// Create wallets
		pub, priv, _ := ed25519.GenerateKey(nil)
		wallets = append(wallets, crypto.BasicWallet{
			Type_:     crypto.BASIC_WALLET,
			Length_:   crypto.BASIC_WALLET_ADDR_SIZE,
			Filename_: "",
			Public_:   pub,
			Private_:  priv,
		})

		// Create validators
		validators = append(validators,
			validator.Validator{
				Id:         uint16(i),
				Active:     true,
				RemoteAddr: nil,
				Hostport:   "0.0.0.0:" + strconv.Itoa(8888+i),
				Conn:       nil,
				Pubkey:     wallets[i].WalletAddress(),
			})
		valsMap[validators[i]] = true
		valIdsMap[uint16(i)] = validators[i]
	}

	genesisCopies := []block.Block{}
	for i := 0; i < nValidators; i++ {
		genesisCopies = append(genesisCopies, block.Genesis(valIdsMap))
	}

	var casanovas []*casanova.Casanova
	for i := 0; i < nValidators; i++ {
		// Create casanova instances
		cas, err := casanova.NewCasanova(validators[i].Id, valIdsMap, net.SendCatchupFactory)
		if err != nil {
			return nil, nil, nil,
				fmt.Errorf("error creating new casanova, %s", err.Error())
		}
		casanovas = append(casanovas, cas)
	}
	return casanovas, validators, wallets, nil
}

func TestAllFinalized(
	cas *casanova.Casanova,
	txns []transaction.Transaction,
) bool {
	for i, txn := range txns {
		status, err := cas.GetPaymentState(txn.Hash(), txn.CheckNumber(), txn.Source())
		if err != nil {
			fmt.Printf("ERROR: Error checking transaction status: %s",
				err.Error())
			return false
		} else if status != "FINALIZED" && status != "ARCHIVED" && status != "TRANSACTION NOT FOUND" {
			fmt.Printf("ERROR: Not all finalized: encountered status "+
				"%s after checking %d/%d txns\n", status, i+1, len(txns))
			return false
		}
	}
	return true
}

// Creates a specified number of Mint transactions, split between an N
// validator network. Blocks are issued on a specified block cycle by each
// validator to the others until each validator has reached a state of
// finalized or archived for all the transactions.
func main() {
	perfNumTxns := pflag.UintP(
		"txns", "t",
		10000,
		"Number of transactions/CDs to create.",
	)
	perfNumValidators := pflag.UintP(
		"vals", "v",
		10,
		"Number of nodes/validators in the network.",
	)
	perfBlockCycle := pflag.UintP(
		"cycle", "c",
		1,
		"Time in seconds between block sending per validator",
	)
	perfDeleteBlocksFreq := pflag.UintP(
		"delete", "d",
		2,
		"Time in seconds between DeleteOldBlocks execution",
	)
	perfFilename := pflag.StringP(
		"file", "f",
		"/var/silvermint/log/performance/log.txt",
		"Log output filename",
	)
	perfTxnsPerSecond := pflag.UintP(
		"rate", "r",
		70,
		"Rate at which transactions are received (TPS)",
	)

	pflag.Parse()

	fmt.Printf("run_once: %d Conflict Domains, %d Validators, %d s "+
		"Block Cycle\n", *perfNumTxns, *perfNumValidators, *perfBlockCycle)

	f, err := os.OpenFile(*perfFilename,
		os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0757)
	if err != nil {
		fmt.Printf("FATAL: error opening log file: %v", err)
		return
	}
	defer f.Close()

	log.SetOutput(f)

	// Set a few config variables so things work
	viper.Set("delete_blocks", true)
	viper.Set("delete_blocks_frequency", *perfDeleteBlocksFreq)
	viper.Set("save_dir", "/tmp")
	viper.Set("block_receiver_workers", 1000)
	viper.Set("block_receiver_channels", 100)
	viper.Set("cycle_duration", strconv.Itoa(int(*perfBlockCycle))+"s")
	viper.Set("max_block_size", 256000)

	// Create validators/network
	casanovas, validators, _, err := TestSetupNValidators(int(*perfNumValidators))
	if err != nil {
		fmt.Printf("FATAL: Error creating validators: %s", err.Error())
	}
	// Create mint transactions
	txns := make([]transaction.Transaction, *perfNumTxns)
	for i := 0; i < int(*perfNumTxns); i++ {
		// Create wallet
		pub, priv, _ := ed25519.GenerateKey(nil)

		wallet := crypto.BasicWallet{
			Type_:     crypto.BASIC_WALLET,
			Length_:   crypto.BASIC_WALLET_ADDR_SIZE,
			Filename_: "",
			Public_:   pub,
			Private_:  priv,
		}
		// Create mint transaction to wallet
		mintTxn := transaction.Mint{
			Src:          wallet.WalletAddress(),
			CheckNumber_: 1,
			Output_: transaction.MintOutput{
				Amount_: 100,
				Dest_:   wallet.WalletAddress(),
			},
			Bytes: []byte{},
			Sig:   crypto.Signature{},
		}
		mb, err := mintTxn.MarshalSignedBinary(wallet)
		if err != nil {
			logging.Fatalf("Unable to marshal signed binary for mint txn: %s",
				err.Error())
		}
		mintTxn.Bytes = mb
		copy(mintTxn.Sig[:], mb[len(mb)-crypto.SIGNATURE_SIZE:])

		txns[i] = &mintTxn
	}

	// Start various block mgmt routines for each casanova
	for i := 0; i < int(*perfNumValidators); i++ {
		go casanovas[i].CatchUpThread()
		go net.ReceiveBlocks(casanovas[i], validators[i].Hostport)
	}

	// Start the block senders at evenly spaced intervals
	for i := 0; i < int(*perfNumValidators); i++ {
		time.Sleep(time.Millisecond * time.Duration(1000 / *perfNumValidators))
		go net.SendBlocks(casanovas[i])
	}

	// Giving time for everyone to sync up
	fmt.Printf("INFO: Sleeping 10 seconds to allow validators to sync up...")
	time.Sleep(time.Duration(*perfBlockCycle) * 10 * time.Second)

	start := time.Now()

	blocks_total := 0
	txns_submitted := 0
	not_finalized := []uint16{}
	block_counts := make([]int, len(validators))
	for _, v := range validators {
		not_finalized = append(not_finalized, v.Id)
	}
	next_time := time.Now()
	for len(not_finalized) > 0 {
		// Sleep until next block cycle, then submit
		// TPS transactions (if there are any left)
		time.Sleep(time.Until(next_time))
		next_time = next_time.Add(time.Duration(*perfBlockCycle) * time.Second)

		for i := len(not_finalized) - 1; i >= 0; i-- {
			for j := 0; j < int(*perfTxnsPerSecond); j++ {
				if txns_submitted >= int(*perfNumTxns) {
					break
				}
				casanovas[i].AddTxn(txns[txns_submitted].Data())
				txns_submitted++
			}
			block_counts[i]++
			if TestAllFinalized(casanovas[i], txns) {
				fmt.Printf("INFO: Validator %d has finalized or "+
					"archived all txns, took %d blocks\n", not_finalized[i], block_counts[i])
				blocks_total += block_counts[i]
				not_finalized = slices.Delete(not_finalized, i, i+1)
			}
			// Give some padding between each validator receiving txns
			// so that we aren't sending them all at once
			padNanoseconds := int(float64(*perfBlockCycle) /
				float64(*perfNumValidators) * TO_NANO)
			time.Sleep(time.Duration(padNanoseconds) * time.Nanosecond)
		}
	}

	finish := time.Since(start).Seconds()

	params := []uint{*perfNumTxns,
		*perfNumValidators,
		*perfBlockCycle,
		*perfDeleteBlocksFreq}

	data := []float64{finish,
		float64(blocks_total),
		testutil.ToFloat64(casanova.CasanovaMicroVec.WithLabelValues("Get")) / TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("BlockState")) / TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("ConvertToStatusTotals")) / TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("GetParentStates")) / TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("getDeletable")) / TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("reduceExtra")) / TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("reducer")) / TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("sortCDs")) / TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("updateVs")) / TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("validTransactions")) / TO_MICRO,
	}

	fmt.Printf("INFO: Finished!\n")

	fmt.Printf("INFO: Took %f seconds\n", finish)
	fmt.Printf("INFO: Created a total of %d blocks\n", blocks_total)
	fmt.Printf("INFO: Finalized %d txns for each of %d validators\n", *perfNumTxns, *perfNumValidators)
	fmt.Printf("INFO: Spent %f seconds in Get() [per-validator average of %f]\n",
		testutil.ToFloat64(casanova.CasanovaMicroVec.WithLabelValues("Get"))/TO_MICRO,
		testutil.ToFloat64(casanova.CasanovaMicroVec.WithLabelValues("Get"))/(TO_MICRO*float64(*perfNumValidators)))
	fmt.Printf("INFO: Spent %f seconds in BlockState() [per-validator average of %f]\n",
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("BlockState"))/TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("BlockState"))/(TO_MICRO*float64(*perfNumValidators)))
	fmt.Printf("INFO: Spent %f seconds in BlockState():ConvertToStatusTotals() [per-validator average of %f]\n",
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("ConvertToStatusTotals"))/TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("ConvertToStatusTotals"))/(TO_MICRO*float64(*perfNumValidators)))
	fmt.Printf("INFO: Spent %f seconds in BlockState():GetParentStates() [per-validator average of %f]\n",
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("GetParentStates"))/TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("GetParentStates"))/(TO_MICRO*float64(*perfNumValidators)))
	fmt.Printf("INFO: Spent %f seconds in BlockState():getDeletable() [per-validator average of %f]\n",
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("getDeletable"))/TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("getDeletable"))/(TO_MICRO*float64(*perfNumValidators)))
	fmt.Printf("INFO: Spent %f seconds in BlockState():reduceExtra() [per-validator average of %f]\n",
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("reduceExtra"))/TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("reduceExtra"))/(TO_MICRO*float64(*perfNumValidators)))
	fmt.Printf("INFO: Spent %f seconds in BlockState():reducer() [per-validator average of %f]\n",
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("reducer"))/TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("reducer"))/(TO_MICRO*float64(*perfNumValidators)))
	fmt.Printf("INFO: Spent %f seconds in BlockState():sortCDs() [per-validator average of %f]\n",
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("sortCDs"))/TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("sortCDs"))/(TO_MICRO*float64(*perfNumValidators)))
	fmt.Printf("INFO: Spent %f seconds in BlockState():updateVs() [per-validator average of %f]\n",
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("updateVs"))/TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("updateVs"))/(TO_MICRO*float64(*perfNumValidators)))
	fmt.Printf("INFO: Spent %f seconds in BlockState():validTransactions+() [per-validator average of %f]\n",
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("validTransactions"))/TO_MICRO,
		testutil.ToFloat64(casanova.ConsensusStateMicroVec.WithLabelValues("validTransactions"))/(TO_MICRO*float64(*perfNumValidators)))

	fmt.Printf("INFO: Test Parameters: %v\n", params)
	fmt.Printf("INFO: Test Data: %v\n", data)
}
