// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"bufio"
	"fmt"
	"image/color"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"github.com/spf13/pflag"
	"gitlab.com/silvermint/silvermint"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/utils"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

// Opens and searches the specified file for a line containing the substring
// and returns the line. Also returns a bool indicating whether it was found.
func findSubstrInFile(filepath string, substr string) (bool, string) {
	fileHandle, err := os.Open(filepath)

	if err != nil {
		log.Fatalln("Unable to find/open file")
	}
	defer fileHandle.Close()

	line := ""
	found := false
	count := 0
	scanner := bufio.NewScanner(fileHandle)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), substr) {
			count++
			line = scanner.Text()
			found = true
		}
	}
	if count > 1 {
		fmt.Printf("WARNING: Found %d lines in file %s with substring %s. "+
			"Possible that '%s' is the wrong one.\n",
			count, filepath, substr, line)
	}
	return found, line
}

// Parses a string of the style "[1 2 3 4]\n"
// and returns the values as a []uint or an error on failure.
func parseUints(str string) ([]uint, error) {
	nums := strings.Split(str, " ")
	result := make([]uint, len(nums))
	for i, num := range nums {
		res, err := strconv.Atoi(strings.Trim(num, "[]\n"))
		if err != nil {
			return nil, err
		}
		result[i] = uint(res)
	}
	return result, nil
}

// Parses a string of the style "[1.0 2.1 3.2 4.3]\n"
// and returns the values as a []float64 or an error on failure.
func parseFloats(str string) ([]float64, error) {
	nums := strings.Split(str, " ")
	result := make([]float64, len(nums))
	for i, num := range nums {
		res, err := strconv.ParseFloat(strings.Trim(num, "[]\n"), 64)
		if err != nil {
			return nil, err
		}
		result[i] = res
	}
	return result, nil
}

func plotResults(inputs [][]uint,
	outputs [][]float64,
	perfNumTxns *[]uint,
	perfNumValidators *[]uint,
	perfBlockCycle *[]uint,
	perfNumIterations *uint,
	perfTxnRate *uint,
) {
	// Make plots, titles, colors, x/y-axis labels
	nOutputs := len(outputs[0])
	ps := make([]*plot.Plot, nOutputs)
	params := []string{fmt.Sprintf("Time to finalize all txns [s], (%d run avg), TPS: %d",
		*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("Blocks to finalization (per validator), (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("Get() (per validator) [s], (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("BlockState (per validator) [s], (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("BlockState::ConvertToStatusTotals (per validator) [s], (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("BlockState::GetParentStates (per validator) [s], (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("BlockState::getDeletable (per validator) [s], (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("BlockState::reduceExtra (per validator) [s], (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("BlockState::reducer (per validator) [s], (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("BlockState::sortCDs (per validator) [s], (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("BlockState::updateVs (per validator) [s], (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
		fmt.Sprintf("BlockState::validTransactions (per validator) [s], (%d run avg), TPS: %d",
			*perfNumIterations, *perfTxnRate),
	}
	colors := []color.Color{color.RGBA{R: 255, G: 0, B: 0, A: 255},
		color.RGBA{R: 0, G: 0, B: 0, A: 255},
		color.RGBA{R: 0, G: 255, B: 0, A: 255},
		color.RGBA{R: 0, G: 0, B: 255, A: 255},
		color.RGBA{R: 255, G: 255, B: 0, A: 255},
		color.RGBA{R: 0, G: 255, B: 255, A: 255},
	}

	for no := 0; no < nOutputs; no++ {
		ps[no] = plot.New()
		ps[no].Title.Text = params[no]
		ps[no].X.Label.Text = "Num. Conflict Domains"
		if no == 1 {
			ps[no].Y.Label.Text = "Blocks"
		} else {
			ps[no].Y.Label.Text = "Time [s]"
		}
	}

	// Form a set of points for each output and each
	// combination of blockCycle/nValidators
	nl := 0
	for _, blockCycle := range *perfBlockCycle {
		for _, nVals := range *perfNumValidators {
			pts := make([]plotter.XYs, nOutputs)
			for j := 0; j < nOutputs; j++ {
				pts[j] = make(plotter.XYs, len(*perfNumTxns))
			}
			n := 0
			for i, input := range inputs {
				if input[1] == nVals && input[2] == blockCycle {
					for j := 0; j < nOutputs; j++ {
						pts[j][n].X = float64(input[0])
						// Average the Y-value across all validators,
						// except for on the first plot
						if j != 0 {
							pts[j][n].Y = outputs[i][j] / float64(nVals)
						} else {
							pts[j][n].Y = outputs[i][j]
						}
					}
					n++
				}
			}
			// For each set of points, create a line on the appropriate plot
			for j := 0; j < nOutputs; j++ {
				l, err := plotter.NewLine(pts[j])
				if err != nil {
					log.Fatalf("Error plotting: %s", err.Error())
				}
				l.LineStyle.Color = colors[nl%len(colors)]
				lpLine, lpPoints, err := plotter.NewLinePoints(pts[j])
				if err != nil {
					log.Fatalf("Error plotting: %s", err.Error())
				}
				lpLine.Color = colors[nl%len(colors)]
				lpPoints.Color = colors[nl%len(colors)]
				ps[j].Add(l, lpLine, lpPoints)
				ps[j].Legend.Add(fmt.Sprintf("nVals=%d, cycle=%ds",
					nVals, blockCycle), l)
			}
			nl++
		}
	}
	// Save the plots
	for j := 0; j < nOutputs; j++ {
		err := ps[j].Save(8*vg.Inch, 8*vg.Inch, fmt.Sprintf(params[j]+".png"))
		if err != nil {
			log.Fatalf("Error saving plot: %s", err.Error())
		}
	}
}

// NB(chuck): We have to call the run_once binary as an executable instead of
// calling as a library function in order to: 1) Reset the Prometheus metrics.
// 2) Clear the memory. Each run spawns a bunch of go-routines that prevent the
// Casanova instances, StateCache, etc. from being garbage-collected. There's
// no way to kill these go-routines without having the program exit.
func main() {
	version := pflag.Bool("version", false, "Print version and exit.")
	perfNumTxns := pflag.UintSliceP(
		"txns", "t",
		[]uint{10000},
		"Number of transactions/CDs to create.",
	)
	perfNumValidators := pflag.UintSliceP(
		"vals", "v",
		[]uint{10},
		"Number of nodes/validators in the network.",
	)
	perfBlockCycle := pflag.UintSliceP(
		"cycle", "c",
		[]uint{1},
		"Time in seconds between block sending per validator",
	)
	perfDeleteBlocksFreq := pflag.UintP(
		"delete", "d",
		2,
		"Time in seconds between DeleteOldBlocks execution",
	)
	perfNumIterations := pflag.UintP(
		"iter", "i",
		1,
		"Number of iterations to perform/average over for each case",
	)
	perfLogDirectory := pflag.StringP(
		"log", "l",
		"/var/silvermint/log/performance",
		"Directory to write log file to",
	)
	perfTxnRate := pflag.UintP(
		"rate", "r",
		70,
		"Rate of transactions submitted to each validator (TPS)",
	)

	pflag.Parse()

	if *version {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	err := utils.MaybeCreateDir(*perfLogDirectory, 0700)
	if err != nil {
		log.Fatalf("Error creating log directory: %s", err.Error())
	}

	// Reverse-sort the number of txns / validators to try the more
	// memory-intensive cases first (if we're gonna crash, do it early)
	reversedTxns := make([]uint, len(*perfNumTxns))
	reversedVals := make([]uint, len(*perfNumValidators))
	copy(reversedTxns, *perfNumTxns)
	copy(reversedVals, *perfNumValidators)
	sort.Slice(reversedTxns, func(i, j int) bool {
		return reversedTxns[j] < reversedTxns[i]
	})
	sort.Slice(reversedVals, func(i, j int) bool {
		return reversedVals[j] < reversedVals[i]
	})

	// Iterate over each combination
	nRuns := len(*perfNumTxns) * len(*perfNumValidators) * len(*perfBlockCycle)
	inputs := make([][]uint, nRuns)
	outputs := make([][]float64, nRuns)
	run := 0
	for _, nTxns := range reversedTxns {
		for _, nVals := range reversedVals {
			for _, bCycle := range *perfBlockCycle {
				input := []uint{}
				output := []float64{}
				for iter := uint(0); iter < *perfNumIterations; iter++ {
					fmt.Printf("Checking performance for %d Conflict Domains,"+
						" %d Validators, %d s Block Cycle, (iter %d)\n", nTxns,
						nVals, bCycle, iter)
					// Run the test
					fn := fmt.Sprintf("performance_log_%dvals_%dtxns_%ds.txt",
						nVals, nTxns, bCycle)
					fpath := filepath.Join(*perfLogDirectory, fn)
					stdout, err := exec.Command("./run_once/run_once",
						fmt.Sprintf("-t=%d", nTxns), fmt.Sprintf("-v=%d", nVals),
						fmt.Sprintf("-c=%d", bCycle), fmt.Sprintf("-d=%d",
							*perfDeleteBlocksFreq), fmt.Sprintf("-f=%s", fpath),
						fmt.Sprintf("-r=%d", *perfTxnRate)).Output()
					if err != nil {
						log.Fatalf("Error with run for [vals,txns,cycle] = [%d,%d,%d]: %s\nOutput: %s",
							nVals, nTxns, bCycle, err.Error(), stdout)
					}
					// Find the input/output lines in the log files
					pFound, pLine := findSubstrInFile(fpath, "Test Parameters: ")
					dFound, dLine := findSubstrInFile(fpath, "Test Data: ")
					if !pFound {
						log.Fatal("Unable to parse run log file for input " +
							"parameters")
					}
					if !dFound {
						log.Fatal("Unable to parse run log file for output " +
							"data")
					}
					// Split on these lines and parse to get the data
					pSplit := strings.Split(pLine, "Test Parameters: ")
					dSplit := strings.Split(dLine, "Test Data: ")
					latestInput, err := parseUints(pSplit[1])
					if err != nil {
						log.Fatalf("Unable to parse run log file for input parameters: %s",
							err.Error())
					}
					latestOutput, err := parseFloats(dSplit[1])
					if err != nil {
						log.Fatalf("Unable to parse run log file for output data: %s",
							err.Error())
					}
					// Sum the output numbers for each iteration
					if iter == 0 {
						input = latestInput
						output = latestOutput
					} else {
						for i, o := range latestOutput {
							output[i] += o
						}
					}
					fmt.Printf("Output was: %v\n", latestOutput)
				}
				// Perform average and store the input/output data in the array
				for i := range output {
					output[i] = output[i] / float64(*perfNumIterations)
				}
				inputs[run] = input
				outputs[run] = output
				fmt.Printf("Average output for %d iterations: %v\n",
					*perfNumIterations, output)

				run++
			}
		}
	}
	plotResults(inputs, outputs, perfNumTxns, perfNumValidators,
		perfBlockCycle, perfNumIterations, perfTxnRate)
}
