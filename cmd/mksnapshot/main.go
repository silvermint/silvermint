// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"io/ioutil"
	logging "log"
	"os"
	"path/filepath"

	"gitlab.com/silvermint/silvermint"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/blockdb"
	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/dag"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var version *bool

func main() {
	version = pflag.Bool("version", false, "Print version and exit.")
	validators_dir := pflag.String("validators_dir", "/tmp/validators",
		"Comma separated list of validator specs: id, ip address, and wallet address.")
	txns_dir := pflag.String("txns_dir", "/tmp/transactions",
		"Comma separated list of wallets specs: the wallet address and starting balance.")
	genesis_dir := pflag.StringP("save_dir", "o", "./snapshot.d", "directory where genesis is saved")

	HideUnusedFlagsAndParse()
	vArr := GetValidators(*validators_dir)
	compactLedger := ProcessTransactions(*txns_dir)
	GenerateGenesis(vArr, compactLedger, *genesis_dir)
}

// Hides all of the unused flags that generate tool does not need, also
func HideUnusedFlagsAndParse() {
	pflag.VisitAll(func(flag *pflag.Flag) {
		flag.Hidden = true
	})
	flags := []string{"validators_file", "balance_file", "save_dir"}
	for _, flag := range flags {
		lookedUpFlag := pflag.Lookup(flag)
		if lookedUpFlag != nil {
			lookedUpFlag.Hidden = false
		}
	}

	//Set the log level to zero so only fatal logs show and initializes the log
	pflag.Lookup("log_level").Value.Set("0")
	logging.SetFlags(0)
	pflag.Parse()

	if *version {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	viper.BindPFlags(pflag.CommandLine)
	log.InitLogs()
}

func GetValidators(validatorDir string) [validator.N_MAX_VALIDATORS]validator.Validator {
	vArr := [validator.N_MAX_VALIDATORS]validator.Validator{}
	entries, err := ioutil.ReadDir(validatorDir)
	if err != nil {
		log.Fatalf("Can't load Validators: %s", err.Error())
	}

	if len(entries) > validator.N_MAX_VALIDATORS {
		log.Fatalf("Too many validators given, max is %d, total given is %d",
			validator.N_MAX_VALIDATORS, len(entries))
	}

	for i, fileInfo := range entries {
		filename := filepath.Join(validatorDir, fileInfo.Name())
		fileBytes, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Fatalf("Can't load all validators: file %s error %s", filename, err.Error())
		}
		err = vArr[i].UnmarshalBinary(fileBytes)
		if err != nil {
			log.Fatalf("Can't load all validators, file %s error: %s", filename, err.Error())
		}
	}
	return vArr
}

func ProcessTransactions(txnsDir string) transaction.Ledger {
	ssLedger := transaction.NewSSLedger(0)
	entries, err := ioutil.ReadDir(txnsDir)
	if err != nil || len(entries) == 0 {
		return ssLedger
	}

	incrLedger := transaction.NewIncrLedger(ssLedger)
	ssLedger.Child_ = incrLedger
	checkNum := 1
	for i, fileInfo := range entries {
		filename := filepath.Join(txnsDir, fileInfo.Name())
		fileBytes, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Fatalf("Can't load all transactions, file %s error: %s", filename, err.Error())
		}
		mint := new(transaction.Mint)
		err = mint.UnmarshalBinary(fileBytes)
		if err != nil {
			log.Fatalf("Can't load all transactions, file %s error: %s", filename, err.Error())
		}
		mint.CheckNumber_ = uint16(checkNum)
		checkNum++
		mint.TxnId_ = ids.TransactionId{BlockId: ids.BlockId{ValId_: 0, Height_: 0}, Index: uint16(i)}
		incrLedger.Wg.Add(1)
		incrLedger.QueueFinalized(mint)
	}
	err = incrLedger.ProcessQueued()
	if err != nil {
		log.Fatalf("Unable to process txns: %s", err.Error())
	}
	compact := transaction.NewSSLedger(0)
	compact.Compact(incrLedger)
	return compact
}

func GenerateGenesis(vArr [validator.N_MAX_VALIDATORS]validator.Validator, ledger transaction.Ledger, dir string) {
	valSet := validator.ValidatorSet{}
	for _, val := range vArr {
		if val.Active {
			valSet[val] = val.Active
		}
	}

	genesisBlock := block.Genesis(vArr)

	stateData := casanova.CasanovaGenesisState(valSet, casanova.CasanovaExtra{Ledger: ledger})

	dag.NewBlockCache(genesisBlock)
	casanova.NewStateCache(stateData)

	ledgerData, _ := ledger.MarshalBinary()
	for _, val := range vArr {
		if val.Active {
			ledgerDir, err := blockdb.NewSlabDir(filepath.Join(dir, fmt.Sprint(val.Id), casanova.SSLEDGER_FOLDER_NAME))
			if err != nil {
				log.Fatalf("Cannot create Ledgers: %s", err.Error())
			}
			_, ledgerSlab := ledgerDir.Current()
			ledgerSlab.Append(ledgerData)
		}
	}
}
