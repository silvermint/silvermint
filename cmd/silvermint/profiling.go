// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"runtime"
	"runtime/pprof"
	"strconv"
	"time"

	pprofmw "github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/silvermint/silvermint/log"
)

func init() {
	pflag.Bool("enable_memprof", false, "Enable memory profiling.")
	pflag.String("memprof_output", "silvermint_mem.prof", "Memory profile output filename.")
	pflag.String("memprof_interval", "1m", "Interval after which to write a memory profile.")
	pflag.Bool("enable_cpuprof", false, "Enable CPU profiling.")
	pflag.String("cpuprof_output", "silvermint_cpu.prof", "CPU profile output filename.")
	pflag.String("cpuprof_interval", "1m", "Interval after which to write a CPU profile.")
	pflag.Bool("enable_mutexprof", false, "Enable mutex profiling.")
	pflag.String("mutexprof_output", "silvermint_mutex.prof", "Mutex profile output filename.")
	pflag.String("mutexprof_interval", "1m", "Interval after which to write a mutex profile.")
	pflag.Int("mutexprof_rate", 1, "Mutex profiling rate. On average, every 1/N contention events are reported.")
	pflag.Int("pprof_port", 8898,
		"The port on which the PPROF data is served.")
	pflag.String("pprof_ipaddr", "0.0.0.0",
		"The IP address that the PPROF data is served on.")

}

func WriteMemProfile(filename string) {
	f, err := os.Create(filename)
	if err != nil {
		log.Fatalf("could not create memory profile: %s", err.Error())
	}
	defer f.Close() // error handling omitted for example
	runtime.GC()    // get up-to-date statistics
	if err := pprof.WriteHeapProfile(f); err != nil {
		log.Fatalf("could not write memory profile: %s", err.Error())
	}
}

func MemProfileThread() {
	filename := viper.Get("memprof_output").(string)
	memprof_interval := viper.Get("memprof_interval").(string)
	memprof_duration, _ := time.ParseDuration(memprof_interval)
	if viper.GetBool("enable_memprof") {
		for {
			time.Sleep(memprof_duration)
			WriteMemProfile(filename)
		}
	}
}

func WriteCpuProfile(filename string, addr string) error {
	resp, err := http.Get(addr)
	if err != nil {
		log.Fatalf("could not fetch CPU profile: %s", err.Error())
	}
	defer resp.Body.Close()
	f, err := os.Create(filename)
	if err != nil {
		log.Fatalf("could not create CPU profile: %s", err.Error())
	} else if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unable to reach CPU profile server, "+
			"got status %d", resp.StatusCode)
	}
	defer f.Close() // error handling omitted for example

	io.Copy(f, resp.Body)
	return nil
}

func CpuProfileThread(pprofAddress string) {
	filename := viper.Get("cpuprof_output").(string)
	cpuprof_interval := viper.Get("cpuprof_interval").(string)
	cpuprof_duration, _ := time.ParseDuration(cpuprof_interval)
	addr := "http://" + pprofAddress + "/debug/pprof/profile?seconds=" +
		strconv.Itoa(int(cpuprof_duration.Seconds()))

	for {
		err := WriteCpuProfile(filename, addr)
		if err != nil {
			log.Errorf("Failed to contact CPU profile server: %s", err.Error())
			return
		}
	}
}
func WriteMutexProfile(filename string, addr string) error {
	resp, err := http.Get(addr)
	if err != nil {
		log.Fatalf("could not fetch mutex profile: %s", err.Error())
	}
	defer resp.Body.Close()
	f, err := os.Create(filename)
	if err != nil {
		log.Fatalf("could not create mutex profile: %s", err.Error())
	} else if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unable to reach mutex profile server, "+
			"got status %d", resp.StatusCode)
	}
	defer f.Close() // error handling omitted for example

	io.Copy(f, resp.Body)
	return nil
}

func MutexProfileThread(pprofAddress string) {
	filename := viper.Get("mutexprof_output").(string)
	mutexprof_interval := viper.Get("mutexprof_interval").(string)
	mutexprof_duration, _ := time.ParseDuration(mutexprof_interval)
	mutexprof_rate := viper.Get("mutexprof_rate").(int)

	runtime.SetMutexProfileFraction(mutexprof_rate)
	addr := "http://" + pprofAddress + "/debug/pprof/mutex"

	for {
		time.Sleep(mutexprof_duration)
		err := WriteMutexProfile(filename, addr)
		if err != nil {
			log.Errorf("Failed to contact mutex profile server: %s", err.Error())
			return
		}
	}
}

func PprofServerThread() {
	if viper.GetBool("enable_cpuprof") || viper.GetBool("enable_mutexprof") {
		ipAddress := viper.GetString("pprof_ipaddr")
		apiPort := viper.GetInt("pprof_port")
		if ipAddress == "" || apiPort == 0 {
			log.Fatal("IP address and port must be specified in config or cmd-line.")
		}

		pprofAddress := fmt.Sprintf("%s:%d", ipAddress, apiPort)

		var router *gin.Engine = gin.Default()
		router.SetTrustedProxies(nil)
		pprofmw.Register(router)
		if viper.GetBool("enable_cpuprof") {
			go CpuProfileThread(pprofAddress)
		}
		if viper.GetBool("enable_mutexprof") {
			go MutexProfileThread(pprofAddress)
		}
		router.Run(pprofAddress)
	}
}
