// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"text/template"

	"gitlab.com/silvermint/silvermint"
	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/clientapi"
	"gitlab.com/silvermint/silvermint/console"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/logwriter"
	"gitlab.com/silvermint/silvermint/net"
	"gitlab.com/silvermint/silvermint/utils"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/fsnotify/fsnotify"
	"github.com/gin-gonic/gin"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var debug bool

func main() {
	//
	// Parse command line flags.
	//
	version := pflag.BoolP("version", "v", false, "Print version and exit.")
	pflag.Bool("debug", false, "Enable debug messages.")
	pflag.String("gin_mode", "release",
		"The mode in which the Gin Engine runs.")

	template_paths := strings.Join(
		[]string{"/usr/lib/silvermint/templates", "./templates"},
		string(filepath.ListSeparator))

	pflag.String("templates_path", template_paths,
		"A list of potential locations to find HTML templates "+
			"(separated by OS-specific path list separator).")

	pflag.String("block_receiver_hostport", "0.0.0.0:900",
		"TCP hostport the block receiver listens on.")
	pflag.String("validator_id", "0", "The validator ID.")
	pflag.String("validators", "0/127.0.0.1:900",
		"Comma separated list of validator specs.")
	pflag.Bool("dont_break_rusty", false, "This is a hack to make the cloud stuff work for now.")
	pflag.String("save_dir", "/var/silvermint/save",
		"Directory for dag snapshot save files. If empty: ~/save.")
	pflag.String("genesis_file", "",
		"Filepath to load a Genesis block from. Default is create new genesis.")
	pflag.Bool("load_snapshot", true, "Attempt to load snapshot files if they exist.")
	pflag.String("config_dir", "/etc/silvermint",
		"Directory for private files that need root permisions to access. If empty: /etc/silvermint.")
	pflag.Parse()

	if *version {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	viper.BindPFlags(pflag.CommandLine)

	viper.SetConfigName("silvermint")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/etc/silvermint")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		log.Warningln("Can't find configuration, will use defaults.")
	}

	var templates_found []string

	viper.OnConfigChange(func(_ fsnotify.Event) {
		viper.ReadInConfig()
		templates_found = findHTMLTemplates()
	})

	viper.WatchConfig()

	// Now we can actually consult the configuration on the various things we
	// are interested in.
	debug = viper.GetBool("debug")

	silvermint_log_dir := viper.GetString("log_dir")
	silvermint_save_dir := viper.GetString("save_dir")
	//silvermint_genesis_file := viper.GetString("genesis_file")
	log.InitLogs()

	err = utils.MaybeCreateDir(silvermint_save_dir, 0755)
	if err != nil {
		log.Fatalf("Error creating Silvermint save directory: %s", err.Error())
	}
	err = ConfigureLogging()
	if err != nil {
		log.Fatalf("Error configuring Silvermint logging: %s", err.Error())
	}

	validator_id, err := strconv.ParseUint(
		viper.GetString("validator_id"), 10, 16)

	if err != nil {
		log.Fatalf("Can't parse validator_id: %s\n", err.Error())
	}

	var vals []validator.Validator
	if viper.GetBool("dont_break_rusty") || (viper.GetString("validators") != pflag.Lookup("validators").DefValue) {
		log.Debugln("Parsing Validators from command arg")
		vals = ParseValidatorSet(viper.GetString("validators"))
	} else {
		log.Debugln("Load Validators from state data")
		vals, err = LoadValidatorSet(uint16(validator_id))
		if err != nil {
			log.Fatalf("Unable to load validators from state data, Error: %s", err)
		}
	}

	vArr := [validator.N_MAX_VALIDATORS]validator.Validator{}
	if len(vals) == 0 {
		log.Fatalln("Can't start Casanova with empty validator list")
	} else {
		log.Debugf("I know about %d validators:", len(vals))
		for _, v := range vals {
			log.Debugf("Validator:     %d / %s", v.Id, v.Hostport)
			vArr[v.Id] = v
		}
	}

	// Load DAG / StateCache if available
	// TODO(leaf): Load a Genesis block if available.
	load_snapshot := viper.GetBool("load_snapshot")
	var cas *casanova.Casanova
	if load_snapshot {
		cas, err = casanova.Load(uint16(validator_id), vArr, net.SendCatchupFactory)
		if err != nil {
			log.Warningf("Failed to load snapshot, initializing with casanova presets: %s", err.Error())
			cas, err = casanova.NewCasanova(uint16(validator_id), vArr, net.SendCatchupFactory)
			if err != nil {
				log.Fatalf("FATAL Can't start Casanova: %s", err.Error())
			}
		} else {
			log.Debugf("Successfully loaded snapshot state")
		}
	} else {
		// Launch Casanova
		cas, err = casanova.NewCasanova(uint16(validator_id), vArr, net.SendCatchupFactory)
	}

	if err != nil {
		log.Fatalf("FATAL Can't start Casanova: %s", err.Error())
	}

	// The BlockGCThread is deprecated. This just reports that to the user if
	// they've tried to pass any command-line flags related to it.
	go cas.BlockGCThread()

	// Find actual locations for HTML templates from suggestions provided via
	// templates_path
	templates_found = findHTMLTemplates()

	//
	// NB(leaf): Launch the subcomponents of the node as goroutines. Each
	//
	gin_mode := viper.Get("gin_mode").(string)
	if gin_mode != "" {
		gin.SetMode(gin_mode)
	}

	gin.DisableConsoleColor()

	gin.DefaultWriter, err = logwriter.NewLogWriter(silvermint_log_dir)
	if err != nil {
		log.Warningf("Couldn't construct log writer for gin: %s", err.Error())
		log.Warningln("Using STDOUT for GIN logging, instead.")
		gin.DefaultWriter = os.Stdout
	}

	// Start a bunch of the threads.
	log.Debugf("Starting console and clientapi with templates: %s",
		strings.Join(templates_found, ", "))

	go console.Run(templates_found, cas)
	go clientapi.Run(templates_found, cas)
	go net.SendBlocks(cas)
	go cas.CatchUpThread()
	go cas.CatchUpListener()
	go cas.CompactionThread()
	go MemProfileThread()
	go PprofServerThread()

	// This blocks the main thread.
	recv_hostport := viper.Get("block_receiver_hostport").(string)
	log.Debugf("I am validator %d / %s", validator_id, recv_hostport)
	net.ReceiveBlocks(cas, recv_hostport)
}

func findHTMLTemplates() []string {
	var templates_found []string
	paths_list := filepath.SplitList(viper.Get("templates_path").(string))
	for _, root := range paths_list {
		filepath.WalkDir(root,
			func(path string, d fs.DirEntry, err error) error {
				if err == nil && d.IsDir() {
					pattern := filepath.Join(path, "*.html")
					tmpl, e := template.ParseGlob(pattern)
					if e == nil && tmpl != nil {
						templates_found = append(templates_found, path)
					}
				}
				return err
			})
	}

	if len(templates_found) == 0 {
		log.Fatalln("FATAL: No HTML templates found in tree at ",
			viper.Get("templates_path"))
	}
	return templates_found
}

func ParseValidatorSet(validators string) []validator.Validator {
	vstrs := strings.Split(validators, ",")
	r := make([](validator.Validator), len(vstrs))
	for i, vstr := range vstrs {
		parts := strings.Split(vstr, "/")

		if len(parts) != 2 {
			log.Fatalf("Invalid Validtor spec, parse failed: %s", vstr)
		}

		v := validator.Validator{Hostport: parts[1]}

		id, err := strconv.ParseUint(parts[0], 10, 16)
		if err != nil {
			log.Fatalf("Can't parse Validator ID: %s", err.Error())
		}
		v.Id = uint16(id)
		v.Active = true
		wal := crypto.BasicWallet{
			Type_:   crypto.BASIC_WALLET,
			Length_: crypto.BASIC_WALLET_ADDR_SIZE,
		}
		v.Pubkey = wal.WalletAddress()
		r[i] = v
	}
	return r
}

func LoadValidatorSet(myValId uint16) ([]validator.Validator, error) {
	vals := []validator.Validator{}

	sc, err := casanova.LoadStateCache()
	if err != nil {
		return nil, err
	}

	stateData := sc.Tip(myValId)

	for val, active := range stateData.Validators {
		if active {
			if val.Id == myValId {
				prevValAddr := val.Pubkey
				LoadMyValidatorKeys(&val)
				postValAddr := val.Pubkey
				if !crypto.CompareWalletAddress(prevValAddr, postValAddr) {
					return nil, fmt.Errorf("my validator pubkey doesnt match validator slot in "+
						"State Data: %x != %x", prevValAddr.Address(), postValAddr.Address())
				}
			}
			vals = append(vals, val)
		}
	}

	return vals, nil
}

func LoadMyValidatorKeys(v *validator.Validator) {
	valWalletDir := filepath.Join(viper.GetString("config_dir"), "keys")
	walletFile := filepath.Join(valWalletDir, "nodekey")
	wal, err := crypto.LoadWalletFromFile(walletFile)
	if err != nil {
		err := utils.MaybeCreateDir(valWalletDir, 0700)
		if err != nil {
			log.Fatalf("Error creating Validator wallet save directory: %s\n", err.Error())
		}
		wal, err = crypto.NewWallet(walletFile, crypto.BASIC_WALLET)
		if err != nil {
			log.Fatalf("Error creating Validator wallet: %s\n", err.Error())
		}
	}
	v.Pubkey = wal.WalletAddress()
}
