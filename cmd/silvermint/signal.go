// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"os"
	"os/signal"
)

type HandlerFunc func()

var signalHandlers = map[os.Signal][]HandlerFunc{}

func RegisterSignalHandler(sig os.Signal, fn HandlerFunc) {
	if list, ok := signalHandlers[sig]; ok {
		signalHandlers[sig] = append(list, fn)
	} else {
		signalHandlers[sig] = []HandlerFunc{fn}
		ch := make(chan os.Signal, 1)
		signal.Notify(ch, sig)
		go CallHandlers(ch)
	}
	return
}

func CallHandlers(ch chan os.Signal) {
	for sig := range ch {
		for _, i := range signalHandlers[sig] {
			i()
		}
	}
}
