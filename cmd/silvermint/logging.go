// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"syscall"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	logging "gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/utils"
)

func init() {
	pflag.String("log_dir", "/var/silvermint/log",
		"Directory for log files. If empty: /var/silvermint/log.")
	pflag.String("log_suffix", "", "Suffix to append to silvermint log filenames.")
	pflag.Bool("logtostdout", false, "Enable to log to STDOUT instead of a log file.")
	RegisterSignalHandler(syscall.SIGHUP, SigHandle)
}

var logfd *os.File

func ConfigureLogging() error {
	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds)
	logtostdout := viper.GetBool("logtostdout")
	if logtostdout {
		logging.Debugln("Logging to stdout.")
		return nil
	} else {
		silvermint_log_dir := viper.GetString("log_dir")
		err := utils.MaybeCreateDir(silvermint_log_dir, 0755)
		if err != nil {
			return err
		}

		OpenLogFile()
	}

	return nil
}

func OpenLogFile() {
	silvermint_log_dir := viper.GetString("log_dir")
	silvermint_log_suffix := viper.GetString("log_suffix")
	logfilename := filepath.Join(silvermint_log_dir, "silvermint"+silvermint_log_suffix+".log")
	fd, err := os.OpenFile(logfilename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		rval := fmt.Errorf("Unable to setup logging to %s: %s", logfilename, err.Error())
		logging.Errorln(rval)
		return
	}
	log.SetOutput(fd)
	if logfd != nil {
		logfd.Close()
	}
	logfd = fd
}

func SigHandle() {
	OpenLogFile()
}
