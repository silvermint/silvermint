// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"bufio"
	"encoding/hex"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/silvermint/silvermint"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/log"
	snet "gitlab.com/silvermint/silvermint/net"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/spf13/pflag"
)

func main() {
	version := pflag.Bool("version", false, "Print version and exit.")
	verbose := pflag.BoolP("verbose", "v", false, "Extra output.")

	hostarg := pflag.String("server", "127.0.0.1", "The server hostname.")
	portarg := pflag.Uint16("port", 900, "The server port.")

	validatorId := pflag.Uint16("validator_id", 31337,
		"The validator id to use.")
	timestamp := pflag.Int64("timestamp", time.Now().UnixMilli(),
		"The timestamp of the block.")

	dataString := pflag.String("data", "",
		"Data portion of the block (in hex).")

	dagRefString := pflag.String("refs", "",
		"Comma separated list of validator_id/timestamp refs.")

	replay := pflag.Bool("replay", false, "Enabled replay mode.")

	maxBlocks := pflag.Int64("max_blocks", -1, "Max blocks to send.")

	// silvermint#132 TODO(leaf): Should take one or more keypairs as identities that can sign
	// blocks and then be able to actually use them to do the signing.

	pflag.Parse()

	if *version {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	// Calculate the hostport of the server we're talking to.
	hostport := fmt.Sprintf("%s:%d", *hostarg, *portarg)

	if *replay {
		err := replayLogfiles(hostport, pflag.Args(),
			validatorId, timestamp, dataString,
			dagRefString, verbose, maxBlocks)
		if err != nil {
			fmt.Printf("sendblock: replay error: %s\n", err.Error())
			os.Exit(1)
		}
	} else {
		err := sendSingleBlock(hostport, validatorId, timestamp, dataString,
			dagRefString, verbose)
		if err != nil {
			fmt.Printf("sendblock: fatal error: %s\n", err.Error())
			os.Exit(1)
		}
	}
	os.Exit(0)
}

func replayLogfiles(hostport string, logfiles []string,
	validatorId *uint16, timestamp *int64, dataString *string,
	dagRefString *string, verbose *bool, maxBlocks *int64) error {

	// Setup the validator.
	v := validator.Validator{Id: *validatorId, Hostport: hostport}

	sourceReaders, sourceNames, err := getReplayReaders(logfiles, verbose)
	if err != nil {
		return err
	}

	if *verbose {
		fmt.Printf("sendblock: %d replay sources:\n", len(sourceReaders))
		for _, name := range sourceNames {
			fmt.Printf("   > %s\n", name)
		}
	}

	// Count the blocks we send.
	var numBlocks int64 = 0

	// Read modes we could try:
	//  * concat
	//  * striping
	//  * random

	// Concatenation
	var currReaderIdx uint16 = 0
	var numReaders uint16 = uint16(len(sourceReaders))
	var nilReaders uint16 = 0
	linebuf := make([]byte, 0)

	for {
		if nilReaders == numReaders {
			break
		}
		reader := sourceReaders[currReaderIdx]
		if reader == nil {
			currReaderIdx += 1 % numReaders
			nilReaders++
			continue
		}

		line, isPrefix, err := reader.ReadLine()
		if err != nil {
			fmt.Printf("sendblock: reader[%d] failed, closing: %s\n",
				currReaderIdx, err.Error())
			// silvermint#133 TODO(leaf): Do we need to close() the underlying file?
			sourceReaders[currReaderIdx] = nil
			continue
		}

		linebuf = append(linebuf, line...)

		if isPrefix {
			// Need more input.
			continue
		}

		if *verbose {
			fmt.Printf("sendblock: line: %s\n", linebuf)

			linestr := string(linebuf)
			if strings.IndexByte(linestr, '\n') != len(linestr)-1 {
				fmt.Printf("sendblock: first newline at idx=%d, whole line is %d bytes.\n",
					strings.IndexByte(linestr, '\n'), len(linestr))
			}
		}

		b := block.SilvermintBlock{}

		rawBytes := make([]byte, hex.DecodedLen(len(linebuf)))
		i, err := hex.Decode(rawBytes, linebuf)
		if err != nil {
			fmt.Printf("sendblock: couldn't decode block: %s\n", linebuf)
			return err
		} else {
			fmt.Printf("sendblock: decoded %d bytes of hex.\n", i)
		}

		err = b.UnmarshalBinary(rawBytes)
		if err != nil {
			fmt.Printf("sendblock: can't decode binary: %s\n", err.Error())
		}

		go mySendToValidator(v, &b)
		numBlocks++
		linebuf = nil

		if *maxBlocks != -1 {
			if numBlocks >= *maxBlocks {
				if *verbose {
					fmt.Printf("sendblock: wrote %d blocks, terminating.\n", numBlocks)
				}
				break
			}
		}
	}

	if *verbose {
		fmt.Printf("sendblock: wrote %d blocks to backend.\n", numBlocks)
	}

	return nil
}

func mySendToValidator(v validator.Validator, b block.Block) {
	err := snet.SendToValidator(v, b, nil)
	if err != nil {
		fmt.Printf("FATAL: couldn't send block to validator: %s\n", err.Error())
		os.Exit(1)
	}
}

func getReplayReaders(logfiles []string, verbose *bool) ([]*bufio.Reader, []string, error) {

	if len(logfiles) == 0 {
		return nil, nil, nil
	}

	readers := make([]*bufio.Reader, len(logfiles))
	names := make([]string, len(logfiles))

	var openFiles []*os.File = make([]*os.File, 0)

	for _, filename := range logfiles {
		file, err := os.Open(filename)
		if err != nil {
			// NB(leaf): This leaks open file descriptors, which doesn't matter
			// in a command-line tool, but if you drag this code into a
			// long-running daemon, then it's on you to fix this leak.
			return nil, nil, err
		}
		openFiles = append(openFiles, file)
	}

	if *verbose {
		fmt.Printf("sendblock: have %d open files from %d logfile names.\n",
			len(openFiles), len(logfiles))
	}

	if len(openFiles) != len(logfiles) {
		return nil, nil, fmt.Errorf("could only open %d of %d logfiles",
			len(openFiles), len(logfiles))
	}

	for i, fd := range openFiles {
		reader := bufio.NewReaderSize(fd, 512)
		if reader == nil {
			// use if [i] here relies on it being an error for the openFiles
			// and logfiles arrays to have different lengths (see above).
			return nil, nil, fmt.Errorf("can't construct buffered reader from "+
				"open file: %s", logfiles[i])
		}
		readers[i] = reader
		names[i] = logfiles[i]
	}

	return readers, names, nil
}

func sendSingleBlock(hostport string,
	validatorId *uint16, timestamp *int64,
	dataString *string, dagRefString *string, verbose *bool) error {

	log.Fatalln("sendSingleBlock no longer implemented. Fix me?")
	return nil
	/*
		b := new(block.Block)
		b.Validator = *validatorId
		b.Timestamp = *timestamp

		// silvermint#134 TODO(leaf): Be able to refer to some other blocks/validators?
		if len(*dagRefString) == 0 {
			b.Tips.Refs = make([]block.DagRef, 1)
			b.Tips.Length = 1

			b.Tips.Refs[0].Validator = 0
			b.Tips.Refs[0].Timestamp = *timestamp
		} else {
			return errors.New("parsing --refs not implemented yet")
		}

		// Parse and decode any payload.
		b.Data = make([]byte, hex.DecodedLen(len(*dataString)))
		_, err := hex.Decode(b.Data, []byte(*dataString))
		if err != nil {
			return fmt.Errorf("fatal error decoding data string: %s", err.Error())
		}

		var tmp uint16 = 76
		tmp += 2
		tmp += 10 * b.Tips.Length
		tmp += uint16(len(b.Data))
		b.Length = tmp

		if *verbose {
			fmt.Printf("sendblock: preparing block:\n"+
				"    Length:          %d\n"+
				" Validator: %d\n"+
				" Timestamp: %d\n"+
				" Signature: %x\n"+
				"    # Tips: %d\n"+
				"      Data: %x\n",
				b.Length, b.Validator, b.Timestamp, b.Signature, b.Tips.Length,
				b.Data)
		}

		v := new(validator.Validator)
		v.Hostport = hostport
		snet.SendToValidator(*v, b)
		return nil
	*/
}
