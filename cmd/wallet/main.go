// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"crypto/ed25519"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/silvermint/silvermint"
	"gitlab.com/silvermint/silvermint/cmd/wallet_utils"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/utils"

	"github.com/spf13/pflag"
	"golang.org/x/exp/maps"
)

type ErrObj struct {
	Reason string `json:"reason"`
}

func DirExists(dir string) bool {
	info, err := os.Stat(dir)
	if err != nil || !info.IsDir() {
		return false
	}
	return true
}

func MaybeCreateConfigDir(dir string) error {
	if DirExists(dir) {
		return nil
	}

	err := os.MkdirAll(dir, 0700)
	if err != nil {
		return fmt.Errorf("Fatal: can't create config dir %s: %s", dir, err.Error())
	}

	return nil
}

func main() {
	pflag.ErrHelp = fmt.Errorf("")
	// Subcommands:
	//  - keygen: 	 generate keys
	//  - sign:   	 sign one or more messages from files (or stdin)
	//  - verify:  	 verify the signature of a message
	//  - pay:    	 create and sign a payment to one or more destinations
	//  - balance:   get the balance for the wallet specified
	//  - mint:      mint an amount of silvermint for a wallet
	//  - txnState check the state of a payment
	subcmds := map[string]bool{
		"keygen":   true,
		"sign":     true,
		"verify":   true,
		"pay":      true,
		"balance":  true,
		"mint":     true,
		"txnstate": true,
	}

	subcmdstr := strings.Join(maps.Keys(subcmds), ", ")

	currentUser, err := user.Current()
	if err != nil {
		fmt.Printf("FATAL: Can't get current user. Is your system broken?\n")
		os.Exit(1)
	}

	ConfigDir := filepath.Join(currentUser.HomeDir, ".silvermint")

	// Global flags.
	globFlags := pflag.NewFlagSet("global", pflag.ExitOnError)
	VersionFlag := globFlags.Bool("version", false, "Print version and exit.")
	VerboseFlag := globFlags.BoolP("verbose", "v", false, "Generate extra output.")
	WalletFilename := globFlags.StringP("wallet", "w",
		filepath.Join(ConfigDir, "wallet"),
		"The filename of the wallet's private key.")
	apiServer := globFlags.StringP("apiserver", "a", "localhost:8880",
		"The ip address and port for your Silvermint node.")

	// Generate a wallet and write it to some files.
	genCmd := pflag.NewFlagSet("keygen", pflag.ExitOnError)
	genFilename := genCmd.StringP("output", "o", filepath.Join(ConfigDir, "wallet"),
		"The output filename for the wallet.")
	genNumber := genCmd.UintP("number", "n", 1,
		"The number of wallets to generate. For n>1, generated as \"output_n\"")

	// Sign a message using a wallet.
	signCmd := pflag.NewFlagSet("sign", pflag.ExitOnError)

	// Verify a message with a signature and public key.
	verifyCmd := pflag.NewFlagSet("verify", pflag.ExitOnError)
	signerFilename := verifyCmd.StringP("pubkey", "p", "signer.pub",
		"The file containing the signer's public key.")
	messageFilename := verifyCmd.StringP("message", "m", "message.txt",
		"The file containing the message that was signed.")
	sigFilename := verifyCmd.StringP("signature", "s", "message.txt.sig",
		"The file containing the signature to be verified.")

	// Generate a transaction, sign it, and write it to disk.
	txnCmd := pflag.NewFlagSet("pay", pflag.ExitOnError)
	transactionDir := txnCmd.StringP("txndir", "t",
		filepath.Join(ConfigDir, "transactions"),
		"The directory to save created transactions.")
	txnDests := txnCmd.StringSliceP("destinations", "d", []string{},
		"The destination wallets and amounts for the payment. "+
			"Syntax is -d=<wallet1>/<amount1> -d=<wallet2>/<amount2> etc.")
	checkNumber := txnCmd.Uint16P("checknum", "c", 0,
		"The transaction check number for the source wallet. "+
			"Default is to look up the value via the Client API")

	txnStateCmd := pflag.NewFlagSet("txnstate", pflag.ExitOnError)
	txnSave := txnStateCmd.StringP("txnfile", "T", "", "The file name of for the saved transaction you'd like to check. Required.")

	// Get wallet balance
	balanceCmd := pflag.NewFlagSet("balance", pflag.ExitOnError)

	// Request mint
	mintCmd := pflag.NewFlagSet("mint", pflag.ExitOnError)
	mintAmount := mintCmd.UintP("amount", "A", 100,
		"The amount of Silvermint to request minted.")
	mintAmtDeprecated := mintCmd.Uint("a", 0,
		"The -a option to mint is deprecated, use -A or --amount instead.")

	if len(os.Args) < 2 {
		fmt.Printf("ERROR: Expected subcommand: keygen, sign, verify, pay, balance, or mint.\n")
		os.Exit(1)
	}

	pflag.Usage = func() {
		fmt.Printf("usage: %s [global-flags] <command> [<flags-and-args>]\n", os.Args[0])
		fmt.Printf("Global flags:\n")
		globFlags.PrintDefaults()
		fmt.Printf("\nkeygen flags:\n")
		genCmd.PrintDefaults()
		fmt.Printf("\nsign flags:\n")
		signCmd.PrintDefaults()
		fmt.Printf("\nverify flags:\n")
		fmt.Printf("Not Implemented yet.\n")
		fmt.Printf("\npay flags:\n")
		txnCmd.PrintDefaults()
		fmt.Printf("\nbalance flags:\n")
		balanceCmd.PrintDefaults()
		fmt.Printf("\ntxn state flags:\n")
		txnStateCmd.PrintDefaults()
		fmt.Printf("\nmint flags:\n")
		mintCmd.PrintDefaults()
	}

	// Find the subcommand in the arg list.
	var cmdIndex int = -1
	for i, a := range os.Args[1:] {
		if a == "--version" {
			fmt.Printf(silvermint.GetVersionString(os.Args[0]))
			os.Exit(0)
		}
		if subcmds[a] {
			cmdIndex = i + 1 // because i indexes the range, not os.Args.
			break
		}
	}

	if cmdIndex == -1 {
		fmt.Printf("ERROR: Expected subcommand: %s.\n", subcmdstr)
		pflag.Usage()
		os.Exit(1)
	}

	// Variable cmdIndex now points at the subcommand.
	// os.Args[1 : cmdIndex - 1] - global flags
	// os.Args[cmdIndex]         - subcommand
	// os.Args[cmdIndex + 1 :]   - subcommand flags and arguments.

	globFlags.Parse(os.Args[1:cmdIndex])

	if *VersionFlag {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	if *VerboseFlag {
		fmt.Printf("Using wallet file: %s\n", *WalletFilename)
	}

	switch os.Args[cmdIndex] {

	case "keygen":
		genCmd.Parse(os.Args[cmdIndex+1:])

		if *VerboseFlag {
			fmt.Printf("wallet keygen: output filename = %s\n", *genFilename)
		}

		fileName := *genFilename

		// Maybe create the directory our output is going to.
		err := MaybeCreateConfigDir(filepath.Dir(fileName))
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}

		n := int(*genNumber)
		for i := 0; i < n; i++ {

			if n > 1 {
				fileName = *genFilename + "_" + strconv.Itoa(i+1)
			}

			// If we have a fileName, NewWallet() will save keys to the files.
			_, err := crypto.NewWallet(fileName, crypto.BASIC_WALLET)
			if err != nil {
				fmt.Printf("FATAL: keygen: %s\n", err.Error())
				os.Exit(1)
			}

			if *VerboseFlag {
				fmt.Printf("key generated: %s\n", fileName)
			}
		}

		os.Exit(0)

	case "sign":
		signCmd.Parse(os.Args[cmdIndex+1:])
		w, err := crypto.LoadWalletFromFile(*WalletFilename)
		if err != nil {
			fmt.Printf("Couldn't load wallet: %s\n", err.Error())
			os.Exit(1)
		}

		for _, arg := range signCmd.Args() {
			data, err := ioutil.ReadFile(arg)
			if err != nil {
				fmt.Printf("Couldn't read message from %s: %s\n",
					arg, err.Error())
				os.Exit(1)
			}

			sig, err := w.Sign(data)
			if err != nil {
				fmt.Printf("Couldn't sign message: %s\n", err.Error())
				os.Exit(1)
			}

			sigFile := fmt.Sprintf("%s.sig", arg)
			f, err := os.OpenFile(sigFile, os.O_RDWR|os.O_CREATE, 0644)
			if err != nil {
				fmt.Printf("Couldn't open signature output: %s: %s\n",
					sigFile, err.Error())
				os.Exit(1)
			}

			t := fmt.Sprintf("%x", sig)
			_, err = f.WriteString(t)
			if err != nil {
				fmt.Printf("Couldn't write signature: %s\n", err.Error())
				os.Exit(1)
			}

			if *VerboseFlag {
				fmt.Printf("Signed %s\n", arg)
			}
			f.Close()
		}

		os.Exit(0)

	case "verify":
		verifyCmd.Parse(os.Args[cmdIndex+1:])
		var pub []byte
		pub, err := crypto.LoadKeyFromFile(*signerFilename)
		if err != nil {
			fmt.Printf("Couldn't load public key: %s\n", err.Error())
			os.Exit(1)
		}

		data, err := ioutil.ReadFile(*messageFilename)
		if err != nil {
			fmt.Printf("ERROR: Can't read message file: %s\n", err.Error())
			os.Exit(1)
		}

		if *VerboseFlag {
			fmt.Printf("Verifying signature of: %s\n", *messageFilename)
		}

		sigData, err := ioutil.ReadFile(*sigFilename)
		if err != nil {
			fmt.Printf("ERROR: Couldn't read signature: %s\n", err.Error())
			os.Exit(1)
		}

		sig := make([]byte, crypto.SIGNATURE_SIZE)
		_, err = hex.Decode(sig, sigData[0:(crypto.SIGNATURE_SIZE*2)])
		if err != nil {
			fmt.Printf("ERROR: Can't decode signature: %s\n", err.Error())
			os.Exit(1)
		}

		if ed25519.Verify(pub, data, sig) {
			os.Exit(0)
		} else {
			fmt.Printf("Invalid signature.\n")
			os.Exit(1)
		}

		if *VerboseFlag {
			fmt.Printf("Signature OK.\n")
		}

	case "pay":
		txnCmd.Parse(os.Args[cmdIndex+1:])

		// After, destData will be a slice of strings of the form "wallet/amount".
		destData := *txnDests
		if len(destData) == 0 {
			fmt.Printf("ERROR: No transaction destination provided\n")
			os.Exit(1)
		}

		pmt := new(transaction.Payment)
		srcWallet, err := crypto.LoadWalletFromFile(*WalletFilename)
		if err != nil {
			fmt.Printf("ERROR: Unable to load wallet from file: %s\n", err.Error())
			os.Exit(1)
		}

		pmt.Src = srcWallet.WalletAddress()
		pmt.CheckNumber_ = *checkNumber
		if !flagAssigned(txnCmd, "checknum") {
			state, err := wallet_utils.GetAcctState(srcWallet.WalletAddress(), *apiServer)
			if err != nil {
				fmt.Printf("ERROR: Unable to get latest check number from Client API: %s\n", err.Error())
				os.Exit(1)
			}
			pmt.CheckNumber_ = state.CheckNumber + 1
		}

		for _, data := range destData {
			splitData := strings.Split(data, "/")
			if len(splitData) != 2 {
				fmt.Printf("ERROR: Format must be <wallet>/<amount> for each transaction destination\n")
				os.Exit(1)
			}

			output := new(transaction.PaymentOutput)
			addr, err := crypto.LoadKeyFromHex(splitData[0])
			if err != nil || len(addr) < crypto.PUBLIC_KEY_SIZE {
				fmt.Printf("ERROR: Bad transaction destination address provided\n")
				os.Exit(1)
			}

			tmp := crypto.BasicWalletAddress{}
			copy(tmp.Address_[:], addr)
			output.Dest_ = tmp
			output.Amount_, err = strconv.ParseUint(splitData[1], 10, 64)
			if err != nil {
				fmt.Printf("ERROR: Bad transaction value provided: %s\n", err.Error())
				os.Exit(1)
			}

			pmt.Outputs_ = append(pmt.Outputs_, *output)
			if *VerboseFlag {
				fmt.Printf("Sending %d to destination wallet: 0x%x\n", output.Amount_, output.Dest_.Address())
			}
		}

		// Retrieve Utxos for the wallet
		state, err := wallet_utils.GetAcctState(srcWallet.WalletAddress(), *apiServer)
		if err != nil {
			fmt.Printf("ERROR: Unable to get wallet state %s\n", err.Error())
			os.Exit(1)
		}

		var outputsTotal uint64
		for i := range pmt.Outputs_ {
			outputsTotal += pmt.Outputs_[i].Amount_
		}

		if *VerboseFlag {
			fmt.Printf("Payment total: %d\n", outputsTotal)
		}

		utxosToBeUsed := make([]transaction.Utxo, 0)
		var utxoCounter uint64
		for i, e := range state.Utxos {
			if utxoCounter < outputsTotal {
				utxosToBeUsed = append(utxosToBeUsed, state.Utxos[i])
				utxoCounter += e.Amount
			}
		}

		if *VerboseFlag {
			fmt.Printf("Source wallet available funds: %v\n", utxoCounter)
		}

		if utxoCounter < outputsTotal {
			fmt.Printf("ERROR: The source wallet has insufficient funds for this transaction\n")
			os.Exit(1)
		}

		pmt.Inputs_ = utxosToBeUsed

		if *VerboseFlag {
			fmt.Printf("Payment Input Utxos:\n")
			for idx, input := range pmt.Inputs_ {
				fmt.Printf("  %d: %s\n", idx, input.String())
			}
			fmt.Printf("Change back to source wallet: %d\n", (utxoCounter - outputsTotal))
		}

		// Add change and source wallet to the payment outputs
		if utxoCounter-outputsTotal > 0 {
			output := new(transaction.PaymentOutput)
			output.Amount_ = utxoCounter - outputsTotal
			addr, err := crypto.LoadKeyFromHex(fmt.Sprintf("%x", srcWallet.WalletAddress().Address()))
			if err != nil || len(addr) < crypto.PUBLIC_KEY_SIZE {
				fmt.Printf("ERROR: Bad transaction destination address provided\n")
				os.Exit(1)
			}
			tmp := crypto.BasicWalletAddress{}
			copy(tmp.Address_[:], addr)
			output.Dest_ = tmp
			pmt.Outputs_ = append(pmt.Outputs_, *output)
		}

		signed, err := pmt.MarshalSignedBinary(srcWallet)
		if err != nil {
			fmt.Printf("ERROR: Unable to sign transaction: %s\n", err.Error())
		}

		if !srcWallet.Verify(signed) {
			fmt.Printf("ERROR: Unable to verify transaction signature\n")
			os.Exit(1)
		}

		if _, err = os.Stat(*transactionDir); errors.Is(err, os.ErrNotExist) {
			err = utils.MaybeCreateDir(*transactionDir, 0700)
			if err != nil {
				fmt.Printf("ERROR: Couldn't create transaction directory: %s\n", err.Error())
			}
		}

		if !flagAssigned(txnCmd, "transactions") {
			txnFile := filepath.Join(*transactionDir,
				fmt.Sprintf("%x_%d", srcWallet.WalletAddress(), pmt.CheckNumber_))

			err = SaveTransaction(signed, txnFile)
			if err != nil {
				fmt.Printf("ERROR: Unable to save transaction file: %s\n", err.Error())
				os.Exit(1)
			}

			if *VerboseFlag {
				fmt.Printf("Transaction successfully saved as %s\n", txnFile)
			}
		}

		pmtRoute := fmt.Sprintf("http://%s/payment", *apiServer)
		req, err := http.Post(pmtRoute, "application/json", bytes.NewBuffer(signed))
		if err != nil {
			fmt.Printf("ERROR: Unable to submit HTTP request: %s\n", err.Error())
			os.Exit(1)
		}

		if req.StatusCode != http.StatusOK {
			errObj := *new(ErrObj)
			body, err := ioutil.ReadAll(req.Body)
			if err != nil {
				fmt.Println(err)
			} else {
				err = json.Unmarshal(body, &errObj)
				if err != nil {
					fmt.Println(err)
				}
			}

			fmt.Printf("%v wallet error %s received when calling "+
				"http://%s/payment. Error: %s\n",
				time.Now(), req.Status, *apiServer, errObj.Reason)

			os.Exit(1)
		}

		if *VerboseFlag {
			fmt.Println("Transaction successfully submitted to the Client API")
		}

		os.Exit(0)

	case "txnstate":
		txnStateCmd.Parse(os.Args[cmdIndex+1:])
		dat, err := ioutil.ReadFile(filepath.Join(currentUser.HomeDir, "/.silvermint/transactions/", *txnSave))
		if err != nil {
			fmt.Printf("ERROR: Unable to read transaction file: %s\n", err.Error())
			os.Exit(1)
		}

		hash := crypto.HashBytes(dat)
		res, err := http.Get("http://" + *apiServer + "/payment-state/" + fmt.Sprintf("%x", hash))
		if err != nil {
			fmt.Printf("%v wallet error received when calling http://%s/payment-state. "+
				"Error: %s\n", time.Now(), *apiServer, err.Error())
			os.Exit(1)
		}

		type TxnStatus struct {
			Status string
		}
		var pmtStatus TxnStatus
		body, err := ioutil.ReadAll(res.Body)
		json.Unmarshal(body, &pmtStatus)
		fmt.Printf("%s\n", pmtStatus.Status)

	case "balance":
		balanceCmd.Parse(os.Args[cmdIndex+1:])
		wallet, err := crypto.LoadWalletFromFile(*WalletFilename)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		state, err := wallet_utils.GetAcctState(wallet.WalletAddress(), *apiServer)
		if err != nil {
			fmt.Printf("ERROR: Unable to get wallet state %s\n", err.Error())
			os.Exit(1)
		}

		fmt.Printf("0x%x: %d as of check number %d\n",
			wallet.WalletAddress().Address(), state.Balance, state.CheckNumber)

	case "mint":
		mintCmd.Parse(os.Args[cmdIndex+1:])
		if *mintAmtDeprecated != 0 {
			fmt.Printf("FATAL: The -a option to the mint sub-command has been " +
				"deprecated.\nUse -A or --amount instead.")
			os.Exit(1)
		}
		wallet, err := crypto.LoadWalletFromFile(*WalletFilename)
		if err != nil {
			fmt.Printf("Error loading wallet from file %s\n", err.Error())
			os.Exit(1)
		}

		sState, err := wallet_utils.GetAcctState(wallet.WalletAddress(), *apiServer)

		if *VerboseFlag {
			fmt.Printf("Minting with check number: %d\n", sState.CheckNumber+1)
		}

		mintTxn := transaction.Mint{
			Src:          wallet.WalletAddress(),
			CheckNumber_: sState.CheckNumber + 1,
			Output_: transaction.MintOutput{
				Amount_: uint64(*mintAmount),
				Dest_:   wallet.WalletAddress(),
			},
			Bytes: []byte{},
			Sig:   crypto.Signature{},
		}

		signed, err := mintTxn.MarshalSignedBinary(wallet)
		pmtRoute := fmt.Sprintf("http://%s/payment", *apiServer)
		req, err := http.Post(pmtRoute, "application/json", bytes.NewBuffer(signed))
		if err != nil {
			fmt.Printf("ERROR: Unable to submit HTTP request: %s\n", err.Error())
			fmt.Printf("%v wallet error received when calling "+
				"http://%s/payment-state. Error: %s\n", time.Now(), *apiServer, err.Error())
			os.Exit(1)
		}

		if req.StatusCode != http.StatusOK {
			fmt.Printf("ERROR: HTTP request returned bad status: %s\n", req.Status)
			os.Exit(1)
		}

		if *VerboseFlag {
			fmt.Printf("Minted %d for wallet %x\n",
				*mintAmount, wallet.WalletAddress().Address())
		}

	default:
		fmt.Printf("Expected a subcommand: %s.\n", subcmdstr)
		os.Exit(1)
	}

	os.Exit(0)
}

func SaveTransaction(data []byte, filename string) error {
	txnFilename := filename

	fi, err := os.Stat(filepath.Dir(filename))
	if err != nil {
		return fmt.Errorf("Couldn't stat parent directory %s: %v",
			filepath.Dir(filename), err.Error())
	}

	ugBits := 0077 & fi.Mode().Perm()
	if ugBits != 0 {
		fmt.Printf("WARNING: transaction save directory has loose permission "+
			"mode %O. Recommend: 0700.", fi.Mode().Perm())
	}

	f, err := os.OpenFile(txnFilename, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return fmt.Errorf("Couldn't save transaction to file %s: %s",
			txnFilename, err.Error())
	}
	defer f.Close()

	s := data
	_, err = f.Write(s)
	if err != nil {
		return fmt.Errorf("Couldn't save transaction: %s", err.Error())
	}

	return nil
}

func flagAssigned(fs *pflag.FlagSet, name string) bool {
	assigned := false
	fs.Visit(func(f *pflag.Flag) {
		if f.Name == name {
			assigned = true
		}
	})
	return assigned
}

func GetTxnState(txnHash transaction.TransactionHash, apiStr string) (string, error) {
	txnStr := fmt.Sprintf("%x", txnHash)
	res, err := http.Get("http://" + apiStr + "/payment-state/" + txnStr)
	if err != nil {
		fmt.Println(err)
		fmt.Printf("%v wallet error received when calling http://%s/payment-state. Error: %s\n", time.Now(), apiStr, err.Error())
		return "UNKNOWN", err
	}

	type TxnStatus struct {
		Status string
	}
	var state TxnStatus

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return "UNKNOWN", err
	}
	json.Unmarshal(body, &state)

	return state.Status, nil
}
