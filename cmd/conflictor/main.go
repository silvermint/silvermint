// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"math"
	"math/rand"
	"os"
	"time"

	"gitlab.com/silvermint/silvermint"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/dag"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/transaction"

	"github.com/spf13/pflag"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

func RandBytes(n int) []byte {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return []byte(string(b))
}
func RandWallet() crypto.Wallet {
	wal, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	return wal
}

func RandTransaction() transaction.Transaction {
	ttype := rand.Intn(3)
	src := RandWallet()

	var txn transaction.Transaction
	switch ttype {
	case 0:
		mint := transaction.Mint{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Output_: transaction.MintOutput{
				Amount_: uint64(rand.Intn(math.MaxInt)),
				Dest_:   src.WalletAddress(),
			},
		}
		txn = &mint
	case 1:
		th := transaction.TransactionHash{}
		copy(th[:], RandBytes(crypto.HASH_SIZE))
		pmt := transaction.Payment{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Inputs_: []transaction.Utxo{
				{
					TxnId:  ids.TransactionId{},
					Amount: rand.Uint64(),
				},
			},
			Outputs_: []transaction.PaymentOutput{
				{
					Amount_: uint64(rand.Intn(math.MaxInt)),
					Dest_:   src.WalletAddress(),
				},
			},
		}
		txn = &pmt
	case 2:
		th := transaction.TransactionHash{}
		copy(th[:], RandBytes(crypto.HASH_SIZE))
		sweep := transaction.Sweep{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(rand.Intn(math.MaxUint16)),
			Inputs_: []transaction.Utxo{
				{
					TxnId:  ids.TransactionId{},
					Amount: rand.Uint64(),
				},
			},
			Output_: transaction.SweepOutput{
				Amount_: uint64(rand.Intn(math.MaxInt)),
				Dest_:   src.WalletAddress(),
			},
		}
		txn = &sweep
	}
	return txn
}

func main() {
	version := pflag.Bool("version", false, "Print version and exit.")
	verbose := pflag.Bool("verbose", false, "Enable verbose messages.")
	num_txns := pflag.Int("txns", 1000000, "Number of transactions.")
	kbint := pflag.Int("keybits", 24, "Number of keybits for the hash table.")
	pflag.Parse()

	if *version {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	keybits := uint16(*kbint)

	list := make([]transaction.Transaction, *num_txns)
	for i := 0; i < *num_txns; i++ {
		list[i] = RandTransaction()
	}

	wal := RandWallet()
	mint1 := transaction.Mint{
		Src:          wal.WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 100,
			Dest_:   wal.WalletAddress(),
		},
	}
	mint2 := transaction.Mint{
		Src:          wal.WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 200,
			Dest_:   wal.WalletAddress(),
		},
	}

	t0 := time.Now()
	tab, _ := dag.NewLHT(keybits)

	t1 := time.Now()

	tab.Insert(&mint1)

	var j int
	dups := []transaction.Transaction{}
	for j = 0; j < len(list); j++ {
		dup, err := tab.Insert(list[j])
		if err != nil {
			fmt.Printf("Error: failed to insert: %s\n", err.Error())
			break
		}
		if dup {
			dups = append(dups, list[j])
		}
	}

	dup, err := tab.Insert(&mint2)
	if err != nil {
		fmt.Printf("Error: failed to insert: %s\n", err.Error())
	}
	if dup {
		dups = append(dups, &mint2)
	}
	j += 2

	t2 := time.Now()

	if len(dups) == 0 {
		// Hrm, we should have at least one.
		key := tab.Key(0xdeadbeefdeadbeef)
		for i := key - 0x10; i < key+0x100; i++ {
			fmt.Printf("%d: 0x%016x\n", i, tab.Get(i))
		}
	}

	t3 := time.Now()

	if *verbose {
		tab.Print()
	}

	fmt.Printf("Time to create table: %d ms.\n", t1.Sub(t0).Milliseconds())
	fmt.Printf("Time to insert %d items: %d ms.\n", j, t2.Sub(t1).Milliseconds())
	fmt.Printf("Time to find %d conflicts: %d ms.\n", len(dups), t3.Sub(t2).Milliseconds())
}
