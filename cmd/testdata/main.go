// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"math/rand"
	"os"
	"sort"
	"sync"
	"time"

	"gitlab.com/silvermint/silvermint"

	"github.com/spf13/pflag"
)

var debug bool

const CDLen int = 5

type CD [CDLen]uint64

func (cd CD) Equal(left, right CD) bool {
	return 0 == ((left[0] ^ right[0]) + (left[1] ^ right[1]) + (left[2] ^ right[2]) +
		(left[3] ^ right[3]) + (left[4] ^ right[4]))
}
func (cd CD) LessThan(other CD) bool {
	for i := 0; i < CDLen; i++ {
		if cd[i] < other[i] {
			return true
		}
		if cd[i] > other[i] {
			return false
		}
	}
	return false
}

type CDList [100]CD

func (cdl CDList) Len() int      { return len(cdl) }
func (cdl CDList) Swap(i, j int) { cdl[i], cdl[j] = cdl[j], cdl[i] }
func (cdl CDList) Less(i, j int) bool {
	for k := 0; k < CDLen; k++ {
		if cdl[i][k] < cdl[j][k] {
			return true
		}
	}
	return false
}

func RandCD() CD {
	var cd CD
	for i := 0; i < CDLen; i++ {
		cd[i] = rand.Uint64()
	}
	return cd
}

func makeTimestamp() int64 {
	return time.Now().UnixNano()
}

func main() {
	//
	// Parse command line flags.
	//
	version := pflag.Bool("version", false, "Print version and exit.")
	pflag.Bool("debug", false, "Enable debug messages.")
	pflag.Parse()
	if *version {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	// Generate test data:
	//   - 2,000 validators * 10 blocks * 100 transactions / block
	//   => 20,000 transaction lists
	//   => 2,000,000 transactions total
	tstamp := makeTimestamp()
	fmt.Printf("Seeding PRNG: %v\n", tstamp)
	rand.Seed(tstamp)

	validators := make([][]*CDList, 2000)

	for vs := 0; vs < 2000; vs++ {
		validator := make([]*CDList, 10)

		for v := 0; v < 10; v++ {
			var cdlist CDList
			for i := 0; i < 100; i++ {
				cdlist[i] = RandCD()
			}
			sort.Sort(cdlist)
			validator[v] = &cdlist
		}

		validators[vs] = validator
	}

	findConflicts(validators)
	return
}

var intersectMs int64 = 0
var intersectMsLock sync.Mutex

func IntersectCDLists(left *CDList, right *CDList) (bool, []CD) {
	start := time.Now()

	conflicts := make([]CD, 0)
	for i := 0; i < 100; i++ {
		me := left[i]
		them := right[i]
		if me == them {
			conflicts = append(conflicts, me)
		}

		if me.LessThan(them) { // go left
			for next := 0; next >= 0; next-- {
				them = right[next]
				if me == them {
					conflicts = append(conflicts, me)
				} else if them.LessThan(me) {
					break
				}
			}
		} else {
			for next := 0; next < len(*left); next++ {
				them = right[next]
				if me == them {
					//fmt.Printf("Conflict found: %d, %d: %v == %v\n", i, next, me, them)
					conflicts = append(conflicts, me)
				} else if me.LessThan(them) {
					break
				}
			}
		}
	}

	duration := time.Since(start)
	func() {
		intersectMsLock.Lock()
		defer intersectMsLock.Unlock()
		intersectMs += duration.Nanoseconds()
		return
	}()

	if len(conflicts) > 0 {
		return true, conflicts
	} else {
		return false, nil
	}
}

func findConflicts(vals [][]*CDList) []CD {
	conflicts := make([]CD, 0)
	allcds := map[CD]bool{}

	fmt.Printf("Starting conflict search: %v\n", time.Now())
	start := time.Now()

	var wg sync.WaitGroup
	var lock sync.Mutex
	for i := range vals {
		jvals := vals[i+1:]
		for j := range jvals {
			wg.Add(1)
			go func() {
				defer wg.Done()

				if uhoh, cds := IntersectCDLists(vals[i][0], jvals[j][0]); uhoh {
					lock.Lock()
					conflicts = append(conflicts, cds...)
					lock.Unlock()
				}
			}()
		}
	}
	wg.Wait()

	duration := time.Since(start)
	fmt.Printf("Finished conflict search: %v\n", time.Now())
	fmt.Printf("Elapsed time in ms to find conflicts: %d\n", duration.Milliseconds())
	fmt.Printf("Elapsed time in intersection: %v\n", intersectMs)
	fmt.Printf("Conflicts found: %d\n", len(conflicts))
	fmt.Printf("Total conflict domains: %d\n", len(allcds))
	return conflicts
}
