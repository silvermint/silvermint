// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"crypto/ed25519"
	"fmt"
	"log"
	"math"
	"math/rand"
	"os"
	"strconv"
	"time"

	"gitlab.com/silvermint/silvermint"
	"gitlab.com/silvermint/silvermint/block"
	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// Creates an nValidator sized network and returns the list of created
// Casanova objects, associated Validators, and associated validator Wallets.
func setupNValidators(nValidators int) ([]*casanova.Casanova, []validator.Validator, []crypto.Wallet) {
	viper.Set("lht_keybits", 20)
	var wallets []crypto.Wallet
	var validators []validator.Validator
	valsMap := map[validator.Validator]bool{}
	valIdsMap := [validator.N_MAX_VALIDATORS]validator.Validator{}
	genesisCopies := []block.Block{}
	for i := 0; i < nValidators; i++ {
		// Create wallets
		pub, priv, _ := ed25519.GenerateKey(nil)

		wallets = append(wallets, crypto.BasicWallet{
			Type_:     crypto.BASIC_WALLET,
			Length_:   crypto.BASIC_WALLET_ADDR_SIZE,
			Filename_: "",
			Public_:   pub,
			Private_:  priv,
		})

		// Create validators
		validators = append(validators,
			validator.Validator{
				Id:         uint16(i),
				Active:     true,
				RemoteAddr: nil,
				Hostport:   "0.0.0.0:" + strconv.Itoa(8888+i),
				Conn:       nil,
				Pubkey:     wallets[i].WalletAddress(),
			})
		valsMap[validators[i]] = true
		valIdsMap[uint16(i)] = validators[i]
	}

	for i := 0; i < nValidators; i++ {
		genesisCopies = append(genesisCopies, block.Genesis(valIdsMap))
	}

	var casanovas []*casanova.Casanova
	for i := 0; i < nValidators; i++ {
		// Create casanova instances
		cas, err := casanova.NewCasanova(validators[i].Id, valIdsMap, nil)
		if err != nil {
			log.Fatalf("Error creating new casanova, %s", err.Error())
		}
		casanovas = append(casanovas, cas)
	}
	return casanovas, validators, wallets
}

// Creates a random BlockId
func RandBlockId() ids.BlockId {
	return ids.BlockId{
		ValId_:  uint16(rand.Intn(math.MaxUint16)),
		Height_: rand.Uint64(),
	}
}

// Creates a random TransactionId
func RandTxnId() ids.TransactionId {
	return ids.TransactionId{
		BlockId: RandBlockId(),
		Index:   uint16(rand.Intn(math.MaxUint16)),
	}
}

// Creates a StateData with nTxns random payments transactions
// in the AddedTxns field.
func makeRandomStateData(nTxns int) casanova.StateData {
	state := casanova.StateData{
		AddedTxns: make([]transaction.Transaction, nTxns),
	}

	for i := 0; i < nTxns; i++ {
		src, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
		dst, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
		pmt := transaction.Payment{
			Src:          src.WalletAddress(),
			CheckNumber_: uint16(1 + rand.Intn(1000)),
			Inputs_: []transaction.Utxo{
				{
					TxnId: RandTxnId(),
				},
			},
			Outputs_: []transaction.PaymentOutput{
				{
					Amount_: uint64(1 + rand.Intn(10000)),
					Dest_:   dst.WalletAddress(),
				},
			},
		}
		data, _ := pmt.MarshalSignedBinary(src)
		pmt.Bytes = data
		pmt.Sig = *(*crypto.Signature)(data[len(data)-crypto.SIGNATURE_SIZE:])
		state.AddedTxns[i] = &pmt
	}
	return state
}

func main() {
	version := pflag.Bool("version", false, "Print version and exit.")
	txns_per_block := pflag.Int("txns_per_block", 60, "Number of transactions per block.")
	vals := pflag.Int("vals", 40, "Number of validators in the network.")
	block_depth := pflag.Int("block_depth", 4, "Number of unfinalized blocks per validator.")
	pflag.Parse()

	if *version {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	// Create casanova object
	casanovas, validators, _ := setupNValidators(1)
	cas := casanovas[0]
	val := validators[0]

	nBlks := *vals * *block_depth
	nTxns := nBlks * *txns_per_block

	// Create a list of fake BlockIds and associated StateData
	// Add the StateData to the StateCache
	bids := make([]ids.BlockId, nBlks)
	states := make([]casanova.StateData, nBlks)
	for i := range bids {
		bids[i] = ids.BlockId{
			ValId_:  0,
			Height_: uint64(i),
		}
		states[i] = makeRandomStateData(*txns_per_block)
		cas.StateCache.Set(bids[i], states[i])
	}
	// Create a fake "result" StateData, that is, the StateData that
	// BlockState would be computing.
	result := casanova.StateData{
		Bvs: casanova.BVS{},
	}
	// Assign the list of fake BlockIds created above as the "active"
	// Blocks that the result StateData is aware of.
	for _, bid := range bids {
		result.Bvs[bid] = new(casanova.VS)
	}
	// Create a fake Block to feed to PopulateActiveTxnTable. This just informs
	// the function which LHT to use for conflict checking.
	blk := &block.SilvermintBlock{
		Creator_: val,
	}

	// Populate the LHT
	t1 := time.Now()
	cas.Consensus.PopulateActiveTxnTable(result, cas.StateCache, blk, cas, "self")
	t2 := time.Now()

	fmt.Printf("Time to insert %d txns: %d ms.\n", nTxns, t2.Sub(t1).Milliseconds())
}
