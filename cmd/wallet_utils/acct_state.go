// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package wallet_utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/transaction"
)

type AccountState struct {
	Balance     uint64             `json:"balance"`
	CheckNumber uint16             `json:"checknum"`
	Utxos       []transaction.Utxo `json:"utxos"`
}

func GetAcctState(address crypto.WalletAddress, apiStr string) (AccountState, error) {
	pubKey := address.Address()
	state := *new(AccountState)

	pubStr := fmt.Sprintf("%x", pubKey)
	res, err := http.Get("http://" + apiStr + "/account-state/" + pubStr)
	if err != nil {
		fmt.Println(err)
		fmt.Printf("%v wallet error received when calling http://%s/account-state. Error: %s\n", time.Now(), apiStr, err.Error())
		return state, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return state, err
	}
	err = json.Unmarshal(body, &state)
	if err != nil {
		fmt.Println(err)
		return state, err
	}
	return state, nil
}
