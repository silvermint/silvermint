// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"time"

	"gitlab.com/silvermint/silvermint/cmd/wallet_utils"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/transaction"
)

func LoadWallets(dir string) ([]crypto.Wallet, error) {
	pubkeyRegexp, _ := regexp.Compile("pub$")
	entries, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	wallets := make([]crypto.Wallet, 0)

	for _, info := range entries {
		filename := filepath.Join(dir, info.Name())
		if pubkeyRegexp.Match([]byte(filename)) {
			continue // skip public keys
		}
		// NB(leaf): Might need to join dir path.
		wallet, err := crypto.LoadWalletFromFile(filename)
		if err != nil {
			fmt.Printf("WARNING: failed to load wallet from %s: %s\n ", filename, err.Error())
			continue
		}
		wallets = append(wallets, wallet)
	}

	return wallets, nil
}

func fileExists(filename string) bool {
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func GenerateWallets(
	wallets []crypto.Wallet,
	users int,
	dir string,
	verbosity int,
) ([]crypto.Wallet, error) {

	num := users - len(wallets)
	if verbosity > 0 {
		fmt.Printf("Generating %d wallets...\n", num)
	}

	rval := make([]crypto.Wallet, len(wallets))
	copy(rval, wallets)

	j := 0 // where to start the filenames
	for i := 0; i < num; i++ {
		var walletName string
		for {
			walletName = filepath.Join(dir, fmt.Sprintf("wallet_%d", j))
			if !fileExists(walletName) {
				if verbosity > 2 {
					fmt.Printf("Creating wallet in filename: %s\n", walletName)
				}
				break
			}

			j += 1

			if j > 50000 {
				fmt.Printf("ERROR: Tried over 50,000 filenames, which is too many. Clear up some space and try again.\n")
				os.Exit(1)
			}
		}

		walletPtr, err := crypto.NewWallet(walletName, crypto.BASIC_WALLET)
		if err != nil {
			fmt.Printf("ERROR: Can't generate wallet: %s\n", err.Error())
			os.Exit(1)
		}

		rval = append(rval, walletPtr)
	}

	return rval, nil
}

// Simulate transactions being submitted to a single backend using a directory
// of wallets and a fixed target TPS.
func simulate(
	wallets []crypto.Wallet,
	minDelay int,
	maxDelay int,
	server string,
	outputs int,
	verbosity int,
	maxUtxos int,
) {
	if outputs == 0 {
		fmt.Printf("ERROR: can't simulate transactions with no outputs\n")
		os.Exit(1)
	}

	if maxUtxos < 1 {
		fmt.Printf("ERROR: maxUtxos < 1 is not allowed (only Sweep transactions would be sent)\n")
		os.Exit(1)
	}

	if verbosity > 0 {
		fmt.Printf("Simulate: Backend is: %s\n", server)
		fmt.Printf("Simulate: Generating txns across %d wallets with max %d txos.\n",
			len(wallets), outputs)
		fmt.Printf("Simulate: Wallet delay range is %d-%d seconds.\n",
			minDelay, maxDelay)
	}

	for _, wallet := range wallets {
		go simulatePayments(wallet, wallets, minDelay, maxDelay, server, outputs, verbosity, maxUtxos)
	}

	select {} // sleep forever
}

func simulatePayments(
	wallet crypto.Wallet,
	wallets []crypto.Wallet,
	minDelay int,
	maxDelay int,
	server string,
	outputs int,
	verbosity int,
	maxUtxos int,
) {

	for {
		minMillis := minDelay * 1000
		maxMillis := maxDelay * 1000
		sleepDelay := minMillis + rand.Intn(maxMillis)
		if verbosity > 3 {
			fmt.Printf("Wallet %x: sleeping %d milliseconds.\n", wallet.WalletAddress(), sleepDelay)
		}

		time.Sleep(time.Duration(sleepDelay) * time.Millisecond)

		if verbosity > 2 {
			fmt.Printf("Simulating payment from %x.\n", wallet.WalletAddress())
		}

		err := simulatePayment(wallet, wallets, server, outputs, verbosity, maxUtxos)
		if err != nil {
			fmt.Printf("Error simulating payment: %s\n", err.Error())
		}
	}
}

// The simulatePayment function takes a set of wallets and an index to start at
// and iterates over the wallets in sequence to generate transactions. This
// isn't necessary, we could choose the source wallet randomly, but doing it
// this way ensures the network has more time to validate a wallet's
// transactions before a new one is generated from the given source wallet.
func simulatePayment(
	wallet crypto.Wallet,
	wallets []crypto.Wallet,
	server string,
	outputs int,
	verbosity int,
	maxUtxos int,
) error {
	state, err := getAcctState(wallet.WalletAddress(), server)
	if err != nil {
		return err
	}

	if state.Balance != 0 {
		err = processSimulatedPayment(
			wallet,
			wallets,
			state,
			server,
			outputs,
			verbosity,
			maxUtxos,
		)
	} else {
		err = processMinting(
			wallet,
			wallets,
			state,
			server,
			outputs,
			verbosity,
		)
	}

	return err
}

func processMinting(
	wallet crypto.Wallet,
	wallets []crypto.Wallet,
	state wallet_utils.AccountState,
	server string,
	outputs int,
	verbosity int,
) error {
	state, err := getAcctState(wallet.WalletAddress(), server)
	if verbosity > 2 {
		fmt.Printf("Minting: account state is: %v\n", state)
		fmt.Printf("Minting: Sending check number: %d\n", state.CheckNumber+1)
	}

	// NB(chuck): "1 + ..." ensures the transaction value is at least 1.
	amount := 1 + uint64(rand.Int63n(1000))

	mintTxn := transaction.Mint{
		Src:          wallet.WalletAddress(),
		CheckNumber_: state.CheckNumber + 1,
		Output_: transaction.MintOutput{
			Amount_: amount,
			Dest_:   wallet.WalletAddress(),
		},
		Bytes: []byte{},
		Sig:   crypto.Signature{},
	}

	err = sendTxn(wallet, &mintTxn, server)
	if err != nil {
		return fmt.Errorf("%v simulate error: Mint txn failed: http://%s/payment: %s\n",
			time.Now(), server, err.Error())
	}

	if verbosity > 1 {
		fmt.Printf("Minting: OK: minted %d coins for wallet %x\n",
			amount, wallet.WalletAddress().Address())
	}
	return nil
}

// Process a simulated payment from a source wallet to one or more target
// wallets that are randomly chosen.
func processSimulatedPayment(
	source crypto.Wallet,
	wallets []crypto.Wallet,
	state wallet_utils.AccountState,
	server string,
	outputs int,
	verbosity int,
	maxUtxos int,
) error {
	usableBalance := state.Balance
	usableUtxos := state.Utxos
	if verbosity > 2 {
		fmt.Printf("Source Wallet %x: usable balance = %d, utxos: %v\n",
			source.WalletAddress().Address(), usableBalance, usableUtxos)
	}

	if usableBalance == 0 {
		if verbosity > 2 {
			fmt.Printf("Aborting payment, wallet %x has no usable balance\n", source.WalletAddress())
			return nil
		}
	}

	if len(usableUtxos) > maxUtxos {
		return processSimulatedSweep(source, wallets, state, usableUtxos, server, verbosity)
	}

	// Select random wallets / random amounts as outputs.
	if outputs < 1 {
		return fmt.Errorf("Invalid number of outputs %d: must be at least one output.", outputs)
	}

	// NB(chuck): rand.Intn(n) operates on the half-open interval [0,n). It panics if n <= 0.
	numOutputs := 1 + rand.Intn(outputs)

	paymentOutputs := make([]transaction.PaymentOutput, 0)
	remainder := usableBalance
	var totalOutput uint64 = 0
	//fmt.Printf("SOURCEWALLET %x NUMOUTPUTS %d USABLEBALANCE %d\n", source.WalletAddress().Address(), numOutputs, usableBalance)
	for i := 0; i < numOutputs; i++ {
		if remainder <= 0 {
			break
		} // we're done here.
		outputWallet := wallets[rand.Intn(len(wallets))]
		// NB(chuck): rand.Int63n(...) permits values in the set [0, n).
		// Thus, 1 + rand.Int63n(...) permits values in the set [1, n],
		// which is what we want to ensure both non-zero payment outputs
		// and an output that is <= remainder.
		outputAmount := 1 + uint64(rand.Int63n(int64(remainder)))
		paymentOutputs = append(
			paymentOutputs,
			transaction.PaymentOutput{
				Amount_: uint64(outputAmount),
				Dest_:   outputWallet.WalletAddress(),
			},
		)
		totalOutput += outputAmount
		remainder -= outputAmount
	}

	// Don't send a payment if it has no outputs
	if len(paymentOutputs) == 0 {
		if verbosity > 2 {
			fmt.Printf("Aborting payment, no payment outputs\n")
		}
		return nil
	}

	if verbosity > 2 {
		fmt.Printf("Payment source Wallet %x: %d total outputs, amount = %d, remaining balance = %d\n",
			source.WalletAddress(), len(paymentOutputs), totalOutput, remainder)
	}

	// Determine the source UTXOs needed for the payment.
	sources := []transaction.Utxo{}
	var spentAmount uint64 = 0
	for _, u := range usableUtxos {
		fmt.Printf("Payment source Wallet %x: u.A = %d\n", source.WalletAddress().Address(), u.Amount)
		if spentAmount > totalOutput {
			break
		}
		sources = append(sources, u)
		spentAmount += u.Amount
	}

	if spentAmount < totalOutput {
		fmt.Printf("ERROR: Couldn't spend enough usable UTXOs to cover total Outputs.\n")
		os.Exit(1)
	}

	// Return any leftover amount to the source wallet.
	if totalOutput < spentAmount {
		paymentOutputs = append(
			paymentOutputs,
			transaction.PaymentOutput{
				Amount_: uint64(spentAmount - totalOutput),
				Dest_:   source.WalletAddress(),
			},
		)
	}

	// Construct the Payment.
	pmt := transaction.Payment{
		Src:          source.WalletAddress(),
		CheckNumber_: state.CheckNumber + 1,
		Inputs_:      sources,
		Outputs_:     paymentOutputs,
	}

	err := sendTxn(source, &pmt, server)
	if err != nil {
		fmt.Printf("ERROR: send Payment failed: %s\n", err.Error())
		return err
	}

	return nil
}

func processSimulatedSweep(
	source crypto.Wallet,
	wallets []crypto.Wallet,
	state wallet_utils.AccountState,
	usableUtxos []transaction.Utxo,
	server string,
	verbosity int,
) error {
	// Determine the source UTXOs for the sweep.
	sources := []transaction.Utxo{}
	totalVal := uint64(0)
	for _, u := range usableUtxos {
		fmt.Printf("Sweep source Wallet %x: u.A = %d\n", source.WalletAddress().Address(), u.Amount)
		sources = append(sources, u)
		totalVal += u.Amount
	}

	out := transaction.SweepOutput{
		Amount_: totalVal,
		Dest_:   source.WalletAddress(),
	}

	// Construct the Sweep.
	sweep := transaction.Sweep{
		Src:          source.WalletAddress(),
		CheckNumber_: state.CheckNumber + 1,
		Inputs_:      sources,
		Output_:      out,
	}

	err := sendTxn(source, &sweep, server)
	if err != nil {
		fmt.Printf("ERROR: send Sweep failed: %s\n", err.Error())
		return err
	}
	return nil
}

// Send a transaction to the backend API server.
func sendTxn(
	source crypto.Wallet,
	txn transaction.Transaction,
	server string,
) error {
	// Marshal to Binary.
	data, err := txn.MarshalSignedBinary(source)
	if err != nil {
		return fmt.Errorf("Error constructing transaction, aborting: %s\n", err.Error())
	}

	// Submit the transaction
	route := fmt.Sprintf("http://%s/payment", server)
	req, err := http.Post(route, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("%v simulate error received when calling http://%s/payment. Error: %s\n", time.Now(), server, err.Error())

	}

	if req.StatusCode != http.StatusOK {
		errObj := *new(ErrObj)
		body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			fmt.Println(err)
		} else {
			err = json.Unmarshal(body, &errObj)
			if err != nil {
				fmt.Println(err)
			}
		}
		return fmt.Errorf("%v simulate error %s received when calling http://%s/payment. Error: %s\n", time.Now(), req.Status, server, errObj.Reason)
	}

	return nil
}

func getAcctState(
	address crypto.WalletAddress,
	apiStr string,
) (
	wallet_utils.AccountState,
	error,
) {
	pubKey := address.Address()
	state := *new(wallet_utils.AccountState)

	pubStr := fmt.Sprintf("%x", pubKey)
	res, err := http.Get("http://" + apiStr + "/account-state/" + pubStr)
	if err != nil {
		fmt.Printf("%v simulate error received when calling http://%s/account-state. Error: %s\n", time.Now(), apiStr, err.Error())
		return state, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("getAcctState: Error reading response body: %s\n", err.Error())
		return state, err
	}
	err = json.Unmarshal(body, &state)
	if err != nil {
		fmt.Printf("getAcctState: Error unmarshaling response body: %s\n", err.Error())
		return state, err
	}
	return state, nil
}
