// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/silvermint/silvermint"
	"gitlab.com/silvermint/silvermint/utils"

	"github.com/spf13/pflag"
)

type ErrObj struct {
	Reason string `json:"reason"`
}

func main() {
	currentUser, err := user.Current()
	if err != nil {
		fmt.Printf("FATAL: Can't get current user. Is your system broken?\n")
		os.Exit(1)
	}

	ConfigDir := filepath.Join(currentUser.HomeDir, ".silvermint")

	// Command flags.
	version := pflag.Bool("version", false, "Print version and exit.")
	verbose := pflag.CountP(
		"verbose", "v",
		"Increased verbosity (multiple times for more).",
	)
	apiServer := pflag.StringP(
		"apiserver", "a",
		"localhost:8880",
		"The ip address and port for your Silvermint node.",
	)
	simulationDir := pflag.StringP(
		"simdir", "d",
		filepath.Join(ConfigDir, "simulation"),
		"The directory load/store wallets from/to.",
	)
	simulationTps := pflag.IntP(
		"tps", "t",
		25,
		"Target transactions per second across all wallets.",
	)
	simulationMaxOutputs := pflag.UintP(
		"out", "o",
		10,
		"Max number of payment outputs.",
	)
	walletDelayRange := pflag.StringP(
		"delay", "D",
		"10-20",
		"Transactions from a wallet generated within this range of seconds.",
	)
	maxUtxos := pflag.IntP(
		"utxos", "u", 5,
		"The maximum allowed UTXOs per wallet before a Sweep transaction is sent.",
	)

	pflag.Parse()

	if *version {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	err = utils.MaybeCreateDir(*simulationDir, 0700)
	if err != nil {
		fmt.Printf("Error creating simulation directory: %s\n", err.Error())
		os.Exit(1)
	}

	delays := strings.SplitN(*walletDelayRange, "-", 2)
	minDelay, _ := strconv.Atoi(delays[0])
	maxDelay, _ := strconv.Atoi(delays[1])
	avgDelay := (minDelay + maxDelay) / 2

	simulatedUsers := *simulationTps * avgDelay

	wallets, err := LoadWallets(*simulationDir)
	if err != nil {
		fmt.Printf("FATAL: %s\n", err.Error())
		os.Exit(1)
	}

	if len(wallets) < simulatedUsers {
		if *verbose > 0 {
			fmt.Printf("We have %d wallets, but want %d... generating the rest in %s\n",
				len(wallets), simulatedUsers, *simulationDir)
		}
		wallets, err = GenerateWallets(wallets, simulatedUsers, *simulationDir, *verbose)
		if err != nil {
			fmt.Printf("FATAL: %s\n", err.Error())
			os.Exit(1)
		}
	} else if len(wallets) > simulatedUsers {
		wallets = wallets[0 : simulatedUsers-1]
	}

	simulate(wallets, minDelay, maxDelay, *apiServer, int(*simulationMaxOutputs), *verbose, *maxUtxos)
	return
}
