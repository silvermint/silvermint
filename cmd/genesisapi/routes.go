package main

import (
	"github.com/gin-gonic/gin"
)

func initRoutes(router *gin.Engine) *gin.Engine {
	router.POST("/validator", AddValidator)
	router.DELETE("/validator/:wallet", DeleteValidator)
	router.POST("/balance", AddWalletAndBalance)
	router.GET("/validator/:wallet", VerifyValidator)
	router.GET("/balance/:wallet", VerifyWalletAndBalance)
	router.GET("/genesis", GetGenesis)
	router.GET("/", GetWebpage)
	router.GET("/validator", GetAllValidators)
	router.GET("/balance", GetAllBalances)

	return router
}
