package main

import (
	"fmt"
	"path/filepath"

	"gitlab.com/silvermint/silvermint/log"
	"github.com/gin-gonic/gin"
	"github.com/spf13/pflag"
)

func Run(templates []string) {
	apiPort := pflag.Int("boot_api_port", 80, "The port on which the client api HTTP server listens.")
	ipAddress := pflag.String("boot_api_ipaddr", "0.0.0.0", "The IP address that the Client API listens on.")
	pflag.Parse()

	if *ipAddress == "" || *apiPort == 0 {
		log.Fatalln("IP address and API port must be specified in config or cmd-line.")
	}

	consoleAddress := fmt.Sprintf("%s:%d", *ipAddress, *apiPort)

	var router *gin.Engine = gin.Default()
	router.SetTrustedProxies(nil)

	for _, path := range templates {
		glob := filepath.Join(path, "*")
		router.LoadHTMLGlob(glob)
		log.Debugf("Console loaded HTML Glob: %s", glob)
	}

	initRoutes(router)

	router.Run(consoleAddress)
}
