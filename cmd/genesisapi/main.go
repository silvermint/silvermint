package main

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"text/template"

	"gitlab.com/silvermint/silvermint"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/utils"
	"gitlab.com/silvermint/silvermint/validator"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var ValId_ *ValId

func main() {
	template_paths := strings.Join(
		[]string{"/usr/lib/silvermint-tools/templates", "./templates"},
		string(filepath.ListSeparator))

	version := pflag.Bool("version", false, "Print version and exit.")
	pflag.String("templates_path", template_paths,
		"A list of potential locations to find HTML templates "+
			"(separated by OS-specific path list separator).")
	pflag.String("save_dir", "/.genesisapi", "Directory for dag snapshot save files. If empty: ~/save.")
	pflag.String("config_dir", "/etc/silvermint",
		"Directory for private files that need root permisions to access. If empty: /etc/silvermint.")
	pflag.Uint16("num_validators", 100, "Total number of validators in genesis ceremony")
	pflag.Parse()

	if *version {
		fmt.Printf(silvermint.GetVersionString(os.Args[0]))
		os.Exit(0)
	}

	viper.BindPFlags(pflag.CommandLine)
	utils.MaybeCreateDir(viper.GetString("save_dir"), 0755)
	fmt.Println("Genesis API Started")
	MakeValId()
	Run(findHTMLTemplates())
}

func findHTMLTemplates() []string {
	var templates_found []string
	paths_list := filepath.SplitList(viper.Get("templates_path").(string))
	for _, root := range paths_list {
		filepath.WalkDir(root,
			func(path string, d fs.DirEntry, err error) error {
				if err == nil && d.IsDir() {
					pattern := filepath.Join(path, "*.html")
					tmpl, e := template.ParseGlob(pattern)
					if e == nil && tmpl != nil {
						templates_found = append(templates_found, path)
					}
				}
				return err
			})
	}

	if len(templates_found) == 0 {
		log.Fatalln("FATAL: No HTML templates found in tree at ",
			viper.Get("templates_path"))
	}
	return templates_found
}

type ValId struct {
	valIdMap map[string]uint16
	mu       sync.Mutex
}

func (v *ValId) SetupValId() {
	v.mu.Lock()
	defer v.mu.Unlock()
	valDir := filepath.Join(viper.GetString("save_dir"), VALIDATOR_FOLDER)
	valFiles, err := os.ReadDir(valDir)
	if err != nil {
		return
	}
	for _, valFile := range valFiles {
		filename := valFile.Name()
		val := validator.Validator{}
		err := utils.Load(&val, VALIDATOR_FOLDER, filename)
		if err != nil {
			continue
		}
		wallStr := fmt.Sprintf("%x", val.Pubkey.Address())
		v.valIdMap[wallStr] = val.Id
	}
}
func (v *ValId) GetValId(wallet string) (uint16, error) {
	v.mu.Lock()
	defer v.mu.Unlock()

	//Check if we have that wallet info saved, if so return its slot
	id, ok := v.valIdMap[wallet]
	if ok {
		return id, nil
	}

	//check to see if map is full
	maxNumValidators := viper.GetUint16("num_validators")

	if len(v.valIdMap) >= int(maxNumValidators) {
		return 0, fmt.Errorf("no Slots")
	}

	//if validator wallet is new find an empty slot
	var i uint16
	for i = 0; i < maxNumValidators; i++ {
		unused := true
		for _, value := range v.valIdMap {
			if value == i {
				unused = false
				break
			}
		}
		if unused {
			v.valIdMap[wallet] = i
			return i, nil
		}
	}
	return 0, fmt.Errorf("no slots")
}

func (v *ValId) DeleteValId(wallet string, filename string) (error, uint16) {
	v.mu.Lock()
	defer v.mu.Unlock()
	valId := v.valIdMap[wallet]
	delete(v.valIdMap, wallet)
	err := os.Remove(filename)
	if err != nil {
		return err, 0
	}
	return nil, valId
}

func MakeValId() {
	ValId_ = &ValId{valIdMap: map[string]uint16{}}
	ValId_.SetupValId()
}
