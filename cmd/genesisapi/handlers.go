package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/utils"
	"gitlab.com/silvermint/silvermint/validator"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

const (
	VALIDATOR_FOLDER   = "validators"
	TRANSACTION_FOLDER = "transactions"
	ADMIN_KEYNAME      = "adminkey"
	KEY_FILENAME       = "keys"
	SNAPSHOT_FILE      = "genesis_snapshot.tar.gz"
)

type ValidatorData struct {
	Pubkey string
	Ip     string
}

func AddValidator(c *gin.Context) {
	valData := new(ValidatorData)
	err := c.ShouldBindJSON(valData)
	if err != nil {
		fmt.Printf("Invalid request payload: %s", err.Error())
		c.String(http.StatusBadRequest, "Invalid request payload")
		return
	}
	//create wallet to make sure its valid
	wallet, err := crypto.UnmarshalPublicKeyFromHex(valData.Pubkey)
	if err != nil {
		fmt.Printf("Invalid wallet address: %s", err.Error())
		c.String(http.StatusBadRequest, "Invalid wallet address")
		return
	}

	id, err := ValId_.GetValId(valData.Pubkey)
	if err != nil {
		fmt.Printf("No Validator Slots")
		c.String(http.StatusBadRequest, "No Validator Slots")
		return
	}

	val := &validator.Validator{
		Id:       id,
		Hostport: valData.Ip,
		Pubkey:   wallet.WalletAddress(),
		Active:   true,
	}

	//save validator
	valFilename := fmt.Sprintf("val_%s.bin", valData.Pubkey)
	err = utils.Overwrite(val, VALIDATOR_FOLDER, valFilename)
	if err != nil {
		fmt.Printf("Failed to save validator: %s", err.Error())
		c.String(http.StatusInternalServerError, "Failed to save validator")
		return
	}

	c.String(http.StatusOK, fmt.Sprintf("Validator Saved, Id: %d", id))
}

type ValidatorWallet struct {
	Pubkey string
}

func DeleteValidator(c *gin.Context) {
	walletStr := c.Param("wallet")

	//create wallet to make sure its valid
	_, err := crypto.UnmarshalPublicKeyFromHex(walletStr)
	if err != nil {
		fmt.Printf("Invalid wallet address: %s", err.Error())
		c.String(http.StatusBadRequest, "Invalid wallet address")
		return
	}

	filename := filepath.Join(viper.GetString("save_dir"), VALIDATOR_FOLDER, fmt.Sprintf("val_%s.bin", walletStr))
	err, valId := ValId_.DeleteValId(walletStr, filename)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Printf("Deleting Unknown Validator: %s", err.Error())
			c.String(http.StatusBadRequest, "Validator does not exists")
		} else {
			fmt.Printf("Failure to Delete file: %s", err.Error())
			c.String(http.StatusBadRequest, "Unable to delete validator info")
		}
		return
	}

	c.String(http.StatusOK, fmt.Sprintf("Validator Slot %d Released", valId))
}

type WalletBalanceData struct {
	Wallet string
	Amount string
}

func AddWalletAndBalance(c *gin.Context) {
	//process data
	walletBalance := new(WalletBalanceData)
	err := c.ShouldBindJSON(walletBalance)
	if err != nil {
		fmt.Printf("Invalid request payload: %s", err.Error())
		c.String(http.StatusBadRequest, "Invalid request payload")
		return
	}

	//create mint transaction
	wallet, err := crypto.UnmarshalPublicKeyFromHex(walletBalance.Wallet)
	if err != nil {
		fmt.Printf("Invalid wallet address: %s", err.Error())
		c.String(http.StatusBadRequest, "Invalid wallet address")
		return
	}

	admin_wallet, err := crypto.LoadWalletFromFile(
		filepath.Join(viper.GetString("config_dir"), KEY_FILENAME, ADMIN_KEYNAME))

	if err != nil {
		fmt.Printf("Failed to sign transaction: %s", err.Error())
		c.String(http.StatusInternalServerError, "Failed to sign transaction")
		return
	}

	amount, err := strconv.ParseUint(walletBalance.Amount, 0, 64)
	if err != nil {
		fmt.Printf("Invalid amount: %s", err.Error())
		c.String(http.StatusBadRequest, "Invalid amount")
		return
	}

	mint := new(transaction.Mint)
	mint.CheckNumber_ = 0
	mint.Src = admin_wallet.WalletAddress()
	mint.Output_ = transaction.MintOutput{Amount_: amount, Dest_: wallet.WalletAddress()}

	//marshal and sign transaction
	mintBytes, err := mint.MarshalSignedBinary(admin_wallet)
	if err != nil {
		fmt.Printf("Failed to process transaction: %s", err.Error())
		c.String(http.StatusInternalServerError, "Failed to process transaction")
		return
	}
	err = mint.UnmarshalBinary(mintBytes)
	if err != nil {
		fmt.Printf("Failed to process transaction: %s", err.Error())
		c.String(http.StatusInternalServerError, "Failed to process transaction")
		return
	}

	//save mint transaction to file
	err = utils.Overwrite(mint, TRANSACTION_FOLDER, fmt.Sprintf("txn_%x.bin", wallet.WalletAddress().Address()))
	if err != nil {
		fmt.Printf("Failed to save transaction: %s", err.Error())
		c.String(http.StatusInternalServerError, "Failed to save transaction")
		return
	}

	//save to transactions folder
	c.String(http.StatusOK, "Transaction Saved")
}

type SimpleValidator struct {
	ValId  string `json:"Validator Id"`
	IpPort string `json:"Ip and Port"`
	Pubkey string `json:"Public Key"`
}

func VerifyValidator(c *gin.Context) {
	wallet := c.Param("wallet")
	_, err := crypto.UnmarshalPublicKeyFromHex(wallet)
	if err != nil {
		fmt.Printf("Invalid Public Key: %s", err.Error())
		c.String(http.StatusBadRequest, "Invalid Public Key")
		return
	}

	varFilename := fmt.Sprintf("val_%s.bin", wallet)
	varFilepath := filepath.Join(viper.GetString("save_dir"), VALIDATOR_FOLDER, varFilename)
	_, err = os.Stat(varFilepath)
	if os.IsNotExist(err) {
		fmt.Printf("Validator does not exist")
		c.String(http.StatusBadRequest, "Validator does not exist")
		return
	}

	val := new(validator.Validator)
	err = utils.Load(val, VALIDATOR_FOLDER, varFilename)
	if err != nil {
		fmt.Printf("Failed to load validator: %s", err.Error())
		c.String(http.StatusInternalServerError, "Failed to load validator")
		return
	}

	c.JSON(http.StatusOK, SimpleValidator{
		ValId:  fmt.Sprintf("%d", val.Id),
		IpPort: val.Hostport,
		Pubkey: fmt.Sprintf("%x", val.Pubkey.Address()),
	})
}

type SimpleBalance struct {
	Wallet  string `json:"Wallet"`
	Balance string `json:"Balance"`
}

func VerifyWalletAndBalance(c *gin.Context) {
	walletAddr := c.Param("wallet")
	_, err := crypto.UnmarshalPublicKeyFromHex(walletAddr)
	if err != nil {
		fmt.Printf("Invalid Wallet Address: %s", err.Error())
		c.String(http.StatusBadRequest, "Invalid Wallet Address")
		return
	}

	txnFilename := fmt.Sprintf("txn_%s.bin", walletAddr)
	txnFilepath := filepath.Join(viper.GetString("save_dir"), TRANSACTION_FOLDER, txnFilename)
	_, err = os.Stat(txnFilepath)
	if os.IsNotExist(err) {
		fmt.Printf("Wallet Balance does not exist")
		c.String(http.StatusBadRequest, "Wallet Balance does not exist")
		return
	}

	mint := new(transaction.Mint)
	err = utils.Load(mint, TRANSACTION_FOLDER, txnFilename)
	if err != nil {
		fmt.Printf("Failed to load Wallet Balance: %s", err.Error())
		c.String(http.StatusInternalServerError, "Failed to load Wallet Balance")
		return
	}

	c.JSON(http.StatusOK, SimpleBalance{
		Wallet:  fmt.Sprintf("%x", mint.Output_.Dest_.Address()),
		Balance: fmt.Sprintf("%d", mint.Output_.Amount_),
	})
}

func GetGenesis(c *gin.Context) {
	snapshotFilepath := filepath.Join(viper.GetString("save_dir"), SNAPSHOT_FILE)
	if _, err := os.Stat(snapshotFilepath); os.IsNotExist(err) {
		c.String(http.StatusNotFound, "Genesis info not found")
		return
	}
	c.Header("Content-Type", "application/octet-stream")
	c.Header("Content-Encoding", "gzip")
	c.Header("Content-Disposition", "attachment; filename="+SNAPSHOT_FILE)
	c.FileAttachment(snapshotFilepath, SNAPSHOT_FILE)
}

func GetWebpage(c *gin.Context) {
	c.HTML(http.StatusOK, "genesisapi.html", gin.H{})
}

func GetAllValidators(c *gin.Context) {
	save_dir := viper.GetString("save_dir")
	folderPath := filepath.Join(save_dir, VALIDATOR_FOLDER)
	files, err := os.ReadDir(folderPath)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			c.Status(http.StatusNoContent)
		} else {
			c.String(http.StatusInternalServerError, "Unable to load validators")
		}
		return
	}

	allVals := []SimpleValidator{}
	for _, file := range files {
		val := validator.Validator{}
		utils.Load(&val, VALIDATOR_FOLDER, file.Name())
		allVals = append(allVals, SimpleValidator{
			ValId:  fmt.Sprintf("%d", val.Id),
			IpPort: val.Hostport,
			Pubkey: fmt.Sprintf("%x", val.Pubkey.Address()),
		})
	}
	if len(allVals) <= 0 {
		c.Status(http.StatusNoContent)
		return
	}
	sort.Slice(allVals, func(i, j int) bool {
		return allVals[i].ValId < allVals[j].ValId
	})
	c.JSON(http.StatusOK, allVals)
}

func GetAllBalances(c *gin.Context) {
	save_dir := viper.GetString("save_dir")
	folderPath := filepath.Join(save_dir, TRANSACTION_FOLDER)
	files, err := os.ReadDir(folderPath)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			c.Status(http.StatusNoContent)
		} else {
			c.String(http.StatusInternalServerError, "Unable to load balances")
		}
		return
	}

	allTxns := []SimpleBalance{}
	for _, file := range files {
		mint := transaction.Mint{}
		err = utils.Load(&mint, TRANSACTION_FOLDER, file.Name())
		if err != nil {
			fmt.Println("Unable to load txns", file.Name())
			fmt.Println(err.Error())
			continue
		}
		allTxns = append(allTxns, SimpleBalance{
			Wallet:  fmt.Sprintf("%x", mint.Output_.Dest_.Address()),
			Balance: fmt.Sprintf("%d", mint.Output_.Amount_),
		})
	}
	if len(allTxns) <= 0 {
		c.Status(http.StatusNoContent)
		return
	}
	c.JSON(http.StatusOK, allTxns)
}
