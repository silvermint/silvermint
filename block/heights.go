package block

import (
	"encoding/binary"
	"fmt"

	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/validator"
)

const MaxUint6 = 1<<6 - 1
const MaxUint14 = 1<<14 - 1
const MaxUint22 = 1<<22 - 1
const MaxUint30 = 1<<30 - 1
const MaxHeightDeltaBytes = 4

type Heights [validator.N_MAX_VALIDATORS]uint64

// BlockId(uint16) returns a BlockId corresponding to the Validator ID provided
// and the Height for that validator from the Heights array.
func (hts Heights) BlockId(vid uint16) ids.BlockId {
	return ids.BlockId{
		ValId_:  vid,
		Height_: hts[vid],
	}
}

// Add(HeightDeltaArray) adds a HeightDeltaArray to a Heights array, returning
// the result as a new Heights array. Doesn't mutate the original Heights.
func (hts Heights) Add(deltas HeightDeltaArray) Heights {
	for i := range hts {
		hts[i] += uint64(deltas.Get(uint16(i)).Delta)
	}
	return hts
}

// Sub(Heights) subtracts the heights in the argument FROM the heights in the
// receiver, and returns the difference as a new Heights array. Doesn't mutate
// the original Heights.
func (greater Heights) Sub(lesser Heights) (HeightDeltaArray, error) {
	deltas := NewHeightDeltaArray()
	for i, h := range greater {
		if lesser[i] > h {
			return nil, fmt.Errorf("can't subtract greater height from lesser"+
				"height: %d > %d", lesser[i], h)
		}
		delta := h - lesser[i]
		if delta > MaxUint30 {
			return nil, fmt.Errorf("invalid height delta %d", delta)
		}
		deltas.setDelta(uint16(i), uint32(delta))
	}
	return deltas, nil
}

// IntervalBlockIds(Heights, ValidatorSet) takes two sets of heights, greater
// and lesser, and returns a list of BlockIds that span the height difference
// between them. Requires that `for i, elem in higher: elem >= lower[i]`. The
// list of BlockIds returned includes the endpoints on both sides. Only
// considers heights from the provided ValidatorSet.
func (greater Heights) IntervalBlockIds(lesser Heights, regs validator.ValidatorSet,
) ([]ids.BlockId, error) {
	bids := []ids.BlockId{}
	for reg := range regs {
		if greater[reg.Id] < lesser[reg.Id] {
			return nil, fmt.Errorf("can't subtract greater height from lesser"+
				"height: %d > %d", lesser[reg.Id], greater[reg.Id])
		}
		currBids, err := greater.BlockId(reg.Id).IntervalBlockId(lesser.BlockId(reg.Id))
		if err != nil {
			return nil, err
		}
		bids = append(bids, currBids...)
	}
	return bids, nil
}

func (hts Heights) MarshalBinary() ([]byte, error) {
	hbytes := make([]byte, validator.N_MAX_VALIDATORS*8)
	for vid, height := range hts {
		binary.LittleEndian.PutUint64(hbytes[8*vid:8*(vid+1)], height)
	}
	return hbytes, nil
}

func (hts *Heights) UnmarshalBinary(data []byte) error {
	for i := range hts {
		hts[i] = binary.LittleEndian.Uint64(data[8*i : 8*(i+1)])
	}
	return nil
}

type HeightDelta struct {
	Delta  uint32
	Length uint8
}

func NewHeightDelta() *HeightDelta {
	return new(HeightDelta)
}

// Store a height delta value in length bytes.
// Store (length-1) in the first two bits of the first byte.
// NB(chuck): Big endian, since we have to read the length at the beginning.
func (bh *HeightDelta) MarshalBinary() ([]byte, error) {
	// Handle bad input for ByteLength by setting to min/max
	if bh.Length < 1 {
		log.Warningf("Bad input for HeightDelta bytes: %d", bh.Length)
		bh.Length = 1
	} else if bh.Length > MaxHeightDeltaBytes {
		log.Warningf("Bad input for HeightDelta bytes: %d", bh.Length)
		bh.Length = MaxHeightDeltaBytes
	}
	// Disallow bad input for delta
	if bh.Delta > MaxUint30 {
		return nil, fmt.Errorf("delta exceeds maximum allowed: %d > %d",
			bh.Delta, MaxUint30)
	}

	data := [MaxHeightDeltaBytes]byte{}
	binary.BigEndian.PutUint32(data[:], bh.Delta)
	// Actual delta length is (the marshaled length) + 1 and is stored in the
	// two high order bits of the datum's initial byte.
	data[MaxHeightDeltaBytes-bh.Length] |= ((bh.Length - 1) << 6)
	return data[MaxHeightDeltaBytes-bh.Length:], nil
}

// Zeroes the specified bit
func clearBit(n *byte, pos uint) {
	mask := byte(^(1 << pos))
	*n = *n & mask
}

func (bh *HeightDelta) UnmarshalBinary(data []byte) {
	firstByte := data[0]
	nBytes := int((firstByte >> 6) + 1)
	clearBit(&data[0], 6)
	clearBit(&data[0], 7)
	for i := 0; i < nBytes; i++ {
		bh.Delta |= uint32(data[nBytes-i-1]) << int(8*i)
	}
	bh.Length = uint8(nBytes)
}

type SilvermintHeightDeltaArray [validator.N_MAX_VALIDATORS]HeightDelta

type HeightDeltaArray interface {
	Get(uint16) HeightDelta
	Length() uint16
	MarshalBinary() ([]byte, error)
	UnmarshalBinary([]byte) error
}

func (bh *SilvermintHeightDeltaArray) Get(val uint16) HeightDelta {
	return bh[val]
}

func (bh *SilvermintHeightDeltaArray) Length() uint16 {
	l := uint16(0)
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		l += uint16(bh.Get(i).Length)
	}
	return l
}

func NewHeightDeltaArray() *SilvermintHeightDeltaArray {
	sbh := new(SilvermintHeightDeltaArray)
	for i := 0; i < validator.N_MAX_VALIDATORS; i++ {
		sbh[i] = *NewHeightDelta()
		sbh[i].Length = 1
	}
	return sbh
}

func (bh *SilvermintHeightDeltaArray) MarshalBinary() ([]byte, error) {
	d := make([]byte, bh.Length())
	offset := uint16(0)
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		height := bh.Get(i)
		data, err := height.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(d[offset:offset+uint16(height.Length)], data)
		offset += uint16(height.Length)
	}
	return d, nil
}

func (bh *SilvermintHeightDeltaArray) UnmarshalBinary(data []byte) error {
	offset := 0
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		if offset >= len(data) {
			return fmt.Errorf("error unmarshaling block heights")
		}
		bh[i].UnmarshalBinary(data[offset:])
		offset += int(bh[i].Length)
	}
	return nil
}

func (bh *SilvermintHeightDeltaArray) setDelta(val uint16, delta uint32) error {
	// Set length
	if delta <= MaxUint6 {
		bh[val].Length = 1
	} else if delta <= MaxUint14 {
		bh[val].Length = 2
	} else if delta <= MaxUint22 {
		bh[val].Length = 3
	} else if delta <= MaxUint30 {
		bh[val].Length = 4
	} else {
		// should never happen
		return fmt.Errorf("HeightDelta exceeded max allowable value: "+
			"%d > %d", delta, MaxUint30)
	}
	bh[val].Delta = delta
	return nil
}
