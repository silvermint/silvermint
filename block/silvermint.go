// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package block

import (
	"encoding/binary"
	"fmt"
	"time"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
)

type SilvermintBlock struct {
	// NB(leaf): The wire format here is as follows:
	//	Block Length:	2 bytes
	//  Creator's Id:   2 bytes
	//  Timestamp:		8 bytes
	//  Parents:		32 + (1 to 4) * validator.N_MAX_VALIDATORS bytes
	//  Num Txns:		2 bytes
	//  Transactions:	varies.
	//  Signature:		64 bytes.
	Hash_          *BlockHash
	Creator_       validator.Validator
	Parents_       Heights
	Deltas_        HeightDeltaArray
	Transactions_  []transaction.Transaction
	Registrations_ []validator.Validator

	Height_    uint64
	Length_    uint64
	Validator_ uint16
	Timestamp_ int64
	Pubkey_    [crypto.BASIC_WALLET_ADDR_SIZE]byte
	Signature_ [crypto.SIGNATURE_SIZE]byte
	Data_      []byte
}

func NewSilvermintBlock() *SilvermintBlock {
	return new(SilvermintBlock)
}

func (*SilvermintBlock) isBlock() {}

func (b *SilvermintBlock) Creator() validator.Validator {
	return b.Creator_
}

func (b *SilvermintBlock) Parents() Heights {
	return b.Parents_
}

func (b *SilvermintBlock) Deltas() HeightDeltaArray {
	return b.Deltas_
}

func (b *SilvermintBlock) Transactions() []transaction.Transaction {
	return b.Transactions_
}

func (b *SilvermintBlock) Registrations() validator.ValidatorSet {
	s := validator.ValidatorSet{}
	for _, rh := range b.Registrations_ {
		s[rh] = true
	}
	return s
}

func (b *SilvermintBlock) Timestamp() int64 {
	return b.Timestamp_
}

// For Silvermint block compatibility.
func (b *SilvermintBlock) Time() time.Time {
	return time.UnixMilli(b.Timestamp_)
}

func (b *SilvermintBlock) ParentsLength() uint64 {
	var l uint64 = 0
	l += uint64(b.Deltas_.Length()) // Parents Block Refs
	return l
}

func (b *SilvermintBlock) HeaderLength() uint64 {
	var l uint64 = 8 // Block Length
	l += 2           // Creator ID
	l += 8           // Timestamp
	l += 8           // Height Data
	return l
}

func (b *SilvermintBlock) LengthWithoutTransactions() uint64 {
	var l uint64
	l = b.HeaderLength()               // Header Length
	l += b.ParentsLength()             // Length of Parents Data
	l += 2                             // Num Transactions
	l += crypto.BASIC_WALLET_ADDR_SIZE // PubKey
	l += crypto.SIGNATURE_SIZE         // Signature
	return l
}

func (b *SilvermintBlock) Length() uint64 {
	var l uint64 = b.LengthWithoutTransactions()
	for _, t := range b.Transactions() {
		l += uint64(t.Length()) // Pmt
	}
	return l
}

// MarshalBinary takes the current object and returns the network bytes you
// can send to another node or whatever. Little endian order. Functional.
func (blk *SilvermintBlock) MarshalBinary() ([]byte, error) {
	if len(blk.Data_) == 0 {
		var offset uint64
		var blockLen uint64 = blk.Length()
		var data []byte = make([]byte, blockLen)

		// NB(leaf): We start writing into the buffer at byte 2. We'll write the length in
		// at the beginning right before we sign it.
		offset = 8

		// Creator Id. (2 bytes)
		binary.LittleEndian.PutUint16(data[offset:offset+2], blk.Creator().Id)
		offset += 2

		// Timestamp. (8 bytes)
		binary.LittleEndian.PutUint64(data[offset:offset+8], uint64(blk.Timestamp()))
		offset += 8

		// Height (8 bytes)
		binary.LittleEndian.PutUint64(data[offset:offset+8], uint64(blk.Height_))
		offset += 8

		// Parents
		deltasData, err := blk.Deltas_.MarshalBinary()
		if err != nil {
			return nil, err
		}
		copy(data[offset:offset+uint64(blk.Deltas_.Length())], deltasData)
		offset += uint64(blk.Deltas_.Length())

		// Num Txns
		binary.LittleEndian.PutUint16(data[offset:offset+2], uint16(len(blk.Transactions())))
		offset += 2

		// Txns
		for _, t := range blk.Transactions() {
			// Casanova#179 TODO(leaf): There's a serious flaw here. We can't
			// MarshalSignedBinary without the user's key and even though we
			// must have the transaction bytes somewhere with a valid
			// signature, we're trying to marshal it again here, which isn't
			// right.
			txnBytes, err := t.MarshalBinary()
			if err != nil {
				log.Errorln("Can't get marshal binary for transaction. Skipping.")
				continue
			}
			copy(data[offset:offset+uint64(t.Length())], txnBytes[0:])
			offset += uint64(t.Length())
		}

		// Space for Public Key
		copy(data[offset:offset+crypto.BASIC_WALLET_ADDR_SIZE], blk.Pubkey_[:])
		offset += crypto.BASIC_WALLET_ADDR_SIZE
		// Space for Signature that we aren't doing.
		copy(data[offset:offset+crypto.SIGNATURE_SIZE], blk.Signature_[:])
		offset += crypto.SIGNATURE_SIZE

		// Block Length
		binary.LittleEndian.PutUint64(data[0:8], offset)

		blk.Data_ = data
	}

	return blk.Data_, nil
}

func (blk *SilvermintBlock) MarshalSignedBinary(wallet crypto.Wallet) ([]byte, error) {
	// NB(leaf): Mike says that we should sign the block without the 64-bytes for the
	// signature and then copy the signature into the buffer, so that's what we do here.
	buf, err := blk.MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("Sign: Can't marshal block for signing: %s", err.Error())
	}

	copy(buf[len(buf)-(crypto.BASIC_WALLET_ADDR_SIZE+crypto.SIGNATURE_SIZE):],
		wallet.WalletAddress().Address())

	sig, err := wallet.Sign(buf[0 : len(buf)-crypto.SIGNATURE_SIZE])
	if err != nil {
		return nil, fmt.Errorf("Sign: Can't sign block: %s", err.Error())
	}

	copy(buf[len(buf)-crypto.SIGNATURE_SIZE:], sig)

	return buf, nil
}

// UnmarshalBinary takes the block from the network as a slice of bytes and
// updates the current object in place. Side effecty.
func (b *SilvermintBlock) UnmarshalBinary(data []byte) error {
	if len(data) == 0 {
		return fmt.Errorf("can't unmarshal empty byte array")
	}
	//log.Debugf("Unmarshal Block data is: %x", data)

	// Block Length
	var offset uint64 = 0
	b.Length_ = binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8

	if int(b.Length_) > len(data) {
		return fmt.Errorf("Unmarshal failed: block has length %d bytes, "+
			"but we have %d bytes of data.", b.Length_, len(data))
	}

	// Creator's Id
	b.Validator_ = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Creator
	b.Creator_ = validator.GetGlobalRegistrations().Get(b.Validator_)
	if !b.Creator_.IsActive() && !IsGenesis(b) {
		return fmt.Errorf("no validator found with id %d", b.Validator_)
	}

	// Timestamp.
	b.Timestamp_ = int64(binary.LittleEndian.Uint64(data[offset : offset+8]))
	offset += 8

	// Height
	b.Height_ = binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8

	// Parents
	b.Deltas_ = NewHeightDeltaArray()
	err := b.Deltas_.UnmarshalBinary(data[offset:])
	if err != nil {
		return fmt.Errorf("SilvermintBlock: can't unmarshal parent data error: %s", err.Error())
	}

	offset += uint64(b.Deltas_.Length())

	// Num Txns
	numTxns := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Transactions
	txns := make([]transaction.Transaction, numTxns)
	for i := range txns {
		l := binary.LittleEndian.Uint16(data[offset : offset+2])

		typ := binary.LittleEndian.Uint16(data[offset+2 : offset+4])

		tid := ids.TransactionId{
			BlockId: ids.BlockId{
				ValId_:  b.Creator_.Id,
				Height_: b.Height_,
			},
			Index: uint16(i),
		}
		switch typ {
		case transaction.PaymentTxn:
			p := new(transaction.Payment)
			p.TxnId_ = tid
			txns[i] = p
		case transaction.MintTxn:
			m := new(transaction.Mint)
			m.TxnId_ = tid
			txns[i] = m
		case transaction.SweepTxn:
			s := new(transaction.Sweep)
			s.TxnId_ = tid
			txns[i] = s
		default:
			return fmt.Errorf("SilvermintBlock: can't decode: "+
				"Invalid transaction type: %d", typ)
		}

		err := txns[i].UnmarshalBinary(data[offset : offset+uint64(l)])
		if err != nil {
			return fmt.Errorf("SilvermintBlock: can't unmarshal transaction: %s",
				err.Error())
		}

		offset += uint64(l)

		if int(offset) >= len(data) {
			return fmt.Errorf(
				"SilvermintBlock: Unmarshal failed: "+
					"offset %d greater than data length %d "+
					"after %d of %d transactions.",
				offset, len(data), i, numTxns)
		}
	}

	b.Transactions_ = txns

	//Pubkey
	copy(b.Pubkey_[:], data[offset:offset+crypto.BASIC_WALLET_ADDR_SIZE])
	offset += crypto.BASIC_WALLET_ADDR_SIZE

	// Signature
	copy(b.Signature_[:], data[offset:offset+crypto.SIGNATURE_SIZE])
	offset += crypto.SIGNATURE_SIZE

	// Data
	b.Data_ = make([]byte, len(data))
	copy(b.Data_, data[:])

	// Registrations aren't marshaled - use ours
	// TODO(chuck): This probably isn't right long-term when we're doing bonding
	// and slashing, where registrations will be a per-creator/per-block
	// determination.
	b.Registrations_ = []validator.Validator{}
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		v := validator.GetGlobalRegistrations().Get(i)
		if v.IsActive() {
			b.Registrations_ = append(b.Registrations_, v)
		}
	}

	return nil
}

func (blk *SilvermintBlock) Hash() BlockHash {
	if blk.Hash_ == nil {
		_, err := blk.MarshalBinary()
		if err != nil {
			return BlockHash{}
		}
		hash := BlockHash(crypto.HashBytes(blk.Data_))
		blk.Hash_ = &hash
	}
	return *blk.Hash_
}

func (blk *SilvermintBlock) BlockId() ids.BlockId {
	return ids.BlockId{
		ValId_:  blk.Creator_.Id,
		Height_: blk.Height_,
	}
}

func (b *SilvermintBlock) SetParents(hts Heights) {
	if hts != b.Parents_ {
		b.Parents_ = hts
	}
}

func (b *SilvermintBlock) Height() uint64 {
	return b.Height_
}

func (b *SilvermintBlock) Pubkey() crypto.WalletAddress {
	return crypto.NewWalletAddress(b.Pubkey_[:])
}
