package block

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/silvermint/silvermint/validator"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

func RandBytes(n int) []byte {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return []byte(string(b))
}

func TestChildrenRoundTrip(t *testing.T) {
	children := Children{}
	nMaxChildren := 10

	n := 0
	for i := 0; i < validator.N_MAX_VALIDATORS; i++ {
		children[i] = []uint64{}
		nChildren := rand.Intn(nMaxChildren)
		for j := 0; j < nChildren; j++ {
			children[i] = append(children[i], rand.Uint64())
			n++
		}
	}
	fmt.Printf("Generated %d test children\n", n)

	cBytes, err := children.MarshalBinary()
	if err != nil {
		t.Fatalf("Error marshaling child bytes: %s", err.Error())
	}
	loadedChildren := Children{}
	err = loadedChildren.UnmarshalBinary(cBytes)
	if err != nil {
		t.Fatalf("Error unmarshaling child bytes: %s", err.Error())
	}

	for i := range children {
		for j := range children[i] {
			if children[i][j] != loadedChildren[i][j] {
				t.Fatalf("Unmarshaled child didn't match marshaled child: %x != %x for [%d, %d]",
					children[i][j], loadedChildren[i][j], i, j)
			}
		}
	}
}
