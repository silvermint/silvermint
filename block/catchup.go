// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package block

import (
	"encoding/binary"
	"fmt"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/validator"
)

type CatchupRequest interface {
	Creator() validator.Validator
	Missing() ids.BlockId
	Length() uint16

	MarshalBinary() ([]byte, error)
	MarshalSignedBinary(crypto.Wallet) ([]byte, error)
	UnmarshalBinary([]byte) error
}

type SilvermintCatchupRequest struct {
	Length_  uint16
	Creator_ validator.Validator
	Missing_ ids.BlockId
}

func NewSilvermintCatchup() *SilvermintCatchupRequest {
	c := new(SilvermintCatchupRequest)
	return c
}

func (req *SilvermintCatchupRequest) Creator() validator.Validator {
	return req.Creator_
}

func (req *SilvermintCatchupRequest) Missing() ids.BlockId {
	return req.Missing_
}

func (req *SilvermintCatchupRequest) Length() uint16 {
	var l uint16 = 2           // request length
	l += 2                     // validator id
	l += 10                    // missing blockid
	l += crypto.SIGNATURE_SIZE // signature
	return l
}

func (req SilvermintCatchupRequest) MarshalBinary() ([]byte, error) {
	var d []byte
	var offset uint16 = 0

	d = make([]byte, req.Length())

	// Request Length
	binary.LittleEndian.PutUint16(d[offset:offset+2], req.Length())
	offset += 2

	// Creator Id
	binary.LittleEndian.PutUint16(d[offset:offset+2], req.Creator().Id)
	offset += 2

	// BlockId
	binary.LittleEndian.PutUint16(d[offset:offset+2], req.Missing_.ValId_)
	offset += 2
	binary.LittleEndian.PutUint64(d[offset:offset+8], req.Missing_.Height_)
	offset += 8

	// Space for Signature.
	offset += 64

	return d, nil
}

func (req SilvermintCatchupRequest) MarshalSignedBinary(wallet crypto.Wallet) ([]byte, error) {
	// NB(leaf): Mike says that we should sign the block without the 64-bytes for the
	// signature and then copy the signature into the buffer, so that's what we do here.
	buf, err := req.MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("Sign: Can't marshal catchup request for signing: %s", err.Error())
	}

	sig, err := wallet.Sign(buf[0 : len(buf)-crypto.SIGNATURE_SIZE])
	if err != nil {
		return nil, fmt.Errorf("Sign: Can't sign catchup request: %s", err.Error())
	}

	copy(buf[len(buf)-crypto.SIGNATURE_SIZE:], sig)
	return buf, nil
}

func (req *SilvermintCatchupRequest) UnmarshalBinary(
	data []byte,
) error {
	// TODO(leaf): This will be wrong once we are doing bonding, because the
	// set of validators will be relative to a block. The validator set we care
	// about are those relative to our latest block, because we're going to
	// ignore catchups from anyone that's already been slashed, unbonded, or
	// frozen (from our perspective). If they come online later, they can catchup
	// once we've finalized it.
	if len(data) == 0 {
		return fmt.Errorf("can't unmarshal empty byte array")
	}

	// Request Length
	var offset uint16 = 0
	req.Length_ = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Validator Id
	valId := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Validator
	cval := validator.GetGlobalRegistrations().Get(valId)
	if !cval.IsActive() {
		return fmt.Errorf("no validator found with id %d", valId)
	}
	req.Creator_ = cval

	// BlockId
	req.Missing_ = ids.BlockId{}
	req.Missing_.ValId_ = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	req.Missing_.Height_ = binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8

	log.Debugf("Unmarshal: len=%d validator_id=%d block_id=%s",
		req.Length_, req.Creator_.Id, req.Missing_)

	// TODO(leaf): Check the signature here, which we should be able to do
	// since we have a Validator object.
	return nil
}
