package block

import (
	"math/rand"
	"reflect"
	"testing"

	"gitlab.com/silvermint/silvermint/validator"
)

func TestBlockHeightMaxBytes(t *testing.T) {
	bh := NewHeightDelta()
	if reflect.TypeOf(bh.Delta).Size() != MaxHeightDeltaBytes {
		t.Fatalf("MAX_BLOCK_HEIGHT_BYTES should match size of BlockHeight.height")
	}
}

func TestMarshalUnmarshalBinary(t *testing.T) {
	bh := NewHeightDelta()
	bh.Delta = MaxUint6
	bh.Length = 1
	data, err := bh.MarshalBinary()
	if err != nil {
		t.Fatalf(err.Error())
	}

	nbh := NewHeightDelta()
	nbh.UnmarshalBinary(data)
	if nbh.Length != 1 {
		t.Fatalf("Wrong block height bytes, expected %d got %d\n", 1, nbh.Length)
	}
	if nbh.Delta != MaxUint6 {
		t.Fatalf("Wrong block height, expected %d got %d\n", MaxUint6, nbh.Delta)
	}

	bh.Delta = bh.Delta + 1
	bh.Length = 2
	data, err = bh.MarshalBinary()
	if err != nil {
		t.Fatalf(err.Error())
	}

	nbh = NewHeightDelta()
	nbh.UnmarshalBinary(data)
	if nbh.Length != 2 {
		t.Fatalf("Wrong block height bytes, expected %d got %d\n", 2, nbh.Length)
	}
	if nbh.Delta != MaxUint6+1 {
		t.Fatalf("Wrong block height, expected %d got %d\n", MaxUint6+1, nbh.Delta)
	}

	bh.Delta = MaxUint14
	bh.Length = 2
	data, err = bh.MarshalBinary()
	if err != nil {
		t.Fatalf(err.Error())
	}

	nbh = NewHeightDelta()
	nbh.UnmarshalBinary(data)
	if nbh.Length != 2 {
		t.Fatalf("Wrong block height bytes, expected %d got %d\n", 2, nbh.Length)
	}
	if nbh.Delta != MaxUint14 {
		t.Fatalf("Wrong block height, expected %d got %d\n", MaxUint14, nbh.Delta)
	}

	bh.Delta = bh.Delta + 1
	bh.Length = 3
	data, err = bh.MarshalBinary()
	if err != nil {
		t.Fatalf(err.Error())
	}

	nbh = NewHeightDelta()
	nbh.UnmarshalBinary(data)
	if nbh.Length != 3 {
		t.Fatalf("Wrong block height bytes, expected %d got %d\n", 3, nbh.Length)
	}
	if nbh.Delta != MaxUint14+1 {
		t.Fatalf("Wrong block height, expected %d got %d\n", MaxUint14+1, nbh.Delta)
	}

	bh.Delta = MaxUint22
	bh.Length = 3
	data, err = bh.MarshalBinary()
	if err != nil {
		t.Fatalf(err.Error())
	}

	nbh = NewHeightDelta()
	nbh.UnmarshalBinary(data)
	if nbh.Length != 3 {
		t.Fatalf("Wrong block height bytes, expected %d got %d\n", 3, nbh.Length)
	}
	if nbh.Delta != MaxUint22 {
		t.Fatalf("Wrong block height, expected %d got %d\n", MaxUint22, nbh.Delta)
	}

	bh.Delta = bh.Delta + 1
	bh.Length = 4
	data, err = bh.MarshalBinary()
	if err != nil {
		t.Fatalf(err.Error())
	}

	nbh = NewHeightDelta()
	nbh.UnmarshalBinary(data)
	if nbh.Length != 4 {
		t.Fatalf("Wrong block height bytes, expected %d got %d\n", 4, nbh.Length)
	}
	if nbh.Delta != MaxUint22+1 {
		t.Fatalf("Wrong block height, expected %d got %d\n", MaxUint22+1, nbh.Delta)
	}

	bh.Delta = MaxUint30
	bh.Length = 4
	data, err = bh.MarshalBinary()
	if err != nil {
		t.Fatalf(err.Error())
	}

	nbh = NewHeightDelta()
	nbh.UnmarshalBinary(data)
	if nbh.Length != 4 {
		t.Fatalf("Wrong block height bytes, expected %d got %d\n", 4, nbh.Length)
	}
	if nbh.Delta != MaxUint30 {
		t.Fatalf("Wrong block height, expected %d got %d\n", MaxUint30, nbh.Delta)
	}

	bh.Delta = bh.Delta + 1
	bh.Length = 5
	data, err = bh.MarshalBinary()
	if err == nil {
		nbh = NewHeightDelta()
		nbh.UnmarshalBinary(data)
		t.Fatalf("Height delta should have exceeded max allowable size %d, got %d", MaxUint30, nbh.Delta)
	}
}

func TestMarshalUnmarshalDeltas(t *testing.T) {
	bhs := NewHeightDeltaArray()
	deltas := make([]uint32, validator.N_MAX_VALIDATORS)
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		delta := uint32(rand.Intn(MaxUint30 + 1))
		deltas[i] = delta
		bhs.setDelta(i, delta)
	}
	data, err := bhs.MarshalBinary()
	if err != nil {
		t.Fatalf("Error marshaling binary: %s", err.Error())
	}
	nbhs := NewHeightDeltaArray()
	err = nbhs.UnmarshalBinary(data)
	if err != nil {
		t.Fatalf("Error unmarshaling binary: %s", err.Error())
	}
	for i := uint16(0); i < validator.N_MAX_VALIDATORS; i++ {
		bh := nbhs.Get(i)
		if bh.Delta != deltas[i] {
			t.Fatalf("Wrong delta after marshal/unmarshal: got %d, expected %d", bh.Delta, deltas[i])
		}
		if bh.Delta <= MaxUint6 {
			if bh.Length != 1 {
				t.Fatalf("Wrong length for delta %d, got %d, expected %d", bh.Delta, bh.Length, 1)
			}
		} else if bh.Delta <= MaxUint14 {
			if bh.Length != 2 {
				t.Fatalf("Wrong length for delta %d, got %d, expected %d", bh.Delta, bh.Length, 2)
			}
		} else if bh.Delta <= MaxUint22 {
			if bh.Length != 3 {
				t.Fatalf("Wrong length for delta %d, got %d, expected %d", bh.Delta, bh.Length, 3)
			}
		} else if bh.Delta <= MaxUint30 {
			if bh.Length != 4 {
				t.Fatalf("Wrong length for delta %d, got %d, expected %d", bh.Delta, bh.Length, 4)
			}
		} else {
			t.Fatalf("Disallowed delta %d, expected <= %d", bh.Delta, MaxUint30)
		}
	}
}
