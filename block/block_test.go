// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package block

import (
	"crypto/ed25519"
	"testing"
	"time"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
)

func TestBytes(t *testing.T) {
	wallet, err := crypto.NewWallet("", crypto.BASIC_WALLET)
	if err != nil {
		t.Fatalf("Couldn't generate key for test: %s", err.Error())
	}
	w := wallet.WalletAddress()

	v := validator.Validator{
		Id:         12345,
		Active:     true,
		RemoteAddr: nil,
		Hostport:   "www.node-op.com:900",
		Conn:       nil,
		Pubkey:     w,
	}

	orig := SilvermintBlock{
		Creator_:       v,
		Parents_:       Heights{},
		Deltas_:        NewHeightDeltaArray(),
		Transactions_:  []transaction.Transaction{},
		Registrations_: []validator.Validator{},
		Timestamp_:     time.Now().UnixMilli(),
	}

	if orig.Creator().Id != v.Id {
		t.Fatalf("Invalid creator: %d != %d", orig.Creator().Id, v.Id)
	}

	// Casanova#177 TODO(leaf): Should be MarshalSignedBinary eventually?
	buf, err := orig.MarshalBinary()
	if err != nil {
		t.Fatalf("Couldn't get bytes for block: %s", err.Error())
	}

	buf2, err := orig.MarshalBinary()
	if err != nil {
		t.Fatalf("Couldn't get bytes for block a second time: %s", err.Error())
	}

	for i := 0; i < len(buf); i++ {
		if buf[i] != buf2[i] {
			t.Fatalf("Marshaled Binary buffers aren't equal for the same block: \n"+
				"   buf1: %s\n   buf2: %s\n", buf, buf2)
		}
	}
}

func TestBytesRoundTrip(t *testing.T) {

	wallet, err := crypto.NewWallet("", crypto.BASIC_WALLET)
	if err != nil {
		t.Fatalf("Couldn't generate key for test: %s", err.Error())
	}

	v := validator.Validator{
		Id:         1234,
		Active:     true,
		RemoteAddr: nil,
		Hostport:   "www.node-op.com:900",
		Conn:       nil,
		Pubkey:     wallet.WalletAddress(),
	}

	validator.SilvermintRegistrations = &validator.Registrations{}
	validator.SilvermintRegistrations.Set(v.Id, v)

	pmt := transaction.Payment{
		Src:          wallet.WalletAddress(),
		CheckNumber_: 123,
		Outputs_: []transaction.PaymentOutput{{
			Amount_: 100,
			Dest_:   wallet.WalletAddress(),
		}},
	}
	var pmtTxn transaction.Transaction
	var itmp interface{}
	itmp = &pmt
	pmtTxn = itmp.(transaction.Transaction)

	orig := SilvermintBlock{
		Creator_: v,
		Parents_: Heights{},
		Deltas_:  NewHeightDeltaArray(),
		Transactions_: []transaction.Transaction{
			pmtTxn,
			pmtTxn,
			pmtTxn,
			pmtTxn,
			pmtTxn,
			pmtTxn,
			pmtTxn,
			pmtTxn,
		},
		Registrations_: []validator.Validator{},
		Timestamp_:     time.Now().UnixMilli(),
	}

	buf, err := orig.MarshalBinary()
	if err != nil {
		t.Fatalf("Couldn't get bytes for Block.\n")
	}

	blk := SilvermintBlock{}
	erx := blk.UnmarshalBinary(buf)
	if erx != nil {
		t.Fatalf("Couldn't unmarshal using BuildFromData: %s", erx.Error())
	}

	if orig.Creator().Id != blk.Creator().Id {
		t.Fatalf("Expected validator %d, got %d.", orig.Creator().Id, blk.Creator().Id)
	}

	if orig.Timestamp_ != blk.Timestamp_ {
		t.Fatalf("Expected timestamp %d, got %d.", orig.Timestamp_, blk.Timestamp_)
	}

	if len(orig.Transactions_) != len(blk.Transactions_) {
		t.Fatalf("Expected %d transaction, got %d.",
			len(orig.Transactions_), len(blk.Transactions_))
	}
}

func TestGenesis(t *testing.T) {
	regs := [validator.N_MAX_VALIDATORS]validator.Validator{}
	blk := Genesis(regs)
	if !IsGenesis(blk) {
		t.Fatalf("Block isn't genesis.")
	}
}

func TestHashIsStableOnTransport(t *testing.T) {
	// Make wallets
	pub1, priv1, err := ed25519.GenerateKey(nil)
	if err != nil {
		t.Fatalf("Unable to generate wallet")
	}

	w1 := crypto.BasicWallet{
		Type_:     crypto.BASIC_WALLET,
		Length_:   crypto.BASIC_WALLET_ADDR_SIZE,
		Filename_: "",
		Public_:   pub1,
		Private_:  priv1,
	}

	pub2, priv2, err := ed25519.GenerateKey(nil)
	if err != nil {
		t.Fatalf("Unable to generate wallet")
	}

	w2 := crypto.BasicWallet{
		Type_:     crypto.BASIC_WALLET,
		Length_:   crypto.BASIC_WALLET_ADDR_SIZE,
		Filename_: "",
		Public_:   pub2,
		Private_:  priv2,
	}

	// Make validator
	val := validator.Validator{
		Id:         1,
		Active:     true,
		RemoteAddr: nil,
		Hostport:   "0.0.0.0:8888",
		Conn:       nil,
		Pubkey:     w1.WalletAddress(),
	}

	// Make mint transaction
	mint := transaction.Mint{
		Src:          w1.WalletAddress(),
		CheckNumber_: 4,
		Output_: transaction.MintOutput{
			Amount_: 123,
			Dest_:   w1.WalletAddress(),
		},
		Bytes: []byte{},
		Sig:   crypto.Signature{},
	}

	mb, err := mint.MarshalSignedBinary(w1)
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for mint txn")
	}

	mint.Bytes = mb
	copy(mint.Sig[:], mb[len(mb)-crypto.SIGNATURE_SIZE:])

	// Make payment transaction
	utxo := transaction.Utxo{
		TxnId: ids.TransactionId{
			BlockId: ids.BlockId{
				ValId_:  123,
				Height_: 4567890,
			},
			Index: 321,
		},
		Amount: 456,
	}

	pmt := transaction.Payment{
		Src:          w1.WalletAddress(),
		CheckNumber_: 5,
		Inputs_:      []transaction.Utxo{utxo},
		Outputs_: []transaction.PaymentOutput{{
			Amount_: 789,
			Dest_:   w2.WalletAddress(),
		},
		},
		Bytes: []byte{},
		Sig:   crypto.Signature{},
	}

	pb, err := pmt.MarshalSignedBinary(w1)
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for payment txn")
	}

	pmt.Bytes = pb
	copy(pmt.Sig[:], pb[len(pb)-crypto.SIGNATURE_SIZE:])

	// Make Genesis block
	regs := [validator.N_MAX_VALIDATORS]validator.Validator{}
	regs[val.Id] = val
	gen := Genesis(regs)

	// Make SilvermintBlock
	b := new(SilvermintBlock)
	b.Creator_ = val
	b.Parents_[val.Id] = gen.Height()
	b.Deltas_ = NewHeightDeltaArray()
	b.Transactions_ = []transaction.Transaction{&mint, &pmt}
	b.Registrations_ = []validator.Validator{val}
	b.Length_ = b.Length()
	b.Validator_ = val.Id
	b.Timestamp_ = time.Now().Unix()

	// Marshal / Unmarshal the block to "send" it
	bb, err := b.MarshalBinary()
	if err != nil {
		t.Fatalf("Unable to marshal signed binary for block")
	}
	b.Data_ = bb
	b.Signature_ = crypto.Signature{}
	copy(b.Signature_[:], bb[len(bb)-crypto.SIGNATURE_SIZE:])

	recvd := new(SilvermintBlock)
	recvd.UnmarshalBinary(b.Data_)

	// Verify block hashes and transaction hashes match
	if len(recvd.Transactions_) != 2 {
		t.Fatalf("Lost transactions during block marshal/unmarshal")
	}

	if recvd.Hash() != b.Hash() {
		t.Fatalf("Received block hash didn't match original")
	}

	if !recvd.Transactions_[0].Hash().Equal(b.Transactions_[0].Hash()) {
		t.Fatalf("Received mint transaction hash didn't match original")
	}

	if !recvd.Transactions_[1].Hash().Equal(b.Transactions_[1].Hash()) {
		t.Fatalf("Received payment transaction hash didn't match original")
	}
}
