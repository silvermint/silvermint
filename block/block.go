// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package block

import (
	"encoding/hex"
	"time"

	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/ids"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"
)

type BlockHash [crypto.HASH_SIZE]byte

func (bh BlockHash) IsEmpty() bool {
	for i := range bh {
		if bh[i] != 0 {
			return false
		}
	}
	return true
}

func (bh BlockHash) MarshalBinary() ([]byte, error) {
	return bh[:], nil
}

type Block interface {
	isBlock()
	Creator() validator.Validator
	Parents() Heights
	Deltas() HeightDeltaArray
	Transactions() []transaction.Transaction
	Registrations() validator.ValidatorSet
	Timestamp() int64
	Time() time.Time
	Length() uint64
	Height() uint64
	Hash() BlockHash
	BlockId() ids.BlockId
	Pubkey() crypto.WalletAddress

	SetParents(Heights)

	MarshalBinary() ([]byte, error)
	MarshalSignedBinary(crypto.Wallet) ([]byte, error)
	UnmarshalBinary([]byte) error
}

func Genesis(vals [validator.N_MAX_VALIDATORS]validator.Validator) Block {
	vSet := validator.ValidatorSet{}
	log.Debug("Casanova initializing with Validator Set:")
	for _, v := range vals {
		if v.IsActive() {
			log.Debugf("Casanova:     %d / %s", v.Id, v.Hostport)
			vSet[v] = true
		}
	}

	blk := new(SilvermintBlock)
	blk.Creator_ = validator.Validator{}
	blk.Parents_ = Heights{}
	blk.Deltas_ = NewHeightDeltaArray()
	blk.Transactions_ = []transaction.Transaction{}
	r := make([]validator.Validator, len(vSet))
	i := 0
	for k := range vSet {
		r[i] = k
		i++
	}
	blk.Height_ = 0
	blk.Registrations_ = r
	blk.Timestamp_ = 0
	return blk
}

func IsGenesis(blk Block) bool {
	return blk.Height() == 0
}

func GetBlockAtHeight(valIndex uint16, height uint16) *SilvermintBlock {
	// Casanova#182 TODO(leaf): We need access to the DAG for this to work, so it's just
	// a stub for now.
	return new(SilvermintBlock)
}

func FromParents(
	creator validator.Validator,
	parents Heights,
	txns []transaction.Transaction,
	tstamp int64,
	registrations map[validator.Validator]bool,
) Block {
	regs := []validator.Validator{}
	for v, b := range registrations {
		if b {
			regs = append(regs, v)
		}
	}
	blk := new(SilvermintBlock)
	blk.Creator_ = creator
	blk.Parents_ = parents
	blk.Height_ = parents[creator.Id] + 1
	blk.Deltas_ = NewHeightDeltaArray()
	blk.Transactions_ = txns
	blk.Registrations_ = regs
	if tstamp == 0 {
		blk.Timestamp_ = time.Now().UnixMilli()
	} else {
		blk.Timestamp_ = tstamp
	}

	return blk
}

func (h *BlockHash) FromHex(data string) error {
	f, err := hex.DecodeString(data)
	if err != nil {
		return err
	}
	for i := range h {
		h[i] = f[i]
	}
	return nil
}

func (h BlockHash) Short() []byte {
	return h[0:3]
}

// Genesis is marked as created by all 2000 validators.
// This is because all validators need to be able to answer the question "what
// was my 0th block?" with Genesis. This function returns a BlockId that will
// correspond to Genesis for the provided Validator ID.
func GenesisId(vid uint16) ids.BlockId {
	return ids.BlockId{
		ValId_:  vid,
		Height_: 0,
	}
}
