package block

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/silvermint/silvermint/validator"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func TestParentsRoundTrip(t *testing.T) {
	parents := Heights{}

	n := 0
	for i := 0; i < validator.N_MAX_VALIDATORS; i++ {
		parents[i] = rand.Uint64()
		n++
	}
	fmt.Printf("Generated %d test parents\n", n)

	pBytes, err := parents.MarshalBinary()
	if err != nil {
		t.Fatalf("Error marshaling parent bytes: %s", err.Error())
	}
	loadedParents := Heights{}
	err = loadedParents.UnmarshalBinary(pBytes)
	if err != nil {
		t.Fatalf("Error unmarshaling parent bytes: %s", err.Error())
	}

	for i := range parents {
		if parents[i] != loadedParents[i] {
			t.Fatalf("Unmarshaled parent didn't match marshaled child: %d != %d for parent %d",
				parents[i], loadedParents[i], i)
		}
	}
}
