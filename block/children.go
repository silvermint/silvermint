// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.
package block

import (
	"encoding/binary"
	"fmt"

	"gitlab.com/silvermint/silvermint/validator"
)

const CHILDREN_FILE_NAME = "children.bin"

type Children [validator.N_MAX_VALIDATORS][]uint64

// We assume that a uint32 is enough to hold the number of children any one block
// may have from any one validator. A uint16 would hold less than one day's worth
// of blocks, while a uint32 could hold 136 years' worth.
func (children Children) Length() uint64 {
	var l uint64 = 8 // Length of byte data
	for vid := 0; vid < validator.N_MAX_VALIDATORS; vid++ {
		if len(children[vid]) != 0 {
			l += 2                              // Validator ID
			l += 4                              // Num Children
			l += uint64(len(children[vid]) * 8) // Child heights
		}
	}

	return l
}

func (children Children) MarshalBinary() ([]byte, error) {
	clen := children.Length()
	data := make([]byte, clen)
	offset := uint64(0)
	binary.LittleEndian.PutUint64(data[offset:offset+8], clen)
	offset += 8

	for vid := uint16(0); vid < validator.N_MAX_VALIDATORS; vid++ {
		if len(children[vid]) != 0 {
			binary.LittleEndian.PutUint16(data[offset:offset+2], vid)
			offset += 2
			binary.LittleEndian.PutUint32(data[offset:offset+4], uint32(len(children[vid])))
			offset += 4
			for _, child := range children[vid] {
				binary.LittleEndian.PutUint64(data[offset:offset+8], child)
				offset += 8
			}
		}
	}

	if offset != clen {
		return nil, fmt.Errorf("Computed children byte length didn't "+
			"match marshaled children byte length: %d != %d", clen, offset)
	}

	return data, nil
}

func (children *Children) UnmarshalBinary(data []byte) error {
	if len(data) == 0 {
		return fmt.Errorf("can't unmarshal empty byte array")
	}
	offset := uint64(0)

	clen := binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8
	if int(clen) > len(data) {
		return fmt.Errorf("Unmarshal failed: children have length %d "+
			"bytes, but we have %d bytes of data.", clen, len(data))
	}

	for offset < clen {
		vid := binary.LittleEndian.Uint16(data[offset : offset+2])
		offset += 2
		nChildren := binary.LittleEndian.Uint32(data[offset : offset+4])
		offset += 4
		children[vid] = make([]uint64, nChildren)
		for i := uint32(0); i < nChildren; i++ {
			children[vid][i] = binary.LittleEndian.Uint64(data[offset : offset+8])
			offset += 8
		}
	}

	if offset != clen {
		return fmt.Errorf("Computed parent byte length didn't "+
			"match unmarshaled parent byte length: %d != %d", clen, offset)
	}

	return nil
}

func (children Children) Save(blk Block) error {
	// TODO(chuck): Turning this off for now, as we don't use children anywhere
	// and we're trying to move to a new disk I/O paradigm.
	// return utils.Save(&children, blk.Folder(), CHILDREN_FILE_NAME)
	return nil
}
