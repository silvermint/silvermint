package ids

import (
	"encoding/binary"
	"fmt"
)

type BlockId struct {
	fmt.Stringer
	ValId_  uint16
	Height_ uint64
}

func BlockIdLength() uint64 {
	var l uint64 = 2 // Validator ID
	l += 8           // Height
	return l
}

func (bid BlockId) String() string {
	return fmt.Sprintf("[vid=%d, ht=%d]", bid.ValId_, bid.Height_)
}

func (bid BlockId) ValId() uint16   { return bid.ValId_ }
func (bid BlockId) Height() uint64  { return bid.Height_ }
func (bid BlockId) IsGenesis() bool { return bid.Height_ == 0 }
func (bid BlockId) Next() BlockId {
	bid.Height_++
	return bid
}
func (bid BlockId) Prev() BlockId {
	if bid.Height_ == 0 {
		return bid
	}
	bid.Height_--
	return bid
}

// IntervalBlockId(BlockId) takes two BlockIds, one greater and one lesser, and
// returns a list of BlockIds that span the height difference between them.
// Requires that greater.Height >= lesser.Height`. The list of BlockIds returned
// includes the endpoints on both sides.
// Returned list uses the greater BlockId's validator ID, in case the lesser
// one was Genesis.
func (greater BlockId) IntervalBlockId(lesser BlockId) ([]BlockId, error) {
	if greater.Height_ < lesser.Height_ {
		return nil, fmt.Errorf("can't subtract greater height from lesser"+
			"height: %d > %d", lesser.Height_, greater.Height_)
	}
	bids := make([]BlockId, 0, greater.Height_-lesser.Height_)
	for ht := lesser.Height_; ht <= greater.Height_; ht++ {
		bids = append(bids, BlockId{
			ValId_:  greater.ValId_,
			Height_: ht,
		})
	}
	return bids, nil
}

func (bid BlockId) MarshalBinary() ([]byte, error) {
	cdlen := BlockIdLength()
	data := make([]byte, cdlen)
	var offset uint64 = 0

	// Validator ID
	binary.LittleEndian.PutUint16(data[offset:offset+2], bid.ValId_)
	offset += 2

	// Height
	binary.LittleEndian.PutUint64(data[offset:offset+8], bid.Height_)
	offset += 8

	if offset != cdlen {
		return nil, fmt.Errorf("ERROR: Offset didn't match computed BlockId length, %d != %d",
			offset, cdlen)
	}

	return data, nil
}

func UnmarshalBlockId(data []byte) (BlockId, error) {
	bid := BlockId{}
	blen := BlockIdLength()
	if int(blen) != len(data) {
		return bid, fmt.Errorf("BlockId length didn't match computed: %d != %d",
			len(data), blen)
	}
	offset := 0
	bid.ValId_ = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2
	bid.Height_ = binary.LittleEndian.Uint64(data[offset : offset+8])
	offset += 8

	return bid, nil
}
