package ids

import (
	"encoding/binary"
	"fmt"
)

type TransactionId struct {
	fmt.Stringer
	BlockId BlockId
	Index   uint16
}

func TransactionIdLength() uint64 {
	return BlockIdLength() + 2
}

func (tid TransactionId) String() string {
	return fmt.Sprintf("[vid=%d, ht=%d, ind=%d]",
		tid.BlockId.ValId_, tid.BlockId.Height_, tid.Index)
}

func (tid TransactionId) MarshalBinary() ([]byte, error) {
	data := make([]byte, TransactionIdLength())
	var offset uint64 = 0

	blen := BlockIdLength()
	bdat, err := tid.BlockId.MarshalBinary()
	if err != nil {
		return nil, err
	}
	copy(data[offset:offset+blen], bdat)
	offset += blen
	binary.LittleEndian.PutUint16(data[offset:offset+2], tid.Index)
	offset += 2

	if offset != uint64(len(data)) {
		return nil, fmt.Errorf("TransactionId length didn't match computed: %d != %d",
			len(data), offset)
	}
	return data, nil
}

func UnmarshalTransactionId(data []byte) (TransactionId, error) {
	tid := TransactionId{}
	tlen := TransactionIdLength()
	if int(tlen) != len(data) {
		return tid, fmt.Errorf("TransactionId length didn't match computed: %d != %d",
			len(data), tlen)
	}
	offset := 0
	var err error
	blen := BlockIdLength()
	tid.BlockId, err = UnmarshalBlockId(data[offset : offset+int(blen)])
	if err != nil {
		return tid, err
	}
	offset += int(blen)
	tid.Index = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	return tid, nil
}
