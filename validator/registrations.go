package validator

import "sync"

var SilvermintRegistrations *Registrations

func init() {
	SilvermintRegistrations = &Registrations{}
}

type Registrations struct {
	vals [N_MAX_VALIDATORS]Validator
	mu   sync.RWMutex
}

func GetGlobalRegistrations() *Registrations {
	return SilvermintRegistrations
}

func (regs *Registrations) Get(vid uint16) Validator {
	regs.mu.RLock()
	defer regs.mu.RUnlock()

	return regs.vals[vid]
}

func (regs *Registrations) Set(vid uint16, val Validator) {
	regs.mu.Lock()
	defer regs.mu.Unlock()

	regs.vals[vid] = val
}

func (regs *Registrations) Active() ValidatorSet {
	regs.mu.RLock()
	defer regs.mu.RUnlock()

	vset := ValidatorSet{}
	for i := uint16(0); i < N_MAX_VALIDATORS; i++ {
		if regs.vals[i].IsActive() {
			vset[regs.vals[i]] = true
		}
	}
	return vset
}

func (regs *Registrations) Validators() [N_MAX_VALIDATORS]Validator {
	regs.mu.RLock()
	defer regs.mu.RUnlock()

	return regs.vals
}

func (regs *Registrations) rangeInternal(f func(uint16, Validator) bool) {
	for vid, val := range regs.vals {
		if !f(uint16(vid), val) {
			return
		}
	}
}

func (regs *Registrations) RangeRead(f func(uint16, Validator) bool) {
	regs.mu.RLock()
	defer regs.mu.RUnlock()

	regs.rangeInternal(f)
}
