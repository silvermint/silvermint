// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package validator

import (
	"encoding/binary"
	"fmt"
	"net"

	"gitlab.com/silvermint/silvermint/crypto"
)

const (
	N_MAX_VALIDATORS = 2000
)

type ValidatorSet = map[Validator]bool

type Validator struct {
	Id         uint16
	Active     bool
	RemoteAddr *net.TCPAddr
	Hostport   string
	Conn       *net.TCPConn
	Pubkey     crypto.WalletAddress
}

func (val Validator) IsActive() bool {
	return val.Active
}

func (val Validator) Length() uint16 {
	var l uint16 = 2                       // Length
	l += 2                                 // Id
	l += 1                                 // Active bool
	l += 2                                 // Len Hostport
	l += uint16(len([]byte(val.Hostport))) // Hostport
	l += val.Pubkey.Length()               // Pubkey
	return l
}

func (val Validator) MarshalBinary() ([]byte, error) {
	vlen := val.Length()
	data := make([]byte, vlen)
	var offset uint16 = 0

	// Length
	binary.LittleEndian.PutUint16(data[offset:offset+2], vlen)
	offset += 2

	// Id
	binary.LittleEndian.PutUint16(data[offset:offset+2], val.Id)
	offset += 2

	// Active bool
	var isActive uint8 = 0
	if val.Active {
		isActive = 1
	}
	data[offset] = isActive
	offset += 1

	// Len Hostport
	hostBytes := []byte(val.Hostport)
	lenHostBytes := uint16(len(hostBytes))
	binary.LittleEndian.PutUint16(data[offset:offset+2], lenHostBytes)
	offset += 2

	// Hostport
	copy(data[offset:offset+lenHostBytes], hostBytes)
	offset += lenHostBytes

	// Pubkey
	wlen := val.Pubkey.Length()
	copy(data[offset:offset+wlen], val.Pubkey.Address())
	offset += wlen

	if offset != vlen {
		return nil, fmt.Errorf("ERROR: Offset didn't match computed Validator length, %d != %d",
			offset, vlen)
	}
	return data, nil
}

func (val *Validator) UnmarshalBinary(data []byte) error {
	var offset uint16 = 0

	// Length
	vlen := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	if int(vlen) != len(data) {
		return fmt.Errorf("ERROR: Validator has length %d bytes, "+
			"but we have %d bytes of data", vlen, len(data))
	}

	// Id
	val.Id = binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Active bool
	val.Active = false
	if uint8(data[offset]) == 1 {
		val.Active = true
	}
	offset += 1

	// Len Hostport
	lenHostBytes := binary.LittleEndian.Uint16(data[offset : offset+2])
	offset += 2

	// Hostport
	val.Hostport = string(data[offset : offset+lenHostBytes])
	offset += lenHostBytes

	// Pubkey
	wlen := binary.LittleEndian.Uint16(data[offset+2 : offset+4])
	val.Pubkey = crypto.NewWalletAddress(data[offset : offset+wlen])
	offset += wlen

	if offset != vlen {
		return fmt.Errorf("ERROR: Offset didn't match given Validator length, %d != %d",
			offset, vlen)
	}
	return nil
}
