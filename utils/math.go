package utils

import "golang.org/x/exp/constraints"

// Returns the maximum of two args. If they're equal returns the second one.
func Max[T constraints.Ordered](l, r T) T {
	if l > r {
		return l
	}
	return r
}

// Returns the minimum of two args. If they're equal returns the second one.
func Min[T constraints.Ordered](l, r T) T {
	if l < r {
		return l
	}
	return r
}
