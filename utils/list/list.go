// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

// The list package implements a doubly-linked list of a type parameterized on any
// go value. The list should work even for go values that are not comparable, because
// the internals use pointers regardless of what U is.
//
// TODO(leaf): Possible future functionality includes sorting, searching, mapping,
// reducing, filtering, and other functional primitives that can be performed on a
// list.
package list

import (
	"fmt"
	"sync"
)

// An Element is a generic structure containing a value. It can only live in a
// single list at a time. Only the Value member is visible, the internal list
// structure has the invariant that for any var e Element[U], the following predicate
// holds:
//
//	(e.list != nil) => (e.next.list == e.prev.list == e.list)
//
// If you want the same value to be in multiple lists, then you should follow this rule:
//   - If you want changes to e.Value to be visible in both lists, U should be a pointer.
//   - If you don't want change visibility, use a literal.
//   - TODO(leaf): I'm not sure what happens if U is an interface.
type Element[U any] struct {
	next  *Element[U]
	prev  *Element[U]
	list  *List[U]
	Value U
}

func (e *Element[U]) Next() *Element[U] { return e.next }
func (e *Element[U]) Prev() *Element[U] { return e.prev }

// A List is a doubly linked list structure. If maintains the following invariants:
//   - len is equal to the number of items in the list.
//   - If len == 1, then head == tail != nil.
//   - If len == 0, then head == tail == nil.
type List[U any] struct {
	head   *Element[U]
	tail   *Element[U]
	length int
	lock   sync.RWMutex
}

// NewList returns an empty list.
func NewList[U any]() *List[U] {
	n := new(List[U])
	n.lock = *new(sync.RWMutex)
	return n
}

// NewListFromSlice returns a list whose elements are derived from the "in"
// argument.  The elements appear in the same order as in the input. If U is
// not a pointer or interface type, then the results will be a copy of the
// slice. Otherwise the Elements of the list will refer to the objects in the
// slice and may modify them.
func NewListFromSlice[U any](in []U) *List[U] {
	n := NewList[U]()
	for _, u := range in {
		n.PushBack(u)
	}
	return n
}

// Len returns the length of the List.
func (l *List[U]) Len() int {
	l.lock.RLock()
	defer l.lock.RUnlock()

	return l.length
}

// Back return the last element.
func (l *List[U]) Back() *Element[U] {
	l.lock.RLock()
	defer l.lock.RUnlock()

	return l.tail
}

// Tail is an alias for Back().
func (l *List[U]) Tail() *Element[U] { return l.Back() }

// Front return the first element.
func (l *List[U]) Front() *Element[U] {
	l.lock.RLock()
	defer l.lock.RUnlock()

	return l.head
}

// Tail is an alias for Front().
func (l *List[U]) Head() *Element[U] { return l.Front() }

// Clear deletes all the elements in the list.
func (l *List[U]) Clear() {
	l.lock.Lock()
	defer l.lock.Unlock()

	l.head = nil
	l.tail = nil
	l.length = 0
}

// Inserts a new Element with value "v" after the element "mark."
func (l *List[U]) InsertAfter(v U, mark *Element[U]) *Element[U] {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.insertAfter_(v, mark)
}

// This is an internal, unexported function that does not lock. The caller
// must hold the write lock already.
func (l *List[U]) insertAfter_(v U, mark *Element[U]) *Element[U] {
	if mark == nil || mark.list != l {
		return nil
	}

	next := mark.next

	elem := &Element[U]{
		next:  next,
		prev:  mark,
		list:  l,
		Value: v,
	}

	mark.next = elem
	if next != nil {
		next.prev = elem
	} else {
		l.tail = elem
	}

	l.length += 1
	return elem
}

// InsertBefore inserts a new Element[U] with value "v" before the element "mark."
func (l *List[U]) InsertBefore(v U, mark *Element[U]) *Element[U] {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.insertBefore_(v, mark)
}

// This is an internal, unexported function that does not lock. The caller
// must hold the write lock already.
func (l *List[U]) insertBefore_(v U, mark *Element[U]) *Element[U] {
	if mark.list != l || mark == nil {
		return nil
	}

	prev := mark.prev

	elem := &Element[U]{
		next:  mark,
		prev:  prev,
		list:  l,
		Value: v,
	}

	mark.prev = elem
	if prev != nil {
		prev.next = elem
	} else {
		l.head = elem
	}

	l.length += 1

	return elem
}

// MoveAfter moves an Element[U] from its current position to directly after
// "mark." A new Element[U] may be constructed and a pointer to it will be
// returned. Callers should be sure to use the new pointer rather than the
// original one.
func (l *List[U]) MoveAfter(e, mark *Element[U]) *Element[U] {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.moveAfter_(e, mark)
}

// This is an internal, unexported function that does not lock. The caller
// must hold the write lock already.
func (l *List[U]) moveAfter_(e, mark *Element[U]) *Element[U] {
	if e.list != mark.list || mark.list != l || e.list != l {
		return nil
	}

	// e is the only element.
	if l.length == 1 {
		return nil
	}
	// detach e.
	uptr := l.remove_(e)
	if uptr == nil {
		return nil
	}

	elem := l.insertAfter_(*uptr, mark)
	return elem
}

// MoveBefore moves an Element[U] from its current position to directly before
// "mark." A new Element[U] may be constructed and a pointer to it will be
// returned. Callers should be sure to use the new pointer rather than the
// original one.
func (l *List[U]) MoveBefore(e, mark *Element[U]) *Element[U] {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.moveBefore_(e, mark)
}

// This is an internal, unexported function that does not lock. The caller
// must hold the write lock already.
func (l *List[U]) moveBefore_(e, mark *Element[U]) *Element[U] {
	if e.list != mark.list || mark.list != l || e.list != l {
		return nil
	}

	// e is the only element
	if l.length == 1 {
		return nil
	}

	// Remove the element.
	uptr := l.remove_(e)
	if uptr == nil {
		return nil
	}
	elem := l.insertBefore_(*uptr, mark)
	return elem
}

// MoveToBack is conceptually equivalent to `list.MoveAfter(list.Back())`, except
// that MoveToBack is thread safe.
func (l *List[U]) MoveToBack(e *Element[U]) *Element[U] {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.moveAfter_(e, l.tail)
}

// MoveToFront is conceptually equivalent to `list.MoveBefore(list.Front())`,
// except that MoveToFront is thread safe.
func (l *List[U]) MoveToFront(e *Element[U]) *Element[U] {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.moveBefore_(e, l.head)
}

// PushBack emplaces an element at the end of the queue, making it the new tail.
func (l *List[U]) PushBack(v U) *Element[U] {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.pushBack_(v)
}

// This is an internal, unexported function that does not lock. The caller
// must hold the write lock already.
func (l *List[U]) pushBack_(v U) *Element[U] {
	old_tail := l.tail

	elem := &Element[U]{
		next:  nil,
		prev:  old_tail,
		list:  l,
		Value: v,
	}

	l.tail = elem
	l.length += 1

	if old_tail != nil {
		old_tail.next = elem
	} else {
		l.head = elem // Because invariant says both are nil or neither are.
	}

	return nil
}

// PushBackList is a thread safe way to copy a list onto the end of an existing
// list. New Element[U]s will be created, but as always if their Value is a
// pointer, then modifications to the original list(s) maybe visible in both
// places.
func (l *List[U]) PushBackList(other *List[U]) {
	l.lock.Lock()
	other.lock.Lock()
	defer l.lock.Unlock()
	defer other.lock.Unlock()

	if other == nil {
		return
	}
	for curr := other.head; curr != nil; curr = curr.next {
		l.pushBack_(curr.Value)
	}
}

// PushFront emplaces an element at the beginning of the queue, making it the
// new head.
func (l *List[U]) PushFront(v U) *Element[U] {
	l.lock.Lock()
	defer l.lock.Unlock()
	return l.pushFront_(v)
}

// This is an internal, unexported function that does not lock. The caller
// must hold the write lock already.
func (l *List[U]) pushFront_(v U) *Element[U] {
	old_head := l.head

	elem := &Element[U]{
		next:  l.head,
		prev:  nil,
		list:  l,
		Value: v,
	}
	l.head = elem
	l.length += 1

	if old_head != nil {
		old_head.prev = elem
	} else {
		l.tail = elem // Because invariant says both are nil or neither are.
	}

	return elem
}

// PushFrontList is a thread safe way to copy a list onto the front of an
// existing list. New Element[U]s will be created, but as always if their Value
// is a pointer, then modifications to the original list(s) maybe visible in
// both places.
func (l *List[U]) PushFrontList(other *List[U]) {
	l.lock.Lock()
	other.lock.Lock()
	defer l.lock.Unlock()
	defer other.lock.Unlock()

	if other == nil {
		return
	}
	for curr := other.tail; curr != nil; curr = curr.prev {
		l.pushFront_(curr.Value)
	}
}

// Remove an Element from a list.
func (l *List[U]) Remove(e *Element[U]) *U {
	l.lock.Lock()
	defer l.lock.Unlock()

	return l.remove_(e)
}

// RemoveAt is a thread-safe way to remove an element at a fixed index.
func (l *List[U]) RemoveAt(i int) *U {
	l.lock.Lock()
	defer l.lock.Unlock()

	elem := l.elementAt_(i)
	if elem == nil {
		return nil
	}
	return l.remove_(elem)
}

// This is an internal, unexported function that does not lock. The caller
// must hold the write lock already.
func (l *List[U]) remove_(e *Element[U]) *U {
	for curr := l.head; curr != nil; curr = curr.next {
		if e == curr {
			prev := curr.prev
			next := curr.next

			if prev == nil && next == nil {
				l.head = nil
				l.tail = nil
				l.length = 0
			} else if curr == l.head {
				next.prev = nil
				l.head = next
			} else if curr == l.tail {
				prev.next = nil
				l.tail = prev
			} else {
				prev.next = next
				next.prev = prev
			}

			val := &curr.Value
			l.length -= 1
			return val
		}
	}

	// Not in the list.
	return nil
}

// Get the Element[U] that exists at a particular index. If the index
// is invalid, then the result will be nil.
func (l *List[U]) ElementAt(i int) *Element[U] {
	l.lock.RLock()
	defer l.lock.RUnlock()
	return l.elementAt_(i)
}

// This is an internal, unexported function that does not lock. The caller
// must hold the write lock already.
func (l *List[U]) elementAt_(i int) *Element[U] {
	curr := l.head
	for j := 0; j < i; j++ {
		if curr == nil {
			return nil
		}
		curr = curr.next
	}
	return curr
}

// HasLoop uses Floyd's Cycle Detection Algorithm to detect with the list has
// loops. This is primarily useful in testing, but it's export so that callers
// can check the invariant for themselves. None of the functions callable on a
// List should induce a loop, but you never can be too careful.
func (l *List[U]) HasLoop() bool {
	l.lock.RLock()
	defer l.lock.RUnlock()

	tortoise := l.head
	hare := l.head
	for {
		if hare == nil || hare.next == nil {
			return false
		}

		hare = hare.next
		if hare.next == nil {
			return false
		}

		hare = hare.next
		tortoise = tortoise.next
		if hare == tortoise {
			return true
		}
	}
}

// String gets a string representation of the List. Output will include the
// address of the list and the comma-separate string values of each
// Element[U]'s Value in the order they appear in the list.
func (l *List[U]) String() string {
	l.lock.RLock()
	defer l.lock.RUnlock()
	return l.string_()
}

// This is an internal, unexported function that does not lock. The caller
// must hold the write lock already.
func (l *List[U]) string_() string {
	var str string
	for curr := l.head; curr != nil; curr = curr.next {
		str += fmt.Sprintf("%v", curr.Value)
		if curr.next != nil {
			str += ", "
		}
	}
	return fmt.Sprintf("{%p}[%s]", l, str)
}
