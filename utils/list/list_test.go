// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package list

import (
	"fmt"
	"testing"
)

func expectList(t *testing.T, list *List[int], expected []int) {
	if list.HasLoop() {
		t.Fatalf("Invalid list: contains a loop.")
	}

	for i := 0; i < list.Len(); i++ {
		ith := list.ElementAt(i)
		if ith == nil {
			t.Fatalf("Element not found at index %d.", i)
		}
		if expected[i] != ith.Value {
			fmt.Printf("Invalid values, list is: %s\n", list.String())
			t.Fatalf("Invalid values: expected %v, got %v", expected[i], ith.Value)
		}
	}
}

func TestNewGL(t *testing.T) {
	list := NewList[int]()
	if list.head != nil || list.tail != nil || list.length != 0 {
		t.Fatalf("Invalid state for newly created list.")
	}
}

func TestPushFront(t *testing.T) {
	list := NewList[int]()
	total := 1000
	for i := 0; i < total; i++ {

		list.PushFront(i)
		//fmt.Printf("Pushed %d. List length is %d: %v\n", i, list.Len(), list)

		if list.head.next == list.head {
			t.Fatalf("PushFront failed: head.next equals head.")
		}

		if list.head == nil || list.tail == nil {
			t.Fatalf("PushFront failed: neither head nor tail can be nil, they were: %p and %p.",
				list.head, list.tail)
		}

		if list.head.prev != nil {
			t.Fatalf("PushFront failed: head.prev == %p, expected nil.", list.head.prev)
		}

		if list.head == nil {
			t.Fatalf("PushFront failed: somehow head is nil.")
		}

		if list.tail == nil {
			t.Fatalf("PushFront failed: somehow tail is nil.")
		}

		elem := list.ElementAt(i)
		if elem == nil {
			t.Fatalf("PushFront failed for element %d: ElementAt(%d) == nil.", i, i)
		}

		if elem.Value != 0 {
			t.Fatalf("Pushed %d elements, expected %d-th value to be 0, but got %d.", i, i, elem.Value)
		}

		elem = list.ElementAt(0)
		if elem == nil {
			t.Fatalf("PushFront failed for element %d: ElementAt(0) == nil.", i)
		}

		if elem.Value != i {
			t.Fatalf("Pushed %d elements, expected 0th element's value to be %d, but got %d.", i, i, elem.Value)
		}
	}

	if list.Len() != total {
		t.Fatalf("Inserted %d elements, but list length is %d.", total, list.Len())
	}
}

func TestPushBack(t *testing.T) {
	list := NewList[int]()
	total := 1000
	for i := 0; i < total; i++ {

		list.PushBack(i)
		//fmt.Printf("Pushed %d. List length is %d: %v\n", i, list.Len(), list)

		if list.tail.prev == list.tail {
			t.Fatalf("PushBack failed: prev.next equals prev.")
		}

		if list.head == nil || list.tail == nil {
			t.Fatalf("PushBack failed: neither head nor tail can be nil, they were: %p and %p.",
				list.head, list.tail)
		}

		if list.tail.next != nil {
			t.Fatalf("PushBack failed: tail.next == %p, expected nil.", list.tail.next)
		}

		if list.head == nil {
			t.Fatalf("PushBack failed: somehow head is nil.")
		}

		if list.tail == nil {
			t.Fatalf("PushBack failed: somehow tail is nil.")
		}

		elem := list.ElementAt(i)
		if elem == nil {
			t.Fatalf("PushBack failed for element %d: ElementAt(%d) == nil.", i, i)
		}

		if elem.Value != i {
			t.Fatalf("Pushed %d elements, expected %d-th value to be 0, but got %d.", i, i, elem.Value)
		}

		elem = list.ElementAt(0)
		if elem == nil {
			t.Fatalf("PushBack failed for element %d: ElementAt(0) == nil.", i)
		}

		if elem.Value != 0 {
			t.Fatalf("Pushed %d elements, expected 0th element's value to be %d, but got %d.", i, 0, elem.Value)
		}
	}

	if list.Len() != total {
		t.Fatalf("Inserted %d elements, but list length is %d.", total, list.Len())
	}
}

func TestPushBackList(t *testing.T) {
	list := NewList[int]()
	list2 := NewList[int]()

	total := 5
	total2 := 7

	for i := 0; i < total; i++ {
		list.PushBack(i)
	}

	if list.Len() != total {
		t.Fatalf("Incorrect list length after PushBack.")
	}

	for j := total; j < total+total2; j++ {
		list2.PushBack(j)
	}

	list.PushBackList(list2)

	if list.Len() != total+total2 {
		t.Fatalf("Incorrect list length after PushBackList: expected %d, got %d.",
			total+total2, list.Len())
	}
}

func TestPushFrontList(t *testing.T) {
	list := NewList[int]()
	list2 := NewList[int]()

	total := 5
	total2 := 7

	for i := 0; i < total; i++ {
		list.PushFront(i)
	}

	if list.Len() != total {
		t.Fatalf("Incorrect list length after PushFront.")
	}

	for j := total; j < total+total2; j++ {
		list2.PushFront(j)
	}

	list.PushFrontList(list2)

	if list.Len() != total+total2 {
		t.Fatalf("Incorrect list length after PushFrontList: expected %d, got %d.",
			total+total2, list.Len())
	}
}

func TestInsertAfter(t *testing.T) {
	list := NewListFromSlice[int]([]int{1, 2, 3, 4})

	list.InsertAfter(5, list.ElementAt(0))
	expectList(t, list, []int{1, 5, 2, 3, 4})

	list.InsertAfter(6, list.Back())
	expectList(t, list, []int{1, 5, 2, 3, 4, 6})

	list.InsertAfter(7, list.Front())
	expectList(t, list, []int{1, 7, 5, 2, 3, 4, 6})

	list.InsertAfter(8, list.ElementAt(4))
	expectList(t, list, []int{1, 7, 5, 2, 3, 8, 4, 6})
}

func TestRemove(t *testing.T) {
	list := NewListFromSlice[int]([]int{0, 1, 2})

	total := 3

	elem := list.ElementAt(1)
	if elem == nil {
		t.Fatalf("Couldn't get ElementAt(1).")
	}

	removed := list.Remove(elem)
	if removed == nil {
		t.Fatalf("Element at index 1 not found by remove.")
	} else if *removed != 1 {
		t.Fatalf("Expected removed element to have value 1.")
	} else if elem.Value != *removed {
		t.Fatalf("ElementAt(1) != Remove(ElementAt(1)).")
	}

	if list.Len() != total-1 {
		t.Fatalf("Removed an element, but list length didn't change.")
	}

	expectList(t, list, []int{0, 2})
}

func TestMoveAfter(t *testing.T) {
	list := NewListFromSlice[int]([]int{0, 1, 2, 3, 4})

	//fmt.Printf("TestMoveAfter: list = %s\n", list.String())

	elem := list.ElementAt(2)
	if elem == nil {
		t.Fatalf("Couldn't get ElementAt(2).")
	} else if elem.Value != 2 {
		t.Fatalf("ElementAt(2) != 2.")
	}

	mark := list.ElementAt(4)
	if mark == nil {
		t.Fatalf("Couldn't get ElementAt(4).")
	} else if mark.Value != 4 {
		t.Fatalf("ElementAt(4) != 4")
	}

	newelem := list.MoveAfter(elem, mark)
	//fmt.Printf("TestMoveAfter: list = %s\n", list.String())
	if newelem == nil {
		t.Fatalf("MoveAfter failed.")
	}

	expectList(t, list, []int{0, 1, 3, 4, 2})
}

func TestMoveBefore(t *testing.T) {
	list := NewList[int]()

	total := 5

	for i := 0; i < total; i++ {
		list.PushBack(i)
	}

	elem := list.ElementAt(2)
	if elem == nil {
		t.Fatalf("Couldn't get ElementAt(2).")
	}
	mark := list.ElementAt(4)
	if mark == nil {
		t.Fatalf("Couldn't get ElementAt(4).")
	}

	newelem := list.MoveBefore(elem, mark)
	if newelem == nil {
		t.Fatalf("MoveBefore failed.")
	}
	expectList(t, list, []int{0, 1, 3, 2, 4})
}

func TestMoveToBack(t *testing.T) {
	list := NewListFromSlice[int]([]int{0, 1, 2, 3, 4})
	elem := list.Front()
	newelem := list.MoveToBack(elem)
	if newelem == nil {
		t.Fatalf("MoveAfter failed.")
	}
	expectList(t, list, []int{1, 2, 3, 4, 0})

	elem = list.ElementAt(3)
	list.MoveToBack(elem)
	expectList(t, list, []int{1, 2, 3, 0, 4})
}

func TestMoveToFront(t *testing.T) {
	list := NewListFromSlice[int]([]int{0, 1, 2, 3, 4})
	elem := list.Back()
	newelem := list.MoveToFront(elem)
	if newelem == nil {
		t.Fatalf("MoveAfter failed.")
	}
	expectList(t, list, []int{4, 0, 1, 2, 3})
}

func TestMoveToFront2(t *testing.T) {
	list := NewListFromSlice[int]([]int{0, 1, 2, 3, 4})
	elem := list.ElementAt(3)
	newelem := list.MoveToFront(elem)
	if newelem == nil {
		t.Fatalf("MoveAfter failed.")
	}
	expectList(t, list, []int{3, 0, 1, 2, 4})

	elem = list.ElementAt(3)
	list.MoveToFront(elem)
	expectList(t, list, []int{2, 3, 0, 1, 4})
}
