package utils

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"gitlab.com/silvermint/silvermint/log"

	"github.com/spf13/viper"
)

// Types that are able to marshal their data to binary are Writeable,
// allowing them to be written to disk using the functions here.
type Writeable interface {
	MarshalBinary() ([]byte, error)
}

// Saves the Writeable object to the file specified.
// Does nothing if the file already exists.
func Save(w Writeable, folder, fname string) error {
	// Save the object to the specified file
	err := save(w, folder, fname, os.O_RDWR|os.O_CREATE|os.O_EXCL)
	if err != nil {
		return err
	}

	return nil
}

// Saves the Writeable object to the file specified.
// Appends to any existing file.
func Append(w Writeable, folder, fname string) error {
	// Save the object to the specified file
	err := save(w, folder, fname, os.O_RDWR|os.O_CREATE|os.O_APPEND)
	if err != nil {
		return err
	}

	return nil
}

// Saves the Writeable object to the file specified.
// Overwrites any existing file.
// TODO(chuck): This should change the original file to .old, write to a new
// file, and then only delete the .old file once the write to the new file is
// complete. That way we don't corrupt the file if a crash happens during the
// write.
func Overwrite(w Writeable, folder, fname string) error {
	// Save the object to the specified file
	err := save(w, folder, fname, os.O_RDWR|os.O_CREATE|os.O_TRUNC)
	if err != nil {
		return err
	}

	return nil
}

// This takes a Writeable object, marshals it to binary, and saves that data
// to save_dir/folder/fname. If no fname is provided, it saves it to a snapshot
// file whose name encodes the current time.
// Accepts option to overwrite or do nothing if the file already exists.
func save(w Writeable, folder string, fname string, flag int) error {
	silvermint_save_dir := saveDir(folder)
	err := MaybeCreateDir(silvermint_save_dir, 0755)
	if err != nil {
		return err
	}

	// If no filename was provided, form a filename using the current time
	if fname == "" {
		now := time.Now()
		now_format := strings.Split(now.Format("01-02-2006 15.04.05"), " ")
		fname = "snapshot_" + now_format[0] + "_" + now_format[1] + ".bin"
	}
	path := filepath.Join(silvermint_save_dir, fname)

	// Special case: don't return an error for the case where we
	// we aren't saving because the file already exists.
	if (flag & os.O_EXCL) == os.O_EXCL {
		if _, err = os.Stat(path); err == nil {
			// File exists
			return nil
		}
	}

	// Open the file using the specified flags
	fd, err := os.OpenFile(path, flag, 0755)
	if err != nil {
		return err
	}
	defer fd.Close()

	// Marshal object to binary
	data, err := w.MarshalBinary()
	if err != nil {
		return err
	}

	// Write to the file
	_, err = fd.Write(data)
	if err != nil {
		return err
	}

	return nil
}

// Returns a string combining the silvermint save_dir with the provided folder
// as save_dir/folder
func saveDir(folder string) string {
	save_dir := viper.GetString("save_dir")
	if save_dir == "" {
		log.Fatalf("FATAL: Couldn't find directory to save to")
	}

	return filepath.Join(save_dir, folder)
}

// Types that are able to unmarshal their data from binary are Loadable,
// allowing them to be loaded from disk using the functions here.
type Loadable interface {
	UnmarshalBinary([]byte) error
}

// This loads the data found in save_dir/folder/fname (or the most recently
// modified file in save_dir/folder if no fname is provided) and unmarshals
// it to the provided Loadable object.
func Load(l Loadable, folder, fname string) error {
	silvermint_save_dir := saveDir(folder)

	// If no file name was provided, try all save files starting with
	// the most recent. Otherwise attempt to load the indicated file.
	if fname == "" {
		// Get all save files in the directory
		files, err := os.ReadDir(silvermint_save_dir)
		if err != nil {
			return err
		}
		// Sort the files based on the time they were last modified
		sort.Slice(files, func(i, j int) bool {
			finfo_i, _ := files[i].Info()
			finfo_j, _ := files[j].Info()
			return finfo_i.ModTime().After(finfo_j.ModTime())
		})
		// Attempt to load and unmarshal each file starting with the most recent
		success := false
		for _, file := range files {
			fpath := filepath.Join(silvermint_save_dir, file.Name())
			fbytes, err := os.ReadFile(fpath)
			if err != nil {
				log.Warningf("Unable to read file at %s: %s",
					fpath, err.Error())
				continue
			}
			log.Infof("Checking File : %s", fpath)
			err = l.UnmarshalBinary(fbytes)
			if err != nil {
				log.Warningf("Unable to unmarshal data from file %s: %s",
					fpath, err.Error())
				continue
			}
			// Only try to load/unmarshal an older file if there was some problem
			// doing so with the most recent. Otherwise, break.
			success = true
			break
		}
		if !success {
			return fmt.Errorf("Failed to unmarshal after trying %d files", len(files))
		}
	} else {
		fpath := filepath.Join(silvermint_save_dir, fname)
		fbytes, err := os.ReadFile(fpath)
		if err != nil {
			return fmt.Errorf("Unable to read file at %s: %s",
				fpath, err.Error())
		}
		log.Infof("Checking File : %s", fpath)
		err = l.UnmarshalBinary(fbytes)
		if err != nil {
			return fmt.Errorf("Unable to unmarshal data from file %s: %s",
				fpath, err.Error())
		}
	}
	return nil
}

// Loads saved bytes from the silvermint save_dir/folder.
// If a filename is provided, this gets the data in save_dir/folder/fname.
// If an empty filename is provided, this gets the data from the most recently
// modified file found in save_dir/folder.
func LoadBytes(folder, fname string) ([]byte, error) {
	silvermint_save_dir := saveDir(folder)

	// If no file name was provided, try all save files starting with
	// the most recent. Otherwise attempt to load the indicated file.
	var fbytes []byte
	if fname == "" {
		// Get all save files in the directory
		files, err := os.ReadDir(silvermint_save_dir)
		if err != nil {
			return nil, err
		}
		// Sort the files based on the time they were last modified
		sort.Slice(files, func(i, j int) bool {
			finfo_i, _ := files[i].Info()
			finfo_j, _ := files[j].Info()
			return finfo_i.ModTime().After(finfo_j.ModTime())
		})
		// Attempt to load each file starting with the most recent
		success := false
		for _, file := range files {
			fpath := filepath.Join(silvermint_save_dir, file.Name())
			fbytes, err = os.ReadFile(fpath)
			if err != nil {
				log.Warningf("Unable to read file at %s: %s",
					fpath, err.Error())
				continue
			}
			// Only try to load/unmarshal an older file if there was some problem
			// doing so with the most recent. Otherwise, break.
			success = true
			break
		}
		if !success {
			return nil, fmt.Errorf("Failed to load bytes after trying %d files", len(files))
		}
	} else {
		fpath := filepath.Join(silvermint_save_dir, fname)
		return os.ReadFile(fpath)
	}
	return fbytes, nil
}
