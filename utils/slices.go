// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package utils

// Take two slices and merge them into a new slice that contains no
// duplicate entries. Note that duplicates will be removed even if
// the duplicate appears in the same list.
//
// Example:
//
//	left := []int{1, 2, 3, 4, 4}
//	right := []int{3, 5, 6}
//	SliceMergeUnique(left, right) => []int{1, 2, 3, 4, 5, 6}
func SliceMergeUnique[T comparable](left []T, right []T) []T {
	keys := make(map[T]bool)
	list := make([]T, 0)
	for _, entry := range left {
		if _, ok := keys[entry]; !ok {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	for _, entry := range right {
		if _, ok := keys[entry]; !ok {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

// Takes a slice and returns a new slice with the given entry
// removed. If you want to modify the slice you have, then use
// the `Delete()` function from `golang.org/x/exp/slices`. The
// slice returned is a new slice. Note that simple types of T
// will act like a completely new copy but for pointer types,
// the two slices will share entries.
func SliceDelete[T comparable](slice []T, i int) []T {
	tmp := make([]T, 0)
	tmp = append(tmp, slice[:i]...)
	tmp = append(tmp, slice[i+1:]...)
	return tmp
}

// Takes a slice and returns a new slice with the given entry
// inserted at the provided index
func SliceInsert[T any](slice []T, i int, val T) []T {
	tmp := make([]T, 0)
	if len(slice) == i {
		tmp = append(tmp, slice...)
		return append(tmp, val)
	}
	tmp = append(tmp, slice[:i+1]...)
	tmp = append(tmp, slice[i:]...)
	tmp[i] = val
	return tmp
}
