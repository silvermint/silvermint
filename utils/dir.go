package utils

import (
	"fmt"
	"io/fs"
	"os"
)

func DirExists(dir string) bool {
	info, err := os.Stat(dir)
	if err != nil || !info.IsDir() {
		return false
	}
	return true
}

func MaybeCreateDir(dir string, perm fs.FileMode) error {
	if len(dir) == 0 {
		return fmt.Errorf("Can't create directory from empty string.")
	}

	if DirExists(dir) {
		return nil
	}

	err := os.MkdirAll(dir, perm)
	if err != nil {
		return fmt.Errorf("Can't create directory %s: %s",
			dir, err.Error())
	}

	return nil
}
