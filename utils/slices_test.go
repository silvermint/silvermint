// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package utils

import (
	"testing"

	"golang.org/x/exp/slices"
)

func TestSliceMergeUnique1(t *testing.T) {
	left := []int{1}
	right := []int{1}

	out := SliceMergeUnique(left, right)
	if len(out) != 1 {
		t.Errorf("Merge of {1} and {1} failed, len(out) == %d", len(out))
	}
}

func TestSliceMergeUnique2(t *testing.T) {
	left := []int{1}
	right := []int{2}

	out := SliceMergeUnique(left, right)
	if len(out) != 2 {
		t.Errorf("Merge of {1} and {1} failed, len(out) == %d", len(out))
	}

	if out[0] != 1 || out[1] != 2 {
		t.Errorf("Merge of {1} and {2} failed, output was: %v", out)
	}
}

func TestSliceMergeUnique3(t *testing.T) {
	left := []int{1, 2}
	right := []int{2, 3}

	out := SliceMergeUnique(left, right)
	if len(out) != 3 {
		t.Errorf("Merge of {1, 2} and {2, 3} failed, len(out) == %d", len(out))
	}

	if out[0] != 1 || out[1] != 2 || out[2] != 3 {
		t.Errorf("merge of {1, 2} and {2, 3} failed, output was: %v", out)
	}
}

func TestSliceMergeUnique4(t *testing.T) {
	left := []int{1, 2, 4, 13, 3406938, 2340957830968, 395}
	right := []int{2, 3, 4, 2034958, 23845734, 23098234}

	out := SliceMergeUnique(left, right)
	if len(out) != 11 {
		t.Errorf("Merge of %v and %v failed, len(out) == %d, expected %d.", left, right, len(out), 11)
	}

	for _, x := range left {
		if !slices.Contains(out, x) {
			t.Errorf("Item from missing: %v not in %v", x, out)
		}
	}
	for _, x := range right {
		if !slices.Contains(out, x) {
			t.Errorf("Item from missing: %v not in %v", x, out)
		}
	}
}

func TestSliceDelete(t *testing.T) {
	s := []int{0, 1, 2, 3, 4}
	o := SliceDelete(s, 2)
	if slices.Contains(o, 2) {
		t.Errorf("Unexpected item in list after delete.")
	}
	if len(o) != 4 {
		t.Errorf("Output slice length was %d, expected %d", len(o), 4)
	}
	if len(s) != 5 {
		t.Errorf("Input slice was modified in pure function!")
	}
}
