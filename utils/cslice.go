package utils

import "sync"

// Permits sorting the values val based on the Keyable value key
type Sortable[T any] struct {
	key Keyable
	val T
}

// A sorted list of Sortable objects that permits concurrency
type ConcurrentSortedSlice[T any] struct {
	items []Sortable[T]
	mu    sync.RWMutex
}

// Creates a new slice of n ConcurrentSortedSlice[T]s
func NewConcurrentSortedSliceArray[T any](n int) []*ConcurrentSortedSlice[T] {
	arr := make([]*ConcurrentSortedSlice[T], n)
	for i := range arr {
		arr[i] = NewConcurrentSortedSlice[T]()
	}
	return arr
}

// Creates a single new ConcurrentSortedSlice[T]
func NewConcurrentSortedSlice[T any]() *ConcurrentSortedSlice[T] {
	return &ConcurrentSortedSlice[T]{
		items: []Sortable[T]{},
	}
}

// The sorting function that operates on the Keyable portion of the Sortable item
func sorter[T any](l, r Sortable[T]) int {
	if l.key.Height() < r.key.Height() {
		return -1
	} else if l.key.Height() > r.key.Height() {
		return 1
	}
	return 0
}

// Adds the element val to the ConcurrentSortedSlice, sorted on the key
func (cs *ConcurrentSortedSlice[T]) Add(key Keyable, val T) {
	cs.mu.Lock()
	defer cs.mu.Unlock()

	elem := Sortable[T]{
		key: key,
		val: val,
	}
	cs.items = InsertToSortedList(cs.items, elem, sorter[T])
}

// Pops the first element of the ConcurrentSortedSlice, returning the associated value, or
// an empty value if there were none left. Returns a boolean of whether there were any left.
func (cs *ConcurrentSortedSlice[T]) Pop() (T, bool) {
	cs.mu.Lock()
	defer cs.mu.Unlock()

	var empty T
	if len(cs.items) != 0 {
		elem := cs.items[0]
		cs.items[0] = Sortable[T]{
			key: nil,
			val: empty,
		}
		cs.items = cs.items[1:]
		return elem.val, true
	}
	return empty, false
}

// Returns the size of the ConcurrentSortedSlice
func (cs *ConcurrentSortedSlice[T]) Size() int {
	cs.mu.RLock()
	defer cs.mu.RUnlock()

	return len(cs.items)
}
