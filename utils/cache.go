package utils

import (
	"sync"

	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/validator"
)

const MIN_CACHE_SIZE = 10

type Keyable interface {
	ValId() uint16
	Height() uint64
}

// NB(chuck): The expectation of the sequence type is that elements will only
// be added to it in their absolute order. That is, for any element N that
// might be added to it, all 0...N-1 must have previously been added to the
// sequence. This is to be enforced by the functions using the cache, (i.e. no
// error checking is done here).
type sequence[T any] struct {
	latest uint64
	sz     uint64
	items  []T
	mu     sync.RWMutex
}

func (s *sequence[T]) at(h uint64) T {
	return s.items[h%s.sz]
}

func (s *sequence[T]) has(h uint64) bool {
	return s.latest-h < s.sz
}

func (s *sequence[T]) seen(h uint64) bool {
	return h <= s.latest
}

func (s *sequence[T]) set(h uint64, t T) {
	s.items[h%s.sz] = t
	if s.latest <= h {
		s.latest = h
	} else {
		log.Warningf("Added element %d to Cache out of order (latest was %d)", h, s.latest)
	}
}

func (s *sequence[T]) rangeInternal(f func(T) bool) {
	for _, t := range s.items {
		if !f(t) {
			return
		}
	}
}

func (s *sequence[T]) Size() uint64 {
	s.mu.RLock()
	defer s.mu.RUnlock()

	return s.sz
}

func (s *sequence[T]) Latest() uint64 {
	s.mu.RLock()
	defer s.mu.RUnlock()

	return s.latest
}

type Cache[T any] struct {
	sequences [validator.N_MAX_VALIDATORS]*sequence[T]
}

func NewCache[T any](size uint64) *Cache[T] {
	if size <= MIN_CACHE_SIZE {
		log.Warningf("Tried to create cache with size %d, defaulting to size %d",
			MIN_CACHE_SIZE)
		size = MIN_CACHE_SIZE
	}
	c := &Cache[T]{}
	for i := 0; i < len(c.sequences); i++ {
		c.sequences[i] = &sequence[T]{
			items: make([]T, size),
			sz:    size,
		}
	}

	return c
}

// Get(Keyable) returns the object associated with that element and a boolean
// indicating true ONLY IF the value is currently in the buffer. If the object
// is not currently in the buffer it returns the zero value for that object and
// false.
func (c *Cache[T]) Get(el Keyable) (T, bool) {
	c.sequences[el.ValId()].mu.RLock()
	defer c.sequences[el.ValId()].mu.RUnlock()

	sh := c.sequences[el.ValId()]

	if sh.has(el.Height()) {
		// It's still in the buffer
		return sh.at(el.Height()), true
	}
	// We've never seen it or it's not currently in the buffer
	var zeroVal T
	return zeroVal, false
}

// Has(Keyable) tells us if, as of this check, the element is in the cache.
// There's no guarantee that it will remain in the cache however, and so if
// you intend to actually access it you should instead use Get(Keyable) and
// check the boolean returned by that function instead.
func (c *Cache[T]) Has(el Keyable) bool {
	c.sequences[el.ValId()].mu.RLock()
	defer c.sequences[el.ValId()].mu.RUnlock()

	return c.sequences[el.ValId()].has(el.Height())
}

// Seen(Keyable) tells if the cache has ever seen the entry corresponding to
// Keyable. It might not actually be in the cache currently.
func (c *Cache[T]) Seen(el Keyable) bool {
	c.sequences[el.ValId()].mu.RLock()
	defer c.sequences[el.ValId()].mu.RUnlock()

	return c.sequences[el.ValId()].seen(el.Height())
}

// Set(Keyable, T) sets the value in sequence[Keyable.ValId()] at element
// Keyable.Height() to value T. Elements MUST be set in order, such that
// all elements [0...Keyable.Height()-1] have been added to the cache prior
// to attemption to add the element at Keyable.Height().
func (c *Cache[T]) Set(el Keyable, t T) {
	c.sequences[el.ValId()].mu.Lock()
	defer c.sequences[el.ValId()].mu.Unlock()

	c.sequences[el.ValId()].set(el.Height(), t)
}

// Size() returns the sum of the size of all sequences. This assumes that all
// elements [0, ..., el.Latest] have been added to the sequence.
func (c *Cache[T]) Size() uint64 {
	var n uint64 = 0
	for _, sequence := range c.sequences {
		n += sequence.Latest()
	}
	return n
}

// Tip(uint16) returns the latest element in the sequence associated with the
// provided ValId.
func (c *Cache[T]) Tip(vid uint16) T {
	c.sequences[vid].mu.RLock()
	defer c.sequences[vid].mu.RUnlock()

	return c.sequences[vid].at(c.sequences[vid].latest)
}

// RangeRead(uint16, f(T) bool) calls f(T) over the elements stored in the
// sequence associated with the provided ValId, stopping when f(T) returns false
// or when all elements have been hit. Should not be used with any f(T) that
// changes the data in the sequence.
func (c *Cache[T]) RangeRead(vid uint16, f func(T) bool) {
	c.sequences[vid].mu.RLock()
	defer c.sequences[vid].mu.RUnlock()

	c.sequences[vid].rangeInternal(f)
}
