package utils

import "sync"

// Launches len(combinable)/2 go-routines, each of which combines two items
// together. Returns the log2(len(combinable)) results once all go-routines
// have concluded.
func Reducer[T any](
	combinable []T,
	combiner func(T, T) T,
) []T {
	n := len(combinable) / 2
	reduced := make([]T, n)
	for i := range reduced {
		reduced[i] = *new(T)
	}
	if len(combinable)%2 == 1 {
		reduced = append(reduced, combinable[len(combinable)-1])
	}
	var wg sync.WaitGroup
	wg.Add(n)
	for i := 0; i < n; i++ {
		i := i
		go ReducerWorker(combinable[2*i], combinable[2*i+1], &reduced[i], combiner, &wg)
	}
	wg.Wait()

	return reduced
}

func ReducerWorker[T any](prev T,
	cur T,
	reduced *T,
	combiner func(T, T) T,
	wg *sync.WaitGroup,
) {
	defer wg.Done()
	*reduced = combiner(prev, cur)
}
