package utils

// Performs a binary search on a sorted slice using the provided comparator.
// The comparator should be of the form:
//
//	func(left T, right T) int {
//	    if (left == right) { return 0 }
//	    if (left <  right) { return -1 }
//	    if (left >  right) { return 1 }  }
func BinarySearch[T any](sorted []T, tgt T, comparator func(T, T) int) (bool, int) {
	if len(sorted) == 0 {
		return false, 0
	}
	n := len(sorted) / 2
	comparison := comparator(sorted[n], tgt)
	if comparison == 0 {
		return true, n
	} else if comparison < 0 {
		found, ind := BinarySearch(sorted[n+1:], tgt, comparator)
		return found, n + 1 + ind
	} else {
		found, ind := BinarySearch(sorted[:n], tgt, comparator)
		return found, ind
	}
}

// Inserts the provided element at the appropriate location in a sorted slice.
// Mutates the slice with the insert and returns it.
func InsertToSortedList[T any](sorted []T, elem T, comparator func(T, T) int) []T {
	_, ind := BinarySearch(sorted, elem, comparator)
	sorted = append(sorted, elem)
	copy(sorted[ind+1:], sorted[ind:])
	sorted[ind] = elem
	return sorted
}
