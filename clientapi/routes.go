// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package clientapi

import (
	"gitlab.com/silvermint/silvermint/casanova"

	"github.com/gin-gonic/gin"
)

func initRoutes(router *gin.Engine, cas *casanova.Casanova) *gin.Engine {
	router.POST("/payment", MakePaymentFunc(cas))
	router.GET("/payment-state/:id", PaymentState(cas))
	router.GET("/account-state/:id", AccountState(cas))
	router.GET("/account-state/:id/:block_number", AccountState(cas))
	//paths added for Redoc and SwaggerUI documentation
	router.GET("/api", GetApiDoc)
	router.GET("/swagger.json", GetSwaggerJSON)
	router.GET("/swagger", GetSwaggerUI)
	return router
}
