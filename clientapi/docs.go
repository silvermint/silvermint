// Package classification Silvermint.
//
// Documentation of Silvermint Client API.
//
//		 Schemes: http
//		 Version: 1.0.0
//
//		 Consumes:
//		 - text/plain
//
//		 Produces:
//	  - text/html
//		 - application/json
//
//		 Security:
//		 - basic
//
//		SecurityDefinitions:
//		basic:
//		  type: basic
//
// swagger:meta
package clientapi
