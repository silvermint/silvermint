package clientapi

import (
	"fmt"
	"path/filepath"

	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/console"
	"gitlab.com/silvermint/silvermint/log"

	"github.com/gin-gonic/gin"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func init() {
	pflag.Int("api_port", 8880,
		"The port on which the client api HTTP server listens.")
	pflag.String("api_ipaddr", "0.0.0.0",
		"The IP address that the Client API listens on.")
}

// Run constructs the gin engine, loads the HTML globs, and runs the request
// router.
func Run(templates []string, cas *casanova.Casanova) {
	ipAddress := viper.GetString("api_ipaddr")
	apiPort := viper.GetInt("api_port")
	if ipAddress == "" || apiPort == 0 {
		log.Fatalln("IP address and API port must be specified in config or cmd-line.")
	}

	consoleAddress := fmt.Sprintf("%s:%d", ipAddress, apiPort)

	var router *gin.Engine = gin.Default()
	router.SetTrustedProxies(nil)

	for _, path := range templates {
		router.LoadHTMLGlob(filepath.Join(path, "*"))
	}

	router.Use(console.IncrRequestAndStatus)
	initRoutes(router, cas)

	router.Run(consoleAddress)
}
