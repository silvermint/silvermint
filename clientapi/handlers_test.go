// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package clientapi

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/transaction"
	"gitlab.com/silvermint/silvermint/validator"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

type Output struct {
	dest crypto.Wallet
	amt  uint64
}

func makeCas() *casanova.Casanova {
	viper.Set("lht_keybits", 10)
	viper.Set("block_cache_size", 100)
	viper.Set("save_dir", "/tmp")
	viper.Set("config_dir", "/tmp")
	viper.Set("num_lhts", 100)
	viper.Set("lht_pool_buffer", 100)
	wal, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	val := validator.Validator{
		Id:         0,
		RemoteAddr: nil,
		Hostport:   "127.0.0.1:8888",
		Conn:       nil,
		Active:     true,
		Pubkey:     wal.WalletAddress(),
	}
	vals := [validator.N_MAX_VALIDATORS]validator.Validator{0: val}
	cas, _ := casanova.NewCasanova(0, vals, nil)
	// Get a block so that we generate a new IncrLedger (the genesis block's
	// SSLedger can't process txns)
	cas.Get()
	return cas
}

func handlerTest(t *testing.T, code int, req *http.Request, cas *casanova.Casanova) {
	w := httptest.NewRecorder()

	g := initRoutes(gin.Default(), cas)
	g.ServeHTTP(w, req)
	if w.Code != code {
		t.Fatalf("Expected status code %v, got %v", code, w.Code)
	}
}

func TestPayment(t *testing.T) {
	rPath := "/payment"

	source, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dest, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	cas := makeCas()
	mintTxn := transaction.Mint{
		Src:          source.WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 100,
			Dest_:   source.WalletAddress(),
		},
	}
	data, err := mintTxn.MarshalSignedBinary(source)
	if err != nil {
		t.Fatalf("Unable to sign mint txn: %s", err.Error())
	}
	cas.AddTxn(data)
	// Issue a few blocks so the mint processes
	for i := 0; i < 5; i++ {
		cas.Get()
	}
	latest := cas.StateCache.Tip(cas.Self_.Id)
	utxos := latest.Extra.Ledger.Utxos(source.WalletAddress())

	pmt := transaction.Payment{
		Src:          source.WalletAddress(),
		CheckNumber_: 2,
		Inputs_:      []transaction.Utxo{utxos[0]},
		Outputs_: []transaction.PaymentOutput{
			transaction.PaymentOutput{
				Amount_: 57,
				Dest_:   dest.WalletAddress(),
			},
		},
	}

	buf, _ := pmt.MarshalSignedBinary(source)
	reader := bytes.NewReader(buf)
	req, _ := http.NewRequest("POST", rPath, reader)

	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	handlerTest(t, 200, req, cas)
}

func TestInsufficientPayment(t *testing.T) {
	rPath := "/payment"

	source, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dest, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	cas := makeCas()

	mintTxn := transaction.Mint{
		Src:          source.WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 57,
			Dest_:   source.WalletAddress(),
		},
	}
	data, err := mintTxn.MarshalSignedBinary(source)
	if err != nil {
		t.Fatalf("Unable to sign mint txn: %s", err.Error())
	}
	cas.AddTxn(data)
	// Issue a few blocks so the mint processes
	for i := 0; i < 5; i++ {
		cas.Get()
	}
	latest := cas.StateCache.Tip(cas.ValidatorId)

	utxos := latest.Extra.Ledger.Utxos(source.WalletAddress())

	pmt := transaction.Payment{
		Src:          source.WalletAddress(),
		CheckNumber_: 2,
		Inputs_:      []transaction.Utxo{utxos[0]},
		Outputs_: []transaction.PaymentOutput{
			transaction.PaymentOutput{
				Amount_: 100,
				Dest_:   dest.WalletAddress(),
			},
		},
	}

	buf, _ := pmt.MarshalBinary()
	reader := bytes.NewReader(buf)
	req, _ := http.NewRequest("POST", rPath, reader)

	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	handlerTest(t, 500, req, cas)
}

// Testing for failure in the case of a missing value
func TestPaymentFail(t *testing.T) {
	rPath := "/payment"

	jsonData := []byte{}

	req, _ := http.NewRequest("POST", rPath, bytes.NewBuffer(jsonData))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	cas := makeCas()

	handlerTest(t, 500, req, cas)
}

// Create and Add Payment then test payment-state path with transaction hash for status
func TestPaymentState(t *testing.T) {
	rPath := "/payment-state/"

	source, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	dest, _ := crypto.NewWallet("", crypto.BASIC_WALLET)

	cas := makeCas()
	mintTxn := transaction.Mint{
		Src:          source.WalletAddress(),
		CheckNumber_: 1,
		Output_: transaction.MintOutput{
			Amount_: 100,
			Dest_:   source.WalletAddress(),
		},
	}

	data, err := mintTxn.MarshalSignedBinary(source)
	if err != nil {
		t.Fatalf("Unable to sign mint txn: %s", err.Error())
	}
	err = mintTxn.UnmarshalBinary(data)
	if err != nil {
		t.Fatalf("Unable to unmarshal mint txn: %s", err.Error())
	}
	err = cas.AddTxn(data)
	if err != nil {
		t.Fatalf("Unable to add mint txn: %s", err.Error())
	}

	// Issue a few blocks so the mint Finalizes
	for i := 0; i < 5; i++ {
		cas.Get()
	}

	latest := cas.StateCache.Tip(cas.ValidatorId)
	utxos := latest.Extra.Ledger.Utxos(source.WalletAddress())

	pmt := transaction.Payment{
		Src:          source.WalletAddress(),
		CheckNumber_: 2,
		Inputs_:      []transaction.Utxo{utxos[0]},
		Outputs_: []transaction.PaymentOutput{
			transaction.PaymentOutput{
				Amount_: 100,
				Dest_:   dest.WalletAddress(),
			},
		},
	}
	buf, _ := pmt.MarshalSignedBinary(source)
	cas.AddTxn(buf)

	pmt.Bytes = buf
	copy(pmt.Sig[:], buf[len(buf)-crypto.SIGNATURE_SIZE:])

	rPath += fmt.Sprintf("%x", pmt.Hash())
	cn := make([]byte, 2)
	binary.LittleEndian.PutUint16(cn, pmt.CheckNumber_)
	rPath += fmt.Sprintf("%x", cn)
	rPath += fmt.Sprintf("%x", pmt.Source().Address())

	req, _ := http.NewRequest("GET", rPath, nil)

	handlerTest(t, 200, req, cas)
}

// Testing for failure in the case of a misspelled key
func TestPaymentStateFail(t *testing.T) {
	rPath := "/payment-state/"

	req, _ := http.NewRequest("GET", rPath, nil)

	cas := makeCas()

	handlerTest(t, 404, req, cas)
}

func TestAccountState(t *testing.T) {
	source, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	rPath := "/account-state/" + fmt.Sprintf("%x", source.WalletAddress().Address())

	req, _ := http.NewRequest("GET", rPath, nil)

	cas := makeCas()

	handlerTest(t, 200, req, cas)
}

// Testing for failure in the case of a missing value
func TestAccountStateFail(t *testing.T) {
	rPath := "/account-state/"

	req, _ := http.NewRequest("GET", rPath, nil)

	cas := makeCas()

	handlerTest(t, 404, req, cas)
}

func TestAccountStateBlockNum(t *testing.T) {
	source, _ := crypto.NewWallet("", crypto.BASIC_WALLET)
	rPath := "/account-state/" + fmt.Sprintf("%x", source.WalletAddress().Address()) + "/45"

	req, _ := http.NewRequest("GET", rPath, nil)

	cas := makeCas()

	handlerTest(t, 200, req, cas)
}
