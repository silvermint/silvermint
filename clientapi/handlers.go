// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package clientapi

import (
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/silvermint/silvermint/casanova"
	"gitlab.com/silvermint/silvermint/crypto"
	"gitlab.com/silvermint/silvermint/log"
	"gitlab.com/silvermint/silvermint/transaction"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

// MakePaymentFunc encloses a pointer to the casanova object.
// It returns a handler function for the /payment route that processes payments submitted
// in []byte form
func MakePaymentFunc(cas *casanova.Casanova) gin.HandlerFunc {

	return func(c *gin.Context) {
		// Get binary from POST
		data, err := c.GetRawData()
		if err != nil {
			log.Errorf("Error getting raw data from POST: %s", err.Error())
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		// Send transaction data to Casanova for handling
		err = cas.AddTxn(data)
		if err != nil {
			type ErrObj struct {
				Reason string `json:"reason"`
			}
			errStr := fmt.Sprintf("Error adding transaction to queue: %s", err.Error())
			log.Error(errStr)
			c.JSON(http.StatusInternalServerError, ErrObj{Reason: errStr})
			return
		}

		c.Status(http.StatusOK)
	}
}

//swagger:operation POST /payment MakePaymentFunc
//
//Description: This route takes the payment string in byte form and processes the request, to send the transaction data to Casanova for handling, either returning an error or ok response
//
//---
//produces:
// - application/json
//parameters:
// - name: payment
//   in: body
//   description: "A marshal signed binary payment string, size ranges from 106-65534 bytes, contains(in-order): Length | Txn Type | Source PubKey | Check Number | Num Inputs | Inputs | Num Outputs | Outputs | Signature"
//   required: true
//   type: string
//   format: binary
//   minlength: 106
//   maxlength: 4194346
//   example: "n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
//responses:
//		'400':
//			description: "Error getting raw data from POST"
//		'500':
//			description: "Error adding transaction to queue"
//		'200':
//			description: "Ok Transaction accepted"

// MakeMintFunc encloses a pointer to the casanova object.
// It returns a handler function for the /mint route that processes mint requests
// in []byte form

func PaymentState(cas *casanova.Casanova) gin.HandlerFunc {
	return func(c *gin.Context) {
		type PaymentStatus struct {
			Status string
		}
		data, err := hex.DecodeString(c.Param("id"))
		if err != nil {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		offset := 0
		if len(data) < crypto.HASH_SIZE {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		hash := (*[crypto.HASH_SIZE]byte)(data[offset : offset+crypto.HASH_SIZE])
		offset += crypto.HASH_SIZE

		checknum := binary.LittleEndian.Uint16(data[offset : offset+2])
		offset += 2

		swal, err := crypto.UnmarshalBinary(data[offset:])
		if err != nil {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		pmtStatus, err := cas.GetPaymentState(*hash, checknum, swal.WalletAddress())
		if err != nil {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		j := PaymentStatus{Status: pmtStatus}
		c.JSON(http.StatusOK, j)
	}
}

//swagger:operation GET /payment-state/{id} PaymentState
//
//Description: This route returns the status of the transaction hash that is given in the path
//
//---
//produces:
// - application/json
//parameters:
// - name: id
//   in: path
//   description: "The Transaction Hash Id, must be in hexidecimal format"
//   required: true
//   type: string
//   format: binary
//   minlength: 64
//   maxlength: 64
//   example: "f72e43824b374da4e512d2ebfec1061fa0067273da7dfae3d5645d66bdb9dcb7"
//responses:
//	'400':
//		description: "Error in Decoding Transaction Hash Id"
//	'200':
//		description: "Returns Payment State information"
//		schema:
//			type: object
//			name: PaymentStatus
//			description: "An object with Status property that contains the response of the status"
//			properties:
//				Status:
//					type: string
//			example: { Status: "PENDING"}

func AccountState(cas *casanova.Casanova) gin.HandlerFunc {
	return func(c *gin.Context) {
		accountByte, err := hex.DecodeString(c.Param("id"))
		if err != nil {
			log.Errorf("Error getting wallet from state request: %s", err.Error())
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		walletAddress := crypto.NewWalletAddress(accountByte)
		if walletAddress == nil {
			log.Errorf("Couldn't get account state for %x", accountByte)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		latest := cas.StateCache.Tip(cas.ValidatorId)
		balance := latest.Extra.Ledger.Balance(walletAddress)
		checknum := latest.Extra.Ledger.CheckNumber(walletAddress)
		utxos := latest.Extra.Ledger.Utxos(walletAddress)
		if err != nil {
			log.Errorf("Couldn't get account state for %x", accountByte)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		// NB(chuck): This should be redundant, but we want to enforce the
		// invariant that we're only returning UTXOs that are UNSPENT as of
		// the time of the query. Since only transaction processing can update
		// a wallets list of UTXOs, ledgers are only accessible after their
		// associated StateData is entered into the StateCache, and no more
		// transactions can be processed to a ledger after it is added to the
		// cache (immutable ledgers), it's sufficient to enforce that the
		// UTXOs we're returning are in the wallet at the time of this ledger.
		state, ok := latest.Extra.Ledger.WalletState(walletAddress)
		if ok {
			for _, utxo := range utxos {
				if !state.Has(utxo) {
					log.Errorf("Wallet %x balance query can't return UTXO %s"+
						" if it's not in the wallet!", utxo.String())
					c.AbortWithStatus(http.StatusInternalServerError)
					return
				}
			}
		} else if len(utxos) != 0 {
			// It's fine for the ledgerMap to not exist for this wallet, but
			// only if that wallet has no balance. Otherwise, return an error.
			log.Errorf("Wallet %x not found in ledger but had %d UTXOs?!",
				walletAddress, len(utxos))
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		c.JSON(http.StatusOK, struct {
			Balance  uint64             `json:"balance"`
			Checknum uint16             `json:"checknum"`
			Utxos    []transaction.Utxo `json:"utxos"`
		}{balance, checknum, utxos})
	}
}

//swagger:operation GET /account-state/{id} AccountState
//
//Description: This route returns the Wallet state from the provided public key, and returns an json object containing details about the account
//
//---
//produces:
// - application/json
//parameters:
// - name: id
//   in: path
//   description: "The wallet public key, must be in hexidecimal format"
//   required: true
//   type: string
//   format: binary
//   minlength: 72
//   maxlength: 72
//   example: "00000000012ab45c8df854585d444f444dd55fff012ab45c8df854585d444f444dd55fff"
//responses:
//	'400':
//		description: "Error getting wallet from state request"
//	'500':
//		description: "Couldn't get account state for id"
//	'200':
//		description: "Returns the Account State Information"
//		schema:
//			type: object
//			properties:
//				balance:
//					type: integer
//					description: "Balance of the Account"
//				checknum:
//					type: integer
//					description: "The check number of the account"
//				utxos:
//					type: array
//					description: "An array of utxo transactions"
//					items:
//						type: object
//						name: UtxoWithAmount
//						description: "utxo information with amounts"
//						properties:
//							U:
//								type: object
//								name: Utxo
//								description: "utxo information"
//								properties:
//									TxnHash:
//										type: array
//										description: "transaction hash"
//										items:
//											type: byte
//									Output:
//										type: integer
//										description: "output of txnhash"
//							A:
//								type: integer
//								description: "the amount associated with utxo"

func GetSwaggerUI(c *gin.Context) {
	c.HTML(http.StatusOK, "swagger.html", gin.H{})
}

func GetSwaggerJSON(c *gin.Context) {
	// first read the config.json file
	var content []byte
	var err error
	templatePaths := viper.GetString("templates_path")
	templateArr := strings.Split(templatePaths, string(filepath.ListSeparator))
	templateArr = append(templateArr, ".")
	for _, templatePath := range templateArr {
		swaggerPath := filepath.Join(templatePath, "swagger.json")
		file, err := os.Open(swaggerPath)
		if err != nil {
			continue
		}
		content, err = io.ReadAll(file)
		if err != nil {
			continue
		}
	}

	if err != nil || content == nil {
		log.Errorf("Unable to load swagger.json file")
		c.Status(http.StatusInternalServerError)
		return
	}

	// unmarshall the data into payload
	var payload map[string]interface{}
	err = json.Unmarshal(content, &payload)
	if err != nil {
		log.Errorf("Error during Unmarshal(): %s", err.Error())
		c.Status(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, payload)
}

func GetApiDoc(c *gin.Context) {
	c.HTML(http.StatusOK, "api.html", gin.H{})
}
