# Silvermint

The Silvermint node.

Our [full documentation](https://docs.silvermint.net/) is available on our
web site. The `silvermint` binary has a man page detailing its usage.

## Installation

This should get you started with development of the Silvermint node. If you
just want to install the node software, then consider installing from one of
the officially supported packages available on the releases page.

[Silvermint releases](https://gitlab.com/silvermint/silvermint/-/releases)

We officially support Debian and Ubuntu distributions, But, `silvermint` should
run on any distribution that uses deb packages, though you may have to work at
it a bit. What that means is outside the scope of this document.


### Prerequisites

Really, the only prerequisite is [Go](https://golang.org). Silvermint is
developed and test on go version 1.20 at this time. Older versions may work and
the development team will attempt to support older versions of go where practical.
However, version 1.18 is the absolute oldest possible, because we use go's
generics, which came out in 1.18.

Hence, DO NOT USE the `golang-go` apt packages for go on Debian bullseye or
older, which is no newer than 1.15 as of this writing.

Ubuntu's `golang-go` package is currently 1.19, which may be recent enough.

If you want to use the latest `go`, then follow the [download and
install](https://go.dev/doc/install) instructions on `go.dev`.


### Building the Software

The easiest way to build the software is to run the `build.sh` script from the
git repository's root directory. If everything goes well, you will have a `silvermint`
binary in the repository's directory.

The build command supports several arguments that may be useful.

```
Usage: build.sh [OPTION]

Build the silvermint node.

Options:
    -h          Display this help message and exit.
    -l=LDFLAGS  Set the ldflags.
    -o=DIR      Set the build output directory to DIR.
    -r          Run the `silvermint` binary after building.
    -T=PATH     Sets the HTML templates path to PATH.
    -t          Run tests after building.
    -v          Produce verbose output.
    -M          Run go mod tidy first.
    -G=DIR      Sets GOROOT to DIR and adds dir to the shell's PATH.
    -g=DIR      Sets GOPATH to DIR.
    -s          Enable swagger.
```

If you have an unusual go setup or other non-standard requirements, you may wish to
run the go build by hand. The `build.sh` ultimately runs `go build ./...` with the
command arguments that it thinks best suit your system. You can run that by hand if
you want to adjust them.


#### Building OpenAPI Docs and UI

If you want to view and use the OpenAPI documentation, SwaggerUI and Redoc, then run
the build with the `-s` option.

```
$ ./build.sh -s
```

##### Prerequisites for OpenAPI

If you want to use the REST API's swagger documentation, then you will also
need [go-swagger](https://goswagger.io/).

First, install [jq](https://stedolan.github.io/jq/), a command-line JSON processor:

```
$ apt-get -y update
$ apt-get -y install jq
```

* Download go-swagger binary as shown. This will work for CPUs that support
the `amd64` architecture. If you have another architecture, you may need to 
change `amd64` in the command below to fit your local machine.

```
$ download_url=$(curl -s https://api.github.com/repos/go-swagger/go-swagger/releases/latest | \jq -r '.assets[] | select(.name | contains("'"$(uname | tr '[:upper:]' '[:lower:]')"'_amd64")) | .browser_download_url')
$ curl -o /usr/local/bin/swagger -L'#' "$download_url"
$ chmod +x /usr/local/bin/swagger
```

## Contributing

If you'd like to contribute to the software or the crypto network, join our
Discord here: https://discord.gg/CYRyY5ywSK

Sign in and select the developer role. Ask questions there to get started!

