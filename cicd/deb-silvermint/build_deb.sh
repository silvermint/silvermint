#!/bin/bash

set -ex

while getopts 'v:h' opt; do
  case "$opt" in
    v)
      VERSION="$OPTARG"
      echo "Setting VERSION to '${OPTARG}'"
      ;;

    h)
      HELP_MESG="Usage: $(basename $0) [-v VERSION]"
      echo ${HELP_MESG}
      exit 0
      ;;

    :)
      echo -e "option requires an argument.\n${HELP_MESG}"
      exit 1
      ;;

    ?)
      echo -e "Invalid command option.\n${HELP_MESG}"
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

DEB_PKG_DIR=$(dirname ${CI_PROJECT_DIR})/deb_packaging
DEB_VER_DIR=${DEB_PKG_DIR}/silvermint-$VERSION
DEBFULLNAME="The Silvermint Team"
DEBEMAIL="silvermint@pyrofex.net"
DEB_DUILD_OPTIONS=""
DEBUILD_DPKG_BUILDPACKAGE_OPTS="-us -uc -sa -rfakeroot"
DEBUILD_LINTIAN_OPTS="-i -I --show-overrides --allow-root"
export VERSION DEB_PKG_DIR DEB_VER_DIR DEBFULLNAME DEBEMAIL DEB_DUILD_OPTIONS DEBUILD_DPKG_BUILDPACKAGE_OPTS DEBUILD_LINTIAN_OPTS

cd $CI_PROJECT_DIR

if [ -d "${DEB_PKG_DIR}" ] 
then
    # remove root deb package dir if it exists
    rm -rf ${DEB_PKG_DIR}
fi

# create root deb package dir
mkdir -p ${DEB_PKG_DIR}

# create deb packages
PACKAGE_NAMES=("silvermint")

for PACKAGE_NAME in ${PACKAGE_NAMES[@]}; do
    # set path to deb directory
    DEB_VER_DIR=${DEB_PKG_DIR}/$PACKAGE_NAME-$VERSION

    # remove any existing deb package dir
    cd $CI_PROJECT_DIR

    if [ -d "${DEB_VER_DIR}" ] 
    then
        # remove deb version dir if it exists
        rm -rf ${DEB_VER_DIR}
    fi

    # create deb version dir
    mkdir -p ${DEB_VER_DIR}

    # copy source code to deb version dir
    cp -r -p $CI_PROJECT_DIR/* ${DEB_VER_DIR}

    cd ${DEB_VER_DIR}
    dh_make -s -y --createorig

    # create root deb package dir if it does not exist
    if [ -d "${DEB_VER_DIR}/debian" ] 
    then
        rm -r ${DEB_VER_DIR}/debian
    fi

    mv ${DEB_VER_DIR}/cicd/deb-silvermint/debian ${DEB_VER_DIR}/debian

    # Creating install directory structure
    TREEROOT="tree"
    mkdir -p ${TREEROOT}/etc/silvermint ${TREEROOT}/usr/lib/silvermint/templates ${TREEROOT}/usr/sbin ${TREEROOT}/var/silvermint/$VERSION
    cp -R templates/* ${TREEROOT}/usr/lib/silvermint/templates
    cp silvermint ${TREEROOT}/usr/sbin
    cp silvermint.yaml ${TREEROOT}/etc/silvermint
    cp swagger.json ${TREEROOT}/usr/lib/silvermint/templates

    # Building .deb package and arranging artifacts
    debuild -i -uc -us -b 
    mkdir -p $CI_PROJECT_DIR/pkgd
    cd ..
    mv -f *.deb *.build *.buildinfo *.changes *.tar.xz $CI_PROJECT_DIR/pkgd/
done
