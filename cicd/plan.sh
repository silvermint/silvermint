#!/bin/sh

set -e

while getopts 'v:r:t:p:u:e:h' opt; do
  case "$opt" in
    v)
      VERSION="$OPTARG"
      echo "Setting VERSION to '${OPTARG}'"
      ;;

    r)
      APT_RELEASE="$OPTARG"
      echo "Setting APT_RELEASE to '${OPTARG}'"
      ;;

    t)
      TF_ADDRESS="$OPTARG"
      echo "Setting TF_ADDRESS to '${OPTARG}'"
      ;;

    p)
      VARS_PATH="$OPTARG"
      echo "Setting VARS_PATH to '${OPTARG}'"
      ;;

    e)
      GITLAB_ENVIRONMENT="$OPTARG"
      echo "Setting GITLAB_ENVIRONMENT to '${OPTARG}'"
      ;;

    u)
      APT_SERVER_URL="$OPTARG"
      echo "Setting APT_SERVER_URL to '${OPTARG}'"
      ;;

    h)
      HELP_MESG="Usage: $(basename $0) [-v VERSION] [-r APT_RELEASE] [-t TF_ADDRESS] [-p VARS_PATH]"
      echo ${HELP_MESG}
      exit 0
      ;;

    :)
      echo -e "option requires an argument.\n${HELP_MESG}"
      exit 1
      ;;

    ?)
      echo -e "Invalid command option.\n${HELP_MESG}"
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

gitlab-terraform init
gitlab-terraform plan -var-file="${VARS_PATH}" \
  -var="silvermint_version=${VERSION}" \
  -var="silvermint_dist=${APT_RELEASE}" \
  -var="gitlab_environment=${GITLAB_ENVIRONMENT}" \
  -var="apt_server_url=${APT_SERVER_URL}"
