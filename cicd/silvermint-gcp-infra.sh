#!/bin/bash

#
# This is the infrastructure creation script for a Silvermint validator on Google Cloud
#

cat <<__EOF__
##################################################################
##    Silvermint Validator GCP Infrastructure Deploy Script     ##
##################################################################
Thank you for being a part of the growing Silvermint community!!

Lets set some variables...
__EOF__

read -p "Google Cloud Login Account: " GCP_LOGIN_ACCOUNT
read -p "Google Cloud Project ID: " GCP_PROJECT_ID
read -p "Google Cloud Project Network Name: " GCP_PROJECT_NET_NAME
read -p "Google Cloud Validator Instance Name: " GCP_INST_NAME
read -p "Google Cloud Instance Zone Name: " GCP_INST_ZONE

cat <<__EOF__
1. Copy URL below and paste in browser...
2. Select appropriate google account for access to Google Cloud...
3. Click Allow...
4. Copy the Authentication Code shown...
5. Paste the Authentication Code into the prompt below...
__EOF__

read -p "Press enter to continue... " ANY_KEY

gcloud auth login "$GCP_LOGIN_ACCOUNT" --format=default
gcloud config set project "$GCP_PROJECT_ID" --format=default

echo "Creating TestNet network named: $GCP_PROJECT_NET_NAME"
CLI_OUTPUT=$(gcloud compute networks list --filter=name:$GCP_PROJECT_NET_NAME --verbosity=none --format=default)

if [[ -z $CLI_OUTPUT ]]; then
  gcloud compute networks create "$GCP_PROJECT_NET_NAME" --format=default
else
  echo "TestNet network already exists."
  gcloud compute networks list --filter=name:$GCP_PROJECT_NET_NAME --format=default
fi

CLI_OUTPUT=""

echo "Creating TestNet allow ssh firewall rule..."
CLI_OUTPUT=$(gcloud compute firewall-rules list --filter=name:$GCP_PROJECT_NET_NAME-allow-ssh --verbosity=none --format=default)
if [[ -z $CLI_OUTPUT ]]; then
  gcloud compute firewall-rules create "$GCP_PROJECT_NET_NAME-allow-ssh" \
    --allow tcp:22 \
    --source-ranges=0.0.0.0/0 \
    --network="$GCP_PROJECT_NET_NAME" \
    --target-tags=silvermint,testnet \
    --format=default
else
  echo "TestNet ssh allow rule already exists."
  gcloud compute firewall-rules list --filter=name:$GCP_PROJECT_NET_NAME-allow-ssh --format=default
fi

CLI_OUTPUT=""

echo "Creating TestNet allow silvermint firewall rule..."
CLI_OUTPUT=$(gcloud compute firewall-rules list --filter=name:$GCP_PROJECT_NET_NAME-allow-silvermint-validator --verbosity=none --format=default)
if [[ -z $CLI_OUTPUT ]]; then
  gcloud compute firewall-rules create "$GCP_PROJECT_NET_NAME-allow-silvermint-validator" \
    --allow tcp:900,tcp:8080 \
    --source-ranges=0.0.0.0/0 \
    --network="$GCP_PROJECT_NET_NAME" \
    --target-tags=silvermint,testnet \
    --format=default
else
  echo "TestNet silvermint allow rule already exists."
  gcloud compute firewall-rules list --filter=name:$GCP_PROJECT_NET_NAME-allow-silvermint-validator --format=default
fi

CLI_OUTPUT=""

echo "Creating TestNet instance named:  $GCP_INST_NAME"
CLI_OUTPUT=$(gcloud compute instances list --filter=name:$GCP_INST_NAME --verbosity=none --format=default)
if [[ -z $CLI_OUTPUT ]]; then
  gcloud compute instances create "$GCP_INST_NAME" --image-family=debian-11 --zone="$GCP_INST_ZONE" --network="$GCP_PROJECT_NET_NAME" --image-project=debian-cloud --machine-type=n2-standard-32 --tags=silvermint,testnet --format=default
else
  echo "TestNet instance $GCP_INST_NAME already exists."
  gcloud compute instances list --filter=name:$GCP_INST_NAME --format=default
fi
