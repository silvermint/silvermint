#!/bin/bash

set -e

go test -v ./... -coverprofile=coverage.txt -covermode=atomic 2>&1 | go-junit-report > report.xml
go tool cover -func coverage.txt
go vet -v ./...