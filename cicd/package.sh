#!/bin/bash

set -ex

sed -i "s/VERSION/${VERSION}/" ./cicd/deb-silvermint/debian/changelog

sed -i "s/VERSION/${VERSION}/" ./cicd/deb-silvermint-tools/debian/changelog

./cicd/deb-silvermint/build_deb.sh -v ${VERSION}

./cicd/deb-silvermint-tools/build_deb.sh -v ${VERSION}

cd /builds/${PROJECT_NAMESPACE}/silvermint/pkgd
find . -maxdepth 1 \
  -type f \
  -exec curl \
  --header "JOB-TOKEN: ${JOB_TOKEN}" \
  --upload-file {} "${API_URL}/projects/${PROJECT_ID}/packages/generic/silvermint/${VERSION}/{}" \;
