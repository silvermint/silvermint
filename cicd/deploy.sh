#!/bin/sh

START_PATH=$(pwd)

set -e

while getopts 'v:r:t:p:e:u:h' opt; do
  case "$opt" in
    v)
      VERSION="$OPTARG"
      echo "Setting VERSION to '${OPTARG}'"
      ;;

    r)
      APT_RELEASE="$OPTARG"
      echo "Setting APT_RELEASE to '${OPTARG}'"
      ;;

    t)
      TF_ADDRESS="$OPTARG"
      echo "Setting TF_ADDRESS to '${OPTARG}'"
      ;;

    p)
      VARS_FILE="$OPTARG"
      echo "Setting VARS_FILE to '${OPTARG}'"
      ;;

    e)
      DEPLOY_ENVIRONMENT="$OPTARG"
      echo "Setting DEPLOY_ENVIRONMENT to '${OPTARG}'"
      ;;

    u)
      APT_SERVER_URL="$OPTARG"
      echo "Setting APT_SERVER_URL to '${OPTARG}'"
      ;;

    h)
      HELP_MESG="Usage: $(basename $0) [-v VERSION] [-r APT_RELEASE] [-t TF_ADDRESS] [-p VARS_FILE] [-e DEPLOY_ENVIRONMENT] [-u APT_SERVER_URL]"
      echo ${HELP_MESG}
      exit 0
      ;;

    :)
      echo -e "option requires an argument.\n${HELP_MESG}"
      exit 1
      ;;

    ?)
      echo -e "Invalid command option.\n${HELP_MESG}"
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

export TF_VAR_deploy_user="${whoami}"
export TF_VAR_silvermint_version="${VERSION}" 
export TF_VAR_silvermint_dist="${APT_RELEASE}" 
export TF_VAR_deploy_environment="${DEPLOY_ENVIRONMENT}" 
export TF_VAR_apt_server_url="${APT_SERVER_URL}"

cd $START_PATH/cicd/terraform/gcp

if [ ! -z "$CI_PIPELINE_ID" ]
then 
  sed -ie 's/backend "local"/backend "http"/g' ./main.tf
  gitlab-terraform init -reconfigure
  gitlab-terraform plan  -var-file "../vars/${VARS_FILE}"
  gitlab-terraform apply
else 
  sed -ie 's/backend "http"/backend "local"/g' ./main.tf
  terraform init -reconfigure
  terraform apply -var-file "../vars/${VARS_FILE}"
fi

if [ ! -z "$CI_PIPELINE_ID" ]
then 
  sed -ie 's/backend "http"/backend "local"/g' ./main.tf
else 
  sed -ie 's/backend "local"/backend "http"/g' ./main.tf
fi

cd $START_PATH
