#!/bin/bash

set -e

while getopts 'k:d:n:u:s:p:r:v:h' opt; do
  case "$opt" in
    k)
      exportPRIVATE_KEY="$OPTARG"
      echo "Setting PRIVATE_KEY to '***********'"
      ;;

    d)
      PROJECT_DIR="$OPTARG"
      echo "Setting PROJECT_DIR to '${OPTARG}'"
      ;;

    n)
      PROJECT_NAMESPACE="$OPTARG"
      echo "Setting PROJECT_NAMESPACE to '${OPTARG}'"
      ;;

    u)
      APT_USER="$OPTARG"
      echo "Setting APT_USER to '${OPTARG}'"
      ;;

    s)
      APT_SERVER="$OPTARG"
      echo "Setting APT_SERVER to '${OPTARG}'"
      ;;

    p)
      APT_PATH="$OPTARG"
      echo "Setting APT_PATH to '${OPTARG}'"
      ;;

    r)
      APT_RELEASE="$OPTARG"
      echo "Setting APT_RELEASE to '${OPTARG}'"
      ;;

    v)
      VERSION="$OPTARG"
      echo "Setting VERSION to '${OPTARG}'"
      ;;

    h)
      HELP_MESG="Usage: $(basename $0) [-k PRIVATE_KEY] [-d PROJECT_DIR] [-n NAMESPACE] [-u APT_USER] [-s APT_SERVER] [-p APT_PATH] [-r APT_RELEASE] [-v VERSION]"
      echo ${HELP_MESG}
      exit 0
      ;;

    :)
      echo -e "option requires an argument.\n${HELP_MESG}"
      exit 1
      ;;

    ?)
      echo -e "Invalid command option.\n${HELP_MESG}"
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

apt-get update -y
apt-get install ssh -y
eval $(ssh-agent -s)
echo "${PRIVATE_KEY}" | tr -d '\r' | ssh-add -
cd ${PROJECT_DIR}/pkgd
find ${PROJECT_DIR}/pkgd -maxdepth 1 -type f -name "*.deb" \
  -exec sh -c 'scp -o StrictHostKeyChecking=no {} '${APT_USER}'@'${APT_SERVER}':./"$(basename "{}")"' \;
find /builds/${PROJECT_NAMESPACE}/silvermint/cicd -maxdepth 1 \
  -type f -name "silvermint-*.sh" -exec sh \
  -c 'scp -o StrictHostKeyChecking=no {} '${APT_USER}'@'${APT_SERVER}':./"$(basename "{}")"' \;

cat << EOF > ./ssh_script.sh
  sudo mkdir -p ${APT_PATH}/pool/main/${APT_RELEASE}
  sudo mkdir -p ${APT_PATH}/scripts
  sudo mkdir -p ${APT_PATH}/dists/${APT_RELEASE}/main/binary-amd64
  sudo mv -f /home/gitlab-cicd/*.sh ${APT_PATH}/scripts/
  sudo mv -f /home/gitlab-cicd/*.deb ${APT_PATH}/pool/main/${APT_RELEASE}/
  sudo sh -c 'cd ${APT_PATH} && sudo dpkg-scanpackages -m --arch amd64 pool/ > dists/${APT_RELEASE}/main/binary-amd64/Packages'
  sudo sh -c 'cd ${APT_PATH} && cat dists/${APT_RELEASE}/main/binary-amd64/Packages | gzip -9 > dists/${APT_RELEASE}/main/binary-amd64/Packages.gz'
  sudo sh -c 'cd ${APT_PATH}/dists/${APT_RELEASE} && sudo /tmp/generate-release.sh ${VERSION} ${APT_RELEASE} > InRelease'
EOF

ssh -o StrictHostKeyChecking=no ${APT_USER}@${APT_SERVER} < ./ssh_script.sh
