#!/bin/bash
#
# This script assumes you have docker correctly installed on your development
# machine. It then runs the build script in more or less the same way that
# the gitlab docker runner does.
#
# You will need to run this as root from the `silvermint` directory.
set -ex

CONTAINER_ID=$(docker run -dit debian-silvermint-pipeline)
CI_BUILDS_DIR=/builds
CI_PROJECT_DIR=${CI_BUILDS_DIR}/leaf/silvermint

docker exec ${CONTAINER_ID} mkdir -p ${CI_BUILDS_DIR}/leaf
docker cp ../silvermint ${CONTAINER_ID}:$(dirname $CI_PROJECT_DIR)

RES=$(docker exec -w ${CI_PROJECT_DIR} ${CONTAINER_ID} ./build.sh \
	-M -o ${CI_PROJECT_DIR}/$(basename ${CI_PROJECT_DIR}) \
	-G /usr/local/go)

docker stop ${CONTAINER_ID}
docker rm ${CONTAINER_ID}

