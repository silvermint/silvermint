#!/bin/bash

set -ex

git config --global --add url."git@git.pyro.cloud:".insteadOf "https://git.pyro.cloud/"
command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )
eval $(ssh-agent -s)
echo "$GIT_SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
mkdir -p ~/.ssh
ssh-keyscan -t rsa git.pyro.cloud >> ~/.ssh/known_hosts
chmod 700 ~/.ssh

# Install go-junit-report
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin
go install github.com/jstemmer/go-junit-report@latest

#Install jq, a command-line JSON processor
apt-get -y update
apt-get -y install jq

#Download go-swagger binary
download_url=$(curl -s https://api.github.com/repos/go-swagger/go-swagger/releases/latest | \jq -r '.assets[] | select(.name | contains("'"$(uname | tr '[:upper:]' '[:lower:]')"'_amd64")) | .browser_download_url')

curl -o /usr/local/bin/swagger -L'#' "$download_url"

#Permission to execute
chmod +x /usr/local/bin/swagger
