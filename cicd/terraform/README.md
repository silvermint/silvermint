# demo

This project is for developing the demo

[![pipeline status](https://git.pyro.cloud/eng/demo/badges/main/pipeline.svg)](https://git.pyro.cloud/eng/demo/-/commits/main)  

# Run a Local Demo with Docker-Compose

Follow the steps below to run a demo of Silvermint Node

## Pre-requisites
1. docker (https://docs.docker.com/engine/install/)
2. Docker Desktop (important on MacOS & Windows)
3. git (configured to work with https://git.pyro.cloud)
4. latest release of sendblock executable
5. full access GitLab PAT for your GitLab account

## Steps to setup your demo dev environment (not necessary to run silvermint demo)
1. fork eng/demo to YOUR_NAMESPACE/demo
2. run the following commands to setup yout fork...
```
git remote add upstream https://git.pyro.cloud/eng/demo.git
git fetch --all
git switch dev
git merge upstream/dev --no-ff
```
4. make changes in code
5. commit and push changes to YOUR_NAMESPACE/demo:dev
6. this will trigger a pipeline that will prepare your demo and you will need to click the deploy button on the deploy_personal job in that pipeline
7. when development is complete create merge request from YOUR_NAMESPACE/demo:dev to eng/demo:dev
8. mark merge request ready and request review

## Steps to launch local demo
1. run `git clone https://git.pyro.cloud/eng/infrastructure`
2. cd to .../infrastructure/local
3. run `docker compose up -d`
4. cd back to your source code root
5. run `git clone https://git.pyro.cloud/eng/demo`
6. go to .../demo/local
7. run `sudo docker login -u youremail@pyrofex.net git.pyro.cloud:5050` use your PAT as your  password
8. run `sudo docker compose up --build --force-recreate -d`
9. browse to http://localhost:3000 for Demo Dashboard
10. login as user: silvermint pass: silvermint
11. browse to http://localhost:7555 for Logging Dashboard
12. login as usr: admin pass: admin

## Steps to end the local demo
1. run `docker compose rm -f`
2. run `docker image prune -af` (!!THIS WILL REMOVE ALL IMAGES FROM YOUR LOCAL DOCKER REGISTRY!!)

## Steps to launch devnet, testnet, and mainnet
all non-peronal demos are launched via automation from merge requests in eng/silvermint or eng/demo in the dev or main branches.  
* devnet is deployed to automatically on every merge to the dev branch of eng/silvermint and/or eng/demo
* testnet is deployed manually from merges to either dev or main branches of eng/silvermint and/or eng/demo
* mainent is deployed manually from merges to the main branches of eng/silvermint and/or eng/demo

## Steps to get logs from google cloud demo
1. log in to google cloud console (https://console.cloud.google.com/storage/browser/corp-331300-corp-logs;tab=objects?forceOnBucketsSortingFiltering=false&project=corp-331300&prefix=&forceOnObjectsSortingFiltering=false)
2. Navigate to the folder with the pipeline id of the demo in the name
3. Navigate to a machine
4. download the desired log
