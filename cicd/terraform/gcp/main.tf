terraform {
  backend "local" { }
}
   
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.43.0"
    }
  }
  required_version = "~> 1.0"
}

provider "google" {
  project = var.gcp_project
}

resource "random_integer" "silvermint-id" {
  min = 1
  max = 999999
}

locals {
  startup_script = [
    "export is_terraform=true",
    "export silvermint_run=${random_integer.silvermint-id.result}",
    "export gcp_project=${var.gcp_project}",
    "export deploy_environment=${var.deploy_environment}",
    "export node_count=${var.silvermint_node_count}",
    "export redis_ip=${data.google_redis_instance.redis.host}",
    "export silvermint_version=${var.silvermint_version}",
    "export silvermint_dist=${var.silvermint_dist}",
    "export tps_per_node=${ceil((var.tps_per_node * 1) / length(var.gcp_region_zone_map))}",
    "export tx_sender_count=${var.tx_sender_count}",
    "export apt_server_url=${var.apt_server_url}",
    "echo \"Redis IP:  $redis_ip\"",
    "echo \"Silvermint Version: $silvermint_version\"",
    "echo \"Silvermint Node Count: $node_count\"",
    "echo \"Simulate Sender Count: $tx_sender_count\"",
    "echo \"GitLab Environment: $deploy_environment\"",
    "echo \"Apt Server URL: $apt_server_url\"",
  ]
}

data "google_client_config" "current" {}

data "google_service_account" "cicd_service_account" {
  account_id   = var.gcp_service_account_id
}

data "google_compute_network" "network" {
  name      = var.gcp_network_name
  project   = var.gcp_project
}

data "google_redis_instance" "redis" {
  name = var.redis_machine_name
  region = "us-west3"
}

data "google_compute_instance" "grafana" {
  zone = var.gcp_region_zone_map["us-west3"]
  name = var.grafana_machine_name
}

data "google_compute_instance" "prometheus" {
  zone = var.gcp_region_zone_map["us-west3"]
  name = var.prometheus_machine_name
}

resource "google_project_iam_member" "node_storage_admin" {
  project = var.gcp_project
  role    = "roles/storage.admin"
  member  = "serviceAccount:${data.google_service_account.cicd_service_account.email}"
}

data "google_compute_image" "node-compute-image" {
  family  = var.gcp_image_family
  project = var.gcp_image_project
}

resource "google_project_iam_member" "sender_storage_admin" {
  project = var.gcp_project
  role    = "roles/storage.admin"
  member  = "serviceAccount:${data.google_service_account.cicd_service_account.email}"
}

data "google_compute_image" "sender-compute-image" {
  family  = var.gcp_image_family
  project = var.gcp_image_project
}

output "redis-url" {
  value = "${data.google_redis_instance.redis.host}:${data.google_redis_instance.redis.port}"
}

output "grafana-ip" {
	  value = "${data.google_compute_instance.grafana.network_interface.0.network_ip}"
}

output "grafana-url" {
  value = "http://${data.google_compute_instance.grafana.network_interface.0.network_ip}:3000"
}

output "prometheus-ip" {
  value = "${data.google_compute_instance.prometheus.network_interface.0.network_ip}"
}

output "prometheus-url" {
  value = "http://${data.google_compute_instance.prometheus.network_interface.0.network_ip}:3000"
}
