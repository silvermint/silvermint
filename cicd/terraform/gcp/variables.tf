variable "apt_server_url"{
  type        = string
  description = "URL of the Apt server to use for install"
  default = "https://apt.pyro.cloud"
}

variable "deploy_environment" {
  type        = string
  description = "Name of the GitLab environment being deployed to"
}

variable "deploy_user"{
  type        = string
  description = "Username of the GitLab user running pipeline"
}

variable "gcp_image_family" {
  type        = string
  description = "The name of the Google image family to use"
}

variable "gcp_image_project" {
  type        = string
  description = "The name of the Google image family project to use"
}

variable "gcp_network_name" {
  type        = string
  description = "The name of the Google Cloud network to use for the shard"
}

variable "gcp_project" {
  type        = string
  description = "The name of the Google Cloud Project where the cluster is to be provisioned"
}

variable "gcp_region_zone_map" {
  type = map
  description = "List of zones to use for validator networks"
}

variable "gcp_service_account_id" {
  type        = string
  description = "The account ID of the GCP service account to use"
}

variable "grafana_machine_name"{
  type        = string
  description = "Instance name of the Google CLoud instance running grafana"
}

variable "prometheus_machine_name"{
  type        = string
  description = "Instance name of the Google CLoud instance running prometheus"
}

variable "redis_machine_name"{
  type        = string
  description = "Instance name of the Google CLoud instance running redis"
}

variable "sender_machine_type" {
  type        = string
  description = "The name of the machine type to use for the cluster sender"
}

variable "silvermint_dist" {
  type        = string
  description = "The distribution of the Silvermint package to deploy"
  default     = "stable"
}

variable "silvermint_node_count" {
  type        = number
  description = "The number of cluster nodes"
}

variable "silvermint_version" {
  type        = string
  description = "The version of the Silvermint package to deploy"
  default     = "0.1.6"
}

variable "tps_per_node" {
  type        = number
  description = "The number of transactions per second per sender"
}

variable "tx_sender_count" {
  type        = number
  description = "The number of cluster nodes"
}

variable "validator_machine_type" {
  type        = string
  description = "The name of the machine type to use for the cluster validator"
}