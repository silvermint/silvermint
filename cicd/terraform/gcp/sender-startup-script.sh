#!/bin/bash

# handler for local silvermint environment variables
if [ -z "$is_terraform" ]
then
    echo node_count=$node_count >> /etc/profile
    export node_count=$node_count
    echo silvermint_run="0.0" >> /etc/profile
    export silvermint_run="0.0"
    echo redis_ip=$redis_ip >> /etc/profile
    export redis_ip=$redis_ip
    echo silvermint_version=$silvermint_version >> /etc/profile
    export silvermint_version=$silvermint_version
    echo silvermint_dist=$silvermint_dist >> /etc/profile
    export silvermint_dist=$silvermint_dist
    echo tps_per_node=$tps_per_node >> /etc/profile
    export tps_per_node=$tps_per_node
    echo "Redis IP:  $redis_ip"
    echo "Silvermint Version: $silvermint_version"
    echo "Silvermint Node Count: $node_count"
fi

# go to /tmp... cuz its a better dir
cd /tmp

# install host tools
apt-get update -y
apt-get install -y curl redis-tools prometheus-node-exporter ca-certificates

# set the wallet path & install silvermint
if [ -f "/tmp/sender/simulate" ]; then
    echo "Local wallet executable found at /tmp/sender/simulate ..."
    simulate_path="/tmp/sender/simulate"
else
    echo "Local simulate executable not found at /tmp/simulate pulling executable from package registry..."
    simulate_path="/usr/sbin/simulate"

    # install silvermint
    echo "deb [trusted=yes] $apt_server_url $silvermint_dist main" | tee -a /etc/apt/sources.list.d/artifact-registry.list
    apt-get update -y
    apt-get install -y silvermint-tools=$silvermint_version
fi

# create silvermint log path
if [ ! -d "/var/log/simulate" ]
then
    mkdir -p "/var/log/simulate"
fi

# start prometheus exporter
service prometheus-node-exporter start

chmod +x $simulate_path

i=0

# create root path for wallets if it doesn't exist
while [ ! -d "$HOME/.silvermint" ]
do
    mkdir $HOME/.silvermint
done

# wait for nodes to register
while [ $(redis-cli -h $redis_ip scard "$silvermint_run") -lt $(($node_count)) ]
do
    echo "Waiting for $node_count nodes to register..."
    sleep 1
done

echo "All $node_count nodes registered."

export GODEBUG="madvdontneed=0"

# wait for a bit to create a gap for baseline
sleep 30

# for each node create paths
while [[ /bin/true ]]
do
    for node in $(redis-cli -h $redis_ip smembers "$silvermint_run")
    do
        # cleanup node path for wallets if it exists
        if [ -d "$HOME/.silvermint/$node" ]
        then
            rm -rf $HOME/.silvermint/$node
        fi

        # Create node path for wallets with mode 770.
        mkdir -p $HOME/.silvermint/$node -m 770

        # Assign path for wallets.
        wallets="$HOME/.silvermint/$node/*"

        simulations=$(ps -ef | grep "${node}:8880" | grep -v grep)

        if [ -z "$simulations" ]
        then
            simulate_command="$simulate_path -v -a=\"$node:8880\" simulate -d=\"$HOME/.silvermint/$node\" -t=$tps_per_node -D=20-30 -o=1 >> \"/var/log/simulate/stderrout-$silvermint_version-node-$node-$RANDOM.log\" 2>&1 &"
            echo "Starting simulation of $tps_per_node tps on node $node with command:  $simulate_command"
            eval $simulate_command
        fi
    done

    sleep 30
done
