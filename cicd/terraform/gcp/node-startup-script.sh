#!/bin/bash

# handler for local silvermint environment variables
if [ -z "$is_terraform" ]
then
    export node_count=$node_count
    echo silvermint_run="0.0" >> /etc/profile
    export silvermint_run="0.0"
    echo redis_ip=$redis_ip >> /etc/profile
    export redis_ip=$redis_ip
    echo silvermint_version=$silvermint_version >> /etc/profile
    export silvermint_version=$silvermint_version
    echo silvermint_dist=$silvermint_dist >> /etc/profile
    export silvermint_dist=$silvermint_dist
    echo "Redis IP:  $redis_ip"
    echo "Silvermint Version: $silvermint_version"
    echo "Silvermint Node Count: $node_count"
fi

# install host tools
apt-get update -y
apt-get install -y curl redis-tools prometheus-node-exporter ca-certificates lvm2

# prep multi-tier disk
sed -i 's/allow_mixed_block_sizes = 0/allow_mixed_block_sizes = 1/g' /etc/lvm/lvm.conf
disks=($(lvmdiskscan -v | grep '99.87 GiB'))  # os
os_disk=${disks[0]}
disks=($(lvmdiskscan -v | grep '2.00 TiB')) # cold
cold_disk=${disks[0]}
disks=($(lvmdiskscan -v | grep '375.00 GiB'))  # hot
hot_disk=${disks[0]}
dd if=/dev/zero of=$cold_disk bs=4096 count=1
dd if=/dev/zero of=$hot_disk bs=4096 count=1
pvcreate $cold_disk -f
pvcreate $hot_disk -f
vgcreate silvermint $cold_disk
vgextend silvermint $hot_disk
lvcreate -n cold -L 2045g silvermint $cold_disk
lvcreate -n cache -L 370g silvermint $hot_disk
lvcreate -n meta -L 124m silvermint $hot_disk
lvconvert -y --type cache-pool --poolmetadata silvermint/meta silvermint/cache
lvconvert -y --type cache --cachepool silvermint/cache silvermint/cold
lvchange --cachemode writeback silvermint/cold

# mount silvermint data disk
DISK=/dev/silvermint/cold
if [ -b ${DISK} ] ; then {
    mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard $DISK
    mkdir -p /var/silvermint
    mount -o discard,defaults $DISK /var/silvermint
    chmod a+w /var/silvermint
} ; fi

# save host IP for use in registering node
# public ip
echo my_host_ip="$(curl icanhazip.com)" >> /etc/profile
my_host_ip="$(curl icanhazip.com)" && export my_host_ip
echo "Exported my_host_ip as: $my_host_ip"
# private ip
#echo my_host_ip="$(hostname -I | xargs)" >> /etc/profile
#my_host_ip="$(hostname -I | xargs)" && export my_host_ip
#echo "Exported my_host_ip as: $my_host_ip"


# install silvermint
if [ -f "/tmp/node/silvermint" ]; then
    # start prometheus exporter
    service prometheus-node-exporter start

    echo ""
    echo "Local silvermint executable found at /tmp/node/silvermint..."
    silvermint_path="/tmp/node/silvermint"
    chmod +x $silvermint_path
    mkdir -p /usr/lib/silvermint/templates
    cp /tmp/templates/index.html  /usr/lib/silvermint/templates/index.html
    cp /tmp/templates/config.html /usr/lib/silvermint/templates/config.html
else
    silvermint_path="silvermint"
    echo ""
    echo "Local silvermint executable not found at /tmp/node/silvermint pulling deb from package registry..."

    # install silvermint
    echo "deb [trusted=yes] $apt_server_url $silvermint_dist main" | tee -a /etc/apt/sources.list.d/artifact-registry.list
    apt-get update -y
    apt-get install -y silvermint=$silvermint_version

    # check for systemctl and use init if doesn't exist
    if [  -z "$is_terraform"  ]
    then
        service silvermint stop &>/dev/null
    else
        systemctl stop silvermint.service &>/dev/null
    fi
fi

# start prometheus exporter
service prometheus-node-exporter start

# register validator node in redis
retVal=-1

while [ $retVal -ne "0" ]
do
    redis-cli -h $redis_ip SREM "$silvermint_run" "$my_host_ip" &> /dev/null
    redis-cli -h $redis_ip SADD "$silvermint_run" "$my_host_ip" &> /dev/null

    retVal=$?

    if [ $retVal -eq "0" ]
    then
        echo "Successfully added ip ($my_host_ip) to $silvermint_run"
    else
        echo "Failed to add ip ($my_host_ip) to $silvermint_run"
        echo "Retrying in 1 second..."
        sleep 1
    fi
done

# wait for nodes to register
while [ $(redis-cli -h $redis_ip scard "$silvermint_run") -lt $(($node_count)) ]
do
    echo "Waiting for $node_count nodes currently only $(redis-cli -h $redis_ip scard "$silvermint_run") nodes registered..."
    sleep .25
done

# build the silvermint.yaml
silvermint_yaml="#api_ipaddr: 0.0.0.0"$'\n'
silvermint_yaml="$silvermint_yaml""#api_mode: debug"$'\n'
silvermint_yaml="$silvermint_yaml""#api_port: 8880"$'\n'
silvermint_yaml="$silvermint_yaml""#debug: false"$'\n'
silvermint_yaml="$silvermint_yaml""#help: false"$'\n'
silvermint_yaml="$silvermint_yaml""#management_console_ipaddr: 0.0.0.0"$'\n'
silvermint_yaml="$silvermint_yaml""#management_console_mode: debug"$'\n'
silvermint_yaml="$silvermint_yaml""#port: 8888"$'\n'
silvermint_yaml="$silvermint_yaml""#templates_path: /usr/lib/silvermint/templates"$'\n'

silvermint_validator_id="validator_id: "
silvermint_validators="validators: "

nodes=$(redis-cli -h $redis_ip smembers "$silvermint_run")
IFS=$'\n' sorted=($(sort <<<"${nodes[*]}")); unset IFS

i=0

for node in "${sorted[@]}"
do
    if [[ "$node" == "$my_host_ip" ]]
    then
        silvermint_validator_id="$silvermint_validator_id""$i"
    fi

    silvermint_validators="$silvermint_validators""$i/$node:900"

    if [[ $i -lt $node_count ]]
    then
        silvermint_validators="$silvermint_validators"","
    fi

    i=$(expr $i + 1)
done

silvermint_yaml="$silvermint_yaml""$silvermint_validator_id"$'\n'

if [[ "${silvermint_validators:0-1}" == "," ]]
then
    silvermint_yaml="$silvermint_yaml""${silvermint_validators::-1}"$'\n'
else
    silvermint_yaml="$silvermint_yaml""$silvermint_validators"$'\n'
fi

echo "$silvermint_yaml" > /etc/silvermint/silvermint.yaml

# start silvermint service
if [ -z systemctl ]
then
    systemctl start silvermint.service &>/dev/null
else
    service silvermint start &>/dev/null
fi

sleep 5

while [[ ! ( -z $(pgrep silvermint) ) ]] 
do
    echo "... wallet still running ..."
    sleep 1
done
