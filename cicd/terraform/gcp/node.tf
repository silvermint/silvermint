resource "google_compute_instance_template" "node-instance-template" {
  for_each = var.gcp_region_zone_map

  depends_on     = [ data.google_redis_instance.redis ]
  name_prefix    = "${var.deploy_environment}-node-${each.key}-"
  machine_type   = var.validator_machine_type
  region         = each.key
  can_ip_forward = false

  tags = ["node", "silvermint", "${var.deploy_environment}"]

  disk {
    device_name  = "sm-os"
    type         = "PERSISTENT"
    disk_type    = "pd-standard"
    source_image = data.google_compute_image.node-compute-image.id
    disk_size_gb = 100
    auto_delete  = true
    boot         = true
  }

  disk {
    device_name  = "sm-cold"
    type         = "PERSISTENT"
    disk_type    = "pd-standard"
    disk_size_gb = 2048
    auto_delete  = true
  }

  disk {
    device_name  = "sm-hot"
    type         = "SCRATCH"
    disk_type    = "local-ssd"
    interface    = "NVME"
    disk_size_gb = 375
    auto_delete  = true
  }

  metadata_startup_script = "${join("; ", local.startup_script)};  ${file("${path.module}/node-startup-script.sh")}"

  network_interface {
    subnetwork = "${each.key}"
    access_config {
      
    }
  }

  lifecycle {
    create_before_destroy = true
  }

  service_account {
    email  = data.google_service_account.cicd_service_account.email
    scopes = ["cloud-platform"]
  }

  labels     = {
    "project" = "silvermint",
    "environment" = "${var.deploy_environment}",
    "user" = "${var.deploy_user}",
    "silvermint_version" = "${replace(replace(var.silvermint_version,".","-"),"+","-")}",
  }
}

resource "google_compute_target_pool" "node-target-pool" {
  for_each = var.gcp_region_zone_map

  region = each.key
  name = "${var.deploy_environment}-node-${each.key}-target-pool"
}

resource "google_compute_instance_group_manager" "node-instance-group-manager" {
  for_each = var.gcp_region_zone_map

  name = "${var.deploy_environment}-node-${each.key}-igm"
  zone = each.value

  version {
    instance_template  = google_compute_instance_template.node-instance-template[each.key].id
    name               = "${var.deploy_environment}-node"
  }

  target_pools       = [google_compute_target_pool.node-target-pool[each.key].id]
  base_instance_name = "${var.deploy_environment}-node"
}

resource "google_compute_autoscaler" "node-autoscaler" {
  for_each = var.gcp_region_zone_map

  name   = "${var.deploy_environment}-node-${each.key}-autoscaler"
  zone   = each.value
  target = google_compute_instance_group_manager.node-instance-group-manager[each.key].id

  autoscaling_policy {
    min_replicas    = ceil(var.silvermint_node_count / length(var.gcp_region_zone_map))
    max_replicas    = ceil(var.silvermint_node_count / length(var.gcp_region_zone_map))
    cooldown_period = 60

    cpu_utilization {
      target = 0.5
    }
  }
}
