resource "google_compute_instance_template" "sender-instance-template" {
  for_each = var.gcp_region_zone_map

  name_prefix       = "${var.deploy_environment}-sender-${each.key}-"
  machine_type      = var.sender_machine_type
  region            = each.key
  can_ip_forward    = false

  tags = ["sender", "silvermint", "${var.deploy_environment}"]

  disk {
    source_image = data.google_compute_image.sender-compute-image.id
	  disk_size_gb = 100
  }

  metadata_startup_script = "${join("; ", local.startup_script)}; ${file("${path.module}/sender-startup-script.sh")}"

  network_interface {
    subnetwork = each.key
  }

  lifecycle {
    create_before_destroy = true
  }

  service_account {
    email  = data.google_service_account.cicd_service_account.email
    scopes = ["cloud-platform"]
  }

  labels     = {
    "project" = "silvermint",
    "environment" = "${var.deploy_environment}",
    "user" = "${var.deploy_user}",
    "silvermint_version" = "${replace(replace(var.silvermint_version,".","-"),"+","-")}",
  }
}

resource "google_compute_target_pool" "sender-target-pool" {
  for_each = var.gcp_region_zone_map

  region = each.key
  name = "${var.deploy_environment}-sender-${each.key}-target-pool"
}

resource "google_compute_instance_group_manager" "sender-instance-group-manager" {
  for_each = var.gcp_region_zone_map

  name = "${var.deploy_environment}-sender-${each.key}-igm"
  zone = each.value

  version {
    instance_template  = google_compute_instance_template.sender-instance-template[each.key].id
    name               = "${var.deploy_environment}-sender"
  }

  target_pools       = [google_compute_target_pool.sender-target-pool[each.key].id]
  base_instance_name = "${var.deploy_environment}-sender"
}

resource "google_compute_autoscaler" "sender-autoscaler" {
  for_each = var.gcp_region_zone_map

  name   = "${var.deploy_environment}-sender-${each.key}-autoscaler"
  zone   = each.value
  
  target = google_compute_instance_group_manager.sender-instance-group-manager[each.key].id

  autoscaling_policy {
    min_replicas    = ceil(var.tx_sender_count / length(var.gcp_region_zone_map))
    max_replicas    = ceil(var.tx_sender_count / length(var.gcp_region_zone_map))
    cooldown_period = 60

    cpu_utilization {
      target = 0.8
    }
  }
}
