#!/bin/bash  

#
#  This is the install script for a silvermint validator node
#

set -e

cat <<__EOF__
##################################################################
##        Silvermint Validator Installation Script              ##
##################################################################
Thank you for being a part of the growing Silvermint community!!

Lets set some variables...
__EOF__

#  check for Silvermint Distribution
while [[ "${SILVERMINT_DISTRIBUTION}" != "testing" && "${SILVERMINT_DISTRIBUTION}" != "unstable" && "${SILVERMINT_DISTRIBUTION}" != "stable" ]]
do
  read -p "Silvermint distribution (testing|unstable|stable): " SILVERMINT_DISTRIBUTION
done

echo "You will need to enter the ID of this validator this can be any positive integer greater than 0"
read -p "Silvermint validator ID: " SILVERMINT_VALIDATOR_ID

echo "You will need to add the validators config entry.  This will be an entry for all validators in the network having the following format:"
echo "VALIDATOR_ID/IP_ADDRESS:TCP_PORT ... here is an example config string:  1/192.168.0.1:900,2/192.168.0.2:900,3/192.168.0.3:900"

read -p "Silvermint validator config: " SILVERMINT_VALIDATORS

echo "\n We are ready to install the Silvermint validator node..."

apt-get update -y
apt-get install ca-certificates -y
echo "deb [trusted=yes] https://apt.pyro.cloud $SILVERMINT_DISTRIBUTION main" | tee -a /etc/apt/sources.list.d/artifact-registry.list
apt-get update -y
apt-get install -y silvermint

systemctl stop silvermint

echo "validator_id: $SILVERMINT_VALIDATOR_ID" > /etc/silvermint/silvermint.yaml

echo "validators: $SILVERMINT_VALIDATORS" >> /etc/silvermint/silvermint.yaml

# this should not have to be done here...
setcap 'cap_net_bind_service=+ep' /usr/sbin/silvermint

systemctl start silvermint
