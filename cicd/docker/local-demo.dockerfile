# syntax=docker/dockerfile:1
FROM debian:latest

COPY ./bin/node/silvermint /tmp/silvermint
COPY ./bin/node/wallet /tmp/wallet
COPY ./bin/node/templates/index.html /usr/lib/silvermint/templates/index.html
COPY ./bin/node/templates/config.html /usr/lib/silvermint/templates/config.html

RUN chmod +x /tmp/silvermint
RUN chmod +x /tmp/wallet
RUN mkdir /var/silvermint
RUN mkdir /var/log/silvermint
RUN mkdir /var/silvermint/validator1
RUN mkdir /var/silvermint/validator2
RUN mkdir /var/silvermint/validator3
RUN mkdir /var/silvermint/validator4
RUN mkdir /var/silvermint/wallet1

RUN /tmp/silvermint --api_port=8001 --delete_blocks=true --block_receiver_hostport=127.0.0.1:911 --logtostdout --port=8881 --validator_id=1 --validators="1/127.0.0.1:911,2/127.0.0.1:912,3/127.0.0.1:913,4/127.0.0.1:914" --log_dir="/var/log/silvermint/validator1"
RUN /tmp/silvermint --api_port=8002 --delete_blocks=true --block_receiver_hostport=127.0.0.1:912 --logtostdout --port=8882 --validator_id=2 --validators="1/127.0.0.1:911,2/127.0.0.1:912,3/127.0.0.1:913,4/127.0.0.1:914" --log_dir="/var/log/silvermint/validator2"
RUN /tmp/silvermint --api_port=8003 --delete_blocks=true --block_receiver_hostport=127.0.0.1:913 --logtostdout --port=8883 --validator_id=3 --validators="1/127.0.0.1:911,2/127.0.0.1:912,3/127.0.0.1:913,4/127.0.0.1:914" --log_dir="/var/log/silvermint/validator3"
RUN /tmp/silvermint --api_port=8004 --delete_blocks=true --block_receiver_hostport=127.0.0.1:914 --logtostdout --port=8884 --validator_id=4 --validators="1/127.0.0.1:911,2/127.0.0.1:912,3/127.0.0.1:913,4/127.0.0.1:914" --log_dir="/var/log/silvermint/validator4"
RUN /tmp/wallet -v keygen -n=1000 -o="$HOME/.silvermint/wallet" >> /var/silvermint/sender1/wallet.log 2>&1
RUN for wallet in $HOME/.silvermint/* ; do /tmp/wallet -a="127.0.0.1:8001" -w="$wallet" mint -A=$RANDOM >> /var/log/simulate/sender1/wallet.log 2>&1 ; done 

CMD ["/tmp/wallet", "-v -a=\"127.0.0.1:8001\" simulate -d=\"$HOME/.silvermint\" -t=70 >> /var/silvermint/sender1/wallet.log"]
