// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package silvermint

import "fmt"

var Version string = "no-version"

func GetVersionString(binary string) string {
	return fmt.Sprintf(
		"%s (Silvermint) %s\n"+
		"Copyright (C) 2023 Pyrofex Corporation\n"+
		"License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.\n"+
		"This is free software: you are free to change and redistribute it.\n"+
		"There is NO WARRANTY, to the extent permitted by law.\n"+
		"\n", binary, Version)
}
