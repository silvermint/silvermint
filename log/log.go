package log

import (
	"fmt"
	"runtime"
	"strings"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	FATAL_LEVEL   int = 0
	ERROR_LEVEL   int = 1
	WARNING_LEVEL int = 2
	INFO_LEVEL    int = 3
	DEBUG_LEVEL   int = 4
	TRACE_LEVEL   int = 5

	FATAL_HEADER   string = "FATAL: "
	ERROR_HEADER   string = "ERROR: "
	WARNING_HEADER string = "WARNING: "
	INFO_HEADER    string = "INFO: "
	DEBUG_HEADER   string = "DEBUG: "
	TRACE_HEADER   string = "TRACE: "
)

type Print func(v ...any)
type Printf func(format string, v ...any)

func EmptyPrint(v ...any)                 {}
func EmptyPrintf(format string, v ...any) {}

func init() {
	pflag.Int("log_level", 4, "Level that determines what logs are recorded,"+
		" default is 4. 0: Fatal, 1: Error, 2: Warning, 3: Info, 4: Debug, 5: Trace")
}

func InitLogs() {
	log_level := viper.GetInt("log_level")
	if log_level >= ERROR_LEVEL {
		Error = ErrorFunc
		Errorf = ErrorfFunc
		Errorln = ErrorlnFunc
	}
	if log_level >= WARNING_LEVEL {
		Warning = WarningFunc
		Warningf = WarningfFunc
		Warningln = WarninglnFunc
	}
	if log_level >= INFO_LEVEL {
		Info = InfoFunc
		Infof = InfofFunc
		Infoln = InfolnFunc
	}
	if log_level >= DEBUG_LEVEL {
		Debug = DebugFunc
		Debugf = DebugfFunc
		Debugln = DebuglnFunc
	}
	if log_level >= TRACE_LEVEL {
		Trace = TraceFunc
		Tracef = TracefFunc
		Traceln = TracelnFunc
	}
}

func caller() string {
	_, file, line, ok := runtime.Caller(2)
	if !ok {
		file = "???"
		line = 0
	} else {
		fileArr := strings.Split(file, "/")
		file = fileArr[len(fileArr)-2] + "/" + fileArr[len(fileArr)-1]
	}
	return fmt.Sprintf("%s:%d: ", file, line)
}
