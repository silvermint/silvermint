package log

import (
	"log"
)

var Trace Print = EmptyPrint
var Tracef Printf = EmptyPrintf
var Traceln Print = EmptyPrint

func TraceFunc(v ...any) {
	log.Print(append([]any{caller(), TRACE_HEADER}, v...)...)
}

func TracefFunc(format string, v ...any) {
	log.Printf(caller()+TRACE_HEADER+format, v...)
}

func TracelnFunc(v ...any) {
	log.Println(append([]any{caller(), TRACE_HEADER}, v...)...)
}
