package log

import (
	"log"
)

func Fatal(v ...any) {
	log.Fatal(append([]any{caller(), FATAL_HEADER}, v...)...)
}

func Fatalf(format string, v ...any) {
	log.Fatalf(caller()+FATAL_HEADER+format, v...)
}

func Fatalln(v ...any) {
	log.Fatalln(append([]any{caller(), FATAL_HEADER}, v...)...)
}
