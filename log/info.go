package log

import (
	"log"
)

var Info Print = EmptyPrint
var Infof Printf = EmptyPrintf
var Infoln Print = EmptyPrint

func InfoFunc(v ...any) {
	log.Print(append([]any{caller(), INFO_HEADER}, v...)...)
}

func InfofFunc(format string, v ...any) {
	log.Printf(caller()+INFO_HEADER+format, v...)
}

func InfolnFunc(v ...any) {
	log.Println(append([]any{caller(), INFO_HEADER}, v...)...)
}
