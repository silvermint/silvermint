package log

import (
	"log"
)

var Debug Print = EmptyPrint
var Debugf Printf = EmptyPrintf
var Debugln Print = EmptyPrint

func DebugFunc(v ...any) {
	log.Print(append([]any{caller(), DEBUG_HEADER}, v...)...)
}

func DebugfFunc(format string, v ...any) {
	log.Printf(caller()+DEBUG_HEADER+format, v...)
}

func DebuglnFunc(v ...any) {
	log.Println(append([]any{caller(), DEBUG_HEADER}, v...)...)
}
