package log

import (
	"log"
)

var Warning Print = EmptyPrint
var Warningf Printf = EmptyPrintf
var Warningln Print = EmptyPrint

func WarningFunc(v ...any) {
	log.Print(append([]any{caller(), WARNING_HEADER}, v...)...)
}

func WarningfFunc(format string, v ...any) {
	log.Printf(caller()+WARNING_HEADER+format, v...)
}

func WarninglnFunc(v ...any) {
	log.Println(append([]any{caller(), WARNING_HEADER}, v...)...)
}
