package log

import (
	"log"
)

var Error Print = EmptyPrint
var Errorf Printf = EmptyPrintf
var Errorln Print = EmptyPrint

func ErrorFunc(v ...any) {
	log.Print(append([]any{caller(), ERROR_HEADER}, v...)...)
}

func ErrorfFunc(format string, v ...any) {
	log.Printf(caller()+ERROR_HEADER+format, v...)
}

func ErrorlnFunc(v ...any) {
	log.Println(append([]any{caller(), ERROR_HEADER}, v...)...)
}
