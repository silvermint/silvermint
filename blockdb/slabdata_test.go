// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package blockdb

import (
	"bytes"
	"fmt"
	//"math"
	"math/rand"
	"os"
	"path/filepath"
	"testing"
	"time"
)

var random *rand.Rand = nil

func init() {
	random = rand.New(rand.NewSource(time.Now().UnixMicro()))
}

func TestOpenData(t *testing.T) {
	filename := filepath.Join(os.TempDir(),
		fmt.Sprintf("slab-%x.data", random.Uint64()))
	dat, err := OpenData(filename)
	if err != nil {
		t.Fatalf("OpenData failed: %s", err.Error())
	}
	dat.Close()
	os.Remove(filename)
}

func TestOnce(t *testing.T) {
	filename := filepath.Join(os.TempDir(), fmt.Sprintf("slab-once-%x.dat", random.Uint64()))
	slab, err := OpenDataFile(filename, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0755)
	if err != nil {
		t.Fatalf("OpenData failed: %s", err.Error())
	}

	var length uint32 = 0xFFFF
	data := make([]byte, length)
	_, err = random.Read(data)

	if err != nil {
		t.Fatalf("Failed to generate random byte array: %s", err.Error())
	}

	sz, err := slab.Size()
	if err != nil {
		t.Fatalf("Size() failed: %s", err.Error())
	} else if sz != 0 {
		t.Fatalf("Newly created slab should have length zero, was: %d.", sz)
	}

	num, err := slab.Append(data)
	if err != nil {
		t.Fatalf("slab.Append failed: %s", err.Error())
	}

	slab.Close()

	slab, err = OpenData(filename)
	if err != nil {
		t.Fatalf("OpenData failed: %s", err.Error())
	}

	blen := uint32(num)
	rdata, err := slab.GetAt(0, blen)
	if len(rdata) != num || len(rdata) != len(data) {
		t.Fatalf("GetAt failed: tried to read %d bytes from slab, read %d instead.",
			num, len(rdata))
	}

	if bytes.Compare(data, rdata) != 0 {
		t.Fatalf("GetAt failed: byte arrays not equal. Data corrupted?")
	}

	slab.Close()
	os.Remove(filename)
}
