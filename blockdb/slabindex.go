// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package blockdb

import (
	"encoding/binary"
	"fmt"
	"io/fs"
	"os"
)

const (
	MAX_LOG_LENGTH = 1024
)

type SlabIndex interface {
	Initial() uint64
	Last() uint64
	Records() uint64

	// For Append() and Get(), the offset refers to the position in the
	// slab file where a record is located. The length is the length of
	// the record.
	Append(offset int64, length uint32) error
	Get(height uint64) (int64, uint32, error)

	Sync() error
	Close()
}

type SSlabIndex struct {
	Initial_  uint64
	Last_     uint64
	fd        *os.File
	Filename_ string
}

func OpenIndex(file string) (SlabIndex, error) {
	return OpenIndexFile(file, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0755)
}

func OpenIndexFile(file string, flag int, mode fs.FileMode) (SlabIndex, error) {
	tmp := SSlabIndex{
		Initial_: 0,
		Last_:    0,
		fd:       nil,
	}

	if len(file) == 0 {
		return nil, fmt.Errorf("Invalid filename.")
	}
	tmp.Filename_ = file

	fd, err := os.OpenFile(file, flag, mode)
	if err != nil {
		return nil, err
	}
	tmp.fd = fd

	info, err := fd.Stat()
	if err != nil {
		return nil, err
	}

	if !info.Mode().IsRegular() {
		fd.Close()
		return nil, fmt.Errorf("Index file %s: not a regular file.", file)
	}

	st, err := tmp.fd.Stat()
	if err != nil {
		return nil, err
	}
	tmp.Last_ = uint64(st.Size() / 12)

	return &tmp, nil
}

func (index *SSlabIndex) Initial() uint64           { return index.Initial_ }
func (index *SSlabIndex) SetInitial(initial uint64) { index.Initial_ = initial }
func (index *SSlabIndex) Last() uint64              { return index.Last_ }
func (index *SSlabIndex) SetLast(initial uint64)    { index.Last_ = initial }

func (index *SSlabIndex) Records() uint64 {
	return index.Last_ - index.Initial_
}

func (index *SSlabIndex) Close() {
	if index.fd != nil {
		index.fd.Close()
	}
}

func (index *SSlabIndex) Append(offset int64, length uint32) error {
	data := make([]byte, 12)
	binary.LittleEndian.PutUint64(data[0:8], uint64(offset))
	binary.LittleEndian.PutUint32(data[8:12], length)
	_, err := index.fd.Write(data)
	if err != nil {
		return err
	}

	index.Last_ += 1
	return nil
}

func (index *SSlabIndex) Sync() error {
	if index.fd != nil {
		return index.fd.Sync()
	}
	return fmt.Errorf("no file handle to sync")
}

func (index *SSlabIndex) Get(height uint64) (int64, uint32, error) {
	if height < index.Initial_ || height > index.Last_ {
		return 0, 0, fmt.Errorf("Height 0x%x out of bounds, range is: 0x%x - 0x%x",
			height, index.Initial_, index.Last_)
	}

	rel := height - index.Initial_
	data := make([]byte, 12)
	var indexOffset int64 = int64(rel) * 12
	_, err := index.fd.ReadAt(data, indexOffset)
	if err != nil {
		return 0, 0, err
	}

	offset := int64(binary.LittleEndian.Uint64(data[0:8]))
	length := binary.LittleEndian.Uint32(data[8:12])
	return offset, length, nil
}
