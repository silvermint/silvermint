// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package blockdb

import (
	"fmt"
	"io/fs"
	"os"
)

type SlabData interface {
	// For Append() and Get(), the offset refers to the position in the
	// slab file where a record is located. The length is the length of
	// the record.
	Append(data []byte) (int, error)
	GetAt(offset int64, length uint32) ([]byte, error)

	Size() (int64, error)

	Sync() error
	Close()
}

type SSlabData struct {
	fd        *os.File
	Filename_ string
}

// Data files are always opened read/write in append mode.
func OpenData(file string) (SlabData, error) {
	return OpenDataFile(file, os.O_CREATE, 0755)
}

// Data files are always opened read/write in append mode.
func OpenDataFile(file string, flag int, mode fs.FileMode) (SlabData, error) {
	tmp := SSlabData{
		fd: nil,
	}

	if len(file) == 0 {
		return nil, fmt.Errorf("Invalid filename.")
	}
	tmp.Filename_ = file

	fd, err := os.OpenFile(file, flag|os.O_APPEND|os.O_RDWR, mode)
	if err != nil {
		return nil, err
	}
	tmp.fd = fd

	info, err := fd.Stat()
	if err != nil {
		return nil, err
	}

	if !info.Mode().IsRegular() {
		fd.Close()
		return nil, fmt.Errorf("Index file %s: not a regular file.", file)
	}

	var retval SlabData
	retval = &tmp
	return retval, nil
}

func (df *SSlabData) Size() (int64, error) {
	if df.fd == nil {
		return -1, fmt.Errorf("Can't take size of a nil file descriptor.")
	}

	st, err := df.fd.Stat()
	if err != nil {
		return -1, err
	}

	return st.Size(), nil
}

func (df *SSlabData) Close() {
	if df.fd == nil {
		return
	}

	df.fd.Close()
	df.fd = nil
}

func (df *SSlabData) Append(data []byte) (int, error) {
	return df.fd.Write(data)
}

func (df *SSlabData) Sync() error {
	if df.fd != nil {
		return df.fd.Sync()
	}
	return fmt.Errorf("no file handle to sync")
}

func (df *SSlabData) GetAt(offset int64, length uint32) ([]byte, error) {
	data := make([]byte, length)
	_, err := df.fd.ReadAt(data, offset)
	if err != nil {
		return nil, err
	}
	return data, nil
}
