// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package blockdb

import (
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"testing"
)

func TestSlabMeta(t *testing.T) {
	return
}

func TestOpen(t *testing.T) {
	filename := filepath.Join(os.TempDir(),
		fmt.Sprintf("slab-%x.meta", rand.Uint64()))
	meta, err := OpenMeta(filename)
	if err != nil {
		t.Fatalf("OpenMeta failed: %s", err.Error())
	}
	meta.Close()
	os.Remove(filename)
}

func TestRoundTrip(t *testing.T) {
	filename := filepath.Join(os.TempDir(),
		fmt.Sprintf("slab-%x.meta", rand.Uint64()))

	var meta *SSlabMeta
	var ometa *SSlabMeta

	imeta, err := OpenMeta(filename)
	if err != nil {
		t.Fatalf("OpenMeta failed: %s", err.Error())
	}

	switch imeta.(type) {
	case *SSlabMeta:
		meta = imeta.(*SSlabMeta)
	default:
		t.Fatalf("OpenMeta return unsupported type!")
	}

	meta.SetInitial(1234)
	meta.SetLast(4567)
	num, err := meta.Write()
	if err != nil {
		t.Fatalf("OpenMeta.Write() failed: wrote %d bytes: %s", num, err.Error())
	}

	meta.Close()

	iometa, err := OpenMeta(filename)
	if err != nil {
		t.Fatalf("OpenMeta failed, couldn't read data file we just wrote: %s", err.Error())
	}

	switch iometa.(type) {
	case *SSlabMeta:
		ometa = iometa.(*SSlabMeta)
	default:
		t.Fatalf("OpenMeta return unsupported type!")
	}

	if meta.Initial() != ometa.Initial() {
		t.Fatalf("OpenMeta failed to read data: read initial 0x%x, expected 0x%x.",
			ometa.Initial(), meta.Initial())
	}
	if meta.Last() != ometa.Last() {
		t.Fatalf("OpenMeta failed to read data: read last 0x%x, expected 0x%x.",
			ometa.Last(), meta.Last())
	}

	ometa.Close()
	os.Remove(filename)
}
