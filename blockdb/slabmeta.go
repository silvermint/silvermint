// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package blockdb

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"io"
	"io/fs"
	"os"
)

type SlabMeta interface {
	Initial() uint64
	Last() uint64
	Write() (int, error)

	Sync() error
	Close()
}

type SSlabMeta struct {
	Initial_  uint64
	Last_     uint64
	fd        *os.File
	Filename_ string
}

func OpenMeta(file string) (SlabMeta, error) {
	return OpenMetaFile(file, os.O_CREATE|os.O_RDWR, 0755)
}

func OpenMetaFile(file string, flag int, mode fs.FileMode) (SlabMeta, error) {
	tmp := SSlabMeta{
		Initial_: 0,
		Last_:    0,
		fd:       nil,
	}

	if len(file) == 0 {
		return nil, fmt.Errorf("Invalid filename.")
	}
	tmp.Filename_ = file

	fd, err := os.OpenFile(file, flag, mode)
	if err != nil {
		return nil, err
	}
	tmp.fd = fd

	info, err := fd.Stat()
	if err != nil {
		return nil, err
	}

	if !info.Mode().IsRegular() {
		fd.Close()
		return nil, fmt.Errorf("Metadata file %s: not a regular file.", file)
	}

	var retval SlabMeta

	if info.Size() != 0 {
		file_data, err := io.ReadAll(fd)
		if err != nil {
			return nil, err
		}

		data := bytes.NewBuffer(file_data)
		decoder := gob.NewDecoder(data)

		err = decoder.Decode(&tmp)
		if err != nil {
			return nil, err
		}
	}

	retval = &tmp
	return retval, nil
}

func (meta *SSlabMeta) Initial() uint64           { return meta.Initial_ }
func (meta *SSlabMeta) SetInitial(initial uint64) { meta.Initial_ = initial }
func (meta *SSlabMeta) Last() uint64              { return meta.Last_ }
func (meta *SSlabMeta) SetLast(initial uint64)    { meta.Last_ = initial }

func (meta *SSlabMeta) Sync() error {
	if meta.fd != nil {
		return meta.fd.Sync()
	}
	return fmt.Errorf("no file handle to sync")
}

func (meta *SSlabMeta) Close() {
	// Write output in case we haven't already.
	meta.Write()

	if meta.fd != nil {
		meta.fd.Close()
	}
}

func (meta *SSlabMeta) Write() (int, error) {
	if meta.fd == nil {
		return -1, fmt.Errorf("SSlabMeta: file is not open for writing: %s", meta.Filename_)
	}

	var data bytes.Buffer
	encoder := gob.NewEncoder(&data)

	err := encoder.Encode(meta)
	if err != nil {
		return -1, err
	}

	err = meta.fd.Truncate(0)
	if err != nil {
		return -1, err
	}

	meta.fd.Seek(0, 0)
	if err != nil {
		return -1, err
	}

	return meta.fd.WriteAt(data.Bytes(), 0)
}
