// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package blockdb

import (
	"fmt"
	"os"
	"path"
)

type Slab interface {
	Append([]byte) error
	Get(uint64) ([]byte, error)
	Size() (int64, error)
	Records() uint64

	Dir() string
	Base() string

	Meta() SlabMeta
	Index() SlabIndex
	Data() SlabData

	Sync() error
	Close()
}

type SSlab struct {
	meta     *SSlabMeta
	index    *SSlabIndex
	data     *SSlabData
	dirname  string
	basename string
	offset   int64
}

func NewSlab(dirname string, basename string) (Slab, error) {
	slab := SSlab{
		meta:     nil,
		index:    nil,
		data:     nil,
		dirname:  dirname,
		basename: basename,
		offset:   0,
	}

	metafile := path.Join(dirname, fmt.Sprintf("%s.meta", basename))
	indexfile := path.Join(dirname, fmt.Sprintf("%s.index", basename))
	datafile := path.Join(dirname, fmt.Sprintf("%s.data", basename))

	// Metadata File creation.
	meta, err := OpenMetaFile(metafile, os.O_CREATE|os.O_EXCL|os.O_RDWR, 0755)
	if err != nil {
		return nil, fmt.Errorf("Can't open metadata: %s", err.Error())
	}
	slab.meta = meta.(*SSlabMeta)

	// Index file creation.
	index, err := OpenIndexFile(indexfile, os.O_CREATE|os.O_EXCL|os.O_RDWR, 0755)
	if err != nil {
		slab.meta.Close()
		return nil, fmt.Errorf("Can't open index: %s", err.Error())
	}
	slab.index = index.(*SSlabIndex)

	// Data file creation.
	data, err := OpenDataFile(datafile, os.O_CREATE, 0755)
	if err != nil {
		slab.meta.Close()
		slab.index.Close()
		return nil, fmt.Errorf("Can't open index: %s", err.Error())
	}
	slab.data = data.(*SSlabData)

	return &slab, nil
}

func OpenSlab(dirname string, basename string) (Slab, error) {
	slab := SSlab{
		meta:     nil,
		index:    nil,
		data:     nil,
		dirname:  dirname,
		basename: basename,
		offset:   0,
	}

	metafile := path.Join(dirname, fmt.Sprintf("%s.meta", basename))
	indexfile := path.Join(dirname, fmt.Sprintf("%s.index", basename))
	datafile := path.Join(dirname, fmt.Sprintf("%s.data", basename))

	// Metadata File creation.
	meta, err := OpenMeta(metafile)
	if err != nil {
		return nil, fmt.Errorf("Can't open metadata: %s", err.Error())
	}
	slab.meta = meta.(*SSlabMeta)

	// Index file creation.
	index, err := OpenIndex(indexfile)
	if err != nil {
		slab.index.Close()
		return nil, fmt.Errorf("Can't open index: %s", err.Error())
	}
	slab.index = index.(*SSlabIndex)

	// Data file creation.
	data, err := OpenData(datafile)
	if err != nil {
		slab.meta.Close()
		slab.index.Close()
		return nil, fmt.Errorf("Can't open index: %s", err.Error())
	}
	slab.data = data.(*SSlabData)

	sz, err := slab.data.Size()
	if err != nil {
		slab.meta.Close()
		slab.index.Close()
		slab.data.Close()
		return nil, fmt.Errorf("Can't get data file size: %s", err.Error())
	}
	slab.offset = sz

	// Let's gooooooo!
	return &slab, nil
}

func (slab *SSlab) Dir() string  { return slab.dirname }
func (slab *SSlab) Base() string { return slab.basename }

func (slab *SSlab) Meta() SlabMeta   { return slab.meta }
func (slab *SSlab) Index() SlabIndex { return slab.index }
func (slab *SSlab) Data() SlabData   { return slab.data }

func (slab *SSlab) Size() (int64, error) { return slab.data.Size() }
func (slab *SSlab) Records() uint64      { return slab.index.Records() }

func (slab *SSlab) Sync() error {
	var merr error
	if slab.meta != nil {
		merr = slab.meta.Sync()
	} else {
		merr = fmt.Errorf("no meta file to sync")
	}
	var ierr error
	if slab.index != nil {
		ierr = slab.index.Sync()
	} else {
		ierr = fmt.Errorf("no index file to sync")
	}
	var derr error
	if slab.data != nil {
		derr = slab.data.Sync()
	} else {
		derr = fmt.Errorf("no data file to sync")
	}
	if merr != nil {
		return merr
	} else if ierr != nil {
		return ierr
	} else if derr != nil {
		return derr
	}
	return nil
}

func (slab *SSlab) Close() {
	if slab.meta != nil {
		slab.meta.Close()
	}
	if slab.index != nil {
		slab.index.Close()
	}
	if slab.data != nil {
		slab.data.Close()
	}
}

func (slab *SSlab) Append(data []byte) error {
	offset := slab.offset

	_, err := slab.data.Append(data)
	if err != nil {
		return err
	}

	length := uint32(len(data))
	err = slab.index.Append(offset, length)
	if err != nil {
		return err
	}

	slab.meta.SetLast(slab.index.Last())
	slab.offset = offset + int64(length)
	return nil
}

func (slab *SSlab) Get(height uint64) ([]byte, error) {
	offset, length, err := slab.index.Get(height)
	if err != nil {
		return nil, err
	}
	return slab.data.GetAt(offset, length)
}
