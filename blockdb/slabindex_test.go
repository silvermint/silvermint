// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package blockdb

import (
	"fmt"
	//"math"
	"math/rand"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixMicro())
}

func TestOpenIndex(t *testing.T) {
	filename := filepath.Join(os.TempDir(),
		fmt.Sprintf("slab-%x.index", rand.Uint64()))
	index, err := OpenIndex(filename)
	if err != nil {
		t.Fatalf("OpenIndex failed: %s", err.Error())
	}
	index.Close()
	os.Remove(filename)
}

func TestTwice(t *testing.T) {
	filename := filepath.Join(os.TempDir(), fmt.Sprintf("slab-once-%x.index", rand.Uint64()))
	index, err := OpenIndexFile(filename, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0755)
	if err != nil {
		t.Fatalf("OpenIndex failed: %s", err.Error())
	}

	var offset int64 = int64(0x0AAAABBBBCCCCDDD)
	var length uint32 = 0xFFFFFFFF
	for i := 0; i < 2; i++ {
		index.Append(offset, length)
	}
	index.Close()
	index, err = OpenIndex(filename)
	if err != nil {
		t.Fatalf("OpenIndex failed: %s", err.Error())
	}

	for i := 0; i < 2; i++ {
		offset_, length_, err := index.Get(uint64(i))
		if offset_ != offset || length_ != length {
			t.Fatalf("Get from index failed: %d != %d or %d != %d", offset_, offset, length_, length)
		}
		if err != nil {
			t.Fatalf("Get Error: %s", err.Error())
		}
	}

	index.Close()
	os.Remove(filename)
}

func TestIndexRoundtrip(t *testing.T) {
	filename := filepath.Join(os.TempDir(),
		fmt.Sprintf("slab-rtt-%x.index", rand.Uint64()))
	index, err := OpenIndexFile(filename, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0755)
	if err != nil {
		t.Fatalf("OpenIndex failed: %s", err.Error())
	}

	num_shots := int(rand.Uint32() % 0xFFFF)
	var offset int64 = int64(0)
	var length uint32 = 0
	for i := 0; i < num_shots; i++ {
		length = rand.Uint32() % 0xFFFF
		index.Append(offset, length)
		offset = offset + int64(length)
	}

	final_offset := offset

	index.Close()
	index, err = OpenIndex(filename)
	if err != nil {
		t.Fatalf("OpenIndex failed: %s", err.Error())
	}

	var last_offset int64 = -1
	var last_length uint32 = 0
	for i := 0; i < num_shots; i++ {
		offset, length, err := index.Get(uint64(i))
		//fmt.Printf("Get: @ %d: off=%d len=%d\n", i, offset, length)
		if err != nil {
			t.Fatalf("Get failed: %s", err.Error())
		}
		if last_offset == -1 {
			last_offset = offset
			last_length = length
			continue
		}

		expected_offset := last_offset + int64(last_length)
		if offset > final_offset {
			t.Fatalf("Get failed: idx offset %d => offset %d, length %d", i, offset, length)
		}

		if offset != expected_offset {
			t.Fatalf("Get failed: @ %d: off=%d len=%d: current: off=%d len=%d: "+
				"Expected current offset %d = %d.",
				i, last_offset, last_length, offset, length, offset, expected_offset)
		}

		last_offset = offset
		last_length = length
	}

	index.Close()
	os.Remove(filename)
}
