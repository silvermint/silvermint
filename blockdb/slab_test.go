// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package blockdb

import (
	"fmt"
	"math/rand"
	"os"
	"testing"
)

func TestOne(t *testing.T) {
	basename := fmt.Sprintf("slab-%x", rand.Uint64())
	slab, err := NewSlab(os.TempDir(), basename)
	if err != nil {
		t.Fatalf("NewSlab failed: %s", err.Error())
	}

	data := make([]byte, 256)
	copy(data[0:4], []byte{1, 2, 3, 4})

	err = slab.Append(data)
	if err != nil {
		t.Fatalf("Slab.Append failed: %s", err.Error())
	}

	rdata, err := slab.Get(0)
	if err != nil {
		t.Fatalf("Slab.Get(0) failed: %s", err.Error())
	}

	if len(rdata) != 256 {
		t.Fatalf("Slab.Get(0) failed: data has %d bytes, expected 256.", len(rdata))
	}

	for i, val := range data {
		if val != rdata[i] {
			t.Fatalf("Slab.Get(0) failed: data not equal at index %d: %d != %d", i, val, rdata[i])
		}
	}

	slab.Close()
	//os.RemoveAll(dirname)
	return
}

func TestABunch(t *testing.T) {
	basename := fmt.Sprintf("slab-256-%x", rand.Uint64())
	slab, err := NewSlab(os.TempDir(), basename)
	if err != nil {
		t.Fatalf("NewSlab failed: %s", err.Error())
	}

	for i := 0; i < 255; i++ {
		data := make([]byte, 256)
		copy(data[0:5], []byte{1, 2, 3, 4, byte(i)})

		err = slab.Append(data)
		if err != nil {
			t.Fatalf("Slab.Append failed: %s", err.Error())
		}
	}

	for i := 0; i < 255; i++ {
		data := make([]byte, 256)
		copy(data[0:5], []byte{1, 2, 3, 4, byte(i)})

		rdata, err := slab.Get(uint64(i))
		if err != nil {
			t.Fatalf("Slab.Get(%d) failed: %s", i, err.Error())
		}

		if len(rdata) != 256 {
			t.Fatalf("Slab.Get(%d) failed: data has %d bytes, expected 256.", i, len(rdata))
		}

		for i, val := range data {
			if val != rdata[i] {
				t.Fatalf("Slab.Get(%d) failed: data not equal: %d != %d", i, val, rdata[i])
			}
		}
	}

	slab.Close()
	//os.RemoveAll(dirname)
	return
}
