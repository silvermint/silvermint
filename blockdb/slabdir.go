// Copyright (C) 2022 Pyrofex Corporation
// Author: Michael A. Stay <stay@pyrofex.net>
// Author: Nash E. Foster <leaf@pyrofex.net>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <https://www.gnu.org/licenses/>.

package blockdb

import (
	"fmt"
	"io/fs"
	"os"
	"strings"
	"time"

	"gitlab.com/silvermint/silvermint/utils"
)

type SlabDir interface {
	Slabs() []Slab
	Slab() Slab

	Current() (int, Slab)
	NewSlab() (int, Slab, error)

	Sync() error
	Close()
}

type SSlabDir struct {
	slabs   []Slab
	dir     *os.File
	dirname string
	current int
}

func (slabdir *SSlabDir) Slabs() []Slab {
	slabs := make([]Slab, len(slabdir.slabs))
	copy(slabs, slabdir.slabs)
	return slabs
}

func (slabdir *SSlabDir) Slab() Slab {
	return slabdir.slabs[slabdir.current]
}

func (slabdir *SSlabDir) Current() (int, Slab) { return slabdir.current, slabdir.Slab() }
func (slabdir *SSlabDir) NewSlab() (int, Slab, error) {
	if len(slabdir.slabs) > 0 {
		_, current := slabdir.Current()
		current.Close()
	}

	slab, err := NewSlab(slabdir.dirname, slabBasename())
	if err != nil {
		return -1, nil, err
	}

	switch slab := slab.(type) {
	case *SSlab:
		slabdir.slabs = append(slabdir.slabs, slab)
		slabdir.current = len(slabdir.slabs) - 1
	default:
		return -1, nil, fmt.Errorf("type error: unknown slab type")
	}

	return slabdir.current, slabdir.Slab(), nil
}

func (slabdir *SSlabDir) Sync() error {
	_, curr := slabdir.Current()
	if curr != nil {
		return curr.Sync()
	}
	return fmt.Errorf("no current slab to sync")
}
func (slabdir *SSlabDir) Close() {
	_, curr := slabdir.Current()
	if curr != nil {
		curr.Close()
	}
	slabdir.dir.Close()
}

func GetSlabFilenames(dirname string) ([]string, error) {
	slabs := make([]string, 0)

	dirents, err := os.ReadDir(dirname)
	if err != nil {
		return nil, err
	}

	for _, dent := range dirents {
		name := dent.Name()
		if strings.HasSuffix(name, "meta") {
			parts := strings.Split(name, ".")
			if len(parts) < 2 {
				// ignore things like "foobarmeta", only "foobar.meta" counts.
				// but allow for "foo.bar.meta"
				continue
			}

			slabs = append(slabs, strings.Join(parts[:len(parts)-1], "."))
		}
	}

	return slabs, nil
}

func slabBasename() string {
	txtbytes, err := time.Now().MarshalText()
	// I can't see this ever failing, but who knows.
	if err != nil {
		return fmt.Sprintf("slab-umilli-%d", time.Now().UnixMilli())
	}
	return fmt.Sprintf("slab-%s", string(txtbytes))
}

func NewSlabDir(dirname string) (SlabDir, error) {
	slabd := SSlabDir{
		slabs:   make([]Slab, 0),
		dir:     nil,
		dirname: dirname,
		current: 0,
	}

	err := utils.MaybeCreateDir(dirname, fs.ModeDir|0755)
	if err != nil {
		return nil, fmt.Errorf("can't create dir %s: %s", dirname, err.Error())
	}

	dir, err := os.Open(dirname)
	if err != nil {
		return nil, fmt.Errorf("can't open dir %s: %s", dirname, err.Error())
	}

	st, err := dir.Stat()
	if err != nil {
		return nil, fmt.Errorf("can't stat dir %s: %s", dirname, err.Error())
	}

	if !st.IsDir() {
		return nil, fmt.Errorf("%s is not a directory", dirname)
	}

	slabd.dir = dir

	slabs, err := GetSlabFilenames(dirname)
	if err != nil {
		return nil, fmt.Errorf("unable to get slab filenames in %s: %s",
			dirname, err.Error())
	}
	slabd.slabs = make([]Slab, len(slabs))
	for i, slab := range slabs {
		slabd.slabs[i], err = OpenSlab(dirname, slab)
		if err != nil {
			return nil, fmt.Errorf("couldn't open slab %s in %s: %s",
				slab, dirname, err.Error())
		}
	}

	if len(slabd.Slabs()) == 0 {
		_, _, err := slabd.NewSlab()
		if err != nil {
			return nil, err
		}
	}

	return &slabd, nil
}
